<?php

// LOGIN //
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('/', 'Auth\LoginController@showLoginForm');

Route::auth();

// LOGOUT //

Route::get('logout', function(){
		Auth::logout();
        return redirect('/');
});

// MIDDLEWARE - INICIO //

Route::group(['middleware' => ['auth']], function() {

  // INICIO //

	Route::get('/home', 'HomeController@index')->name('home');

  // MODULO CONFIGURACIONES //
  Route::get('config',['as'=>'config.index','uses'=>'ConfigController@index'
    //,'middleware' => ['permission:config-index']
  ]);
  Route::get('config/edit/{id}',['as'=>'config.edit','uses'=>'ConfigController@edit'
    //,'middleware' => ['permission:config-edit']
  ]);
  Route::patch('config/{id}',['as'=>'config.update','uses'=>'ConfigController@update'
    //,'middleware' => ['permission:config-edit']
  ]);

  // MODULO SEGURIDAD - INICIO //

  /* SUB-MODULO - USUARIOS - INICIO*/

  Route::post('municipioAjax', 'Seguridad\UserController@municipioAjax');
  Route::post('parroquiaAjax', 'Seguridad\UserController@parroquiaAjax');

  Route::get('users/excel',['as'=>'users.excel','uses'=>'Seguridad\UserController@excel','middleware' => ['permission:user-excel']]);
  Route::get('users/pdf',['as'=>'users.pdf','uses'=>'Seguridad\UserController@pdf','middleware' => ['permission:user-pdf']]);
  Route::get('users/imprimirPdf',['as'=>'users.imprimirPdf','uses'=>'Seguridad\UserController@imprimirPdf','middleware' => ['permission:user-imprimirPdf']]);
  Route::get('users',['as'=>'users.index','uses'=>'Seguridad\UserController@index','middleware' => ['permission:user-list|user-create|user-edit|user-delete']]);
	Route::get('users/create',['as'=>'users.create','uses'=>'Seguridad\UserController@create','middleware' => ['permission:user-create']]);
	Route::post('users/create',['as'=>'users.store','uses'=>'Seguridad\UserController@store','middleware' => ['permission:user-create']]);
	Route::get('users/{id}',['as'=>'users.show','uses'=>'Seguridad\UserController@show']);
	Route::get('users/{id}/edit',['as'=>'users.edit','uses'=>'Seguridad\UserController@edit','middleware' => ['permission:user-edit']]);
	Route::patch('users/{id}',['as'=>'users.update','uses'=>'Seguridad\UserController@update','middleware' => ['permission:user-edit']]);
	Route::delete('users/{id}',['as'=>'users.destroy','uses'=>'Seguridad\UserController@destroy','middleware' => ['permission:user-delete']]);

  /* SUB-MODULO - USUARIOS - FIN*/

  /* SUB-MODULO - ASIGNAR PERMISOS A ROLES DEL SISTEMA - INICIO*/

  Route::get('rolepermissions/asign',['as'=>'rolepermissions.asign','uses'=>'Seguridad\RolepermissionController@asign','middleware' => ['permission:rolepermission-asign']]);
  Route::get('rolepermissions/excel',['as'=>'rolepermissions.excel','uses'=>'Seguridad\RolepermissionController@excel','middleware' => ['permission:rolepermission-excel']]);
  Route::get('rolepermissions/pdf',['as'=>'rolepermissions.pdf','uses'=>'Seguridad\RolepermissionController@pdf','middleware' => ['permission:rolepermission-pdf']]);
  Route::get('rolepermissions/imprimirPdf',['as'=>'rolepermissions.imprimirPdf','uses'=>'Seguridad\RolepermissionController@imprimirPdf','middleware' => ['permission:rolepermission-imprimirPdf']]);
  Route::get('rolepermissions',['as'=>'rolepermissions.index','uses'=>'Seguridad\RolepermissionController@index','middleware' => ['permission:rolepermission-list|rolepermission-create|rolepermission-edit|rolepermission-delete']]);
  Route::get('rolepermissions/create',['as'=>'rolepermissions.create','uses'=>'Seguridad\RolepermissionController@create','middleware' => ['permission:rolepermission-create']]);
  Route::post('rolepermissions/create',['as'=>'rolepermissions.store','uses'=>'Seguridad\RolepermissionController@store','middleware' => ['permission:rolepermission-create']]);
  Route::get('rolepermissions/{id}',['as'=>'rolepermissions.show','uses'=>'Seguridad\RolepermissionController@show']);
  Route::get('rolepermissions/{id}/edit',['as'=>'rolepermissions.edit','uses'=>'Seguridad\RolepermissionController@edit','middleware' => ['permission:rolepermission-edit']]);
  Route::patch('rolepermissions/{id}',['as'=>'rolepermissions.update','uses'=>'Seguridad\RolepermissionController@update','middleware' => ['permission:rolepermission-edit']]);
  Route::delete('rolepermissions/{id}',['as'=>'rolepermissions.destroy','uses'=>'Seguridad\RolepermissionController@destroy','middleware' => ['permission:rolepermission-delete']]);

  /* SUB-MODULO - ASIGNAR PERMISOS A ROLES DEL SISTEMA - FIN*/

  /* SUB-MODULO - ROLES - INICIO*/

  Route::get('roles/excel',['as'=>'roles.excel','uses'=>'Seguridad\RoleController@excel','middleware' => ['permission:role-excel']]);
  Route::get('roles/pdf',['as'=>'roles.pdf','uses'=>'Seguridad\RoleController@pdf','middleware' => ['permission:role-pdf']]);
  Route::get('roles/imprimirPdf',['as'=>'roles.imprimirPdf','uses'=>'Seguridad\RoleController@imprimirPdf','middleware' => ['permission:role-imprimirPdf']]);
  Route::get('roles',['as'=>'roles.index','uses'=>'Seguridad\RoleController@index','middleware' => ['permission:role-list|role-create|role-edit|role-delete']]);
	Route::get('roles/create',['as'=>'roles.create','uses'=>'Seguridad\RoleController@create','middleware' => ['permission:role-create']]);
	Route::post('roles/create',['as'=>'roles.store','uses'=>'Seguridad\RoleController@store','middleware' => ['permission:role-create']]);
	Route::get('roles/{id}',['as'=>'roles.show','uses'=>'Seguridad\RoleController@show']);
	Route::get('roles/{id}/edit',['as'=>'roles.edit','uses'=>'Seguridad\RoleController@edit','middleware' => ['permission:role-edit']]);
	Route::patch('roles/{id}',['as'=>'roles.update','uses'=>'Seguridad\RoleController@update','middleware' => ['permission:role-edit']]);
	Route::delete('roles/{id}',['as'=>'roles.destroy','uses'=>'Seguridad\RoleController@destroy','middleware' => ['permission:role-delete']]);

  /* SUB-MODULO - ROLES - FIN*/

  /* SUB-MODULO - MODULOS DEL SISTEMA - INICIO*/

  Route::get('modules/excel',['as'=>'modules.excel','uses'=>'Seguridad\ModuleController@excel','middleware' => ['permission:module-excel']]);
  Route::get('modules/pdf',['as'=>'modules.pdf','uses'=>'Seguridad\ModuleController@pdf','middleware' => ['permission:module-pdf']]);
  Route::get('modules/imprimirPdf',['as'=>'modules.imprimirPdf','uses'=>'Seguridad\ModuleController@imprimirPdf','middleware' => ['permission:module-imprimirPdf']]);
  Route::get('modules',['as'=>'modules.index','uses'=>'Seguridad\ModuleController@index','middleware' => ['permission:module-list|module-create|module-edit|module-delete']]);
	Route::get('modules/create',['as'=>'modules.create','uses'=>'Seguridad\ModuleController@create','middleware' => ['permission:module-create']]);
	Route::post('modules/create',['as'=>'modules.store','uses'=>'Seguridad\ModuleController@store','middleware' => ['permission:module-create']]);
	Route::get('modules/{id}',['as'=>'modules.show','uses'=>'Seguridad\ModuleController@show']);
	Route::get('modules/{id}/edit',['as'=>'modules.edit','uses'=>'Seguridad\ModuleController@edit','middleware' => ['permission:module-edit']]);
	Route::patch('modules/{id}',['as'=>'modules.update','uses'=>'Seguridad\ModuleController@update','middleware' => ['permission:module-edit']]);
	Route::delete('modules/{id}',['as'=>'modules.destroy','uses'=>'Seguridad\ModuleController@destroy','middleware' => ['permission:module-delete']]);

  /* SUB-MODULO - MODULOS DEL SISTEMA - FIN*/

  /* SUB-MODULO - MENUS DEL SISTEMA - INICIO*/

  Route::get('menus/excel',['as'=>'menus.excel','uses'=>'Seguridad\MenuController@excel','middleware' => ['permission:menu-excel']]);
  Route::get('menus/pdf',['as'=>'menus.pdf','uses'=>'Seguridad\MenuController@pdf','middleware' => ['permission:menu-pdf']]);
  Route::get('menus/imprimirPdf',['as'=>'menus.imprimirPdf','uses'=>'Seguridad\MenuController@imprimirPdf','middleware' => ['permission:menu-imprimirPdf']]);
  Route::get('menus',['as'=>'menus.index','uses'=>'Seguridad\MenuController@index','middleware' => ['permission:menu-list|menu-create|menu-edit|menu-delete']]);
	Route::get('menus/create',['as'=>'menus.create','uses'=>'Seguridad\MenuController@create','middleware' => ['permission:menu-create']]);
	Route::post('menus/create',['as'=>'menus.store','uses'=>'Seguridad\MenuController@store','middleware' => ['permission:menu-create']]);
	Route::get('menus/{id}',['as'=>'menus.show','uses'=>'Seguridad\MenuController@show']);
	Route::get('menus/{id}/edit',['as'=>'menus.edit','uses'=>'Seguridad\MenuController@edit','middleware' => ['permission:menu-edit']]);
	Route::patch('menus/{id}',['as'=>'menus.update','uses'=>'Seguridad\MenuController@update','middleware' => ['permission:menu-edit']]);
	Route::delete('menus/{id}',['as'=>'menus.destroy','uses'=>'Seguridad\MenuController@destroy','middleware' => ['permission:menu-delete']]);

  /* SUB-MODULO - MENUS DEL SISTEMA - FIN*/

  /* SUB-MODULO - PERMISOS DEL SISTEMA - INICIO*/

  Route::get('permissions/excel',['as'=>'permissions.excel','uses'=>'Seguridad\PermissionController@excel','middleware' => ['permission:permission-excel']]);
  Route::get('permissions/pdf',['as'=>'permissions.pdf','uses'=>'Seguridad\PermissionController@pdf','middleware' => ['permission:permission-pdf']]);
  Route::get('permissions/imprimirPdf',['as'=>'permissions.imprimirPdf','uses'=>'Seguridad\PermissionController@imprimirPdf','middleware' => ['permission:permission-imprimirPdf']]);
  Route::get('permissions',['as'=>'permissions.index','uses'=>'Seguridad\PermissionController@index','middleware' => ['permission:permission-list|permission-create|permission-edit|permission-delete']]);
	Route::get('permissions/create',['as'=>'permissions.create','uses'=>'Seguridad\PermissionController@create','middleware' => ['permission:permission-create']]);
	Route::post('permissions/create',['as'=>'permissions.store','uses'=>'Seguridad\PermissionController@store','middleware' => ['permission:permission-create']]);
	Route::get('permissions/{id}',['as'=>'permissions.show','uses'=>'Seguridad\PermissionController@show']);
	Route::get('permissions/{id}/edit',['as'=>'permissions.edit','uses'=>'Seguridad\PermissionController@edit','middleware' => ['permission:permission-edit']]);
	Route::patch('permissions/{id}',['as'=>'permissions.update','uses'=>'Seguridad\PermissionController@update','middleware' => ['permission:permission-edit']]);
	Route::delete('permissions/{id}',['as'=>'permissions.destroy','uses'=>'Seguridad\PermissionController@destroy','middleware' => ['permission:permission-delete']]);

  /* SUB-MODULO - PERMISOS DEL SISTEMA - FIN*/

  /* SUB-MODULO - SESIONES DEL SISTEMA - INICIO*/


  Route::get('usersession',['as'=>'usersession.index','uses'=>'Seguridad\SesionesController@index'
    //,'middleware' => ['permission:usersession-list|usersession-create|usersession-edit|usersession-delete']
  ]);

  Route::get('usersessionExcel/{request}',['as'=>'usersession.excel','uses'=>'Seguridad\SesionesController@excel','middleware' => ['permission:usersession-list|usersession-create|usersession-edit|usersession-delete']]);

  Route::get('usersession/destroy/{id}',['as'=>'usersession.destroy','uses'=>'Seguridad\SesionesController@destroy'/*,'middleware' => ['permission:usersession-delete']*/]);
Route::get('usersessionUsuario/{id}',['as'=>'usersession.sesionXUser','uses'=>'Seguridad\SesionesController@sesionXUser'/*,'middleware' => ['permission:usersession-list|usersession-create|usersession-edit|usersession-delete']*/]);
Route::get('usersession/apiSessions/{username}/{fechaInicio}/{fechaFin}',['as'=>'usersession.apiSessions','uses'=>'Seguridad\SesionesController@apiSessions'/*,'middleware' => ['permission:usersession-delete']*/]);
  /* SUB-MODULO - SESIONES DEL SISTEMA - FIN*/

  /* SUB-MODULO - ESTATUS GENERICO - INICIO*/

  Route::get('genericstatus/excel',['as'=>'genericstatus.excel','uses'=>'Seguridad\GenericStatusController@excel','middleware' => ['permission:genericstatus-excel']]);
  Route::get('genericstatus/pdf',['as'=>'genericstatus.pdf','uses'=>'Seguridad\GenericStatusController@pdf','middleware' => ['permission:genericstatus-pdf']]);
  Route::get('genericstatus/imprimirPdf',['as'=>'genericstatus.imprimirPdf','uses'=>'Seguridad\GenericStatusController@imprimirPdf','middleware' => ['permission:genericstatus-imprimirPdf']]);
  Route::get('genericstatus',['as'=>'genericstatus.index','uses'=>'Seguridad\GenericStatusController@index','middleware' => ['permission:genericstatus-list|genericstatus-create|genericstatus-edit|genericstatus-delete']]);
	Route::get('genericstatus/create',['as'=>'genericstatus.create','uses'=>'Seguridad\GenericStatusController@create','middleware' => ['permission:genericstatus-create']]);
	Route::post('genericstatus/create',['as'=>'genericstatus.store','uses'=>'Seguridad\GenericStatusController@store','middleware' => ['permission:genericstatus-create']]);
	Route::get('genericstatus/{id}',['as'=>'genericstatus.show','uses'=>'Seguridad\GenericStatusController@show']);
	Route::get('genericstatus/{id}/edit',['as'=>'genericstatus.edit','uses'=>'Seguridad\GenericStatusController@edit','middleware' => ['permission:genericstatus-edit']]);
	Route::patch('genericstatus/{id}',['as'=>'genericstatus.update','uses'=>'Seguridad\GenericStatusController@update','middleware' => ['permission:genericstatus-edit']]);
	Route::delete('genericstatus/{id}',['as'=>'genericstatus.destroy','uses'=>'Seguridad\GenericStatusController@destroy','middleware' => ['permission:genericstatus-delete']]);

  /* SUB-MODULO - ESTATUS GENERICO - FIN*/

  /* SUB-MODULO - ESTATUS USUARIOS - INICIO*/

  Route::get('userstatus/excel',['as'=>'userstatus.excel','uses'=>'Seguridad\UserStatusController@excel','middleware' => ['permission:userstatus-excel']]);
  Route::get('userstatus/pdf',['as'=>'userstatus.pdf','uses'=>'Seguridad\UserStatusController@pdf','middleware' => ['permission:userstatus-pdf']]);
  Route::get('userstatus/imprimirPdf',['as'=>'userstatus.imprimirPdf','uses'=>'Seguridad\GenericStatusController@imprimirPdf','middleware' => ['permission:userstatus-imprimirPdf']]);
  Route::get('userstatus',['as'=>'userstatus.index','uses'=>'Seguridad\UserStatusController@index','middleware' => ['permission:userstatus-list|userstatus-create|userstatus-edit|userstatus-delete']]);
	Route::get('userstatus/create',['as'=>'userstatus.create','uses'=>'Seguridad\UserStatusController@create','middleware' => ['permission:userstatus-create']]);
	Route::post('userstatus/create',['as'=>'userstatus.store','uses'=>'Seguridad\UserStatusController@store','middleware' => ['permission:userstatus-create']]);
	Route::get('userstatus/{id}',['as'=>'userstatus.show','uses'=>'Seguridad\UserStatusController@show']);
	Route::get('userstatus/{id}/edit',['as'=>'userstatus.edit','uses'=>'Seguridad\UserStatusController@edit','middleware' => ['permission:userstatus-edit']]);
	Route::patch('userstatus/{id}',['as'=>'userstatus.update','uses'=>'Seguridad\UserStatusController@update','middleware' => ['permission:userstatus-edit']]);
	Route::delete('userstatus/{id}',['as'=>'userstatus.destroy','uses'=>'Seguridad\UserStatusController@destroy','middleware' => ['permission:userstatus-delete']]);

  /* SUB-MODULO - ESTATUS USUARIOS - FIN*/

  // MODULO SEGURIDAD - FIN //

  // MODULO AUDITORIA - INICIO //

  /* SUB-MODULO - CONSULTA TRAZAS - INICIO*/

  Route::get('traces',['as'=>'traces.index','uses'=>'Auditoria\TracesController@index']);
  Route::get('traces/excel',['as'=>'traces.excel','uses'=>'Auditoria\TracesController@excel']);
  // 'middleware' => ['permission:traces-list|traces-create|traces-edit|traces-delete']

  /* SUB-MODULO - CONSULTA TRAZAS - FIN*/

  /* SUB-MODULO - REGISTRO DE LOGUEO - INICIO*/

  Route::get('logslogin',['as'=>'logslogin.index','uses'=>'Auditoria\LogsloginController@index']);
  Route::get('logslogin/excel',['as'=>'logslogin.excel','uses'=>'Auditoria\LogsloginController@excel']);
  // 'middleware' => ['permission:traces-list|traces-create|traces-edit|traces-delete']

  /* SUB-MODULO - REGISTRO DE LOGUEO - FIN*/

  // MODULO AUDITORIA - FIN //

  // MODULO PARAMETROS - INICIO //

  /* SUB-MODULO - CATEGORIAS*/

  Route::get('/parametros/categorias', function () {
      return view('parametros.categorias.index');
  })->name('parametros.categorias.index');

  Route::get('/parametros/categorias/create', function () {
      return view('parametros.categorias.create');
  })->name('parametros.categorias.create');

  Route::get('/parametros/categorias/edit', function () {
      return view('parametros.categorias.edit');
  })->name('parametros.categorias.edit');

  /* SUB-MODULO - TIPO ARTICULO*/

  Route::get('/parametros/tipo-articulo', function () {
      return view('parametros.tipo-articulo.index');
  })->name('parametros.tipo-articulo.index');

  Route::get('/parametros/tipo-articulo/create', function () {
      return view('parametros.tipo-articulo.create');
  })->name('parametros.tipo-articulo.create');

  Route::get('/parametros/tipo-articulo/edit', function () {
      return view('parametros.tipo-articulo.edit');
  })->name('parametros.tipo-articulo.edit');

  /* SUB-MODULO - TIPO CLIENTE*/

  Route::get('/parametros/tipo-cliente', function () {
      return view('parametros.tipo-cliente.index');
  })->name('parametros.tipo-cliente.index');

  Route::get('/parametros/tipo-cliente/create', function () {
      return view('parametros.tipo-cliente.create');
  })->name('parametros.tipo-cliente.create');

  Route::get('/parametros/tipo-cliente/edit', function () {
      return view('parametros.tipo-cliente.edit');
  })->name('parametros.tipo-cliente.edit');

  /* SUB-MODULO - TIPO COLABORADOR*/

  Route::get('/parametros/tipo-colaborador', function () {
      return view('parametros.tipo-colaborador.index');
  })->name('parametros.tipo-colaborador.index');

  Route::get('/parametros/tipo-colaborador/create', function () {
      return view('parametros.tipo-colaborador.create');
  })->name('parametros.tipo-colaborador.create');

  Route::get('/parametros/tipo-colaborador/edit', function () {
      return view('parametros.tipo-colaborador.edit');
  })->name('parametros.tipo-colaborador.edit');

  /* SUB-MODULO - ASIGNACION*/

  Route::get('/parametros/tipo-asignacion', function () {
      return view('parametros.tipo-asignacion.index');
  })->name('parametros.tipo-asignacion.index');

  Route::get('/parametros/tipo-asignacion/create', function () {
      return view('parametros.tipo-asignacion.create');
  })->name('parametros.tipo-asignacion.create');

  Route::get('/parametros/tipo-asignacion/edit', function () {
      return view('parametros.tipo-asignacion.edit');
  })->name('parametros.tipo-asignacion.edit');

  /* SUB-MODULO - INSTRUCCIONES*/

  Route::get('/parametros/instrucciones', function () {
      return view('parametros.instrucciones.index');
  })->name('parametros.instrucciones.index');

  Route::get('/parametros/instrucciones/create', function () {
      return view('parametros.instrucciones.create');
  })->name('parametros.instrucciones.create');

  /* SUB-MODULO - URL*/

  Route::get('/parametros/url', function () {
      return view('parametros.url.index');
  })->name('parametros.url.index');

  Route::get('/parametros/url/create', function () {
      return view('parametros.url.create');
  })->name('parametros.url.create');

  Route::get('/parametros/url/edit', function () {
      return view('parametros.url.edit');
  })->name('parametros.url.edit');

  // MODULO PARAMETROS - FIN //

  // VISTAS EJEMPLO - INICIO //

    Route::get('/ejemplo/index-criteros-tabla', function () {
        return view('examples.index-criterios-tabla');
    })->name('examples.index-criteros-tabla');

    Route::get('/ejemplo/index-tabla', function () {
        return view('examples.index-tabla');
    })->name('examples.index-tabla');

    Route::get('/ejemplo/create', function () {
        return view('examples.create-edit');
    })->name('examples.create');

  // VISTAS EJEMPLO - FIN //

});

// MIDDLEWARE - FIN //
