<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;


class Municipality extends Model implements Auditable
{
  use \OwenIt\Auditing\Auditable;

  protected $table = "municipalities";

  public static function comboMunicipalities()
  {
    return Municipality::select('id', 'name')
                    ->from('municipalities')
                    ->where('id','>',0)
                    ->orderBy('name', 'ASC')
                    ->get();
  }

  public static function listaMunicipios($estado)
  {
    return Municipality::select('id', 'name')
                    ->where('state_id', $estado)
                    ->orderBy('name', 'asc')
                    ->get();
  }


  protected $fillable = [
      'name', 'state_id',
  ];
}
