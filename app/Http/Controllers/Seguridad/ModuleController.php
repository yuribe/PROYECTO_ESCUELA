<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Modules\StoreModuleRequest;
use App\Http\Requests\Modules\UpdateModuleRequest;
use App\Exports\ModulesExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use App\Module;
use App\Permission;
use App\Rolepermission;
use App\GenericStatus;
use App\Menu;

use DB;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request);
        $modules = Module::modulelist($request->get('name'),$request->get('display_name'),$request->get('status_module_id'))->get();
        $generics = GenericStatus::comboGenericsModule();
          return view('modules.index',compact('modules'))
          ->with([
                  'i'=> ($request->input('page', 1) - 1) * 10,
                  'name'=>$request->get('name'),
                  'display_name'=>$request->get('display_name'),
                  'status_module_id'=>$request->get('status_module_id'),
                  'generics'=>$generics,
               ]);
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $generics = GenericStatus::comboGenerics();
      return view('modules.create')->with([
                  'generics'=>$generics,
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreModuleRequest $request)
    {
      try
      {
        $module = new Module();
        $module->name = $request->input('name');
        $module->display_name = $request->input('display_name');
        $module->description = strtoupper($request->input('description'));
        $module->status_module_id = $request->input('status_module_id');
        $module->save();

        $LastInsertIdModule = $module->id;

        //Registros ocultos para poder cargar modulos en la asignación de permisos
        $permiso = new Permission();

        $permiso->name = $LastInsertIdModule.'_OCULTO';
        $permiso->display_name = $LastInsertIdModule.'_OCULTO';
        $permiso->description = 'OCULTO';
        $permiso->module_id = $LastInsertIdModule;
        $permiso->generic_status_id = 1;
        $permiso->created_at = now();
        $permiso->updated_at = now();
        $permiso->save();
        $LastInsertIdPermiso = $permiso->id;

        $permission_role = new Rolepermission();
        $permission_role->role_id = 0;
        $permission_role->permission_id = $LastInsertIdPermiso;
        $permission_role->created_at = now();
        $permission_role->updated_at = now();
        $permission_role->save();

        return redirect()->route('modules.index')->with('message_success', __('message.infoguardada'));

      }
        catch(\Exception $e)
      {
        DB::rollBack();
        return redirect()->route('modules.index')->with('message_error', __('message.infonoguardada'.$e));
      }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module = Module::find($id);

        return view('modules.show',compact('module'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module = Module::find($id);
        $generics = GenericStatus::comboGenerics();

        return view('modules.edit',compact('module'))->with([
                    'generics'=>$generics,
                  ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateModuleRequest $request, $id)
    {
        $module = Module::find($id);
        $module->name = $request->input('name');
        $module->display_name = $request->input('display_name');
        $module->description = strtoupper($request->input('description'));
        $module->status_module_id = $request->input('status_module_id');

        if($module->save())
        {
           return redirect()->route('modules.index')->with('message_success', __('message.infoactualizada'));
        }
        else
        {
           return redirect()->route('modules.index')->with('message_error', __('message.infonoactualizada'));
        }
    }
    /**
     * Remove the specified rModule::all();esource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try
      {
        $modules = Module::find($id);
        $permission = Permission::where('module_id', $modules->id)->get();
        foreach($permission as $permiso)
        {
          Rolepermission::where('id', $permiso->id)->delete();
        }
        Menu::where('module_menu_id', $modules->id)->delete();
        Permission::where('module_id', $modules->id)->delete();
        Module::where('id', $id)->delete();

        return redirect()->route('modules.index')->with('message_success', __('message.infoeliminada'));
      }
        catch(\Exception $e)
      {
        DB::rollBack();
        return redirect()->route('modules.index')->with('message_error', __('message.infonoeliminada'));
      }
    }

    public function excel(Request $request)
    {
      //dd($request);
      $name=$request->get('name');
      $display_name=$request->get('display_name');
      $status_module_id=$request->get('status_module_id');

      return Excel::download(new ModulesExport($name,$display_name,$status_module_id), 'modules.xlsx');

    }

    public function pdf(Request $request)
    {
      //dd($request);
      $modules = Module::modulelist($request->get('name'),$request->get('display_name'),$request->get('status_module_id'))->get();
      return view('modules.imprimirPdf',compact('modules'));
    }

}
