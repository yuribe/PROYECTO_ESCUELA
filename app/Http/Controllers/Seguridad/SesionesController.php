<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Support\Facades\Redis;
use App\Exports\SessionsExport;
use App\Notifications\NotificationsSession;
use App\Sessions;
use App\User;
use Excel;
use DB;

class SesionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if(isset($request->nameUser) || isset($request->datepicker1) || $request->datepicker2){

            $username = $request->nameUser;
            $query = Sessions::select(
                'sessions.id',
                'users.email as username',
                'roles.name',
                'sessions.ip',
                'sessions.access_date'
            )
            ->join('users','users.id','sessions.user_id')
            ->join('role_user','role_user.user_id','sessions.user_id')
            ->join('roles','roles.id','role_user.role_id')
            ->where('sessions.deleted_at',null);

            if($request->nameUser != ''){
                $query = $query->where('users.email','ilike','%'.$username.'%');
            }

            if($request->datepicker1 != '' && $request->datepicker2 != ''){

                $fechaInicio = explode("/", $request->datepicker1);
                $fechaInicio = $fechaInicio[2]."-".$fechaInicio[1]."-".$fechaInicio[0];
                $fechaInicio = strtotime($fechaInicio);
                $fechaInicio = date("Y-m-d 00:00:00",$fechaInicio);

                $fechaFinal = explode("/", $request->datepicker2);
                $fechaFinal = $fechaFinal[2]."-".$fechaFinal[1]."-".$fechaFinal[0];
                $fechaFinal = strtotime($fechaFinal);
                $fechaFinal = date("Y-m-d 23:59:59",$fechaFinal);

                $query = $query->whereBetween('sessions.access_date',array($fechaInicio,$fechaFinal));

                $fechaInicio = strtotime($fechaInicio);
                $fechaInicio = date("d/m/Y",$fechaInicio);
                $fechaFinal = strtotime($fechaFinal);
                $fechaFinal = date("d/m/Y",$fechaFinal);

            }

            $sesiones = $query->get();

            if(count($sesiones) !=0){
              $sesiones = json_encode($sesiones);
                return view('sessions.index',compact(['sesiones','username','fechaInicio','fechaFinal']));
            }else{
                return redirect()->route('usersession.index')->with('message_error', __('message.sindataencontrada'));
            }
            
        }else{
            $sesiones = Sessions::select(
                'sessions.id',
                'users.email as username',
                'roles.name',
                'sessions.ip',
                'sessions.access_date'
            )
            ->join('users','users.id','sessions.user_id')
            ->join('role_user','role_user.user_id','sessions.user_id')
            ->join('roles','roles.id','role_user.role_id')
            ->where('sessions.deleted_at',null)
            ->get();
            $sesiones = json_encode($sesiones);
            return view('sessions.index',compact('sesiones'));
        }
    }

    public function sesionXUser($id){
        $dataSesiones = Sessions::where("user_id",$id)->get();

        return response()->json([
            'data'=> $dataSesiones
        ],
        200);

    }

    public function apiSessions($username,$fechaInicio,$fechaFinal){

        if($username || $fechaInicio || $fechaFinal){

            $query = Sessions::select(
                'sessions.id',
                'users.email as username',
                'roles.name',
                'sessions.ip',
                'sessions.access_date'
            )
            ->join('users','users.id','sessions.user_id')
            ->join('role_user','role_user.user_id','sessions.user_id')
            ->join('roles','roles.id','role_user.role_id')
            ->where('sessions.deleted_at',null);

            if($username != '0'){
                $query = $query->where('users.email','ilike','%'.$username.'%');
            }

            if($fechaInicio != '0' && $fechaFinal != '0'){

                $fechaInicio = explode("-", $fechaInicio);
                $fechaInicio = $fechaInicio[2]."-".$fechaInicio[1]."-".$fechaInicio[0];
                $fechaInicio = strtotime($fechaInicio);
                $fechaInicio = date("Y-m-d 00:00:00",$fechaInicio);

                $fechaFinal = explode("-", $fechaFinal);
                $fechaFinal = $fechaFinal[2]."-".$fechaFinal[1]."-".$fechaFinal[0];
                $fechaFinal = strtotime($fechaFinal);
                $fechaFinal = date("Y-m-d 23:59:59",$fechaFinal);

                $query = $query->whereBetween('sessions.access_date',array($fechaInicio,$fechaFinal));

                $fechaInicio = strtotime($fechaInicio);
                $fechaInicio = date("d/m/Y",$fechaInicio);
                $fechaFinal = strtotime($fechaFinal);
                $fechaFinal = date("d/m/Y",$fechaFinal);

            }
            $sesiones = $query->get();

        }else{

          $sesiones = Sessions::select(
                'sessions.id',
                'users.email as username',
                'roles.name',
                'sessions.ip',
                'sessions.access_date'
            )
            ->join('users','users.id','sessions.user_id')
            ->join('role_user','role_user.user_id','sessions.user_id')
            ->join('roles','roles.id','role_user.role_id')
            ->where('sessions.deleted_at',null)
            ->get();

        }

        return response()->json([
            'sesiones'=> $sesiones
        ],
        200);

    }

    public function excel($request){

        return Excel::download(new SessionsExport("","",""),'sesiones.xlsx');

        $request = json_decode($request);
       
        if($request->username =='' && $request->fechaInicio =='-undefined-undefined' && $request->fechaFinal =='-undefined-undefined'){
                
                return Excel::download(new SessionsExport("","",""),'sesiones.xlsx');

        }else{
            $query = Sessions::select(
                'users.email as username',
                'roles.name',
                'sessions.ip',
                'sessions.access_date'
            )
            ->join('users','users.id','sessions.user_id')
            ->join('role_user','role_user.user_id','sessions.user_id')
            ->join('roles','roles.id','role_user.role_id');

            if($request->username != ''){
                $query = $query->where('users.email','ilike','%'.$request->username.'%');
            }

            if($request->fechaInicio != '' && $request->fechaFinal != ''){

                $fechaInicio = explode("-", $request->fechaInicio);
                $fechaInicio = $fechaInicio[2]."-".$fechaInicio[0]."-".$fechaInicio[1];
                $fechaInicio = strtotime($fechaInicio);
                $fechaInicio = date("Y-m-d 00:00:00",$fechaInicio);

                $fechaFinal = explode("-", $request->fechaFinal);
                $fechaFinal = $fechaFinal[2]."-".$fechaFinal[0]."-".$fechaFinal[1];
                $fechaFinal = strtotime($fechaFinal);
                $fechaFinal = date("Y-m-d 23:59:59",$fechaFinal);

                $query = $query->whereBetween('sessions.access_date',array($fechaInicio,$fechaFinal));

            }

            $sesiones = $query->get();
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   

        $session = Sessions::where("id",$id)->first();        
        $user = User::where("id",$session->user_id)->first();

        try{
            Sessions::where("id",$id)->delete();

            User::find($user->id)
                ->notify(
                      new NotificationsSession($session));

            return redirect()->route('usersession.index')->with('message_success', __('message.infoeliminada'));
        }catch(\Exception $e){

            DB::rollBack();
            return redirect()->route('usersession.index')->with('message_error', __('message.infonoeliminada'));
        }
    }



}
