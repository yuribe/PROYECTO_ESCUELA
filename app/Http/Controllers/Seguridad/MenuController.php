<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Menus\StoreMenuRequest;
use App\Http\Requests\Menus\UpdateMenuRequest;
use App\Exports\MenusExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use DB;

use App\Menu;
use App\Permission;
use App\Rolepermission;
use App\Module;
use App\GenericStatus;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $modules = Module::comboModulosMenu();
      $generics = GenericStatus::comboGenericsMenu();
      $menus = Menu::menulist($request->get('name'),$request->get('display_name'),$request->get('module_id'),$request->get('status_menu_id'))->get();
          return view('menus.index',compact('menus'))
          ->with([
                  'i'=> ($request->input('page', 1) - 1) * 10,
                  'name'=>$request->get('name'),
                  'display_name'=>$request->get('display_name'),
                  'module_id'=>$request->get('module_id'),
                  'status_menu_id'=>$request->get('status_menu_id'),
                  'modules'=>$modules,
                  'generics'=>$generics,
               ]);
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $modules = Module::comboModulosMenu();
      $generics = GenericStatus::comboGenerics();
      $menus = Menu::comboMenusCrea();

      return view('menus.create')->with([
                  'modules'=>$modules,
                  'generics'=>$generics,
                  'menus'=>$menus,
               ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMenuRequest $request)
    {
      try
      {
        $menu = new Menu();
        $menu->name = $request->input('name');
        $menu->display_name = $request->input('display_name');
        $menu->description = strtoupper($request->input('description'));
        $menu->menu_parent = $request->input('menu_parent');
        $menu->menu_level = $request->input('menu_level');
        $menu->menu_icon = $request->input('menu_icon');
        $menu->module_menu_id = $request->input('module_menu_id');
        $menu->status_menu_id = $request->input('status_menu_id');

        $menu->save();

        $LastInsertIdMenu = $menu->id;


        return redirect()->route('menus.index')->with('message_success', __('message.infoguardada'));

      }
        catch(\Exception $e)
      {
        DB::rollBack();
        return redirect()->route('menus.index')->with('message_error', __('message.infonoguardada'.$e));
      }

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = Menu::find($id);

        return view('menus.show',compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::find($id);
        $modules = Module::comboModulos();
        $generics = GenericStatus::comboGenerics();
        $menus = Menu::comboMenus();

        return view('menus.edit',compact('menu'))->with([
                    'modules'=>$modules,
                    'generics'=>$generics,
                    'menus'=>$menus,
                 ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMenuRequest $request, $id)
    {
        $menu = Menu::find($id);
        $menu->name = $request->input('name');
        $menu->display_name = $request->input('display_name');
        $menu->description = strtoupper($request->input('description'));
        $menu->menu_parent = $request->input('menu_parent');
        $menu->menu_level = $request->input('menu_level');
        $menu->menu_icon = $request->input('menu_icon');
        $menu->module_menu_id = $request->input('module_menu_id');
        $menu->status_menu_id = $request->input('status_menu_id');


        if($menu->save())
        {
           return redirect()->route('menus.index')->with('message_success', __('message.infoactualizada'));
        }
        else
        {
           return redirect()->route('menus.index')->with('message_error', __('message.infonoactualizada'));
        }
    }
    /**
     * Remove the specified rModule::all();esource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menus = Menu::find($id);

        if(Menu::where('id', $id)->delete())
        {
          return redirect()->route('menus.index')->with('message_success', __('message.infoeliminada'));
        }
        else
        {
          return redirect()->route('menus.index')->with('message_error', __('message.infonoeliminada'));
        }
    }

    public function excel(Request $request)
    {

      $name=$request->get('name');
      $display_name=$request->get('display_name');
      $module_id=$request->get('module_id');
      $status_menu_id=$request->get('status_menu_id');

      return Excel::download(new MenusExport($name,$display_name,$module_id,$status_menu_id), 'menus.xlsx');

    }

    public function pdf(Request $request)
    {
      //dd($request);
      $menus = Menu::menulist($request->get('name'),$request->get('display_name'),$request->get('module_id'),$request->get('status_menu_id'))->get();
      return view('menus.imprimirPdf',compact('menus'));
    }

}
