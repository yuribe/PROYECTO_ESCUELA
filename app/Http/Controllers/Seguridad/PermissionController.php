<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Permissions\StorePermissionRequest;
use App\Http\Requests\Permissions\UpdatePermissionRequest;
use App\Exports\PermissionsExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use App\Permission;
use App\Rolepermission;
use App\Module;
use App\GenericStatus;
use DB;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request);
        $modules = Module::comboModulosPermission();
        $generics = GenericStatus::comboGenericsPermission();
        $permissions = Permission::permissionlist($request->get('name'),$request->get('display_name'),$request->get('module_id'),$request->get('generic_status_id'))->get();

        return view('permissions.index',compact('permissions'))
        ->with([
                  'i'=> ($request->input('page', 1) - 1) * 10,
                  'name'=>$request->get('name'),
                  'display_name'=>$request->get('display_name'),
                  'module_id'=>$request->get('module_id'),
                  'generic_status_id'=>$request->get('generic_status_id'),
                  'modules'=>$modules,
                  'generics'=>$generics,
               ]);
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $modules = Module::comboModulos();
      $generics = GenericStatus::comboGenerics();

      return view('permissions.create',compact('permission'))->with([
                  'modules'=>$modules,
                  'generics'=>$generics,
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePermissionRequest $request)
    {
        //dd('prueba');

      try
      {
        $permission = new Permission();
        $permission->name = $request->input('name');
        $permission->display_name = $request->input('display_name');
        $permission->description = strtoupper($request->input('description'));
        $permission->generic_status_id = $request->input('generic_status_id');
        $permission->module_id = $request->input('module_id');
        $permission->save();

        $LastInsertIdPermission = $permission->id;

        //Registro para acceso del Superusuario
        $permission_role = new Rolepermission();
        $permission_role->role_id = 1;
        $permission_role->permission_id = $LastInsertIdPermission;
        $permission_role->created_at = now();
        $permission_role->updated_at = now();
        $permission_role->save();

        return redirect()->route('permissions.index')->with('message_success', __('message.infoguardada'));
      }
      catch(\Exception $e)
      {
        DB::rollBack();
        return redirect()->route('permissions.index')->with('message_error', __('message.infonoguardada'.$e));
      }

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::find($id);

        return view('permissions.show',compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modules = Module::comboModulos();
        $generics = GenericStatus::comboGenerics();
        $permission = Permission::find($id);

        return view('permissions.edit',compact('permission'))->with([
                    'modules'=>$modules,
                    'generics'=>$generics,
                  ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePermissionRequest $request, $id)
    {
        $permission = Permission::find($id);
        $permission->name = $request->input('name');
        $permission->display_name = $request->input('display_name');
        $permission->description = strtoupper($request->input('description'));
        $permission->generic_status_id = $request->input('generic_status_id');
        $permission->module_id = $request->input('module_id');

        if($permission->save())
        {
           return redirect()->route('permissions.index')->with('message_success', __('message.infoactualizada'));
        }
        else
        {
           return redirect()->route('permissions.index')->with('message_error', __('message.infonoactualizada'));
        }
    }
    /**
     * Remove the specified rModule::all();esource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //$permission = Permission::find($id);
      //$menus = Menu::where('id', $permission->module_id)->first();
      //$modules = Module::where('id', $permission->module_id)->first();

      /*
      if ($modules)
      {
        return redirect()->route('permissions.index')->with('message_error', __('message.infodependiente'));
      }
      else
      {
      */
      try
      {
         Permission::where('id', $id)->delete();
         Rolepermission::where('permission_id', $id)->delete();
         return redirect()->route('permissions.index')->with('message_success', __('message.infoeliminada'));
      }
      catch(\Exception $e)
      {
         DB::rollBack();
         return redirect()->route('permissions.index')->with('message_error', __('message.infonoeliminada'));
      }
      //}
    }

    public function excel(Request $request)
    {
      //dd($request);
      $name=$request->get('name');
      $display_name=$request->get('display_name');
      $module_id=$request->get('module_id');
      $generic_status_id=$request->get('generic_status_id');

      return Excel::download(new PermissionsExport($name,$display_name,$module_id,$generic_status_id), 'permissions.xlsx');

    }

    public function pdf(Request $request)
    {
      //dd($request);
      $permissions = Permission::permissionlist($request->get('name'),$request->get('display_name'),$request->get('module_id'),$request->get('generic_status_id'))->get();
      return view('permissions.imprimirPdf',compact('permissions'));
    }

}
