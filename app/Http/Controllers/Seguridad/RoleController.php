<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Roles\StoreRoleRequest;
use App\Http\Requests\Roles\UpdateRoleRequest;
use App\Exports\RolesExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use App\Role;
use App\Roleuser;
use App\Rolepermission;
use App\GenericStatus;
use DB;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $roles = Role::rolelist($request->get('name'),$request->get('display_name'),$request->get('status_role_id'))->paginate(10);
      $generics = GenericStatus::comboGenericsRole();
      return view('roles.index',compact('roles'))
              ->with([
                      'i'=> ($request->input('page', 1) - 1) * 10,
                      'name'=>strtoupper($request->get('name')),
                      'display_name'=>strtoupper($request->get('display_name')),
                      'status_role_id'=>$request->get('status_role_id'),
                      'generics'=>$generics,
                   ]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::get();
        $generics = GenericStatus::comboGenerics();
        return view('roles.create',compact('role'))->with([
                    'generics'=>$generics,
                  ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(StoreRoleRequest $request)
     {
         $role = new Role();
         $role->name = strtoupper($request->input('name'));
         $role->display_name = strtoupper($request->input('display_name'));
         $role->description = strtoupper($request->input('description'));
         $role->status_role_id = $request->input('status_role_id');

         if ($role->save())
         {
           return redirect()->route('roles.index')->with('message_success', __('message.infoguardada'));
         }
         else
         {
           return redirect()->route('roles.index')->with('message_error', __('message.infonoguardada'));
         }
     }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);

        return view('roles.show',compact('role'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $generics = GenericStatus::comboGenerics();
      $role = Role::find($id);

      return view('roles.edit',compact('role'))->with([
                  'generics'=>$generics,
                ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(UpdateRoleRequest $request, $id)
     {
         $role = Role::find($id);
         $role->name = strtoupper($request->input('name'));
         $role->display_name = strtoupper($request->input('display_name'));
         $role->description = strtoupper($request->input('description'));
         $role->status_role_id = $request->input('status_role_id');


         if($role->save())
         {
            return redirect()->route('roles.index')->with('message_success', __('message.infoactualizada'));
         }
         else
         {
            return redirect()->route('roles.index')->with('message_error', __('message.infonoactualizada'));
         }
     }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $roles = Role::find($id);
      $roleuser = Roleuser::where('role_id', $roles->id)->first();

      if ($roleuser)
      {
        return redirect()->route('roles.index')->with('message_error', __('message.infodependiente'));
      }
      else
      {
          try
          {
             Rolepermission::where('role_id', $id)->delete();
             Roleuser::where('role_id', $id)->delete();
             Role::where('id', $id)->delete();
             return redirect()->route('roles.index')->with('message_success', __('message.infoeliminada'));
          }
          catch(\Exception $e)
          {
            DB::rollBack();
            return redirect()->route('roles.index')->with('message_error', __('message.infonoeliminada'.$e));
          }
      }
    }

    public function excel(Request $request)
    {
      $name=$request->get('name');
      $display_name=$request->get('display_name');
      $status_role_id=$request->get('status_role_id');

      return Excel::download(new RolesExport($name,$display_name,$status_role_id), 'roles.xlsx');

    }

    public function pdf(Request $request)
    {
      $roles = Role::rolelist($request->get('name'),$request->get('display_name'),$request->get('status_role_id'))->get();
      return view('roles.imprimirPdf',compact('roles'));
    }
}
