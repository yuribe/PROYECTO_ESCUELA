<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserStatus\StoreUserStatusRequest;
use App\Http\Requests\UserStatus\UpdateUserStatusRequest;
use App\Exports\UserStatusExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use App\UserStatus;
use DB;

class UserStatusController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
      //dd($request);
      $userstatus = UserStatus::userstatuslist($request->get('name'),$request->get('description'))->paginate(10);

        return view('userStatus.index',compact('userstatus'))
        ->with([
                'i'=> ($request->input('page', 1) - 1) * 10,
                'name'=>$request->get('name'),
                'description'=>$request->get('description'),
             ]);
    }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('userStatus.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreUserStatusRequest $request)
  {
    try
    {
      $userstatus = new UserStatus();
      $userstatus->name = strtoupper($request->input('name'));
      $userstatus->description = strtoupper($request->input('description'));
      $userstatus->save();

      return redirect()->route('userstatus.index')->with('message_success', __('message.infoguardada'));

    }
      catch(\Exception $e)
    {
      DB::rollBack();
      return redirect()->route('userstatus.index')->with('message_error', __('message.infonoguardada'.$e));
    }
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      $userstatus = UserStatus::find($id);

      return view('userStatus.show',compact('userstatus'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      $userstatus = UserStatus::find($id);

      return view('userStatus.edit',compact('userstatus'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateUserStatusRequest $request, $id)
  {
      $userstatus = UserStatus::find($id);
      $userstatus->name = strtoupper($request->input('name'));
      $userstatus->description = strtoupper($request->input('description'));

      if($userstatus->save())
      {
         return redirect()->route('userstatus.index')->with('message_success', __('message.infoactualizada'));
      }
      else
      {
         return redirect()->route('userstatus.index')->with('message_error', __('message.infonoactualizada'));
      }
  }
  /**
   * Remove the specified rModule::all();esource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {

    if(UserStatus::where('id', $id)->delete())
    {
      return redirect()->route('userstatus.index')->with('message_success', __('message.infoeliminada'));
    }
    else
    {
      return redirect()->route('userstatus.index')->with('message_error', __('message.infonoeliminada'));
    }
  }

  public function excel(Request $request)
  {
    //dd($request);
    $name=$request->get('name');
    $description=$request->get('description');

    return Excel::download(new UserStatusExport($name,$description), 'userstatus.xlsx');

  }

  public function pdf(Request $request)
  {
    //dd($request);
    $userstatus = UserStatus::userstatuslist($request->get('name'),$request->get('description'))->get();
    return view('userStatus.imprimirPdf',compact('userstatus'));
  }

}
