<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\GenericStatus\StoreGenericStatusRequest;
use App\Http\Requests\GenericStatus\UpdateGenericStatusRequest;
use App\Exports\GenericStatusExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use App\GenericStatus;
use DB;

class GenericStatusController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
      //dd($request);
      $generics = GenericStatus::genericlist($request->get('name'),$request->get('description'))->paginate(10);

        return view('genericStatus.index',compact('generics'))
        ->with([
                'i'=> ($request->input('page', 1) - 1) * 10,
                'name'=>$request->get('name'),
                'description'=>$request->get('description'),
             ]);
    }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('genericStatus.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreGenericStatusRequest $request)
  {
    try
    {
      $genericos = new GenericStatus();
      $genericos->name = strtoupper($request->input('name'));
      $genericos->description = strtoupper($request->input('description'));
      $genericos->save();

      return redirect()->route('genericstatus.index')->with('message_success', __('message.infoguardada'));

    }
      catch(\Exception $e)
    {
      DB::rollBack();
      return redirect()->route('genericstatus.index')->with('message_error', __('message.infonoguardada'.$e));
    }
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      $generic = GenericStatus::find($id);

      return view('genericStatus.show',compact('generic'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      $generic = GenericStatus::find($id);

      return view('genericStatus.edit',compact('generic'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateGenericStatusRequest $request, $id)
  {
      $generic = GenericStatus::find($id);
      $generic->name = strtoupper($request->input('name'));
      $generic->description = strtoupper($request->input('description'));

      if($generic->save())
      {
         return redirect()->route('genericstatus.index')->with('message_success', __('message.infoactualizada'));
      }
      else
      {
         return redirect()->route('genericstatus.index')->with('message_error', __('message.infonoactualizada'));
      }
  }
  /**
   * Remove the specified rModule::all();esource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {

    if(GenericStatus::where('id', $id)->delete())
    {
      return redirect()->route('genericstatus.index')->with('message_success', __('message.infoeliminada'));
    }
    else
    {
      return redirect()->route('genericstatus.index')->with('message_error', __('message.infonoeliminada'));
    }
  }

  public function excel(Request $request)
  {
    //dd($request);
    $name=$request->get('name');
    $description=$request->get('description');

    return Excel::download(new GenericStatusExport($name,$description), 'genericstatus.xlsx');

  }

  public function pdf(Request $request)
  {
    //dd($request);
    $generics = GenericStatus::genericlist($request->get('name'),$request->get('description'))->get();
    return view('genericStatus.imprimirPdf',compact('generics'));
  }

}
