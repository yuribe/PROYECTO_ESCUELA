<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\StoreUserRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use App\User;
use App\Role;
use App\UserStatus;
use DB;
use Hash;
use App\Country;
use App\State;
use App\Municipality;
use App\Parishe;



class UserController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $roles = Role::comboRoles();
      $statusUsers = UserStatus::comboUserPermisosFiltrado();
      $data = User::userfilter(
        $request->get('name'),
        $request->get('email'),
        $request->get('role_id'),
        $request->get('user_status_id'),
        $request->get('identification_number'),
        $request->get('first_name'),
        $request->get('last_name'))
        ->get();
      return view('users.index',compact('data'))
            ->with([
                      'i'=> ($request->input('page', 1) - 1) * 10,
                      'name'=>$request->get('name'),
                      'email'=>$request->get('email'),
                      'role_id'=>$request->get('role_id'),
                      'user_status_id'=>$request->get('user_status_id'),
                      'identification_number'=>$request->get('identification_number'),
                      'first_name'=>$request->get('first_name'),
                      'last_name'=>$request->get('last_name'),
                      'roles'=>$roles,
                      'statusUsers'=>$statusUsers,
                   ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $roles = Role::pluck('display_name','id');
      $generics = UserStatus::comboUserPermisos();
      $countries = Country::comboCountries();
      $states = State::comboStates();

      return view('users.create')->with([
                  'roles'=>$roles,
                  'generics'=>$generics,
                  'countries'=>$countries,
                  'states'=>$states,

               ]);
      return view('users.create',compact('roles'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
      try
      {
        $user = new User();

        $user->photo_directory = $request->input('photo_directory');
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input['password']);
        $user->user_status_id = $request->input('user_status_id');
        $user->identification_number = $request->input('identification_number');
        $user->gender = strtoupper($request->input('gender'));
        $user->first_name = strtoupper($request->input('first_name'));
        $user->middle_name = strtoupper($request->input('middle_name'));
        $user->last_name = strtoupper($request->input('last_name'));
        $user->second_surname = strtoupper($request->input('second_surname'));
        $user->phone = $request->input('phone');
        $user->work_phone = $request->input('work_phone');
        $user->cellphone = $request->input('cellphone');
        $user->alternative_email = $request->input('alternative_email');
        $user->home_address = strtoupper($request->input('home_address'));
        $user->work_address = strtoupper($request->input('work_address'));
        $user->country_id = $request->input('country_id');
        $user->location = strtoupper($request->input('location character'));
        $user->state_id = $request->input('state_id');
        $user->municipality_id = $request->input('municipality_id');
        $user->parishe_id = $request->input('parishe_id');

        $user->save();

        foreach ($request->input('roles') as $key => $value)
        {
            $user->attachRole($value);
        }

        return redirect()->route('users.index')->with('message_success', __('message.infoguardada'));

      }
        catch(\Exception $e)
      {
        DB::rollBack();
        return redirect()->route('users.index')->with('message_error', __('message.infonoguardada'.$e));
      }
        /*
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        */
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $user = User::find($id);
       return view('users.show',compact('user'));

        //$user = User::find($id);
        //return view('users.show',compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('display_name','id');
        $userRole = $user->roles->pluck('id','id')->toArray();
        $generics = UserStatus::comboUserPermisos();
        $countries = Country::comboCountries();
        $states = State::comboStates();
        $municipalities = Municipality::listaMunicipios($user->state_id);
        $parishes = Parishe::listaParroquias($user->municipality_id);


        return view('users.edit',compact('user','roles','userRole','generics','countries','states','municipalities','parishes'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);


        $input = $request->all();
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));
        }


        $user = User::find($id);
        $user->update($input);
        DB::table('role_user')->where('user_id',$id)->delete();


        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }


        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }

    public function excel(Request $request)
    {
      //dd($request);
      $name=$request->get('name');
      $email=$request->get('email');
      $role_id=$request->get('role_id');
      $user_status_id=$request->get('user_status_id');
      $identification_number=$request->get('identification_number');
      $first_name=$request->get('first_name');
      $last_name=$request->get('last_name');

      return Excel::download(new UsersExport($name,$email,$role_id,$user_status_id,$identification_number,$first_name,$last_name), 'users.xlsx');

    }

    public function pdf(Request $request)
    {
      //dd($request);
      $data = User::userfilter(
        $request->get('name'),
        $request->get('email'),
        $request->get('role_id'),
        $request->get('user_status_id'),
        $request->get('identification_number'),
        $request->get('first_name'),
        $request->get('last_name'))
        ->get();
      return view('users.imprimirPdf',compact('data'));
    }

    public function municipioAjax(Request $request)
  {
    if ($request->ajax())
    {
      $lista = Municipality::listaMunicipios($request->estado);
      echo '<option disabled selected value="">Seleccione una opci&oacute;n</option>';

      foreach ($lista as $value)
      {
          echo '<option value=' . $value->id . '>' . $value->name . '</option>';
      }
    }
  }

  public function parroquiaAjax(Request $request)
  {
    if ($request->ajax())
    {
      $lista = Parishe::listaParroquias($request->municipio);
      echo '<option disabled selected value="">Seleccione una opci&oacute;n</option>';

      foreach ($lista as $value)
      {
        echo '<option value=' . $value->id . '>' . $value->name . '</option>';
      }
    }
  }

}
