<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\RolepermissionsExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Zizaco\Entrust\EntrustRole;
use Illuminate\Support\Facades\Auth;

use App\Rolepermission;
use App\Permission;
use App\Module;
use App\Role;
use App\GenericStatus;
use DB;

class RolepermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request);
        $roles = Role::comboRolePermission();
        $modules = Module::comboModulosPermission();
        $rolepermissions = Rolepermission::rolepermissionlist($request->get('name'),$request->get('display_name'),$request->get('module_id'),$request->get('role_id'))->distinct()->get();

        return view('rolepermissions.index',compact('rolepermissions'))
        ->with([
                  'i'=> ($request->input('page', 1) - 1) * 10,
                  'name'=>$request->get('name'),
                  'display_name'=>$request->get('display_name'),
                  'role_id'=>$request->get('role_id'),
                  'module_id'=>$request->get('module_id'),
                  'roles'=>$roles,
                  'modules'=>$modules,
               ]);
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePermissionRequest $request)
    {
        //dd('prueba');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //
    }

    public function asign(Request $request)
    {
      $superUser = Auth::user()->hasRole('SUPERUSUARIO');
      $rol="NULO";
      if($request->get('role_id') != '')
      {
        $rol = Role::find($request->get('role_id'));
        $idrol = $request->get('role_id');
      }
      else
      {
        $idrol = 0;
      }
      $modules = Module::comboModulos();
      $roles = Role::comboRoles();
      $permisos = Permission::comboPermisos();
      $rolepermisos = Permission::comboPermisosRoles($idrol);

      $modulepermissions = Module::asignpermissionlist($request->get('role_id'), $superUser)->distinct()->get();
      //dd($modulepermissions);

      return view('rolepermissions.asign',compact('modulepermissions'))->with([
                  'roles'=>$roles,
                  'rol'=>$rol,
                  'modules'=>$modules,
                  'permisos'=>$permisos,
                  'rolepermisos'=>$rolepermisos,
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //dd($request);
      $role_id = $request->id_rol;
      $module_id = $id;

      if(isset($request->bloqueaModulo))
      {
        if($module_id > 100) //Módulos reservados del sistema
        {
          $permiso = Permission::where('name',$module_id.'_OCULTO' )->update(['deleted_at' => now()]);
        }

        foreach ($request->hiddenPermiso as $hid=>$value)
        {
          Rolepermission::where('permission_id', $hid)
                          ->where('role_id', $role_id)
                          ->delete();
        }
      }
      else
      {
        if($module_id > 100) //Módulos reservados del sistema
        {
          $permiso = Permission::where('name',$module_id.'_OCULTO' )->update(['deleted_at' => null]);
        }
        foreach ($request->hiddenPermiso as $hid=>$value)
        {
          Rolepermission::where('permission_id', $hid)
                          ->where('role_id', $role_id)
                          ->delete();
        }
        foreach ($request->hiddenPermiso as $hid=>$value)
        {
          if(isset($request->checkPermiso))
          {
            foreach ($request->checkPermiso as $check=>$val)
            {
              if($hid === $check)
              {
                $rolepermiso = Rolepermission::where('permission_id', $check)
                                             ->where('role_id', $role_id)
                                             ->count();
                if($rolepermiso === 0)
                {
                   $permission_role = new Rolepermission();
                   $permission_role->permission_id = $check;
                   $permission_role->role_id = $role_id;
                   $permission_role->save();
                }
              }
            }
          }
        }
      }

      $superUser = Auth::user()->hasRole('SUPERUSUARIO');
      $rol = Role::find($role_id);
      $idrol = $role_id;
      $modules = Module::comboModulos();
      $roles = Role::comboRoles();
      $permisos = Permission::comboPermisos();
      $rolepermisos = Permission::comboPermisosRoles($idrol);

      $modulepermissions = Module::asignpermissionlist($idrol, $superUser)->distinct()->get();

      return view('rolepermissions.asign',compact('modulepermissions'))->with([
                  'roles'=>$roles,
                  'rol'=>$rol,
                  'modules'=>$modules,
                  'permisos'=>$permisos,
                  'rolepermisos'=>$rolepermisos,
                ]);
   }

    /**
     * Remove the specified rModule::all();esource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //
    }

    public function excel(Request $request)
    {
      //dd($request);
      $name=$request->get('name');
      $display_name=$request->get('display_name');
      $module_id=$request->get('module_id');
      $role_id=$request->get('role_id');

      return Excel::download(new RolepermissionsExport($name,$display_name,$module_id,$role_id), 'rolepermissions.xlsx');

    }

    public function pdf(Request $request)
    {
      //dd($request);
      $rolepermissions = Rolepermission::rolepermissionlist($request->get('name'),$request->get('display_name'),$request->get('module_id'),$request->get('role_id'))->distinct()->get();
      return view('rolepermissions.imprimirPdf',compact('rolepermissions'));
    }

}
