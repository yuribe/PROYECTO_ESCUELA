<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Configuration as Config;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'g-recaptcha-response' => 'required|captcha',
            'g-recaptcha-response' => 'required',
        ]);
    }

    public function __construct()
    {

        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        $datCaptcha = Config::select('value')->where('type','CAPTCHA')->first();
        $datCaptcha = json_decode($datCaptcha->value);

        return view('auth.passwords.email',compact('datCaptcha'));
    }
}
