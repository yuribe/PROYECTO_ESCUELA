<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Role;
use Socialite;
use App\Loglogin;
use Carbon\Carbon;
use App\Rules\Captcha;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Configuration as Config;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    

    protected $maxAttempts = 3; // Default is 5
    protected $decayMinutes = 1; // Default is 1

     /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $role = Role::pluck('display_name','id');
        $userRole = $user->roles->pluck('id','id')->toArray();

        $role = Role::find(implode($userRole));
        $role = $role->display_name;

        Loglogin::create([
            'email' => $user->email,
            'rol' => $role,
            'last_login_ip' => $request->getClientIp(),
            'created_at' => Carbon::now(),
        ]);
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($provider)
    {
        
        //return $provider;
        /*if ($provider == 'facebook') {
            
            $config = [
                'client_id' => "312488282763823",
                'client_secret' => "6858c3552dbcdd5051972d5966ea41d1",
                'redirect' => "http://proyecto-base.com/login/facebook/callback",
            ];
        
            return Socialite::buildProvider(\Laravel\Socialite\Two\FacebookProvider::class, array_get($config, 'client_id', 'client_secret', 'redirect'));
        }*/
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from$provider
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {

        $user = Socialite::driver($provider)->stateless()->user();
        $authUser = $this->findOrCreateUser($user, $provider);

        Auth::login($authUser, true);

        return redirect($this->redirectTo);
    }

    public function findOrCreateUser($user, $provider){

        $authUser = User::where('provider_id', $user->id)->first();
        if($authUser){
            return $authUser;
        }

        return User::create([
            'name'  => $user->name,
            'email' => $user->email,
            'provider'  => strtoupper($provider),
            'provider_id'   => $user->id,
            'user_status_id' => 1,
            'identification_number' => 1,
        ]);
    }

    protected function validateLogin(Request $request)
    {

        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
            'g-recaptcha-response' => 'required|captcha',
            'g-recaptcha-response' => 'required',
        ]);
    }



    public function showLoginForm()
    {

        $auth = Config::orderBy('id')->skip(1)->first();
        $auth = ($auth) ? json_decode($auth->value) : 0;
        
        $registro = Config::orderBy('id')->skip(2)->first();
        $registro = ($registro) ? json_decode($registro->value) : 0;
        $data = \Config::get('mail');
        $datCaptcha = Config::select('value')->where('type','CAPTCHA')->first();
        $datCaptcha = json_decode($datCaptcha->value);
        //dd($data);


        return view('auth.login', compact('auth', 'registro','datCaptcha'));
    }
}
