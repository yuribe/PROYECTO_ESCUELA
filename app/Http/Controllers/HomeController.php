<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sessions;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user= User::find(Auth::user()->id);
        $ip = \Request::ip();
        $fAcceso = date("Y-m-d H:m:s");

        $sesion = new \App\Sessions();
        $sesion->user_id = $user->id;
        $sesion->ip = $ip;
        $sesion->access_date = $fAcceso;

        $sesion->save();

        return view('home');
    }
}
