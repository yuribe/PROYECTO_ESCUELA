<?php

namespace App\Http\Controllers\Auditoria;

use App\Audit;
use Carbon\Carbon;
use App\Exports\AuditsExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Http\Requests\Audits\TraceRequest;

class TracesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(TraceRequest $request)
  { 

      $email = $request->email;
      $operation = $request->operation;
      $desde = strtotime($request->desde);
      $hasta = strtotime($request->hasta);
      $desde = Carbon::parse($desde)->format('Y-m-d').' 00:00:00';
      $hasta = Carbon::parse($hasta)->format('Y-m-d').' 23:59:59';

      $trace = Audit::select('audits.*','users.email')
      ->leftjoin('users','users.id','audits.user_id')
      ->orderBy('id','DESC')
      ->email($email)
      ->date($desde,$hasta)
      ->operation($operation)
      ->get();

      return view('traces.index',compact('trace'))
          ->with('i');

  }

  public function excel() 
    {
        return Excel::download(new AuditsExport, 'trazas.xlsx');
    }

}
