<?php

namespace App\Http\Controllers\Auditoria;

use App\Loglogin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogsloginController extends Controller
{
    
/**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  { 
    $email = $request->email;
    $desde = strtotime($request->desde);
    $hasta = strtotime($request->hasta);
    // dd($hasta);  
    // dd(gettype($desde));
    // dd(gettype($hasta));
    // dd($request->desde);
    // dd($request->hasta);
    // dd(gettype($request->desde));
    // dd(gettype($request->hasta));
    
    if ($request->desde != null) {
      $desde = Carbon::parse($desde)->format('Y-m-d').' 00:00:00';
      // dd($desde);
      $hasta = Carbon::parse($hasta)->format('Y-m-d').' 23:59:59';
    }


    $log = Loglogin::select('logslogin.*')
    ->email($email)
    ->dates($desde,$hasta)
    ->get();

    return view('logsLogin.index',compact('log'))
          ->with('i');
  }

}
