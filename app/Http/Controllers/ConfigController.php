<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuration as Config;
use App\Http\Requests\UpdateConfigRequest;

class ConfigController extends Controller
{
    //
	public function index(Request $request){

		$configs = Config::nombre($request->get('name'))->orderBy('id')->get();
		//dd($configs);
		return view('config.index', compact('configs'));
	}

	public function edit($id){

		$config = Config::find($id);
		$decode = json_decode($config->value);
		return view('config.edit', compact('config', 'decode'));
	}

	public function update(UpdateConfigRequest $request, $id){
		//dd($request);
		$config = Config::find($id);
		
		switch ($config->id) {
			case 1:
				$array = [
					'driver'	=>	strtoupper($request->driver),
					'host'		=>	strtoupper($request->host),
					'username'	=>	strtoupper($request->username),
					'password'	=>	$request->password,
					'port'		=>	$request->port,
					'from_address'	=>	strtoupper($request->address),
					'from_name'		=>	strtoupper($request->name),
					'encryption'=>	strtoupper($request->encryption)
				];
				break;
			case 2:
				$array = [
					'login'	=>	$request->login,
					'google'		=>	$request->google,
					'facebook'	=>	$request->facebook,
					'twitter'	=>	$request->twitter,
					'instagram'		=>	$request->instagram,
				];
				break;
			case 3:
				$array = [
					'interno'	=>	$request->interno,
					'externo'		=>	$request->externo,
				];
				break;
			case 4:
				$array = [
					'lifetime'	=>	$request->lifetime,
				];
				
				break;
			case 5:
				$array = [
					'captcha'	=>	$request->captcha,
				];
				
				break;	

			default:
				
				break;
		}
		//dd($array);
		$config->value = json_encode($array);
		//$config->save();
		if ($config->save())
         {
           return redirect()->route('config.index')->with('message_success', __('message.infoguardada'));
         }
         else
         {
           return redirect()->route('config.index')->with('message_error', __('message.infonoguardada'));
         }
		//return redirect('config')->with('message_success', __('message.infoguardada');
	}
}
