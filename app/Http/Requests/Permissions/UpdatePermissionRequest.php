<?php

namespace App\Http\Requests\Permissions;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'name'         => 'required|string|between:2,255|unique:permissions,name,'.$this->segment(2).',id',
        'display_name' => 'required|string|between:2,255|unique:permissions,display_name,'.$this->segment(2).',id',
        'description'  => 'nullable|string|between:10,255',
        'module_id' => 'required|integer',
        'generic_status_id' => 'required|integer',
      ];

    }

    public function messages()
    {
     return [
            'name.required' => 'El valor es obligatorio.',
            'name.unique' => 'El valor ya existe.',
            'name.between' => 'Mínimo 2, máximo 255 caracteres.',
            'display_name.required' => 'El valor es obligatorio.',
            'display_name.unique' => 'El valor ya existe.',
            'display_name.between' => 'Mínimo 2, máximo 255 caracteres.',
            'description.between' => 'Mínimo 10, máximo 255 caracteres.',
            'modules_id.required' => 'El valor es obligatorio.',
            'generic_status_id.required' => 'El valor es obligatorio.',
        ];
    }

}
