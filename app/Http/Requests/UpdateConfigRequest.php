<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd('hola');
        return [
            //
            //'nombre'    => 'required',
            'driver'    => 'sometimes|required',
            'host'      => 'sometimes|required',
            'username'  => 'sometimes|required',
            'password'  => 'sometimes|required',
            'port'      => 'sometimes|required',
            'address'   => 'sometimes|required',
            'name'      => 'sometimes|required',
            'encryption'=> 'sometimes|required',
        ];
    }
}
