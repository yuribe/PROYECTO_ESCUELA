<?php

namespace App\Http\Requests\UserStatus;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'name'         => 'required|string|between:2,50|unique:user_status,name',
        'description'  => 'nullable|string|between:10,255',
      ];

    }

    public function messages()
    {
     return [
            'name.required' => 'El valor es obligatorio.',
            'name.unique' => 'El valor ya existe.',
            'name.between' => 'Mínimo 2, máximo 50 caracteres.',
            'description.between' => 'Mínimo 10, máximo 255 caracteres.',

        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'name' => strtoupper($this->name),
        ]);
    }

}
