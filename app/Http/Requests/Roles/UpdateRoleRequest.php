<?php

namespace App\Http\Requests\Roles;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

      return [
        'name'         => 'required|string|between:2,50|unique:roles,name,'.$this->segment(2).',id',
        'display_name' => 'required|string|between:2,50|unique:roles,display_name,'.$this->segment(2).',id',
        'description'  => 'nullable|string|between:10,255',
        'status_role_id' => 'required|integer',
      ];

    }

    public function messages()
    {
     return [
            'name.required' => 'El valor es obligatorio.',
            'name.unique' => 'El valor ya existe.',
            'name.between' => 'Mínimo 2, máximo 50 caracteres.',
            'display_name.required' => 'El valor es obligatorio.',
            'display_name.unique' => 'El valor ya existe.',
            'display_name.between' => 'Mínimo 2, máximo 50 caracteres.',
            'description.between' => 'Mínimo 10, máximo 255 caracteres.',
            'status_role_id.required' => 'El valor es obligatorio.',

        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'name' => strtoupper($this->name),
            'display_name' => strtoupper($this->display_name),
        ]);
    }

}
