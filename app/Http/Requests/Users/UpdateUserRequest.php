<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

      return [
        'photo_directory'       => 'required|string|max:255',
        'name'                  => 'required|string|between:2,255',
        'email'                 => 'required|email|between:9,255',
        'password'              => 'required|string|confirmed|between:8,255',
        'password_confirmation' => 'required|string|between:8,255',
        'user_status_id'        => 'required|integer',
        'identification_number' => 'required|string|max:255',
        'gender'                => 'required|alpha|between:1,1',
        'first_name'            => 'required|alpha|between:2,30',
        'middle_name'           => 'nullable|alpha|between:2,30',
        'last_name'             => 'required|alpha|between:2,30',
        'second_surname'        => 'nullable|alpha|between:2,30',
        'phone'                 => 'nullable|string|max:20',
        'work_phone'            => 'nullable|string|max:20',
        'cellphone'             => 'required|string|max:20',
        'alternative_email'     => 'nullable|email|different:email|max:255',
        'home_address'          => 'required|string|max:500',
        'work_address'          => 'nullable|string|max:500',
        'country_id'            => 'required|integer',
        'location character'    => 'nullable|string|max:255',
        'state_id'              => 'required|integer',
        'municipality_id'       => 'required|integer',
        'parishe_id'            => 'required|integer',
      ];

    }

    public function messages()
    {
     return [
       'photo_directory.max'            => 'Máximo 255 caracteres.',
       'photo_directory.required'       => 'El valor es obligatorio.',
       'name.required'                  => 'El valor es obligatorio.',
       'name.between'                   => 'Mínimo 2, máximo 255 caracteres.',
       'email.required'                 => 'El valor es obligatorio.',
       'email.between'                  => 'Mínimo 9, máximo 255 caracteres.',
       'email.email'                    => 'El correo es inválido.',
       'password.required'              => 'El valor es obligatorio.',
       'password.max'                   => 'Máximo 255 caracteres.',
       'password.confirmed'             => 'La confirmación de la contraseña no coincide .',
       'password_confirmation.required' => 'El valor es obligatorio.',
       'password_confirmation.max'      => 'Máximo 255 caracteres.',
       'user_status_id.required'        => 'El valor es obligatorio.',
       'identification_number.required' => 'El valor es obligatorio.',
       'identification_number.max'      => 'Máximo 255 caracteres.',
       'gender.required'                => 'El valor es obligatorio.',
       'gender.alpha'                   => 'Sólo se acepta letra.',
       'gender.between'                 => 'Mínimo , máximo 1 caracteres.',
       'first_name.required'            => 'El valor es obligatorio.',
       'first_name.alpha'               => 'Sólo se aceptan letras.',
       'first_name.between'             => 'Mínimo 2, máximo 30 caracteres.',
       'middle_name.alpha'              => 'Sólo se aceptan letras.',
       'middle_name.between'            => 'Mínimo 2, máximo 30 caracteres.',
       'last_name.required'             => 'El valor es obligatorio.',
       'last_name.alpha'                => 'Sólo se aceptan letras.',
       'last_name.between'              => 'Mínimo 2, máximo 30 caracteres.',
       'second_surname.alpha'           => 'Sólo se aceptan letras.',
       'second_surname.between'         => 'Mínimo 2, máximo 30 caracteres.',
       'phone.max'                      => 'Máximo 20 caracteres.',
       'work_phone.max'                 => 'Máximo 20 caracteres.',
       'cellphone.required'             => 'El valor es obligatorio.',
       'cellphone.max'                  => 'Máximo 20 caracteres.',
       'alternative_email.between'      => 'Mínimo 9, máximo 255 caracteres.',
       'alternative_email.different'    => 'El correo alternativo y el correo electrónico deben ser diferentes.',
       'alternative_email.email'        => 'El correo es inválido.',
       'home_address.required'          => 'El valor es obligatorio.',
       'home_address.max'               => 'Mínimo 2, máximo 500 caracteres.',
       'work_address.max'               => 'Mínimo 2, máximo 500 caracteres.',
       'country_id.required'            => 'El valor es obligatorio.',
       'location.max'                   => 'Máximo 255 caracteres.',
       'state_id.required'              => 'El valor es obligatorio.',
       'municipality_id.required'       => 'El valor es obligatorio.',
       'parishe_id.required'            => 'El valor es obligatorio.',

        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'gender' => strtoupper($this->gender),

        ]);
    }

}
