<?php

namespace App\Http\Requests\Menus;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

      return [
        'name'         => 'required|string|between:2,50|unique:menus,name,'.$this->segment(2).',id',
        'display_name' => 'required|string|between:2,50|unique:menus,display_name,'.$this->segment(2).',id',
        'description'  => 'nullable|string|between:10,255',
        'menu_parent'     => 'required|integer',
        'menu_level'     => 'required|integer',
        'menu_icon'      => 'nullable|string|between:6,50',
        'module_menu_id' => 'required|integer',
        'status_menu_id' => 'required|integer',
      ];

    }

    public function messages()
    {
     return [
       'name.required' => 'El valor es obligatorio.',
       'name.unique' => 'El valor ya existe.',
       'name.between' => 'Mínimo 2, máximo 50 caracteres.',
       'display_name.required' => 'El valor es obligatorio.',
       'display_name.unique' => 'El valor ya existe.',
       'display_name.between' => 'Mínimo 2, máximo 50 caracteres.',
       'description.between' => 'Mínimo 10, máximo 255 caracteres.',
       'menu_icon.between' => 'Mínimo 6, máximo 50 caracteres.',
       'menu_level.between' => 'Mínimo 0.',
       'menu_level.required' => 'El valor es obligatorio.',
       'menu_parent.between' => 'Mínimo 0.',
       'menu_parent.required' => 'El valor es obligatorio.',
       'module_menu_id.required' => 'El valor es obligatorio.',
       'status_menu_id.required' => 'El valor es obligatorio.',

        ];
    }

}
