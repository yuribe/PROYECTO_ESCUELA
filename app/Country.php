<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;

class Country extends Model implements Auditable
{

  use \OwenIt\Auditing\Auditable;

  protected $table = "countries";

  public static function comboCountries()
  {
    return Country::select('id', 'name')
                    ->from('countries')
                    ->where('id','>',0)
                    ->orderBy('name','asc')
                    ->get();
  }

  public static function listaCountries($country)
  {
    return Parishe::select('id', 'name')
                    ->where('country_id', $country)
                    ->orderBy('name', 'asc')
                    ->get();
  }

  protected $fillable = [
      'name', 'iso_2','iso_3',
  ];
}
