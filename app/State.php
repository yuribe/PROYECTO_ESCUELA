<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;


class State extends Model implements Auditable
{

  use \OwenIt\Auditing\Auditable;

  protected $table = "states";

  public static function comboStates()
  {
    return State::select('id', 'name')
                    ->from('states')
                    ->where('id','>',0)
                    ->orderBy('name','asc')
                    ->get();
  }

  public static function listaStates($state)
  {
    return Parishe::select('id', 'name')
                    ->where('state_id', $state)
                    ->orderBy('name', 'asc')
                    ->get();
  }

  protected $fillable = [
      'name',
    ];
}
