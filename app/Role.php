<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;

class Role extends EntrustRole implements Auditable
{
  //use SoftDeletes;
  use \OwenIt\Auditing\Auditable;

  protected $table = 'roles';
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  public function getCreatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function getUpdatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function generics()
  {
    return $this->BelongsTo('App\GenericStatus', 'status_role_id', 'id');
  }

  public static function comboRoles()
  {
    return Role::select('id', 'name','display_name')
                    ->from('roles')
                    ->where('status_role_id',1)
                    ->where('id','>',1) // Omitiendo Superusuario
                    ->orderBy('name', 'ASC')
                    ->get();
  }

  public static function comboRolePermission()
  {
    return Role::select('id', 'name','display_name')
                    ->from('roles')
                    ->where('status_role_id',1)
                    ->where('id','>',1) // Omitiendo Superusuario
                    ->whereIn('id',Rolepermission::pluck('role_id'))
                    ->orderBy('name', 'ASC')
                    ->get();
  }

  public function scopeRolelist($query, $name = null, $display_name = null, $status_role_id = null)
  {
    if ($name <> "")
    {
      $name = strtoupper($name);
      $query = $query->where('name', 'like', '%' . $name . '%');
    }
    if ($display_name <> "")
    {
      $display_name = strtoupper($display_name);
      $query = $query->where('display_name', 'like', '%' . $display_name . '%');
    }

    if ($status_role_id <> "")
    {
      $query = $query->where('status_role_id', $status_role_id);
    }
    $query = $query->where('id', '>', 1);  //Omitiendo Superusuario
    $query = $query->where('status_role_id', '!=', 0);
    $query = $query->orderBy('name', 'ASC');

    return $query;
  }

  protected $fillable = [
      'name', 'display_name', 'description','status_role_id',
  ];
}
