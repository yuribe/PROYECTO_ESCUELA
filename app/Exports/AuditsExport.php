<?php

namespace App\Exports;

use App\Audit;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class AuditsExport implements
  FromQuery,
  Responsable,
  WithMapping,
  WithHeadings,
  WithColumnFormatting,
  WithTitle,
  WithStrictNullComparison,
  WithEvents,
  ShouldAutoSize
{

    use Exportable;

    public function query()
    {
        return Audit::select('audits.id', 'audits.ip_address', 'users.email', 'translate.spanish', 'audits.old_values', 'audits.new_values', 'audits.created_at')
        ->leftjoin('users','users.id','audits.user_id')
        ->join('translate','translate.english','audits.event')
        ->orderBy('id','ASC')
        ->get();
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Reporte de trazas';
    }

    public function headings(): array
    {
        return [
            'N°',
            'Ip',
            'Usuario',
            'Operación',
            'Dato Anterior',
            'Dato Posterior',
            'Fecha/Hora creación',
        ];
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->ip_address,
            $row->email,
            $row->spanish,
            $row->old_values,
            $row->new_values,
            $row->created_at,
        ];
    }

    public function columnFormats(): array
    {
        return [
            'G' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function registerEvents(): array
    {
        return [
                AfterSheet::class => function(AfterSheet $event) {
                //Inicio cálculo de correlativo
                $count = 1;
                $fila = 2;
                $letra='A';
                $criterio = '';
                $valor = '';
                do {
                    $criterio = $letra.$fila;
                    $valor = $event->getSheet()->getCell($criterio)->getValue();
                    if($valor != '')
                    {
                      $event->getSheet()->setCellValue($criterio,$count);
                      $fila = $fila+1;
                      $count = $count+1;
                    }
                    else
                    {
                      $count = 0;
                    }
                 } while ($count > 0);
                //Fin cálculo de correlativo
                $event->getSheet()->insertNewRowBefore(1, 7);
                $event->getSheet()->mergeCells('D3:F3');
                $event->getSheet()->mergeCells('D4:F4');
                $event->getSheet()->setCellValue('D3', 'SISTEMA PROYECTO BASE');
                $event->getSheet()->setCellValue('D4', 'Reporte Trazas del Sistema');
                $event->getSheet()->autoSize();
                $event->getSheet()->getStyle('A8:F8')->getFill()
                       ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                       ->getStartColor()->setARGB('E3EBF2');
                $event->getSheet()->getStyle('A8:F8')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $event->getSheet()->getStyle('A8:F8')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $event->getSheet()->getStyle('A8:F8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $event->getSheet()->getStyle('A8:F8')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $event->getSheet()->getStyle('A8:F8')->getFont()->setBold(true);
                $event->getSheet()->getStyle('D3')->getFont()->setBold(true);
                $event->getSheet()->getStyle('D4')->getFont()->setBold(true);
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('Logo');
                $drawing->setDescription('Logo');
                $drawing->setPath(public_path('img/arthalogo.png'));
                $drawing->setHeight(120);
                $drawing->setCoordinates('A1');
                $drawing->setWorksheet($event->sheet->getDelegate());
            },
        ];
    }
}
