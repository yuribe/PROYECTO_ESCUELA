<?php

namespace App\Exports;

use App\Sessions;
use App\User;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

class SessionsExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents
{
   
    use Exportable;

    protected $nameUser;
    protected $fechaInicio;
    protected $fechaFinal;

    public function __construct($nameUser,$fechaInicio,$fechaFinal)
    {
         $this->nameUser = $nameUser;
         $this->fechaInicio = $fechaInicio;
         $this->fechaFinal = $fechaFinal;
    }

    public function query()
    {
    
    	return Sessions::sessionslist($this->nameUser,$this->fechaInicio,$this->fechaFinal);
    	
    }

    public function headings(): array
    {
        return [
            'Id',
            'Username',
            'Ip',
            'Fecha/Hora de Acceso',
        ];

    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
              $cellRange = 'A1:W1'; // All headers
              $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }


}
