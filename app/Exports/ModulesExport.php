<?php

namespace App\Exports;

use App\Module;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
class ModulesExport implements
  FromQuery,
  Responsable,
  WithMapping,
  WithHeadings,
  WithColumnFormatting,
  WithTitle,
  WithStrictNullComparison,
  WithEvents,
  ShouldAutoSize
{
    use Exportable;

    protected $name;
    protected $display_name;
    protected $status_module_id;

    public function __construct($name,$display_name,$status_module_id)
    {
         $this->name = $name;
         $this->display_name = $display_name;
         $this->status_module_id = $status_module_id;
    }

    public function query()
    {
      return Module::modulelist($this->name,$this->display_name,$this->status_module_id);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Reporte de módulos del sistema';
    }

    public function headings(): array
    {
        return [
            'N°',
            'Nombre del módulo(ruta)',
            'Nombre a mostrar',
            'Descripción',
            'Fecha/Hora creación',
            'Estatus'
        ];
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->name,
            $row->display_name,
            $row->description,
            $row->created_at,
            $row->genericosmod->name
        ];
    }

    public function columnFormats(): array
    {
        return [
            'E' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function registerEvents(): array
    {
        return [
                AfterSheet::class => function(AfterSheet $event) {
                //Inicio cálculo de correlativo
                $count = 1;
                $fila = 2;
                $letra='A';
                $criterio = '';
                $valor = '';
                do {
                    $criterio = $letra.$fila;
                    $valor = $event->getSheet()->getCell($criterio)->getValue();
                    if($valor != '')
                    {
                      $event->getSheet()->setCellValue($criterio,$count);
                      $fila = $fila+1;
                      $count = $count+1;
                    }
                    else
                    {
                      $count = 0;
                    }
                 } while ($count > 0);
                //Fin cálculo de correlativo

                $event->getSheet()->insertNewRowBefore(1, 7);
                $event->getSheet()->mergeCells('C3:F3');
                $event->getSheet()->mergeCells('C4:F4');
                $event->getSheet()->setCellValue('C3', 'SISTEMA PROYECTO BASE');
                $event->getSheet()->setCellValue('C4', 'Reporte Módulos del Sistema');
                $event->getSheet()->autoSize();
                $event->getSheet()->getStyle('A8:F8')->getFill()
                       ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                       ->getStartColor()->setARGB('E3EBF2');
                $event->getSheet()->getStyle('A8:F8')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $event->getSheet()->getStyle('A8:F8')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $event->getSheet()->getStyle('A8:F8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $event->getSheet()->getStyle('A8:F8')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $event->getSheet()->getStyle('A8:F8')->getFont()->setBold(true);
                $event->getSheet()->getStyle('C3')->getFont()->setBold(true);
                $event->getSheet()->getStyle('C4')->getFont()->setBold(true);
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('Logo');
                $drawing->setDescription('Logo');
                $drawing->setPath(public_path('img/arthalogo.png'));
                $drawing->setHeight(120);
                $drawing->setCoordinates('A1');
                $drawing->setWorksheet($event->sheet->getDelegate());
            },
        ];
    }


}
