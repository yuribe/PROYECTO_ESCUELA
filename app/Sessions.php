<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sessions extends Model
{
	use SoftDeletes;
    protected $table = 'sessions';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
		'user_id',
		'ip',
		'access_date',
  	];

  	public function scopeSessionslist($query,$username=null,$fechaInicio=null,$fechaFinal=null){  

  		$query = Sessions::select(
  				'users.email as username',
                'roles.name',
                'sessions.ip',
                'sessions.access_date'
  		)
  		->join('users','users.id','sessions.user_id')
        ->join('role_user','role_user.user_id','sessions.user_id')
        ->join('roles','roles.id','role_user.role_id')
        ->get();

        return $query;
  	}

}
