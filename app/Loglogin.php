<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loglogin extends Model
{
    protected $table = 'logslogin';

    protected $fillable = [
        'email',
        'rol',
        'last_login_ip',
    ];

    //SCOPES

    public function ScopeEmail($query, $email)
    {
        if($email){
            return $query->where('email','LIKE',"%$email%");
        }
    }

    public function ScopeDates($query, $desde, $hasta)
    {
        if($desde){
            $query->where('logslogin.created_at','>=',"%$desde%");
        }
        if($desde && $hasta){
            $query->where('logslogin.created_at','<=',"%$hasta%");
        }
            
            return $query;
    }

}
