<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Configuration extends Model
{
    protected $table = 'configurations';
  	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  	protected $fillable = [
  		'type',
		'value',
		'active'
  	];

  	public function scopeNombre($query, $name = null){
  		if ($name <> "")
	    {
	      $name = strtoupper($name);
	      $query = $query->where('type', 'like', '%' . $name . '%');
	    }
  	}

    
}
