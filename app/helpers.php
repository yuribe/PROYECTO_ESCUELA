<?php

    function buildMenu()
    {
      $genmenus = DB::table('menus')
        ->join('modules', 'modules.id', '=', 'menus.module_menu_id')
        ->select('menus.id','menus.display_name as display_names','menus.module_menu_id','menus.menu_icon','menus.menu_parent','menus.menu_level','modules.name as module_name')
        ->where('menus.status_menu_id','=', 1)
        ->where('menus.id','>', '0')
        ->whereNull('menus.deleted_at')
        ->orderBy('menus.menu_parent','ASC')
        ->orderBy('menus.menu_level','ASC')
        ->get();

        return($genmenus);
     }

     function buildMenuChild($id)
     {
       $genmenusChild = DB::table('menus')
         ->join('modules', 'modules.id', '=', 'menus.module_menu_id')
         ->select('menus.id','menus.display_name as display_names','menus.module_menu_id','menus.menu_icon','menus.menu_parent','menus.menu_level','modules.name as module_name')
         ->where('menus.status_menu_id','=', 1)
         ->where('menus.id','>', '0')
         ->where('menus.menu_parent',$id)
         ->whereNull('menus.deleted_at')
         ->orderBy('menus.menu_level','ASC')
         ->get();

         return($genmenusChild);
      }

      function buildMenuCount($id)
      {
        $genmenusChild = DB::table('menus')
          ->join('modules', 'modules.id', '=', 'menus.module_menu_id')
          ->select('menus.*','modules.name as module_name')
          ->where('menus.status_menu_id','=', 1)
          ->where('menus.id','>', '0')
          ->where('menus.menu_parent',$id)
          ->whereNull('menus.deleted_at')
          ->count();


          return($genmenusChild);
       }
