<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Config;
class ConfigServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        if (\Schema::hasTable('configurations')) {
            $mail = DB::table('configurations')->where('id', 1)->first();
            //$lifetime = DB::table('configurations')->where('id', 4)->first();
            if ($mail) //checking if table is not empty
            {
                $variable = json_decode($mail->value);
                //dd($mail->value);
                $config = array(
                    'driver'     => strtolower($variable->driver),
                    'host'       => strtolower($variable->host),
                    'port'       => $variable->port,
                    'from'       => array('address' => $variable->from_address, 'name' => $variable->from_name),
                    'encryption' => strtolower($variable->encryption),
                    'username'   => strtolower($variable->username),
                    'password'   => $variable->password,
                    'sendmail'   => '/usr/sbin/sendmail -bs',
                    'pretend'    => false,
                );
                Config::set('mail', $config);
            }
            $lifetime = DB::table('configurations')->where('id', 4)->first();
            if ($lifetime) {
                $variable = json_decode($lifetime->value);

                Config::set('session.lifetime', $variable->lifetime);
                //dd(\Config::get('session.lifetime'));
            }
        }
    }
}
