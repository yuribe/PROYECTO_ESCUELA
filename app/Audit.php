<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{

    //SCOPES

    public function ScopeEmail($query, $email)
    {
        if($email){
            return $query->where('email','LIKE',"%$email%");
        }
    }

    public function ScopeOperation($query, $operation)
    {
        if($operation){
            return $query->where('event','LIKE',"%$operation%");
        }
    }

    public function ScopeDate($query, $desde, $hasta)
    {
        if($desde){
            $query->where('audits.created_at','>=',"%$desde%");
        }
        if($desde && $hasta){
            $query->where('audits.created_at','<=',"%$hasta%");
        }
            
            return $query;
    }

}
