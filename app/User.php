<?php

namespace App;

use App\Notifications\ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use OwenIt\Auditing\Contracts\Auditable;
//use Illuminate\Database\Eloquent\SoftDeletes;

use QCod\ImageUp\HasImageUploads;
use App\Roleuser;
use App\Role;
use App\UserStatus;
use App\Country;
use App\State;
use App\Municipality;
use App\Parishe;

class User extends Authenticatable implements Auditable
{
    //use SoftDeletes;
    use Notifiable;
    use EntrustUserTrait;
    use \OwenIt\Auditing\Auditable;
    use HasImageUploads;

    //Formateo  fechas

    public function getCreatedAtAttribute($value)
    {
      return date('d-m-Y H:i:s', strtotime($value));
    }

    public function getUpdatedAtAttribute($value)
    {
      return date('d-m-Y H:i:s', strtotime($value));
    }

    public function getActivatedAtAttribute($value)
    {
      return date('d-m-Y H:i:s', strtotime($value));
    }

    public function getEmailVerifiedAtAttribute($value)
    {
      return date('d-m-Y H:i:s', strtotime($value));
    }

    //Fin formateo  fechas

    public function usuarioroles()
    {
      return $this->hasMany('App\Roleuser', 'user_id', 'id');
    }

    public function estatususers()
    {
      return $this->belongsTo('App\UserStatus', 'user_status_id', 'id');
    }

    public function countries()
    {
      return $this->belongsTo('App\Country', 'country_id', 'id');
    }

    public function municipalities()
    {
      return $this->belongsTo('App\Municipality', 'municipality_id', 'id');
    }

    public function parishes()
    {
      return $this->belongsTo('App\Parishe', 'parishe_id', 'id');
    }

    public function states()
    {
      return $this->belongsTo('App\State', 'state_id', 'id');
    }

    public function scopeUserfilter($query,$name = null,$email = null,$role_id = null,$user_status_id = null,$identification_number = null,$first_name = null,$last_name = null)

    {
      if ($name <> "")
      {
        $name = strtoupper($name);
        $query = $query->where('users.name', 'like', '%' . $name . '%');
      }

      if ($email <> "")
      {
        $query = $query->where('email', 'like', '%' . $email . '%');
      }

      /*
      if ($role_id <> "")
      {
        $role_id = (int) $role_id;
        $query = $query->where('usuarioroles.id', $role_id);
      }
      */
      if ($user_status_id <> "")
      {
        $user_status_id = (int) $user_status_id;
        $query = $query->where('user_status_id', $user_status_id);
      }
      if ($identification_number <> "")
      {
        $identification_number = (int) $identification_number;
        $query = $query->where('identification_number', $identification_number);
      }

      if ($first_name <> "")
      {
        $first_name = strtoupper($first_name);
        $query = $query->where('first_name', 'like', '%' . $first_name . '%');
      }
      if ($last_name <> "")
      {
        $last_name = strtoupper($last_name);
        $query = $query->where('last_name', 'like', '%' . $last_name . '%');
      }

      $query = $query->orderBy('users.identification_number', 'ASC');
      return $query;
    }

    protected $hidden = [
       'remember_token', 'activation_code', 'reset_password_code',
     ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected static $imageFields = [
        'photo_directory' => [
            // width to resize image after upload
            'width' => 200,

            // height to resize image after upload
            'height' => 100,

            // set true to crop image with the given width/height and you can also pass arr [x,y] coordinate for crop.
            'crop' => true,

            // what disk you want to upload, default config('imageup.upload_disk')
            'disk' => 'public',

            // a folder path on the above disk, default config('imageup.upload_directory')
            'path' => 'avatars',

            // placeholder image if image field is empty
            'placeholder' => '/img/icono_usuario.png',

            // validation rules when uploading image
            'rules' => 'image|max:2000',

            // override global auto upload setting coming from config('imageup.auto_upload_images')
            'auto_upload' => false,

            // if request file is don't have same name, default will be the field name
            'file_input' => 'photo',

            // a hook that is triggered before the image is saved
            'before_save' => BlurFilter::class,

            // a hook that is triggered after the image is saved
            'after_save' => CreateWatermarkImage::class
        ]
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [

      'name',
      'email',
      'email_verified_at',
      'password',
      'user_status_id',
      'activated',
      'activated_at',
      'identification_number',
      'phone',
      'work_phone',
      'cellphone',
      'home_address',
      'work_address',
      'alternative_email',
      'first_name',
      'last_name',
      'middle_name',
      'second_surname',
      'photo_directory',
      'provider',
      'provider_id',
      'country_id',
      'location',
      'parish_id',

    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

}
