<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;

class Roleuser extends Model implements Auditable
{
  use \OwenIt\Auditing\Auditable;

  protected $table = 'role_user';
  protected $dates = ['created_at', 'updated_at'];

  public function getCreatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function getUpdatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  protected $fillable = [
      'user_id', 'role_id',
  ];
}
