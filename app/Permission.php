<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustPermission;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;

use App\Permission;
use App\Module;
use DB;

class Permission extends EntrustPermission implements Auditable
{
  //use SoftDeletes;
  use \OwenIt\Auditing\Auditable;

  protected $table = 'permissions';
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  public function getCreatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function getUpdatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function modules()
  {
    return $this->BelongsTo('App\Module', 'module_id', 'id');
  }

  public function generics()
  {
    return $this->BelongsTo('App\GenericStatus', 'generic_status_id', 'id');
  }

  public static function comboPermisos()
  {
    return Permission::select('id', 'name', 'module_id')
                    ->from('permissions')
                    ->where('generic_status_id',1)
                    ->orderBy('name', 'ASC')
                    ->get();
  }

  public static function comboPermisosRoles($id_rol)
  {
    return Permission::select('permissions.id as id_permiso', 'permissions.name as permiso_name', 'permissions.module_id as permiso_id_module', 'rol as role_permiso_role_id')
                             ->from('permissions')
                             ->leftJoin(DB::raw("(SELECT
                                permission_id, role_id as rol
                                FROM permission_role
                                WHERE permission_role.role_id =".$id_rol."
                                AND permission_role.role_id > 0
                               ) as permisoroles"),
                                         function($leftjoin){
                                           $leftjoin->on('permisoroles.permission_id','=','permissions.id');
                                       })
                              ->orderBy('permissions.id', 'ASC')
                              ->get();
  }

  public function scopePermissionlist($query, $name = null, $display_name = null, $module_id = null, $generic_status_id = null)
  {
    if ($name <> "")
    {
      $query = $query->where('name', 'like', '%' . $name . '%');
    }
    if ($display_name <> "")
    {
      $query = $query->where('display_name', 'like', '%' . $display_name . '%');
    }

    if ($module_id <> "")
    {
      $query = $query->where('module_id', $module_id);
    }

    if ($generic_status_id <> "")
    {
      $query = $query->where('generic_status_id', $generic_status_id);
    }

    if(Auth::user()->hasRole('SUPERUSUARIO'))
    {
      $query = $query->where('id', '>', 0);
    }
    else
    {
      $query = $query->where('id', '>', 1000);
    }

    $query = $query->where('permissions.name', 'not like', '%' . '_OCULTO' . '%');

    $query = $query->where('generic_status_id', '!=', 0);
    $query = $query->where('module_id', '!=', 0);
    $query = $query->orderBy('module_id', 'ASC');
    $query = $query->orderBy('name', 'ASC');

    return $query;
  }

  protected $fillable = [
      'name', 'display_name', 'description','module_id','generic_status_id',
  ];

}
