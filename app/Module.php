<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;

class Module extends Model implements Auditable
{
  /**
   * Los primeros 100 registros están reservados para módulos del sistema que no se pueden editar ni eliminar
   */
  use \OwenIt\Auditing\Auditable;
  //use SoftDeletes;

  protected $table = 'modules';
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  public function getCreatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function getUpdatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function permisos()
  {
    return $this->hasMany('App\Permission', 'module_id', 'id');
  }

  public function genericosmod()
  {
    return $this->BelongsTo('App\GenericStatus','status_module_id','id');
  }

  public static function comboModulos()
  {
    return Module::select('id', 'name')
                    ->from('modules')
                    ->where('status_module_id',1)
                    ->orderBy('name', 'ASC')
                    ->get();
  }

  public static function comboModulosMenu()
  {
    return Module::select('id', 'name')
                    ->from('modules')
                    ->where('status_module_id',1)
                    ->whereIn('id',Menu::pluck('module_menu_id'))
                    ->orderBy('name', 'ASC')
                    ->get();
  }

  public static function comboModulosPermission()
  {
    return Module::select('id', 'name')
                    ->from('modules')
                    ->where('status_module_id',1)
                    ->whereIn('id',Permission::where('description','!=','OCULTO')->pluck('module_id'))
                    ->orderBy('name', 'ASC')
                    ->get();
  }

  public function scopeModulelist($query, $name = null, $display_name = null, $status_module_id = null)
  {
    if ($name <> "")
    {
      $query = $query->where('name', 'like', '%' . $name . '%');
    }
    if ($display_name <> "")
    {
      $query = $query->where('display_name', 'like', '%' . $display_name . '%');
    }

    if ($status_module_id <> "")
    {
      $query = $query->where('status_module_id', $status_module_id);
    }
    $query = $query->where('status_module_id', '!=', 0);

    if(Auth::user()->hasRole('SUPERUSUARIO'))
    {
      $query = $query->where('id', '>', 0);
    }
    else
    {
      $query = $query->where('id', '>', 100);
    }

    $query = $query->orderBy('name', 'ASC');

    return $query;
  }

  public function scopeAsignpermissionlist($query, $role_id = null, $superUser = null)
  {
    $query = $query->join('permissions', 'permissions.module_id', '=', 'modules.id');
    $query = $query->join('permission_role', 'permission_role.permission_id', '=', 'permissions.id');
    $query = $query->join('roles', 'roles.id', '=', 'permission_role.role_id');
    $query = $query->select('modules.id as module_id','modules.name as module_name','modules.deleted_at as inhabilitado');

    if (($role_id <> "") && ($superUser == false))
    {
      $query = $query->where('roles.id', $role_id);
      $query = $query->where('permissions.module_id', '>', 100); //Códigos reservados para el sistema/SuperUsuario
    }

    if ($superUser == false)
    {
      $query = $query->orWhere('roles.id',  0);
      $query = $query->where('permissions.module_id', '>', 100);
      $query = $query->where('permissions.deleted_at', null);
    }

    $query = $query->where('modules.status_module_id', 1);
    $query = $query->where('permissions.generic_status_id', 1);
    $query = $query->orderBy('modules.name', 'ASC');

    //dd($query);
    return $query;
  }

  protected $fillable = [
      'name', 'display_name', 'description','status_module_id',
  ];
}
