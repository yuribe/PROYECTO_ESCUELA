<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;
use Zizaco\Entrust\EntrustPermission;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

use App\Permission;
use App\Module;
use App\Role;

class Rolepermission extends Model implements Auditable
{
  //use SoftDeletes;
  use \OwenIt\Auditing\Auditable;

  protected $table = 'permission_role';
  protected $dates = ['created_at', 'updated_at'];

  public function getCreatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function getUpdatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function scopeRolepermissionlist($query, $name = null, $display_name = null, $module_id = null, $role_id = null)
  {
    $query = $query->join('roles', 'roles.id', '=', 'permission_role.role_id');
    $query = $query->join('permissions', 'permissions.id', '=', 'permission_role.permission_id');
    $query = $query->join('modules', 'modules.id', '=', 'permissions.module_id');

    $query = $query->select('roles.id as role_id','roles.name as role_name','roles.status_role_id as role_status',
                            'permissions.id as permission_id','permissions.name as permission_name',
                            'permissions.display_name as permission_display','permissions.generic_status_id as permission_status',
                            'modules.id as module_id','modules.name as module_name','modules.status_module_id as module_status');

    if ($name <> "")
    {
      $query = $query->where('permissions.name', 'like', '%' . $name . '%');
    }
    if ($display_name <> "")
    {
      $query = $query->where('permissions.display_name', 'like', '%' . $display_name . '%');
    }

    if ($role_id <> "")
    {
      $query = $query->where('roles.id', $role_id);
    }

    if ($module_id <> "")
    {
      $query = $query->where('modules.id', $module_id);
    }

    if(Auth::user()->hasRole('SUPERUSUARIO'))
    {
      $query = $query;
    }
    else
    {
      $query = $query->where('roles.id', auth()->user()->usuarioroles[0]->role_id);
    }

    $query = $query->where('roles.status_role_id', 1);
    $query = $query->where('modules.status_module_id', 1);
    $query = $query->where('permissions.generic_status_id', 1);
    $query = $query->where('permissions.name', 'not like', '%' . '_OCULTO' . '%');
    //$query = $query->where('roles.id','>', 1); //Omitiendo Superusuario
    $query = $query->orderBy('roles.name', 'ASC');
    $query = $query->orderBy('modules.name', 'ASC');
    $query = $query->orderBy('permissions.name', 'ASC');

    return $query;
  }

  protected $fillable = [
      'permission_id', 'role_id',
  ];
}
