<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;

class UserStatus extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;

  protected $table = 'user_status';
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  public function getCreatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function getUpdatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function userpermisos()
  {
    return $this->hasMany('App\Users', 'user_status_id', 'id');
  }

  public static function comboUserPermisos()
  {
    return UserStatus::select('id', 'name')
                    ->from('user_status')
                    ->where('id','>',0)
                    ->orderBy('name')
                    ->get();
  }

  public static function comboUserPermisosFiltrado()
  {
    return UserStatus::select('id', 'name')
                    ->from('user_status')
                    ->where('id','>',0)
                    ->whereIn('id',User::pluck('user_status_id'))
                    ->orderBy('name')
                    ->get();
  }

  public function scopeUserstatuslist($query, $name = null, $description = null)
  {
    if ($name <> "")
    {
      $name = strtoupper($name);
      $query = $query->where('name', 'like', '%' . $name . '%');
    }
    if ($description <> "")
    {
      $description = strtoupper($description);
      $query = $query->where('description', 'like', '%' . $description . '%');
    }

    $query = $query->orderBy('name', 'ASC');

    return $query;
  }

  protected $fillable = [
      'name', 'description',
  ];//
}
