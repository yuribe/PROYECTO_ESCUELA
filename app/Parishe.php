<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;


class Parishe extends Model implements Auditable
{

  use \OwenIt\Auditing\Auditable;

  protected $table = "parishes";

  public static function comboParishes()
  {
    return Parishe::select('id', 'name')
                    ->from('parishes')
                    ->where('id','>',0)
                    ->orderBy('name', 'asc')
                    ->get();
  }

  public static function listaParroquias($municipio)
  {
    return Parishe::select('id', 'name')
                    ->where('municipality_id', $municipio)
                    ->orderBy('name', 'asc')
                    ->get();
  }




  protected $fillable = [
      'name', 'municipality_id',
  ];
}
