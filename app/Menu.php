<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;

use App\Module;

class Menu extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;

  protected $table = 'menus';
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  public function getCreatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function getUpdatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function modulos()
  {
    return $this->BelongsTo('App\Module','module_menu_id','id');
  }

  public function padres()
  {
    return $this->BelongsTo('App\Menu','menu_parent','id');
  }

  public function genericos()
  {
    return $this->BelongsTo('App\GenericStatus','status_menu_id','id');
  }

  public static function comboMenus()
  {
    return Menu::select('id', 'display_name')
                    ->from('menus')
                    ->where('status_menu_id',1)
                    ->where('id','>',0)
                    ->orderBy('display_name', 'ASC')
                    ->get();
  }

  public static function comboMenusCrea()
  {
    return Menu::select('id', 'display_name')
                    ->from('menus')
                    ->where('status_menu_id',1)
                    ->where('id','>',0)
                    ->whereNotIn('id',Rolepermission::pluck('role_id'))
                    ->orderBy('display_name', 'ASC')
                    ->get();
  }


  public function scopeMenulist($query, $name = null, $display_name = null, $module_id = null, $status_menu_id = null)
  {
    //dd('prueba'.$name);
    if ($name <> "")
    {
      $query = $query->where('name', 'like', '%' . $name . '%');
    }
    if ($display_name <> "")
    {
      $query = $query->where('display_name', 'like', '%' . $display_name . '%');    }

    if ($module_id <> "")
    {
      $query = $query->where('module_menu_id', $module_id);    }

    if ($status_menu_id <> "")
    {
      $query = $query->where('status_menu_id', $status_menu_id);
    }
    $query = $query->where('status_menu_id', '!=', 0);
    $query = $query->where('id', '>', 0);

    if(Auth::user()->hasRole('SUPERUSUARIO'))
    {
      $query = $query->where('id', '>', 0);
    }
    else
    {
      $query = $query->where('id', '>', 100);
    }

    $query = $query->orderBy('name', 'ASC');

    return $query;
  }


  protected $fillable = [
      'name',
      'display_name',
      'description',
      'menu_parent',
      'menu_level',
      'menu_icon',
      'status_menu_id',
      'module_menu_id',
  ];
}
