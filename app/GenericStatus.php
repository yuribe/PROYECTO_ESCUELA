<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;

use App\GenericStatus;
use App\Role;
use App\Module;
use App\Permissions;

class GenericStatus extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;

  protected $table = "generic_status";
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  public function getCreatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public function getUpdatedAtAttribute($value)
  {
    return date('d-m-Y H:i:s', strtotime($value));
  }

  public static function comboGenerics()
  {
    return GenericStatus::select('id', 'name')
                    ->from('generic_status')
                    ->where('id','>',0)
                    ->orderBy('name')
                    ->get();
  }

  public static function comboGenericsRole()
  {
    return GenericStatus::select('id', 'name')
                    ->from('generic_status')
                    ->where('id','>',0)
                    ->whereIn('id',Role::pluck('status_role_id'))
                    ->orderBy('name')
                    ->get();
  }

  public static function comboGenericsMenu()
  {
    return GenericStatus::select('id', 'name')
                    ->from('generic_status')
                    ->where('id','>',0)
                    ->whereIn('id',Menu::pluck('status_menu_id'))
                    ->orderBy('name')
                    ->get();
  }

  public static function comboGenericsModule()
  {
    return GenericStatus::select('id', 'name')
                    ->from('generic_status')
                    ->where('id','>',0)
                    ->whereIn('id',Module::pluck('status_module_id'))
                    ->orderBy('name')
                    ->get();
  }

  public static function comboGenericsPermission()
  {
    return GenericStatus::select('id', 'name')
                    ->from('generic_status')
                    ->where('id','>',0)
                    ->whereIn('id',Permission::pluck('generic_status_id'))
                    ->orderBy('name')
                    ->get();
  }

  public function scopeGenericlist($query, $name = null, $description = null)
  {
    if ($name <> "")
    {
      $name = strtoupper($name);
      $query = $query->where('name', 'like', '%' . $name . '%');
    }
    if ($description <> "")
    {
      $description = strtoupper($description);
      $query = $query->where('description', 'like', '%' . $description . '%');
    }

    $query = $query->where('id', '>', 0);
    $query = $query->orderBy('name', 'ASC');

    return $query;
  }

  protected $fillable = [
      'name', 'description',
  ];
}
