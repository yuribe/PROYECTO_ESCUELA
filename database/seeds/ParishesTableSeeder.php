<?php

use Illuminate\Database\Seeder;

class ParishesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('parishes')->delete();
        
        \DB::table('parishes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Alto Orinoco',
                'municipality_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Huachamacare Acanaña',
                'municipality_id' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Marawaka Toky Shamanaña',
                'municipality_id' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Mavaka Mavaka',
                'municipality_id' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Sierra Parima Parimabé',
                'municipality_id' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Ucata Laja Lisa',
                'municipality_id' => 2,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Yapacana Macuruco',
                'municipality_id' => 2,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Caname Guarinuma',
                'municipality_id' => 2,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Fernando Girón Tovar',
                'municipality_id' => 3,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Luis Alberto Gómez',
                'municipality_id' => 3,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Pahueña Limón de Parhueña',
                'municipality_id' => 3,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Platanillal Platanillal',
                'municipality_id' => 3,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Samariapo',
                'municipality_id' => 4,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Sipapo',
                'municipality_id' => 4,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Munduapo',
                'municipality_id' => 4,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Guayapo',
                'municipality_id' => 4,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Alto Ventuari',
                'municipality_id' => 5,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Medio Ventuari',
                'municipality_id' => 5,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Bajo Ventuari',
                'municipality_id' => 5,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Victorino',
                'municipality_id' => 6,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Comunidad',
                'municipality_id' => 6,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Casiquiare',
                'municipality_id' => 7,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Cocuy',
                'municipality_id' => 7,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'San Carlos de Río Negro',
                'municipality_id' => 7,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Solano',
                'municipality_id' => 7,
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Anaco',
                'municipality_id' => 8,
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'San Joaquín',
                'municipality_id' => 8,
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Cachipo',
                'municipality_id' => 9,
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Aragua de Barcelona',
                'municipality_id' => 9,
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Lechería',
                'municipality_id' => 11,
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'El Morro',
                'municipality_id' => 11,
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Puerto Píritu',
                'municipality_id' => 12,
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'San Miguel',
                'municipality_id' => 12,
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Sucre',
                'municipality_id' => 12,
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Valle de Guanape',
                'municipality_id' => 13,
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Santa Bárbara',
                'municipality_id' => 13,
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'El Chaparro',
                'municipality_id' => 14,
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Tomás Alfaro',
                'municipality_id' => 14,
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Calatrava',
                'municipality_id' => 14,
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'Guanta',
                'municipality_id' => 15,
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Chorrerón',
                'municipality_id' => 15,
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Mamo',
                'municipality_id' => 16,
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Soledad',
                'municipality_id' => 16,
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Mapire',
                'municipality_id' => 17,
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'Piar',
                'municipality_id' => 17,
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'Santa Clara',
                'municipality_id' => 17,
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'San Diego de Cabrutica',
                'municipality_id' => 17,
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Uverito',
                'municipality_id' => 17,
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Zuata',
                'municipality_id' => 17,
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Puerto La Cruz',
                'municipality_id' => 18,
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Pozuelos',
                'municipality_id' => 18,
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Onoto',
                'municipality_id' => 19,
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'San Pablo',
                'municipality_id' => 19,
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'San Mateo',
                'municipality_id' => 20,
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'El Carito',
                'municipality_id' => 20,
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Santa Inés',
                'municipality_id' => 20,
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'La Romereña',
                'municipality_id' => 20,
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Atapirire',
                'municipality_id' => 21,
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Boca del Pao',
                'municipality_id' => 21,
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'El Pao',
                'municipality_id' => 21,
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Pariaguán',
                'municipality_id' => 21,
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'Cantaura',
                'municipality_id' => 22,
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Libertador',
                'municipality_id' => 22,
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Santa Rosa',
                'municipality_id' => 22,
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Urica',
                'municipality_id' => 22,
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'Píritu',
                'municipality_id' => 23,
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'San Francisco',
                'municipality_id' => 23,
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'San José de Guanipa',
                'municipality_id' => 24,
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Boca de Uchire',
                'municipality_id' => 25,
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Boca de Chávez',
                'municipality_id' => 25,
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Pueblo Nuevo',
                'municipality_id' => 26,
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'Santa Ana',
                'municipality_id' => 26,
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Bergantín',
                'municipality_id' => 27,
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Caigua',
                'municipality_id' => 27,
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'El Carmen',
                'municipality_id' => 27,
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'El Pilar',
                'municipality_id' => 27,
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'Naricual',
                'municipality_id' => 27,
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'San Crsitóbal',
                'municipality_id' => 27,
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'Edmundo Barrios',
                'municipality_id' => 28,
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Miguel Otero Silva',
                'municipality_id' => 28,
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Achaguas',
                'municipality_id' => 29,
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Apurito',
                'municipality_id' => 29,
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'El Yagual',
                'municipality_id' => 29,
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Guachara',
                'municipality_id' => 29,
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Mucuritas',
                'municipality_id' => 29,
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Queseras del medio',
                'municipality_id' => 29,
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Biruaca',
                'municipality_id' => 30,
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'Bruzual',
                'municipality_id' => 31,
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'Mantecal',
                'municipality_id' => 31,
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'Quintero',
                'municipality_id' => 31,
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Rincón Hondo',
                'municipality_id' => 31,
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'San Vicente',
                'municipality_id' => 31,
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'Guasdualito',
                'municipality_id' => 32,
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Aramendi',
                'municipality_id' => 32,
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'El Amparo',
                'municipality_id' => 32,
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'San Camilo',
                'municipality_id' => 32,
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'Urdaneta',
                'municipality_id' => 32,
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'San Juan de Payara',
                'municipality_id' => 33,
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Codazzi',
                'municipality_id' => 33,
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'Cunaviche',
                'municipality_id' => 33,
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'Elorza',
                'municipality_id' => 34,
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'La Trinidad',
                'municipality_id' => 34,
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'San Fernando',
                'municipality_id' => 35,
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'El Recreo',
                'municipality_id' => 35,
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'Peñalver',
                'municipality_id' => 35,
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'San Rafael de Atamaica',
                'municipality_id' => 35,
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Pedro José Ovalles',
                'municipality_id' => 36,
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'Joaquín Crespo',
                'municipality_id' => 36,
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'José Casanova Godoy',
                'municipality_id' => 36,
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'Madre María de San José',
                'municipality_id' => 36,
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Andrés Eloy Blanco',
                'municipality_id' => 36,
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Los Tacarigua',
                'municipality_id' => 36,
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'Las Delicias',
                'municipality_id' => 36,
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'Choroní',
                'municipality_id' => 36,
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'Bolívar',
                'municipality_id' => 37,
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'Camatagua',
                'municipality_id' => 38,
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'Carmen de Cura',
                'municipality_id' => 38,
            ),
            117 => 
            array (
                'id' => 118,
                'name' => 'Santa Rita',
                'municipality_id' => 39,
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'Francisco de Miranda',
                'municipality_id' => 39,
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'Moseñor Feliciano González',
                'municipality_id' => 39,
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'Santa Cruz',
                'municipality_id' => 40,
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'José Félix Ribas',
                'municipality_id' => 41,
            ),
            122 => 
            array (
                'id' => 123,
                'name' => 'Castor Nieves Ríos',
                'municipality_id' => 41,
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'Las Guacamayas',
                'municipality_id' => 41,
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'Pao de Zárate',
                'municipality_id' => 41,
            ),
            125 => 
            array (
                'id' => 126,
                'name' => 'Zuata',
                'municipality_id' => 41,
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'José Rafael Revenga',
                'municipality_id' => 42,
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'Palo Negro',
                'municipality_id' => 43,
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'San Martín de Porres',
                'municipality_id' => 43,
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'El Limón',
                'municipality_id' => 44,
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'Caña de Azúcar',
                'municipality_id' => 44,
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'Ocumare de la Costa',
                'municipality_id' => 45,
            ),
            132 => 
            array (
                'id' => 133,
                'name' => 'San Casimiro',
                'municipality_id' => 46,
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'Güiripa',
                'municipality_id' => 46,
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'Ollas de Caramacate',
                'municipality_id' => 46,
            ),
            135 => 
            array (
                'id' => 136,
                'name' => 'Valle Morín',
                'municipality_id' => 46,
            ),
            136 => 
            array (
                'id' => 137,
                'name' => 'San Sebastían',
                'municipality_id' => 47,
            ),
            137 => 
            array (
                'id' => 138,
                'name' => 'Turmero',
                'municipality_id' => 48,
            ),
            138 => 
            array (
                'id' => 139,
                'name' => 'Arevalo Aponte',
                'municipality_id' => 48,
            ),
            139 => 
            array (
                'id' => 140,
                'name' => 'Chuao',
                'municipality_id' => 48,
            ),
            140 => 
            array (
                'id' => 141,
                'name' => 'Samán de Güere',
                'municipality_id' => 48,
            ),
            141 => 
            array (
                'id' => 142,
                'name' => 'Alfredo Pacheco Miranda',
                'municipality_id' => 48,
            ),
            142 => 
            array (
                'id' => 143,
                'name' => 'Santos Michelena',
                'municipality_id' => 49,
            ),
            143 => 
            array (
                'id' => 144,
                'name' => 'Tiara',
                'municipality_id' => 49,
            ),
            144 => 
            array (
                'id' => 145,
                'name' => 'Cagua',
                'municipality_id' => 50,
            ),
            145 => 
            array (
                'id' => 146,
                'name' => 'Bella Vista',
                'municipality_id' => 50,
            ),
            146 => 
            array (
                'id' => 147,
                'name' => 'Tovar',
                'municipality_id' => 51,
            ),
            147 => 
            array (
                'id' => 148,
                'name' => 'Urdaneta',
                'municipality_id' => 52,
            ),
            148 => 
            array (
                'id' => 149,
                'name' => 'Las Peñitas',
                'municipality_id' => 52,
            ),
            149 => 
            array (
                'id' => 150,
                'name' => 'San Francisco de Cara',
                'municipality_id' => 52,
            ),
            150 => 
            array (
                'id' => 151,
                'name' => 'Taguay',
                'municipality_id' => 52,
            ),
            151 => 
            array (
                'id' => 152,
                'name' => 'Zamora',
                'municipality_id' => 53,
            ),
            152 => 
            array (
                'id' => 153,
                'name' => 'Magdaleno',
                'municipality_id' => 53,
            ),
            153 => 
            array (
                'id' => 154,
                'name' => 'San Francisco de Asís',
                'municipality_id' => 53,
            ),
            154 => 
            array (
                'id' => 155,
                'name' => 'Valles de Tucutunemo',
                'municipality_id' => 53,
            ),
            155 => 
            array (
                'id' => 156,
                'name' => 'Augusto Mijares',
                'municipality_id' => 53,
            ),
            156 => 
            array (
                'id' => 157,
                'name' => 'Sabaneta',
                'municipality_id' => 54,
            ),
            157 => 
            array (
                'id' => 158,
                'name' => 'Juan Antonio Rodríguez Domínguez',
                'municipality_id' => 54,
            ),
            158 => 
            array (
                'id' => 159,
                'name' => 'El Cantón',
                'municipality_id' => 55,
            ),
            159 => 
            array (
                'id' => 160,
                'name' => 'Santa Cruz de Guacas',
                'municipality_id' => 55,
            ),
            160 => 
            array (
                'id' => 161,
                'name' => 'Puerto Vivas',
                'municipality_id' => 55,
            ),
            161 => 
            array (
                'id' => 162,
                'name' => 'Ticoporo',
                'municipality_id' => 56,
            ),
            162 => 
            array (
                'id' => 163,
                'name' => 'Nicolás Pulido',
                'municipality_id' => 56,
            ),
            163 => 
            array (
                'id' => 164,
                'name' => 'Andrés Bello',
                'municipality_id' => 56,
            ),
            164 => 
            array (
                'id' => 165,
                'name' => 'Arismendi',
                'municipality_id' => 57,
            ),
            165 => 
            array (
                'id' => 166,
                'name' => 'Guadarrama',
                'municipality_id' => 57,
            ),
            166 => 
            array (
                'id' => 167,
                'name' => 'La Unión',
                'municipality_id' => 57,
            ),
            167 => 
            array (
                'id' => 168,
                'name' => 'San Antonio',
                'municipality_id' => 57,
            ),
            168 => 
            array (
                'id' => 169,
                'name' => 'Barinas',
                'municipality_id' => 58,
            ),
            169 => 
            array (
                'id' => 170,
                'name' => 'Alberto Arvelo Larriva',
                'municipality_id' => 58,
            ),
            170 => 
            array (
                'id' => 171,
                'name' => 'San Silvestre',
                'municipality_id' => 58,
            ),
            171 => 
            array (
                'id' => 172,
                'name' => 'Santa Inés',
                'municipality_id' => 58,
            ),
            172 => 
            array (
                'id' => 173,
                'name' => 'Santa Lucía',
                'municipality_id' => 58,
            ),
            173 => 
            array (
                'id' => 174,
                'name' => 'Torumos',
                'municipality_id' => 58,
            ),
            174 => 
            array (
                'id' => 175,
                'name' => 'El Carmen',
                'municipality_id' => 58,
            ),
            175 => 
            array (
                'id' => 176,
                'name' => 'Rómulo Betancourt',
                'municipality_id' => 58,
            ),
            176 => 
            array (
                'id' => 177,
                'name' => 'Corazón de Jesús',
                'municipality_id' => 58,
            ),
            177 => 
            array (
                'id' => 178,
                'name' => 'Ramón Ignacio Méndez',
                'municipality_id' => 58,
            ),
            178 => 
            array (
                'id' => 179,
                'name' => 'Alto Barinas',
                'municipality_id' => 58,
            ),
            179 => 
            array (
                'id' => 180,
                'name' => 'Manuel Palacio Fajardo',
                'municipality_id' => 58,
            ),
            180 => 
            array (
                'id' => 181,
                'name' => 'Juan Antonio Rodríguez Domínguez',
                'municipality_id' => 58,
            ),
            181 => 
            array (
                'id' => 182,
                'name' => 'Dominga Ortiz de Páez',
                'municipality_id' => 58,
            ),
            182 => 
            array (
                'id' => 183,
                'name' => 'Barinitas',
                'municipality_id' => 59,
            ),
            183 => 
            array (
                'id' => 184,
                'name' => 'Altamira de Cáceres',
                'municipality_id' => 59,
            ),
            184 => 
            array (
                'id' => 185,
                'name' => 'Calderas',
                'municipality_id' => 59,
            ),
            185 => 
            array (
                'id' => 186,
                'name' => 'Barrancas',
                'municipality_id' => 60,
            ),
            186 => 
            array (
                'id' => 187,
                'name' => 'El Socorro',
                'municipality_id' => 60,
            ),
            187 => 
            array (
                'id' => 188,
                'name' => 'Mazparrito',
                'municipality_id' => 60,
            ),
            188 => 
            array (
                'id' => 189,
                'name' => 'Santa Bárbara',
                'municipality_id' => 61,
            ),
            189 => 
            array (
                'id' => 190,
                'name' => 'Pedro Briceño Méndez',
                'municipality_id' => 61,
            ),
            190 => 
            array (
                'id' => 191,
                'name' => 'Ramón Ignacio Méndez',
                'municipality_id' => 61,
            ),
            191 => 
            array (
                'id' => 192,
                'name' => 'José Ignacio del Pumar',
                'municipality_id' => 61,
            ),
            192 => 
            array (
                'id' => 193,
                'name' => 'Obispos',
                'municipality_id' => 62,
            ),
            193 => 
            array (
                'id' => 194,
                'name' => 'Guasimitos',
                'municipality_id' => 62,
            ),
            194 => 
            array (
                'id' => 195,
                'name' => 'El Real',
                'municipality_id' => 62,
            ),
            195 => 
            array (
                'id' => 196,
                'name' => 'La Luz',
                'municipality_id' => 62,
            ),
            196 => 
            array (
                'id' => 197,
                'name' => 'Ciudad Bolívia',
                'municipality_id' => 63,
            ),
            197 => 
            array (
                'id' => 198,
                'name' => 'José Ignacio Briceño',
                'municipality_id' => 63,
            ),
            198 => 
            array (
                'id' => 199,
                'name' => 'José Félix Ribas',
                'municipality_id' => 63,
            ),
            199 => 
            array (
                'id' => 200,
                'name' => 'Páez',
                'municipality_id' => 63,
            ),
            200 => 
            array (
                'id' => 201,
                'name' => 'Libertad',
                'municipality_id' => 64,
            ),
            201 => 
            array (
                'id' => 202,
                'name' => 'Dolores',
                'municipality_id' => 64,
            ),
            202 => 
            array (
                'id' => 203,
                'name' => 'Santa Rosa',
                'municipality_id' => 64,
            ),
            203 => 
            array (
                'id' => 204,
                'name' => 'Palacio Fajardo',
                'municipality_id' => 64,
            ),
            204 => 
            array (
                'id' => 205,
                'name' => 'Ciudad de Nutrias',
                'municipality_id' => 65,
            ),
            205 => 
            array (
                'id' => 206,
                'name' => 'El Regalo',
                'municipality_id' => 65,
            ),
            206 => 
            array (
                'id' => 207,
                'name' => 'Puerto Nutrias',
                'municipality_id' => 65,
            ),
            207 => 
            array (
                'id' => 208,
                'name' => 'Santa Catalina',
                'municipality_id' => 65,
            ),
            208 => 
            array (
                'id' => 209,
                'name' => 'Cachamay',
                'municipality_id' => 66,
            ),
            209 => 
            array (
                'id' => 210,
                'name' => 'Chirica',
                'municipality_id' => 66,
            ),
            210 => 
            array (
                'id' => 211,
                'name' => 'Dalla Costa',
                'municipality_id' => 66,
            ),
            211 => 
            array (
                'id' => 212,
                'name' => 'Once de Abril',
                'municipality_id' => 66,
            ),
            212 => 
            array (
                'id' => 213,
                'name' => 'Simón Bolívar',
                'municipality_id' => 66,
            ),
            213 => 
            array (
                'id' => 214,
                'name' => 'Unare',
                'municipality_id' => 66,
            ),
            214 => 
            array (
                'id' => 215,
                'name' => 'Universidad',
                'municipality_id' => 66,
            ),
            215 => 
            array (
                'id' => 216,
                'name' => 'Vista al Sol',
                'municipality_id' => 66,
            ),
            216 => 
            array (
                'id' => 217,
                'name' => 'Pozo Verde',
                'municipality_id' => 66,
            ),
            217 => 
            array (
                'id' => 218,
                'name' => 'Yocoima',
                'municipality_id' => 66,
            ),
            218 => 
            array (
                'id' => 219,
                'name' => '5 de Julio',
                'municipality_id' => 66,
            ),
            219 => 
            array (
                'id' => 220,
                'name' => 'Cedeño',
                'municipality_id' => 67,
            ),
            220 => 
            array (
                'id' => 221,
                'name' => 'Altagracia',
                'municipality_id' => 67,
            ),
            221 => 
            array (
                'id' => 222,
                'name' => 'Ascensión Farreras',
                'municipality_id' => 67,
            ),
            222 => 
            array (
                'id' => 223,
                'name' => 'Guaniamo',
                'municipality_id' => 67,
            ),
            223 => 
            array (
                'id' => 224,
                'name' => 'La Urbana',
                'municipality_id' => 67,
            ),
            224 => 
            array (
                'id' => 225,
                'name' => 'Pijiguaos',
                'municipality_id' => 67,
            ),
            225 => 
            array (
                'id' => 226,
                'name' => 'El Callao',
                'municipality_id' => 68,
            ),
            226 => 
            array (
                'id' => 227,
                'name' => 'Gran Sabana',
                'municipality_id' => 69,
            ),
            227 => 
            array (
                'id' => 228,
                'name' => 'Ikabarú',
                'municipality_id' => 69,
            ),
            228 => 
            array (
                'id' => 229,
                'name' => 'Catedral',
                'municipality_id' => 70,
            ),
            229 => 
            array (
                'id' => 230,
                'name' => 'Zea',
                'municipality_id' => 70,
            ),
            230 => 
            array (
                'id' => 231,
                'name' => 'Orinoco',
                'municipality_id' => 70,
            ),
            231 => 
            array (
                'id' => 232,
                'name' => 'José Antonio Páez',
                'municipality_id' => 70,
            ),
            232 => 
            array (
                'id' => 233,
                'name' => 'Marhuanta',
                'municipality_id' => 70,
            ),
            233 => 
            array (
                'id' => 234,
                'name' => 'Agua Salada',
                'municipality_id' => 70,
            ),
            234 => 
            array (
                'id' => 235,
                'name' => 'Vista Hermosa',
                'municipality_id' => 70,
            ),
            235 => 
            array (
                'id' => 236,
                'name' => 'La Sabanita',
                'municipality_id' => 70,
            ),
            236 => 
            array (
                'id' => 237,
                'name' => 'Panapana',
                'municipality_id' => 70,
            ),
            237 => 
            array (
                'id' => 238,
                'name' => 'Andrés Eloy Blanco',
                'municipality_id' => 71,
            ),
            238 => 
            array (
                'id' => 239,
                'name' => 'Pedro Cova',
                'municipality_id' => 71,
            ),
            239 => 
            array (
                'id' => 240,
                'name' => 'Raúl Leoni',
                'municipality_id' => 72,
            ),
            240 => 
            array (
                'id' => 241,
                'name' => 'Barceloneta',
                'municipality_id' => 72,
            ),
            241 => 
            array (
                'id' => 242,
                'name' => 'Santa Bárbara',
                'municipality_id' => 72,
            ),
            242 => 
            array (
                'id' => 243,
                'name' => 'San Francisco',
                'municipality_id' => 72,
            ),
            243 => 
            array (
                'id' => 244,
                'name' => 'Roscio',
                'municipality_id' => 73,
            ),
            244 => 
            array (
                'id' => 245,
                'name' => 'Salóm',
                'municipality_id' => 73,
            ),
            245 => 
            array (
                'id' => 246,
                'name' => 'Sifontes',
                'municipality_id' => 74,
            ),
            246 => 
            array (
                'id' => 247,
                'name' => 'Dalla Costa',
                'municipality_id' => 74,
            ),
            247 => 
            array (
                'id' => 248,
                'name' => 'San Isidro',
                'municipality_id' => 74,
            ),
            248 => 
            array (
                'id' => 249,
                'name' => 'Sucre',
                'municipality_id' => 75,
            ),
            249 => 
            array (
                'id' => 250,
                'name' => 'Aripao',
                'municipality_id' => 75,
            ),
            250 => 
            array (
                'id' => 251,
                'name' => 'Guarataro',
                'municipality_id' => 75,
            ),
            251 => 
            array (
                'id' => 252,
                'name' => 'Las Majadas',
                'municipality_id' => 75,
            ),
            252 => 
            array (
                'id' => 253,
                'name' => 'Moitaco',
                'municipality_id' => 75,
            ),
            253 => 
            array (
                'id' => 254,
                'name' => 'Padre Pedro Chien',
                'municipality_id' => 76,
            ),
            254 => 
            array (
                'id' => 255,
                'name' => 'Río Grande',
                'municipality_id' => 76,
            ),
            255 => 
            array (
                'id' => 256,
                'name' => 'Bejuma',
                'municipality_id' => 77,
            ),
            256 => 
            array (
                'id' => 257,
                'name' => 'Canoabo',
                'municipality_id' => 77,
            ),
            257 => 
            array (
                'id' => 258,
                'name' => 'Simón Bolívar',
                'municipality_id' => 77,
            ),
            258 => 
            array (
                'id' => 259,
                'name' => 'Güigüe',
                'municipality_id' => 78,
            ),
            259 => 
            array (
                'id' => 260,
                'name' => 'Carabobo',
                'municipality_id' => 78,
            ),
            260 => 
            array (
                'id' => 261,
                'name' => 'Tacarigua',
                'municipality_id' => 78,
            ),
            261 => 
            array (
                'id' => 262,
                'name' => 'Mariara',
                'municipality_id' => 79,
            ),
            262 => 
            array (
                'id' => 263,
                'name' => 'Aguas Calientes',
                'municipality_id' => 79,
            ),
            263 => 
            array (
                'id' => 264,
                'name' => 'Ciudad Alianza',
                'municipality_id' => 80,
            ),
            264 => 
            array (
                'id' => 265,
                'name' => 'Guacara',
                'municipality_id' => 80,
            ),
            265 => 
            array (
                'id' => 266,
                'name' => 'Yagua',
                'municipality_id' => 80,
            ),
            266 => 
            array (
                'id' => 267,
                'name' => 'Morón',
                'municipality_id' => 81,
            ),
            267 => 
            array (
                'id' => 268,
                'name' => 'Yagua',
                'municipality_id' => 81,
            ),
            268 => 
            array (
                'id' => 269,
                'name' => 'Tocuyito',
                'municipality_id' => 82,
            ),
            269 => 
            array (
                'id' => 270,
                'name' => 'Independencia',
                'municipality_id' => 82,
            ),
            270 => 
            array (
                'id' => 271,
                'name' => 'Los Guayos',
                'municipality_id' => 83,
            ),
            271 => 
            array (
                'id' => 272,
                'name' => 'Miranda',
                'municipality_id' => 84,
            ),
            272 => 
            array (
                'id' => 273,
                'name' => 'Montalbán',
                'municipality_id' => 85,
            ),
            273 => 
            array (
                'id' => 274,
                'name' => 'Naguanagua',
                'municipality_id' => 86,
            ),
            274 => 
            array (
                'id' => 275,
                'name' => 'Bartolomé Salóm',
                'municipality_id' => 87,
            ),
            275 => 
            array (
                'id' => 276,
                'name' => 'Democracia',
                'municipality_id' => 87,
            ),
            276 => 
            array (
                'id' => 277,
                'name' => 'Fraternidad',
                'municipality_id' => 87,
            ),
            277 => 
            array (
                'id' => 278,
                'name' => 'Goaigoaza',
                'municipality_id' => 87,
            ),
            278 => 
            array (
                'id' => 279,
                'name' => 'Juan José Flores',
                'municipality_id' => 87,
            ),
            279 => 
            array (
                'id' => 280,
                'name' => 'Unión',
                'municipality_id' => 87,
            ),
            280 => 
            array (
                'id' => 281,
                'name' => 'Borburata',
                'municipality_id' => 87,
            ),
            281 => 
            array (
                'id' => 282,
                'name' => 'Patanemo',
                'municipality_id' => 87,
            ),
            282 => 
            array (
                'id' => 283,
                'name' => 'San Diego',
                'municipality_id' => 88,
            ),
            283 => 
            array (
                'id' => 284,
                'name' => 'San Joaquín',
                'municipality_id' => 89,
            ),
            284 => 
            array (
                'id' => 285,
                'name' => 'Candelaria',
                'municipality_id' => 90,
            ),
            285 => 
            array (
                'id' => 286,
                'name' => 'Catedral',
                'municipality_id' => 90,
            ),
            286 => 
            array (
                'id' => 287,
                'name' => 'El Socorro',
                'municipality_id' => 90,
            ),
            287 => 
            array (
                'id' => 288,
                'name' => 'Miguel Peña',
                'municipality_id' => 90,
            ),
            288 => 
            array (
                'id' => 289,
                'name' => 'Rafael Urdaneta',
                'municipality_id' => 90,
            ),
            289 => 
            array (
                'id' => 290,
                'name' => 'San Blas',
                'municipality_id' => 90,
            ),
            290 => 
            array (
                'id' => 291,
                'name' => 'San José',
                'municipality_id' => 90,
            ),
            291 => 
            array (
                'id' => 292,
                'name' => 'Santa Rosa',
                'municipality_id' => 90,
            ),
            292 => 
            array (
                'id' => 293,
                'name' => 'Negro Primero',
                'municipality_id' => 90,
            ),
            293 => 
            array (
                'id' => 294,
                'name' => 'Cojedes',
                'municipality_id' => 91,
            ),
            294 => 
            array (
                'id' => 295,
                'name' => 'Juan de Mata Suárez',
                'municipality_id' => 91,
            ),
            295 => 
            array (
                'id' => 296,
                'name' => 'Tinaquillo',
                'municipality_id' => 92,
            ),
            296 => 
            array (
                'id' => 297,
                'name' => 'El Baúl',
                'municipality_id' => 93,
            ),
            297 => 
            array (
                'id' => 298,
                'name' => 'Sucre',
                'municipality_id' => 93,
            ),
            298 => 
            array (
                'id' => 299,
                'name' => 'La Aguadita',
                'municipality_id' => 94,
            ),
            299 => 
            array (
                'id' => 300,
                'name' => 'Macapo',
                'municipality_id' => 94,
            ),
            300 => 
            array (
                'id' => 301,
                'name' => 'El Pao',
                'municipality_id' => 95,
            ),
            301 => 
            array (
                'id' => 302,
                'name' => 'El Amparo',
                'municipality_id' => 96,
            ),
            302 => 
            array (
                'id' => 303,
                'name' => 'Libertad de Cojedes',
                'municipality_id' => 96,
            ),
            303 => 
            array (
                'id' => 304,
                'name' => 'Rómulo Gallegos',
                'municipality_id' => 97,
            ),
            304 => 
            array (
                'id' => 305,
                'name' => 'San Carlos de Austria',
                'municipality_id' => 98,
            ),
            305 => 
            array (
                'id' => 306,
                'name' => 'Juan Ángel Bravo',
                'municipality_id' => 98,
            ),
            306 => 
            array (
                'id' => 307,
                'name' => 'Manuel Manrique',
                'municipality_id' => 98,
            ),
            307 => 
            array (
                'id' => 308,
                'name' => 'General en Jefe José Laurencio Silva',
                'municipality_id' => 99,
            ),
            308 => 
            array (
                'id' => 309,
                'name' => 'Curiapo',
                'municipality_id' => 100,
            ),
            309 => 
            array (
                'id' => 310,
                'name' => 'Almirante Luis Brión',
                'municipality_id' => 100,
            ),
            310 => 
            array (
                'id' => 311,
                'name' => 'Francisco Aniceto Lugo',
                'municipality_id' => 100,
            ),
            311 => 
            array (
                'id' => 312,
                'name' => 'Manuel Renaud',
                'municipality_id' => 100,
            ),
            312 => 
            array (
                'id' => 313,
                'name' => 'Padre Barral',
                'municipality_id' => 100,
            ),
            313 => 
            array (
                'id' => 314,
                'name' => 'Santos de Abelgas',
                'municipality_id' => 100,
            ),
            314 => 
            array (
                'id' => 315,
                'name' => 'Imataca',
                'municipality_id' => 101,
            ),
            315 => 
            array (
                'id' => 316,
                'name' => 'Cinco de Julio',
                'municipality_id' => 101,
            ),
            316 => 
            array (
                'id' => 317,
                'name' => 'Juan Bautista Arismendi',
                'municipality_id' => 101,
            ),
            317 => 
            array (
                'id' => 318,
                'name' => 'Manuel Piar',
                'municipality_id' => 101,
            ),
            318 => 
            array (
                'id' => 319,
                'name' => 'Rómulo Gallegos',
                'municipality_id' => 101,
            ),
            319 => 
            array (
                'id' => 320,
                'name' => 'Pedernales',
                'municipality_id' => 102,
            ),
            320 => 
            array (
                'id' => 321,
                'name' => 'Luis Beltrán Prieto Figueroa',
                'municipality_id' => 102,
            ),
            321 => 
            array (
                'id' => 322,
            'name' => 'San José (Delta Amacuro)',
                'municipality_id' => 103,
            ),
            322 => 
            array (
                'id' => 323,
                'name' => 'José Vidal Marcano',
                'municipality_id' => 103,
            ),
            323 => 
            array (
                'id' => 324,
                'name' => 'Juan Millán',
                'municipality_id' => 103,
            ),
            324 => 
            array (
                'id' => 325,
                'name' => 'Leonardo Ruíz Pineda',
                'municipality_id' => 103,
            ),
            325 => 
            array (
                'id' => 326,
                'name' => 'Mariscal Antonio José de Sucre',
                'municipality_id' => 103,
            ),
            326 => 
            array (
                'id' => 327,
                'name' => 'Monseñor Argimiro García',
                'municipality_id' => 103,
            ),
            327 => 
            array (
                'id' => 328,
            'name' => 'San Rafael (Delta Amacuro)',
                'municipality_id' => 103,
            ),
            328 => 
            array (
                'id' => 329,
                'name' => 'Virgen del Valle',
                'municipality_id' => 103,
            ),
            329 => 
            array (
                'id' => 330,
                'name' => 'Clarines',
                'municipality_id' => 10,
            ),
            330 => 
            array (
                'id' => 331,
                'name' => 'Guanape',
                'municipality_id' => 10,
            ),
            331 => 
            array (
                'id' => 332,
                'name' => 'Sabana de Uchire',
                'municipality_id' => 10,
            ),
            332 => 
            array (
                'id' => 333,
                'name' => 'Capadare',
                'municipality_id' => 104,
            ),
            333 => 
            array (
                'id' => 334,
                'name' => 'La Pastora',
                'municipality_id' => 104,
            ),
            334 => 
            array (
                'id' => 335,
                'name' => 'Libertador',
                'municipality_id' => 104,
            ),
            335 => 
            array (
                'id' => 336,
                'name' => 'San Juan de los Cayos',
                'municipality_id' => 104,
            ),
            336 => 
            array (
                'id' => 337,
                'name' => 'Aracua',
                'municipality_id' => 105,
            ),
            337 => 
            array (
                'id' => 338,
                'name' => 'La Peña',
                'municipality_id' => 105,
            ),
            338 => 
            array (
                'id' => 339,
                'name' => 'San Luis',
                'municipality_id' => 105,
            ),
            339 => 
            array (
                'id' => 340,
                'name' => 'Bariro',
                'municipality_id' => 106,
            ),
            340 => 
            array (
                'id' => 341,
                'name' => 'Borojó',
                'municipality_id' => 106,
            ),
            341 => 
            array (
                'id' => 342,
                'name' => 'Capatárida',
                'municipality_id' => 106,
            ),
            342 => 
            array (
                'id' => 343,
                'name' => 'Guajiro',
                'municipality_id' => 106,
            ),
            343 => 
            array (
                'id' => 344,
                'name' => 'Seque',
                'municipality_id' => 106,
            ),
            344 => 
            array (
                'id' => 345,
                'name' => 'Zazárida',
                'municipality_id' => 106,
            ),
            345 => 
            array (
                'id' => 346,
                'name' => 'Valle de Eroa',
                'municipality_id' => 106,
            ),
            346 => 
            array (
                'id' => 347,
                'name' => 'Cacique Manaure',
                'municipality_id' => 107,
            ),
            347 => 
            array (
                'id' => 348,
                'name' => 'Norte',
                'municipality_id' => 108,
            ),
            348 => 
            array (
                'id' => 349,
                'name' => 'Carirubana',
                'municipality_id' => 108,
            ),
            349 => 
            array (
                'id' => 350,
                'name' => 'Santa Ana',
                'municipality_id' => 108,
            ),
            350 => 
            array (
                'id' => 351,
                'name' => 'Urbana Punta Cardón',
                'municipality_id' => 108,
            ),
            351 => 
            array (
                'id' => 352,
                'name' => 'La Vela de Coro',
                'municipality_id' => 109,
            ),
            352 => 
            array (
                'id' => 353,
                'name' => 'Acurigua',
                'municipality_id' => 109,
            ),
            353 => 
            array (
                'id' => 354,
                'name' => 'Guaibacoa',
                'municipality_id' => 109,
            ),
            354 => 
            array (
                'id' => 355,
                'name' => 'Las Calderas',
                'municipality_id' => 109,
            ),
            355 => 
            array (
                'id' => 356,
                'name' => 'Macoruca',
                'municipality_id' => 109,
            ),
            356 => 
            array (
                'id' => 357,
                'name' => 'Dabajuro',
                'municipality_id' => 110,
            ),
            357 => 
            array (
                'id' => 358,
                'name' => 'Agua Clara',
                'municipality_id' => 111,
            ),
            358 => 
            array (
                'id' => 359,
                'name' => 'Avaria',
                'municipality_id' => 111,
            ),
            359 => 
            array (
                'id' => 360,
                'name' => 'Pedregal',
                'municipality_id' => 111,
            ),
            360 => 
            array (
                'id' => 361,
                'name' => 'Piedra Grande',
                'municipality_id' => 111,
            ),
            361 => 
            array (
                'id' => 362,
                'name' => 'Purureche',
                'municipality_id' => 111,
            ),
            362 => 
            array (
                'id' => 363,
                'name' => 'Adaure',
                'municipality_id' => 112,
            ),
            363 => 
            array (
                'id' => 364,
                'name' => 'Adícora',
                'municipality_id' => 112,
            ),
            364 => 
            array (
                'id' => 365,
                'name' => 'Baraived',
                'municipality_id' => 112,
            ),
            365 => 
            array (
                'id' => 366,
                'name' => 'Buena Vista',
                'municipality_id' => 112,
            ),
            366 => 
            array (
                'id' => 367,
                'name' => 'Jadacaquiva',
                'municipality_id' => 112,
            ),
            367 => 
            array (
                'id' => 368,
                'name' => 'El Vínculo',
                'municipality_id' => 112,
            ),
            368 => 
            array (
                'id' => 369,
                'name' => 'El Hato',
                'municipality_id' => 112,
            ),
            369 => 
            array (
                'id' => 370,
                'name' => 'Moruy',
                'municipality_id' => 112,
            ),
            370 => 
            array (
                'id' => 371,
                'name' => 'Pueblo Nuevo',
                'municipality_id' => 112,
            ),
            371 => 
            array (
                'id' => 372,
                'name' => 'Agua Larga',
                'municipality_id' => 113,
            ),
            372 => 
            array (
                'id' => 373,
                'name' => 'El Paují',
                'municipality_id' => 113,
            ),
            373 => 
            array (
                'id' => 374,
                'name' => 'Independencia',
                'municipality_id' => 113,
            ),
            374 => 
            array (
                'id' => 375,
                'name' => 'Mapararí',
                'municipality_id' => 113,
            ),
            375 => 
            array (
                'id' => 376,
                'name' => 'Agua Linda',
                'municipality_id' => 114,
            ),
            376 => 
            array (
                'id' => 377,
                'name' => 'Araurima',
                'municipality_id' => 114,
            ),
            377 => 
            array (
                'id' => 378,
                'name' => 'Jacura',
                'municipality_id' => 114,
            ),
            378 => 
            array (
                'id' => 379,
                'name' => 'Tucacas',
                'municipality_id' => 115,
            ),
            379 => 
            array (
                'id' => 380,
                'name' => 'Boca de Aroa',
                'municipality_id' => 115,
            ),
            380 => 
            array (
                'id' => 381,
                'name' => 'Los Taques',
                'municipality_id' => 116,
            ),
            381 => 
            array (
                'id' => 382,
                'name' => 'Judibana',
                'municipality_id' => 116,
            ),
            382 => 
            array (
                'id' => 383,
                'name' => 'Mene de Mauroa',
                'municipality_id' => 117,
            ),
            383 => 
            array (
                'id' => 384,
                'name' => 'San Félix',
                'municipality_id' => 117,
            ),
            384 => 
            array (
                'id' => 385,
                'name' => 'Casigua',
                'municipality_id' => 117,
            ),
            385 => 
            array (
                'id' => 386,
                'name' => 'Guzmán Guillermo',
                'municipality_id' => 118,
            ),
            386 => 
            array (
                'id' => 387,
                'name' => 'Mitare',
                'municipality_id' => 118,
            ),
            387 => 
            array (
                'id' => 388,
                'name' => 'Río Seco',
                'municipality_id' => 118,
            ),
            388 => 
            array (
                'id' => 389,
                'name' => 'Sabaneta',
                'municipality_id' => 118,
            ),
            389 => 
            array (
                'id' => 390,
                'name' => 'San Antonio',
                'municipality_id' => 118,
            ),
            390 => 
            array (
                'id' => 391,
                'name' => 'San Gabriel',
                'municipality_id' => 118,
            ),
            391 => 
            array (
                'id' => 392,
                'name' => 'Santa Ana',
                'municipality_id' => 118,
            ),
            392 => 
            array (
                'id' => 393,
                'name' => 'Boca del Tocuyo',
                'municipality_id' => 119,
            ),
            393 => 
            array (
                'id' => 394,
                'name' => 'Chichiriviche',
                'municipality_id' => 119,
            ),
            394 => 
            array (
                'id' => 395,
                'name' => 'Tocuyo de la Costa',
                'municipality_id' => 119,
            ),
            395 => 
            array (
                'id' => 396,
                'name' => 'Palmasola',
                'municipality_id' => 120,
            ),
            396 => 
            array (
                'id' => 397,
                'name' => 'Cabure',
                'municipality_id' => 121,
            ),
            397 => 
            array (
                'id' => 398,
                'name' => 'Colina',
                'municipality_id' => 121,
            ),
            398 => 
            array (
                'id' => 399,
                'name' => 'Curimagua',
                'municipality_id' => 121,
            ),
            399 => 
            array (
                'id' => 400,
                'name' => 'San José de la Costa',
                'municipality_id' => 122,
            ),
            400 => 
            array (
                'id' => 401,
                'name' => 'Píritu',
                'municipality_id' => 122,
            ),
            401 => 
            array (
                'id' => 402,
                'name' => 'San Francisco',
                'municipality_id' => 123,
            ),
            402 => 
            array (
                'id' => 403,
                'name' => 'Sucre',
                'municipality_id' => 124,
            ),
            403 => 
            array (
                'id' => 404,
                'name' => 'Pecaya',
                'municipality_id' => 124,
            ),
            404 => 
            array (
                'id' => 405,
                'name' => 'Tocópero',
                'municipality_id' => 125,
            ),
            405 => 
            array (
                'id' => 406,
                'name' => 'El Charal',
                'municipality_id' => 126,
            ),
            406 => 
            array (
                'id' => 407,
                'name' => 'Las Vegas del Tuy',
                'municipality_id' => 126,
            ),
            407 => 
            array (
                'id' => 408,
                'name' => 'Santa Cruz de Bucaral',
                'municipality_id' => 126,
            ),
            408 => 
            array (
                'id' => 409,
                'name' => 'Bruzual',
                'municipality_id' => 127,
            ),
            409 => 
            array (
                'id' => 410,
                'name' => 'Urumaco',
                'municipality_id' => 127,
            ),
            410 => 
            array (
                'id' => 411,
                'name' => 'Puerto Cumarebo',
                'municipality_id' => 128,
            ),
            411 => 
            array (
                'id' => 412,
                'name' => 'La Ciénaga',
                'municipality_id' => 128,
            ),
            412 => 
            array (
                'id' => 413,
                'name' => 'La Soledad',
                'municipality_id' => 128,
            ),
            413 => 
            array (
                'id' => 414,
                'name' => 'Pueblo Cumarebo',
                'municipality_id' => 128,
            ),
            414 => 
            array (
                'id' => 415,
                'name' => 'Zazárida',
                'municipality_id' => 128,
            ),
            415 => 
            array (
                'id' => 416,
                'name' => 'Churuguara',
                'municipality_id' => 113,
            ),
            416 => 
            array (
                'id' => 417,
                'name' => 'Camaguán',
                'municipality_id' => 129,
            ),
            417 => 
            array (
                'id' => 418,
                'name' => 'Puerto Miranda',
                'municipality_id' => 129,
            ),
            418 => 
            array (
                'id' => 419,
                'name' => 'Uverito',
                'municipality_id' => 129,
            ),
            419 => 
            array (
                'id' => 420,
                'name' => 'Chaguaramas',
                'municipality_id' => 130,
            ),
            420 => 
            array (
                'id' => 421,
                'name' => 'El Socorro',
                'municipality_id' => 131,
            ),
            421 => 
            array (
                'id' => 422,
                'name' => 'Tucupido',
                'municipality_id' => 132,
            ),
            422 => 
            array (
                'id' => 423,
                'name' => 'San Rafael de Laya',
                'municipality_id' => 132,
            ),
            423 => 
            array (
                'id' => 424,
                'name' => 'Altagracia de Orituco',
                'municipality_id' => 133,
            ),
            424 => 
            array (
                'id' => 425,
                'name' => 'San Rafael de Orituco',
                'municipality_id' => 133,
            ),
            425 => 
            array (
                'id' => 426,
                'name' => 'San Francisco Javier de Lezama',
                'municipality_id' => 133,
            ),
            426 => 
            array (
                'id' => 427,
                'name' => 'Paso Real de Macaira',
                'municipality_id' => 133,
            ),
            427 => 
            array (
                'id' => 428,
                'name' => 'Carlos Soublette',
                'municipality_id' => 133,
            ),
            428 => 
            array (
                'id' => 429,
                'name' => 'San Francisco de Macaira',
                'municipality_id' => 133,
            ),
            429 => 
            array (
                'id' => 430,
                'name' => 'Libertad de Orituco',
                'municipality_id' => 133,
            ),
            430 => 
            array (
                'id' => 431,
                'name' => 'Cantaclaro',
                'municipality_id' => 134,
            ),
            431 => 
            array (
                'id' => 432,
                'name' => 'San Juan de los Morros',
                'municipality_id' => 134,
            ),
            432 => 
            array (
                'id' => 433,
                'name' => 'Parapara',
                'municipality_id' => 134,
            ),
            433 => 
            array (
                'id' => 434,
                'name' => 'El Sombrero',
                'municipality_id' => 135,
            ),
            434 => 
            array (
                'id' => 435,
                'name' => 'Sosa',
                'municipality_id' => 135,
            ),
            435 => 
            array (
                'id' => 436,
                'name' => 'Las Mercedes',
                'municipality_id' => 136,
            ),
            436 => 
            array (
                'id' => 437,
                'name' => 'Cabruta',
                'municipality_id' => 136,
            ),
            437 => 
            array (
                'id' => 438,
                'name' => 'Santa Rita de Manapire',
                'municipality_id' => 136,
            ),
            438 => 
            array (
                'id' => 439,
                'name' => 'Valle de la Pascua',
                'municipality_id' => 137,
            ),
            439 => 
            array (
                'id' => 440,
                'name' => 'Espino',
                'municipality_id' => 137,
            ),
            440 => 
            array (
                'id' => 441,
                'name' => 'San José de Unare',
                'municipality_id' => 138,
            ),
            441 => 
            array (
                'id' => 442,
                'name' => 'Zaraza',
                'municipality_id' => 138,
            ),
            442 => 
            array (
                'id' => 443,
                'name' => 'San José de Tiznados',
                'municipality_id' => 139,
            ),
            443 => 
            array (
                'id' => 444,
                'name' => 'San Francisco de Tiznados',
                'municipality_id' => 139,
            ),
            444 => 
            array (
                'id' => 445,
                'name' => 'San Lorenzo de Tiznados',
                'municipality_id' => 139,
            ),
            445 => 
            array (
                'id' => 446,
                'name' => 'Ortiz',
                'municipality_id' => 139,
            ),
            446 => 
            array (
                'id' => 447,
                'name' => 'Guayabal',
                'municipality_id' => 140,
            ),
            447 => 
            array (
                'id' => 448,
                'name' => 'Cazorla',
                'municipality_id' => 140,
            ),
            448 => 
            array (
                'id' => 449,
                'name' => 'San José de Guaribe',
                'municipality_id' => 141,
            ),
            449 => 
            array (
                'id' => 450,
                'name' => 'Uveral',
                'municipality_id' => 141,
            ),
            450 => 
            array (
                'id' => 451,
                'name' => 'Santa María de Ipire',
                'municipality_id' => 142,
            ),
            451 => 
            array (
                'id' => 452,
                'name' => 'Altamira',
                'municipality_id' => 142,
            ),
            452 => 
            array (
                'id' => 453,
                'name' => 'El Calvario',
                'municipality_id' => 143,
            ),
            453 => 
            array (
                'id' => 454,
                'name' => 'El Rastro',
                'municipality_id' => 143,
            ),
            454 => 
            array (
                'id' => 455,
                'name' => 'Guardatinajas',
                'municipality_id' => 143,
            ),
            455 => 
            array (
                'id' => 456,
                'name' => 'Capital Urbana Calabozo',
                'municipality_id' => 143,
            ),
            456 => 
            array (
                'id' => 457,
                'name' => 'Quebrada Honda de Guache',
                'municipality_id' => 144,
            ),
            457 => 
            array (
                'id' => 458,
                'name' => 'Pío Tamayo',
                'municipality_id' => 144,
            ),
            458 => 
            array (
                'id' => 459,
                'name' => 'Yacambú',
                'municipality_id' => 144,
            ),
            459 => 
            array (
                'id' => 460,
                'name' => 'Fréitez',
                'municipality_id' => 145,
            ),
            460 => 
            array (
                'id' => 461,
                'name' => 'José María Blanco',
                'municipality_id' => 145,
            ),
            461 => 
            array (
                'id' => 462,
                'name' => 'Catedral',
                'municipality_id' => 146,
            ),
            462 => 
            array (
                'id' => 463,
                'name' => 'Concepción',
                'municipality_id' => 146,
            ),
            463 => 
            array (
                'id' => 464,
                'name' => 'El Cují',
                'municipality_id' => 146,
            ),
            464 => 
            array (
                'id' => 465,
                'name' => 'Juan de Villegas',
                'municipality_id' => 146,
            ),
            465 => 
            array (
                'id' => 466,
                'name' => 'Santa Rosa',
                'municipality_id' => 146,
            ),
            466 => 
            array (
                'id' => 467,
                'name' => 'Tamaca',
                'municipality_id' => 146,
            ),
            467 => 
            array (
                'id' => 468,
                'name' => 'Unión',
                'municipality_id' => 146,
            ),
            468 => 
            array (
                'id' => 469,
                'name' => 'Aguedo Felipe Alvarado',
                'municipality_id' => 146,
            ),
            469 => 
            array (
                'id' => 470,
                'name' => 'Buena Vista',
                'municipality_id' => 146,
            ),
            470 => 
            array (
                'id' => 471,
                'name' => 'Juárez',
                'municipality_id' => 146,
            ),
            471 => 
            array (
                'id' => 472,
                'name' => 'Juan Bautista Rodríguez',
                'municipality_id' => 147,
            ),
            472 => 
            array (
                'id' => 473,
                'name' => 'Cuara',
                'municipality_id' => 147,
            ),
            473 => 
            array (
                'id' => 474,
                'name' => 'Diego de Lozada',
                'municipality_id' => 147,
            ),
            474 => 
            array (
                'id' => 475,
                'name' => 'Paraíso de San José',
                'municipality_id' => 147,
            ),
            475 => 
            array (
                'id' => 476,
                'name' => 'San Miguel',
                'municipality_id' => 147,
            ),
            476 => 
            array (
                'id' => 477,
                'name' => 'Tintorero',
                'municipality_id' => 147,
            ),
            477 => 
            array (
                'id' => 478,
                'name' => 'José Bernardo Dorante',
                'municipality_id' => 147,
            ),
            478 => 
            array (
                'id' => 479,
                'name' => 'Coronel Mariano Peraza ',
                'municipality_id' => 147,
            ),
            479 => 
            array (
                'id' => 480,
                'name' => 'Bolívar',
                'municipality_id' => 148,
            ),
            480 => 
            array (
                'id' => 481,
                'name' => 'Anzoátegui',
                'municipality_id' => 148,
            ),
            481 => 
            array (
                'id' => 482,
                'name' => 'Guarico',
                'municipality_id' => 148,
            ),
            482 => 
            array (
                'id' => 483,
                'name' => 'Hilario Luna y Luna',
                'municipality_id' => 148,
            ),
            483 => 
            array (
                'id' => 484,
                'name' => 'Humocaro Alto',
                'municipality_id' => 148,
            ),
            484 => 
            array (
                'id' => 485,
                'name' => 'Humocaro Bajo',
                'municipality_id' => 148,
            ),
            485 => 
            array (
                'id' => 486,
                'name' => 'La Candelaria',
                'municipality_id' => 148,
            ),
            486 => 
            array (
                'id' => 487,
                'name' => 'Morán',
                'municipality_id' => 148,
            ),
            487 => 
            array (
                'id' => 488,
                'name' => 'Cabudare',
                'municipality_id' => 149,
            ),
            488 => 
            array (
                'id' => 489,
                'name' => 'José Gregorio Bastidas',
                'municipality_id' => 149,
            ),
            489 => 
            array (
                'id' => 490,
                'name' => 'Agua Viva',
                'municipality_id' => 149,
            ),
            490 => 
            array (
                'id' => 491,
                'name' => 'Sarare',
                'municipality_id' => 150,
            ),
            491 => 
            array (
                'id' => 492,
                'name' => 'Buría',
                'municipality_id' => 150,
            ),
            492 => 
            array (
                'id' => 493,
                'name' => 'Gustavo Vegas León',
                'municipality_id' => 150,
            ),
            493 => 
            array (
                'id' => 494,
                'name' => 'Trinidad Samuel',
                'municipality_id' => 151,
            ),
            494 => 
            array (
                'id' => 495,
                'name' => 'Antonio Díaz',
                'municipality_id' => 151,
            ),
            495 => 
            array (
                'id' => 496,
                'name' => 'Camacaro',
                'municipality_id' => 151,
            ),
            496 => 
            array (
                'id' => 497,
                'name' => 'Castañeda',
                'municipality_id' => 151,
            ),
            497 => 
            array (
                'id' => 498,
                'name' => 'Cecilio Zubillaga',
                'municipality_id' => 151,
            ),
            498 => 
            array (
                'id' => 499,
                'name' => 'Chiquinquirá',
                'municipality_id' => 151,
            ),
            499 => 
            array (
                'id' => 500,
                'name' => 'El Blanco',
                'municipality_id' => 151,
            ),
        ));
        \DB::table('parishes')->insert(array (
            0 => 
            array (
                'id' => 501,
                'name' => 'Espinoza de los Monteros',
                'municipality_id' => 151,
            ),
            1 => 
            array (
                'id' => 502,
                'name' => 'Lara',
                'municipality_id' => 151,
            ),
            2 => 
            array (
                'id' => 503,
                'name' => 'Las Mercedes',
                'municipality_id' => 151,
            ),
            3 => 
            array (
                'id' => 504,
                'name' => 'Manuel Morillo',
                'municipality_id' => 151,
            ),
            4 => 
            array (
                'id' => 505,
                'name' => 'Montaña Verde',
                'municipality_id' => 151,
            ),
            5 => 
            array (
                'id' => 506,
                'name' => 'Montes de Oca',
                'municipality_id' => 151,
            ),
            6 => 
            array (
                'id' => 507,
                'name' => 'Torres',
                'municipality_id' => 151,
            ),
            7 => 
            array (
                'id' => 508,
                'name' => 'Heriberto Arroyo',
                'municipality_id' => 151,
            ),
            8 => 
            array (
                'id' => 509,
                'name' => 'Reyes Vargas',
                'municipality_id' => 151,
            ),
            9 => 
            array (
                'id' => 510,
                'name' => 'Altagracia',
                'municipality_id' => 151,
            ),
            10 => 
            array (
                'id' => 511,
                'name' => 'Siquisique',
                'municipality_id' => 152,
            ),
            11 => 
            array (
                'id' => 512,
                'name' => 'Moroturo',
                'municipality_id' => 152,
            ),
            12 => 
            array (
                'id' => 513,
                'name' => 'San Miguel',
                'municipality_id' => 152,
            ),
            13 => 
            array (
                'id' => 514,
                'name' => 'Xaguas',
                'municipality_id' => 152,
            ),
            14 => 
            array (
                'id' => 515,
                'name' => 'Presidente Betancourt',
                'municipality_id' => 179,
            ),
            15 => 
            array (
                'id' => 516,
                'name' => 'Presidente Páez',
                'municipality_id' => 179,
            ),
            16 => 
            array (
                'id' => 517,
                'name' => 'Presidente Rómulo Gallegos',
                'municipality_id' => 179,
            ),
            17 => 
            array (
                'id' => 518,
                'name' => 'Gabriel Picón González',
                'municipality_id' => 179,
            ),
            18 => 
            array (
                'id' => 519,
                'name' => 'Héctor Amable Mora',
                'municipality_id' => 179,
            ),
            19 => 
            array (
                'id' => 520,
                'name' => 'José Nucete Sardi',
                'municipality_id' => 179,
            ),
            20 => 
            array (
                'id' => 521,
                'name' => 'Pulido Méndez',
                'municipality_id' => 179,
            ),
            21 => 
            array (
                'id' => 522,
                'name' => 'La Azulita',
                'municipality_id' => 180,
            ),
            22 => 
            array (
                'id' => 523,
                'name' => 'Santa Cruz de Mora',
                'municipality_id' => 181,
            ),
            23 => 
            array (
                'id' => 524,
                'name' => 'Mesa Bolívar',
                'municipality_id' => 181,
            ),
            24 => 
            array (
                'id' => 525,
                'name' => 'Mesa de Las Palmas',
                'municipality_id' => 181,
            ),
            25 => 
            array (
                'id' => 526,
                'name' => 'Aricagua',
                'municipality_id' => 182,
            ),
            26 => 
            array (
                'id' => 527,
                'name' => 'San Antonio',
                'municipality_id' => 182,
            ),
            27 => 
            array (
                'id' => 528,
                'name' => 'Canagua',
                'municipality_id' => 183,
            ),
            28 => 
            array (
                'id' => 529,
                'name' => 'Capurí',
                'municipality_id' => 183,
            ),
            29 => 
            array (
                'id' => 530,
                'name' => 'Chacantá',
                'municipality_id' => 183,
            ),
            30 => 
            array (
                'id' => 531,
                'name' => 'El Molino',
                'municipality_id' => 183,
            ),
            31 => 
            array (
                'id' => 532,
                'name' => 'Guaimaral',
                'municipality_id' => 183,
            ),
            32 => 
            array (
                'id' => 533,
                'name' => 'Mucutuy',
                'municipality_id' => 183,
            ),
            33 => 
            array (
                'id' => 534,
                'name' => 'Mucuchachí',
                'municipality_id' => 183,
            ),
            34 => 
            array (
                'id' => 535,
                'name' => 'Fernández Peña',
                'municipality_id' => 184,
            ),
            35 => 
            array (
                'id' => 536,
                'name' => 'Matriz',
                'municipality_id' => 184,
            ),
            36 => 
            array (
                'id' => 537,
                'name' => 'Montalbán',
                'municipality_id' => 184,
            ),
            37 => 
            array (
                'id' => 538,
                'name' => 'Acequias',
                'municipality_id' => 184,
            ),
            38 => 
            array (
                'id' => 539,
                'name' => 'Jají',
                'municipality_id' => 184,
            ),
            39 => 
            array (
                'id' => 540,
                'name' => 'La Mesa',
                'municipality_id' => 184,
            ),
            40 => 
            array (
                'id' => 541,
                'name' => 'San José del Sur',
                'municipality_id' => 184,
            ),
            41 => 
            array (
                'id' => 542,
                'name' => 'Tucaní',
                'municipality_id' => 185,
            ),
            42 => 
            array (
                'id' => 543,
                'name' => 'Florencio Ramírez',
                'municipality_id' => 185,
            ),
            43 => 
            array (
                'id' => 544,
                'name' => 'Santo Domingo',
                'municipality_id' => 186,
            ),
            44 => 
            array (
                'id' => 545,
                'name' => 'Las Piedras',
                'municipality_id' => 186,
            ),
            45 => 
            array (
                'id' => 546,
                'name' => 'Guaraque',
                'municipality_id' => 187,
            ),
            46 => 
            array (
                'id' => 547,
                'name' => 'Mesa de Quintero',
                'municipality_id' => 187,
            ),
            47 => 
            array (
                'id' => 548,
                'name' => 'Río Negro',
                'municipality_id' => 187,
            ),
            48 => 
            array (
                'id' => 549,
                'name' => 'Arapuey',
                'municipality_id' => 188,
            ),
            49 => 
            array (
                'id' => 550,
                'name' => 'Palmira',
                'municipality_id' => 188,
            ),
            50 => 
            array (
                'id' => 551,
                'name' => 'San Cristóbal de Torondoy',
                'municipality_id' => 189,
            ),
            51 => 
            array (
                'id' => 552,
                'name' => 'Torondoy',
                'municipality_id' => 189,
            ),
            52 => 
            array (
                'id' => 553,
                'name' => 'Antonio Spinetti Dini',
                'municipality_id' => 190,
            ),
            53 => 
            array (
                'id' => 554,
                'name' => 'Arias',
                'municipality_id' => 190,
            ),
            54 => 
            array (
                'id' => 555,
                'name' => 'Caracciolo Parra Pérez',
                'municipality_id' => 190,
            ),
            55 => 
            array (
                'id' => 556,
                'name' => 'Domingo Peña',
                'municipality_id' => 190,
            ),
            56 => 
            array (
                'id' => 557,
                'name' => 'El Llano',
                'municipality_id' => 190,
            ),
            57 => 
            array (
                'id' => 558,
                'name' => 'Gonzalo Picón Febres',
                'municipality_id' => 190,
            ),
            58 => 
            array (
                'id' => 559,
                'name' => 'Jacinto Plaza',
                'municipality_id' => 190,
            ),
            59 => 
            array (
                'id' => 560,
                'name' => 'Juan Rodríguez Suárez',
                'municipality_id' => 190,
            ),
            60 => 
            array (
                'id' => 561,
                'name' => 'Lasso de la Vega',
                'municipality_id' => 190,
            ),
            61 => 
            array (
                'id' => 562,
                'name' => 'Mariano Picón Salas',
                'municipality_id' => 190,
            ),
            62 => 
            array (
                'id' => 563,
                'name' => 'Milla',
                'municipality_id' => 190,
            ),
            63 => 
            array (
                'id' => 564,
                'name' => 'Osuna Rodríguez',
                'municipality_id' => 190,
            ),
            64 => 
            array (
                'id' => 565,
                'name' => 'Sagrario',
                'municipality_id' => 190,
            ),
            65 => 
            array (
                'id' => 566,
                'name' => 'El Morro',
                'municipality_id' => 190,
            ),
            66 => 
            array (
                'id' => 567,
                'name' => 'Los Nevados',
                'municipality_id' => 190,
            ),
            67 => 
            array (
                'id' => 568,
                'name' => 'Andrés Eloy Blanco',
                'municipality_id' => 191,
            ),
            68 => 
            array (
                'id' => 569,
                'name' => 'La Venta',
                'municipality_id' => 191,
            ),
            69 => 
            array (
                'id' => 570,
                'name' => 'Piñango',
                'municipality_id' => 191,
            ),
            70 => 
            array (
                'id' => 571,
                'name' => 'Timotes',
                'municipality_id' => 191,
            ),
            71 => 
            array (
                'id' => 572,
                'name' => 'Eloy Paredes',
                'municipality_id' => 192,
            ),
            72 => 
            array (
                'id' => 573,
                'name' => 'San Rafael de Alcázar',
                'municipality_id' => 192,
            ),
            73 => 
            array (
                'id' => 574,
                'name' => 'Santa Elena de Arenales',
                'municipality_id' => 192,
            ),
            74 => 
            array (
                'id' => 575,
                'name' => 'Santa María de Caparo',
                'municipality_id' => 193,
            ),
            75 => 
            array (
                'id' => 576,
                'name' => 'Pueblo Llano',
                'municipality_id' => 194,
            ),
            76 => 
            array (
                'id' => 577,
                'name' => 'Cacute',
                'municipality_id' => 195,
            ),
            77 => 
            array (
                'id' => 578,
                'name' => 'La Toma',
                'municipality_id' => 195,
            ),
            78 => 
            array (
                'id' => 579,
                'name' => 'Mucuchíes',
                'municipality_id' => 195,
            ),
            79 => 
            array (
                'id' => 580,
                'name' => 'Mucurubá',
                'municipality_id' => 195,
            ),
            80 => 
            array (
                'id' => 581,
                'name' => 'San Rafael',
                'municipality_id' => 195,
            ),
            81 => 
            array (
                'id' => 582,
                'name' => 'Gerónimo Maldonado',
                'municipality_id' => 196,
            ),
            82 => 
            array (
                'id' => 583,
                'name' => 'Bailadores',
                'municipality_id' => 196,
            ),
            83 => 
            array (
                'id' => 584,
                'name' => 'Tabay',
                'municipality_id' => 197,
            ),
            84 => 
            array (
                'id' => 585,
                'name' => 'Chiguará',
                'municipality_id' => 198,
            ),
            85 => 
            array (
                'id' => 586,
                'name' => 'Estánquez',
                'municipality_id' => 198,
            ),
            86 => 
            array (
                'id' => 587,
                'name' => 'Lagunillas',
                'municipality_id' => 198,
            ),
            87 => 
            array (
                'id' => 588,
                'name' => 'La Trampa',
                'municipality_id' => 198,
            ),
            88 => 
            array (
                'id' => 589,
                'name' => 'Pueblo Nuevo del Sur',
                'municipality_id' => 198,
            ),
            89 => 
            array (
                'id' => 590,
                'name' => 'San Juan',
                'municipality_id' => 198,
            ),
            90 => 
            array (
                'id' => 591,
                'name' => 'El Amparo',
                'municipality_id' => 199,
            ),
            91 => 
            array (
                'id' => 592,
                'name' => 'El Llano',
                'municipality_id' => 199,
            ),
            92 => 
            array (
                'id' => 593,
                'name' => 'San Francisco',
                'municipality_id' => 199,
            ),
            93 => 
            array (
                'id' => 594,
                'name' => 'Tovar',
                'municipality_id' => 199,
            ),
            94 => 
            array (
                'id' => 595,
                'name' => 'Independencia',
                'municipality_id' => 200,
            ),
            95 => 
            array (
                'id' => 596,
                'name' => 'María de la Concepción Palacios Blanco',
                'municipality_id' => 200,
            ),
            96 => 
            array (
                'id' => 597,
                'name' => 'Nueva Bolivia',
                'municipality_id' => 200,
            ),
            97 => 
            array (
                'id' => 598,
                'name' => 'Santa Apolonia',
                'municipality_id' => 200,
            ),
            98 => 
            array (
                'id' => 599,
                'name' => 'Caño El Tigre',
                'municipality_id' => 201,
            ),
            99 => 
            array (
                'id' => 600,
                'name' => 'Zea',
                'municipality_id' => 201,
            ),
            100 => 
            array (
                'id' => 601,
                'name' => 'Aragüita',
                'municipality_id' => 223,
            ),
            101 => 
            array (
                'id' => 602,
                'name' => 'Arévalo González',
                'municipality_id' => 223,
            ),
            102 => 
            array (
                'id' => 603,
                'name' => 'Capaya',
                'municipality_id' => 223,
            ),
            103 => 
            array (
                'id' => 604,
                'name' => 'Caucagua',
                'municipality_id' => 223,
            ),
            104 => 
            array (
                'id' => 605,
                'name' => 'Panaquire',
                'municipality_id' => 223,
            ),
            105 => 
            array (
                'id' => 606,
                'name' => 'Ribas',
                'municipality_id' => 223,
            ),
            106 => 
            array (
                'id' => 607,
                'name' => 'El Café',
                'municipality_id' => 223,
            ),
            107 => 
            array (
                'id' => 608,
                'name' => 'Marizapa',
                'municipality_id' => 223,
            ),
            108 => 
            array (
                'id' => 609,
                'name' => 'Cumbo',
                'municipality_id' => 224,
            ),
            109 => 
            array (
                'id' => 610,
                'name' => 'San José de Barlovento',
                'municipality_id' => 224,
            ),
            110 => 
            array (
                'id' => 611,
                'name' => 'El Cafetal',
                'municipality_id' => 225,
            ),
            111 => 
            array (
                'id' => 612,
                'name' => 'Las Minas',
                'municipality_id' => 225,
            ),
            112 => 
            array (
                'id' => 613,
                'name' => 'Nuestra Señora del Rosario',
                'municipality_id' => 225,
            ),
            113 => 
            array (
                'id' => 614,
                'name' => 'Higuerote',
                'municipality_id' => 226,
            ),
            114 => 
            array (
                'id' => 615,
                'name' => 'Curiepe',
                'municipality_id' => 226,
            ),
            115 => 
            array (
                'id' => 616,
                'name' => 'Tacarigua de Brión',
                'municipality_id' => 226,
            ),
            116 => 
            array (
                'id' => 617,
                'name' => 'Mamporal',
                'municipality_id' => 227,
            ),
            117 => 
            array (
                'id' => 618,
                'name' => 'Carrizal',
                'municipality_id' => 228,
            ),
            118 => 
            array (
                'id' => 619,
                'name' => 'Chacao',
                'municipality_id' => 229,
            ),
            119 => 
            array (
                'id' => 620,
                'name' => 'Charallave',
                'municipality_id' => 230,
            ),
            120 => 
            array (
                'id' => 621,
                'name' => 'Las Brisas',
                'municipality_id' => 230,
            ),
            121 => 
            array (
                'id' => 622,
                'name' => 'El Hatillo',
                'municipality_id' => 231,
            ),
            122 => 
            array (
                'id' => 623,
                'name' => 'Altagracia de la Montaña',
                'municipality_id' => 232,
            ),
            123 => 
            array (
                'id' => 624,
                'name' => 'Cecilio Acosta',
                'municipality_id' => 232,
            ),
            124 => 
            array (
                'id' => 625,
                'name' => 'Los Teques',
                'municipality_id' => 232,
            ),
            125 => 
            array (
                'id' => 626,
                'name' => 'El Jarillo',
                'municipality_id' => 232,
            ),
            126 => 
            array (
                'id' => 627,
                'name' => 'San Pedro',
                'municipality_id' => 232,
            ),
            127 => 
            array (
                'id' => 628,
                'name' => 'Tácata',
                'municipality_id' => 232,
            ),
            128 => 
            array (
                'id' => 629,
                'name' => 'Paracotos',
                'municipality_id' => 232,
            ),
            129 => 
            array (
                'id' => 630,
                'name' => 'Cartanal',
                'municipality_id' => 233,
            ),
            130 => 
            array (
                'id' => 757,
                'name' => 'Villa Rosa',
                'municipality_id' => 294,
            ),
            131 => 
            array (
                'id' => 631,
                'name' => 'Santa Teresa del Tuy',
                'municipality_id' => 233,
            ),
            132 => 
            array (
                'id' => 632,
                'name' => 'La Democracia',
                'municipality_id' => 234,
            ),
            133 => 
            array (
                'id' => 633,
                'name' => 'Ocumare del Tuy',
                'municipality_id' => 234,
            ),
            134 => 
            array (
                'id' => 634,
                'name' => 'Santa Bárbara',
                'municipality_id' => 234,
            ),
            135 => 
            array (
                'id' => 635,
                'name' => 'San Antonio de los Altos',
                'municipality_id' => 235,
            ),
            136 => 
            array (
                'id' => 636,
                'name' => 'Río Chico',
                'municipality_id' => 236,
            ),
            137 => 
            array (
                'id' => 637,
                'name' => 'El Guapo',
                'municipality_id' => 236,
            ),
            138 => 
            array (
                'id' => 638,
                'name' => 'Tacarigua de la Laguna',
                'municipality_id' => 236,
            ),
            139 => 
            array (
                'id' => 639,
                'name' => 'Paparo',
                'municipality_id' => 236,
            ),
            140 => 
            array (
                'id' => 640,
                'name' => 'San Fernando del Guapo',
                'municipality_id' => 236,
            ),
            141 => 
            array (
                'id' => 641,
                'name' => 'Santa Lucía del Tuy',
                'municipality_id' => 237,
            ),
            142 => 
            array (
                'id' => 642,
                'name' => 'Cúpira',
                'municipality_id' => 238,
            ),
            143 => 
            array (
                'id' => 643,
                'name' => 'Machurucuto',
                'municipality_id' => 238,
            ),
            144 => 
            array (
                'id' => 644,
                'name' => 'Guarenas',
                'municipality_id' => 239,
            ),
            145 => 
            array (
                'id' => 645,
                'name' => 'San Antonio de Yare',
                'municipality_id' => 240,
            ),
            146 => 
            array (
                'id' => 646,
                'name' => 'San Francisco de Yare',
                'municipality_id' => 240,
            ),
            147 => 
            array (
                'id' => 647,
                'name' => 'Leoncio Martínez',
                'municipality_id' => 241,
            ),
            148 => 
            array (
                'id' => 648,
                'name' => 'Petare',
                'municipality_id' => 241,
            ),
            149 => 
            array (
                'id' => 649,
                'name' => 'Caucagüita',
                'municipality_id' => 241,
            ),
            150 => 
            array (
                'id' => 650,
                'name' => 'Filas de Mariche',
                'municipality_id' => 241,
            ),
            151 => 
            array (
                'id' => 651,
                'name' => 'La Dolorita',
                'municipality_id' => 241,
            ),
            152 => 
            array (
                'id' => 652,
                'name' => 'Cúa',
                'municipality_id' => 242,
            ),
            153 => 
            array (
                'id' => 653,
                'name' => 'Nueva Cúa',
                'municipality_id' => 242,
            ),
            154 => 
            array (
                'id' => 654,
                'name' => 'Guatire',
                'municipality_id' => 243,
            ),
            155 => 
            array (
                'id' => 655,
                'name' => 'Bolívar',
                'municipality_id' => 243,
            ),
            156 => 
            array (
                'id' => 656,
                'name' => 'San Antonio de Maturín',
                'municipality_id' => 258,
            ),
            157 => 
            array (
                'id' => 657,
                'name' => 'San Francisco de Maturín',
                'municipality_id' => 258,
            ),
            158 => 
            array (
                'id' => 658,
                'name' => 'Aguasay',
                'municipality_id' => 259,
            ),
            159 => 
            array (
                'id' => 659,
                'name' => 'Caripito',
                'municipality_id' => 260,
            ),
            160 => 
            array (
                'id' => 660,
                'name' => 'El Guácharo',
                'municipality_id' => 261,
            ),
            161 => 
            array (
                'id' => 661,
                'name' => 'La Guanota',
                'municipality_id' => 261,
            ),
            162 => 
            array (
                'id' => 662,
                'name' => 'Sabana de Piedra',
                'municipality_id' => 261,
            ),
            163 => 
            array (
                'id' => 663,
                'name' => 'San Agustín',
                'municipality_id' => 261,
            ),
            164 => 
            array (
                'id' => 664,
                'name' => 'Teresen',
                'municipality_id' => 261,
            ),
            165 => 
            array (
                'id' => 665,
                'name' => 'Caripe',
                'municipality_id' => 261,
            ),
            166 => 
            array (
                'id' => 666,
                'name' => 'Areo',
                'municipality_id' => 262,
            ),
            167 => 
            array (
                'id' => 667,
                'name' => 'Capital Cedeño',
                'municipality_id' => 262,
            ),
            168 => 
            array (
                'id' => 668,
                'name' => 'San Félix de Cantalicio',
                'municipality_id' => 262,
            ),
            169 => 
            array (
                'id' => 669,
                'name' => 'Viento Fresco',
                'municipality_id' => 262,
            ),
            170 => 
            array (
                'id' => 670,
                'name' => 'El Tejero',
                'municipality_id' => 263,
            ),
            171 => 
            array (
                'id' => 671,
                'name' => 'Punta de Mata',
                'municipality_id' => 263,
            ),
            172 => 
            array (
                'id' => 672,
                'name' => 'Chaguaramas',
                'municipality_id' => 264,
            ),
            173 => 
            array (
                'id' => 673,
                'name' => 'Las Alhuacas',
                'municipality_id' => 264,
            ),
            174 => 
            array (
                'id' => 674,
                'name' => 'Tabasca',
                'municipality_id' => 264,
            ),
            175 => 
            array (
                'id' => 675,
                'name' => 'Temblador',
                'municipality_id' => 264,
            ),
            176 => 
            array (
                'id' => 676,
                'name' => 'Alto de los Godos',
                'municipality_id' => 265,
            ),
            177 => 
            array (
                'id' => 677,
                'name' => 'Boquerón',
                'municipality_id' => 265,
            ),
            178 => 
            array (
                'id' => 678,
                'name' => 'Las Cocuizas',
                'municipality_id' => 265,
            ),
            179 => 
            array (
                'id' => 679,
                'name' => 'La Cruz',
                'municipality_id' => 265,
            ),
            180 => 
            array (
                'id' => 680,
                'name' => 'San Simón',
                'municipality_id' => 265,
            ),
            181 => 
            array (
                'id' => 681,
                'name' => 'El Corozo',
                'municipality_id' => 265,
            ),
            182 => 
            array (
                'id' => 682,
                'name' => 'El Furrial',
                'municipality_id' => 265,
            ),
            183 => 
            array (
                'id' => 683,
                'name' => 'Jusepín',
                'municipality_id' => 265,
            ),
            184 => 
            array (
                'id' => 684,
                'name' => 'La Pica',
                'municipality_id' => 265,
            ),
            185 => 
            array (
                'id' => 685,
                'name' => 'San Vicente',
                'municipality_id' => 265,
            ),
            186 => 
            array (
                'id' => 686,
                'name' => 'Aparicio',
                'municipality_id' => 266,
            ),
            187 => 
            array (
                'id' => 687,
                'name' => 'Aragua de Maturín',
                'municipality_id' => 266,
            ),
            188 => 
            array (
                'id' => 688,
                'name' => 'Chaguamal',
                'municipality_id' => 266,
            ),
            189 => 
            array (
                'id' => 689,
                'name' => 'El Pinto',
                'municipality_id' => 266,
            ),
            190 => 
            array (
                'id' => 690,
                'name' => 'Guanaguana',
                'municipality_id' => 266,
            ),
            191 => 
            array (
                'id' => 691,
                'name' => 'La Toscana',
                'municipality_id' => 266,
            ),
            192 => 
            array (
                'id' => 692,
                'name' => 'Taguaya',
                'municipality_id' => 266,
            ),
            193 => 
            array (
                'id' => 693,
                'name' => 'Cachipo',
                'municipality_id' => 267,
            ),
            194 => 
            array (
                'id' => 694,
                'name' => 'Quiriquire',
                'municipality_id' => 267,
            ),
            195 => 
            array (
                'id' => 695,
                'name' => 'Santa Bárbara',
                'municipality_id' => 268,
            ),
            196 => 
            array (
                'id' => 696,
                'name' => 'Barrancas',
                'municipality_id' => 269,
            ),
            197 => 
            array (
                'id' => 697,
                'name' => 'Los Barrancos de Fajardo',
                'municipality_id' => 269,
            ),
            198 => 
            array (
                'id' => 698,
                'name' => 'Uracoa',
                'municipality_id' => 270,
            ),
            199 => 
            array (
                'id' => 699,
                'name' => 'Antolín del Campo',
                'municipality_id' => 271,
            ),
            200 => 
            array (
                'id' => 700,
                'name' => 'Arismendi',
                'municipality_id' => 272,
            ),
            201 => 
            array (
                'id' => 701,
                'name' => 'García',
                'municipality_id' => 273,
            ),
            202 => 
            array (
                'id' => 702,
                'name' => 'Francisco Fajardo',
                'municipality_id' => 273,
            ),
            203 => 
            array (
                'id' => 703,
                'name' => 'Bolívar',
                'municipality_id' => 274,
            ),
            204 => 
            array (
                'id' => 704,
                'name' => 'Guevara',
                'municipality_id' => 274,
            ),
            205 => 
            array (
                'id' => 705,
                'name' => 'Matasiete',
                'municipality_id' => 274,
            ),
            206 => 
            array (
                'id' => 706,
                'name' => 'Santa Ana',
                'municipality_id' => 274,
            ),
            207 => 
            array (
                'id' => 707,
                'name' => 'Sucre',
                'municipality_id' => 274,
            ),
            208 => 
            array (
                'id' => 708,
                'name' => 'Aguirre',
                'municipality_id' => 275,
            ),
            209 => 
            array (
                'id' => 709,
                'name' => 'Maneiro',
                'municipality_id' => 275,
            ),
            210 => 
            array (
                'id' => 710,
                'name' => 'Adrián',
                'municipality_id' => 276,
            ),
            211 => 
            array (
                'id' => 711,
                'name' => 'Juan Griego',
                'municipality_id' => 276,
            ),
            212 => 
            array (
                'id' => 712,
                'name' => 'Yaguaraparo',
                'municipality_id' => 276,
            ),
            213 => 
            array (
                'id' => 713,
                'name' => 'Porlamar',
                'municipality_id' => 277,
            ),
            214 => 
            array (
                'id' => 714,
                'name' => 'San Francisco de Macanao',
                'municipality_id' => 278,
            ),
            215 => 
            array (
                'id' => 715,
                'name' => 'Boca de Río',
                'municipality_id' => 278,
            ),
            216 => 
            array (
                'id' => 716,
                'name' => 'Tubores',
                'municipality_id' => 279,
            ),
            217 => 
            array (
                'id' => 717,
                'name' => 'Los Baleales',
                'municipality_id' => 279,
            ),
            218 => 
            array (
                'id' => 718,
                'name' => 'Vicente Fuentes',
                'municipality_id' => 280,
            ),
            219 => 
            array (
                'id' => 719,
                'name' => 'Villalba',
                'municipality_id' => 280,
            ),
            220 => 
            array (
                'id' => 720,
                'name' => 'San Juan Bautista',
                'municipality_id' => 281,
            ),
            221 => 
            array (
                'id' => 721,
                'name' => 'Zabala',
                'municipality_id' => 281,
            ),
            222 => 
            array (
                'id' => 722,
                'name' => 'Capital Araure',
                'municipality_id' => 283,
            ),
            223 => 
            array (
                'id' => 723,
                'name' => 'Río Acarigua',
                'municipality_id' => 283,
            ),
            224 => 
            array (
                'id' => 724,
                'name' => 'Capital Esteller',
                'municipality_id' => 284,
            ),
            225 => 
            array (
                'id' => 725,
                'name' => 'Uveral',
                'municipality_id' => 284,
            ),
            226 => 
            array (
                'id' => 726,
                'name' => 'Guanare',
                'municipality_id' => 285,
            ),
            227 => 
            array (
                'id' => 727,
                'name' => 'Córdoba',
                'municipality_id' => 285,
            ),
            228 => 
            array (
                'id' => 728,
                'name' => 'San José de la Montaña',
                'municipality_id' => 285,
            ),
            229 => 
            array (
                'id' => 729,
                'name' => 'San Juan de Guanaguanare',
                'municipality_id' => 285,
            ),
            230 => 
            array (
                'id' => 730,
                'name' => 'Virgen de la Coromoto',
                'municipality_id' => 285,
            ),
            231 => 
            array (
                'id' => 731,
                'name' => 'Guanarito',
                'municipality_id' => 286,
            ),
            232 => 
            array (
                'id' => 732,
                'name' => 'Trinidad de la Capilla',
                'municipality_id' => 286,
            ),
            233 => 
            array (
                'id' => 733,
                'name' => 'Divina Pastora',
                'municipality_id' => 286,
            ),
            234 => 
            array (
                'id' => 734,
                'name' => 'Monseñor José Vicente de Unda',
                'municipality_id' => 287,
            ),
            235 => 
            array (
                'id' => 735,
                'name' => 'Peña Blanca',
                'municipality_id' => 287,
            ),
            236 => 
            array (
                'id' => 736,
                'name' => 'Capital Ospino',
                'municipality_id' => 288,
            ),
            237 => 
            array (
                'id' => 737,
                'name' => 'Aparición',
                'municipality_id' => 288,
            ),
            238 => 
            array (
                'id' => 738,
                'name' => 'La Estación',
                'municipality_id' => 288,
            ),
            239 => 
            array (
                'id' => 739,
                'name' => 'Páez',
                'municipality_id' => 289,
            ),
            240 => 
            array (
                'id' => 740,
                'name' => 'Payara',
                'municipality_id' => 289,
            ),
            241 => 
            array (
                'id' => 741,
                'name' => 'Pimpinela',
                'municipality_id' => 289,
            ),
            242 => 
            array (
                'id' => 742,
                'name' => 'Ramón Peraza',
                'municipality_id' => 289,
            ),
            243 => 
            array (
                'id' => 743,
                'name' => 'Papelón',
                'municipality_id' => 290,
            ),
            244 => 
            array (
                'id' => 744,
                'name' => 'Caño Delgadito',
                'municipality_id' => 290,
            ),
            245 => 
            array (
                'id' => 745,
                'name' => 'San Genaro de Boconoito',
                'municipality_id' => 291,
            ),
            246 => 
            array (
                'id' => 746,
                'name' => 'Antolín Tovar',
                'municipality_id' => 291,
            ),
            247 => 
            array (
                'id' => 747,
                'name' => 'San Rafael de Onoto',
                'municipality_id' => 292,
            ),
            248 => 
            array (
                'id' => 748,
                'name' => 'Santa Fe',
                'municipality_id' => 292,
            ),
            249 => 
            array (
                'id' => 749,
                'name' => 'Thermo Morles',
                'municipality_id' => 292,
            ),
            250 => 
            array (
                'id' => 750,
                'name' => 'Santa Rosalía',
                'municipality_id' => 293,
            ),
            251 => 
            array (
                'id' => 751,
                'name' => 'Florida',
                'municipality_id' => 293,
            ),
            252 => 
            array (
                'id' => 752,
                'name' => 'Sucre',
                'municipality_id' => 294,
            ),
            253 => 
            array (
                'id' => 753,
                'name' => 'Concepción',
                'municipality_id' => 294,
            ),
            254 => 
            array (
                'id' => 754,
                'name' => 'San Rafael de Palo Alzado',
                'municipality_id' => 294,
            ),
            255 => 
            array (
                'id' => 755,
                'name' => 'Uvencio Antonio Velásquez',
                'municipality_id' => 294,
            ),
            256 => 
            array (
                'id' => 756,
                'name' => 'San José de Saguaz',
                'municipality_id' => 294,
            ),
            257 => 
            array (
                'id' => 758,
                'name' => 'Turén',
                'municipality_id' => 295,
            ),
            258 => 
            array (
                'id' => 759,
                'name' => 'Canelones',
                'municipality_id' => 295,
            ),
            259 => 
            array (
                'id' => 760,
                'name' => 'Santa Cruz',
                'municipality_id' => 295,
            ),
            260 => 
            array (
                'id' => 761,
                'name' => 'San Isidro Labrador',
                'municipality_id' => 295,
            ),
            261 => 
            array (
                'id' => 762,
                'name' => 'Mariño',
                'municipality_id' => 296,
            ),
            262 => 
            array (
                'id' => 763,
                'name' => 'Rómulo Gallegos',
                'municipality_id' => 296,
            ),
            263 => 
            array (
                'id' => 764,
                'name' => 'San José de Aerocuar',
                'municipality_id' => 297,
            ),
            264 => 
            array (
                'id' => 765,
                'name' => 'Tavera Acosta',
                'municipality_id' => 297,
            ),
            265 => 
            array (
                'id' => 766,
                'name' => 'Río Caribe',
                'municipality_id' => 298,
            ),
            266 => 
            array (
                'id' => 767,
                'name' => 'Antonio José de Sucre',
                'municipality_id' => 298,
            ),
            267 => 
            array (
                'id' => 768,
                'name' => 'El Morro de Puerto Santo',
                'municipality_id' => 298,
            ),
            268 => 
            array (
                'id' => 769,
                'name' => 'Puerto Santo',
                'municipality_id' => 298,
            ),
            269 => 
            array (
                'id' => 770,
                'name' => 'San Juan de las Galdonas',
                'municipality_id' => 298,
            ),
            270 => 
            array (
                'id' => 771,
                'name' => 'El Pilar',
                'municipality_id' => 299,
            ),
            271 => 
            array (
                'id' => 772,
                'name' => 'El Rincón',
                'municipality_id' => 299,
            ),
            272 => 
            array (
                'id' => 773,
                'name' => 'General Francisco Antonio Váquez',
                'municipality_id' => 299,
            ),
            273 => 
            array (
                'id' => 774,
                'name' => 'Guaraúnos',
                'municipality_id' => 299,
            ),
            274 => 
            array (
                'id' => 775,
                'name' => 'Tunapuicito',
                'municipality_id' => 299,
            ),
            275 => 
            array (
                'id' => 776,
                'name' => 'Unión',
                'municipality_id' => 299,
            ),
            276 => 
            array (
                'id' => 777,
                'name' => 'Santa Catalina',
                'municipality_id' => 300,
            ),
            277 => 
            array (
                'id' => 778,
                'name' => 'Santa Rosa',
                'municipality_id' => 300,
            ),
            278 => 
            array (
                'id' => 779,
                'name' => 'Santa Teresa',
                'municipality_id' => 300,
            ),
            279 => 
            array (
                'id' => 780,
                'name' => 'Bolívar',
                'municipality_id' => 300,
            ),
            280 => 
            array (
                'id' => 781,
                'name' => 'Maracapana',
                'municipality_id' => 300,
            ),
            281 => 
            array (
                'id' => 782,
                'name' => 'Libertad',
                'municipality_id' => 302,
            ),
            282 => 
            array (
                'id' => 783,
                'name' => 'El Paujil',
                'municipality_id' => 302,
            ),
            283 => 
            array (
                'id' => 784,
                'name' => 'Yaguaraparo',
                'municipality_id' => 302,
            ),
            284 => 
            array (
                'id' => 785,
                'name' => 'Cruz Salmerón Acosta',
                'municipality_id' => 303,
            ),
            285 => 
            array (
                'id' => 786,
                'name' => 'Chacopata',
                'municipality_id' => 303,
            ),
            286 => 
            array (
                'id' => 787,
                'name' => 'Manicuare',
                'municipality_id' => 303,
            ),
            287 => 
            array (
                'id' => 788,
                'name' => 'Tunapuy',
                'municipality_id' => 304,
            ),
            288 => 
            array (
                'id' => 789,
                'name' => 'Campo Elías',
                'municipality_id' => 304,
            ),
            289 => 
            array (
                'id' => 790,
                'name' => 'Irapa',
                'municipality_id' => 305,
            ),
            290 => 
            array (
                'id' => 791,
                'name' => 'Campo Claro',
                'municipality_id' => 305,
            ),
            291 => 
            array (
                'id' => 792,
                'name' => 'Maraval',
                'municipality_id' => 305,
            ),
            292 => 
            array (
                'id' => 793,
                'name' => 'San Antonio de Irapa',
                'municipality_id' => 305,
            ),
            293 => 
            array (
                'id' => 794,
                'name' => 'Soro',
                'municipality_id' => 305,
            ),
            294 => 
            array (
                'id' => 795,
                'name' => 'Mejía',
                'municipality_id' => 306,
            ),
            295 => 
            array (
                'id' => 796,
                'name' => 'Cumanacoa',
                'municipality_id' => 307,
            ),
            296 => 
            array (
                'id' => 797,
                'name' => 'Arenas',
                'municipality_id' => 307,
            ),
            297 => 
            array (
                'id' => 798,
                'name' => 'Aricagua',
                'municipality_id' => 307,
            ),
            298 => 
            array (
                'id' => 799,
                'name' => 'Cogollar',
                'municipality_id' => 307,
            ),
            299 => 
            array (
                'id' => 800,
                'name' => 'San Fernando',
                'municipality_id' => 307,
            ),
            300 => 
            array (
                'id' => 801,
                'name' => 'San Lorenzo',
                'municipality_id' => 307,
            ),
            301 => 
            array (
                'id' => 802,
            'name' => 'Villa Frontado (Muelle de Cariaco)',
                'municipality_id' => 308,
            ),
            302 => 
            array (
                'id' => 803,
                'name' => 'Catuaro',
                'municipality_id' => 308,
            ),
            303 => 
            array (
                'id' => 804,
                'name' => 'Rendón',
                'municipality_id' => 308,
            ),
            304 => 
            array (
                'id' => 805,
                'name' => 'San Cruz',
                'municipality_id' => 308,
            ),
            305 => 
            array (
                'id' => 806,
                'name' => 'Santa María',
                'municipality_id' => 308,
            ),
            306 => 
            array (
                'id' => 807,
                'name' => 'Altagracia',
                'municipality_id' => 309,
            ),
            307 => 
            array (
                'id' => 808,
                'name' => 'Santa Inés',
                'municipality_id' => 309,
            ),
            308 => 
            array (
                'id' => 809,
                'name' => 'Valentín Valiente',
                'municipality_id' => 309,
            ),
            309 => 
            array (
                'id' => 810,
                'name' => 'Ayacucho',
                'municipality_id' => 309,
            ),
            310 => 
            array (
                'id' => 811,
                'name' => 'San Juan',
                'municipality_id' => 309,
            ),
            311 => 
            array (
                'id' => 812,
                'name' => 'Raúl Leoni',
                'municipality_id' => 309,
            ),
            312 => 
            array (
                'id' => 813,
                'name' => 'Gran Mariscal',
                'municipality_id' => 309,
            ),
            313 => 
            array (
                'id' => 814,
                'name' => 'Cristóbal Colón',
                'municipality_id' => 310,
            ),
            314 => 
            array (
                'id' => 815,
                'name' => 'Bideau',
                'municipality_id' => 310,
            ),
            315 => 
            array (
                'id' => 816,
                'name' => 'Punta de Piedras',
                'municipality_id' => 310,
            ),
            316 => 
            array (
                'id' => 817,
                'name' => 'Güiria',
                'municipality_id' => 310,
            ),
            317 => 
            array (
                'id' => 818,
                'name' => 'Andrés Bello',
                'municipality_id' => 341,
            ),
            318 => 
            array (
                'id' => 819,
                'name' => 'Antonio Rómulo Costa',
                'municipality_id' => 342,
            ),
            319 => 
            array (
                'id' => 820,
                'name' => 'Ayacucho',
                'municipality_id' => 343,
            ),
            320 => 
            array (
                'id' => 821,
                'name' => 'Rivas Berti',
                'municipality_id' => 343,
            ),
            321 => 
            array (
                'id' => 822,
                'name' => 'San Pedro del Río',
                'municipality_id' => 343,
            ),
            322 => 
            array (
                'id' => 823,
                'name' => 'Bolívar',
                'municipality_id' => 344,
            ),
            323 => 
            array (
                'id' => 824,
                'name' => 'Palotal',
                'municipality_id' => 344,
            ),
            324 => 
            array (
                'id' => 825,
                'name' => 'General Juan Vicente Gómez',
                'municipality_id' => 344,
            ),
            325 => 
            array (
                'id' => 826,
                'name' => 'Isaías Medina Angarita',
                'municipality_id' => 344,
            ),
            326 => 
            array (
                'id' => 827,
                'name' => 'Cárdenas',
                'municipality_id' => 345,
            ),
            327 => 
            array (
                'id' => 828,
                'name' => 'Amenodoro Ángel Lamus',
                'municipality_id' => 345,
            ),
            328 => 
            array (
                'id' => 829,
                'name' => 'La Florida',
                'municipality_id' => 345,
            ),
            329 => 
            array (
                'id' => 830,
                'name' => 'Córdoba',
                'municipality_id' => 346,
            ),
            330 => 
            array (
                'id' => 831,
                'name' => 'Fernández Feo',
                'municipality_id' => 347,
            ),
            331 => 
            array (
                'id' => 832,
                'name' => 'Alberto Adriani',
                'municipality_id' => 347,
            ),
            332 => 
            array (
                'id' => 833,
                'name' => 'Santo Domingo',
                'municipality_id' => 347,
            ),
            333 => 
            array (
                'id' => 834,
                'name' => 'Francisco de Miranda',
                'municipality_id' => 348,
            ),
            334 => 
            array (
                'id' => 835,
                'name' => 'García de Hevia',
                'municipality_id' => 349,
            ),
            335 => 
            array (
                'id' => 836,
                'name' => 'Boca de Grita',
                'municipality_id' => 349,
            ),
            336 => 
            array (
                'id' => 837,
                'name' => 'José Antonio Páez',
                'municipality_id' => 349,
            ),
            337 => 
            array (
                'id' => 838,
                'name' => 'Guásimos',
                'municipality_id' => 350,
            ),
            338 => 
            array (
                'id' => 839,
                'name' => 'Independencia',
                'municipality_id' => 351,
            ),
            339 => 
            array (
                'id' => 840,
                'name' => 'Juan Germán Roscio',
                'municipality_id' => 351,
            ),
            340 => 
            array (
                'id' => 841,
                'name' => 'Román Cárdenas',
                'municipality_id' => 351,
            ),
            341 => 
            array (
                'id' => 842,
                'name' => 'Jáuregui',
                'municipality_id' => 352,
            ),
            342 => 
            array (
                'id' => 843,
                'name' => 'Emilio Constantino Guerrero',
                'municipality_id' => 352,
            ),
            343 => 
            array (
                'id' => 844,
                'name' => 'Monseñor Miguel Antonio Salas',
                'municipality_id' => 352,
            ),
            344 => 
            array (
                'id' => 845,
                'name' => 'José María Vargas',
                'municipality_id' => 353,
            ),
            345 => 
            array (
                'id' => 846,
                'name' => 'Junín',
                'municipality_id' => 354,
            ),
            346 => 
            array (
                'id' => 847,
                'name' => 'La Petrólea',
                'municipality_id' => 354,
            ),
            347 => 
            array (
                'id' => 848,
                'name' => 'Quinimarí',
                'municipality_id' => 354,
            ),
            348 => 
            array (
                'id' => 849,
                'name' => 'Bramón',
                'municipality_id' => 354,
            ),
            349 => 
            array (
                'id' => 850,
                'name' => 'Libertad',
                'municipality_id' => 355,
            ),
            350 => 
            array (
                'id' => 851,
                'name' => 'Cipriano Castro',
                'municipality_id' => 355,
            ),
            351 => 
            array (
                'id' => 852,
                'name' => 'Manuel Felipe Rugeles',
                'municipality_id' => 355,
            ),
            352 => 
            array (
                'id' => 853,
                'name' => 'Libertador',
                'municipality_id' => 356,
            ),
            353 => 
            array (
                'id' => 854,
                'name' => 'Doradas',
                'municipality_id' => 356,
            ),
            354 => 
            array (
                'id' => 855,
                'name' => 'Emeterio Ochoa',
                'municipality_id' => 356,
            ),
            355 => 
            array (
                'id' => 856,
                'name' => 'San Joaquín de Navay',
                'municipality_id' => 356,
            ),
            356 => 
            array (
                'id' => 857,
                'name' => 'Lobatera',
                'municipality_id' => 357,
            ),
            357 => 
            array (
                'id' => 858,
                'name' => 'Constitución',
                'municipality_id' => 357,
            ),
            358 => 
            array (
                'id' => 859,
                'name' => 'Michelena',
                'municipality_id' => 358,
            ),
            359 => 
            array (
                'id' => 860,
                'name' => 'Panamericano',
                'municipality_id' => 359,
            ),
            360 => 
            array (
                'id' => 861,
                'name' => 'La Palmita',
                'municipality_id' => 359,
            ),
            361 => 
            array (
                'id' => 862,
                'name' => 'Pedro María Ureña',
                'municipality_id' => 360,
            ),
            362 => 
            array (
                'id' => 863,
                'name' => 'Nueva Arcadia',
                'municipality_id' => 360,
            ),
            363 => 
            array (
                'id' => 864,
                'name' => 'Delicias',
                'municipality_id' => 361,
            ),
            364 => 
            array (
                'id' => 865,
                'name' => 'Pecaya',
                'municipality_id' => 361,
            ),
            365 => 
            array (
                'id' => 866,
                'name' => 'Samuel Darío Maldonado',
                'municipality_id' => 362,
            ),
            366 => 
            array (
                'id' => 867,
                'name' => 'Boconó',
                'municipality_id' => 362,
            ),
            367 => 
            array (
                'id' => 868,
                'name' => 'Hernández',
                'municipality_id' => 362,
            ),
            368 => 
            array (
                'id' => 869,
                'name' => 'La Concordia',
                'municipality_id' => 363,
            ),
            369 => 
            array (
                'id' => 870,
                'name' => 'San Juan Bautista',
                'municipality_id' => 363,
            ),
            370 => 
            array (
                'id' => 871,
                'name' => 'Pedro María Morantes',
                'municipality_id' => 363,
            ),
            371 => 
            array (
                'id' => 872,
                'name' => 'San Sebastián',
                'municipality_id' => 363,
            ),
            372 => 
            array (
                'id' => 873,
                'name' => 'Dr. Francisco Romero Lobo',
                'municipality_id' => 363,
            ),
            373 => 
            array (
                'id' => 874,
                'name' => 'Seboruco',
                'municipality_id' => 364,
            ),
            374 => 
            array (
                'id' => 875,
                'name' => 'Simón Rodríguez',
                'municipality_id' => 365,
            ),
            375 => 
            array (
                'id' => 876,
                'name' => 'Sucre',
                'municipality_id' => 366,
            ),
            376 => 
            array (
                'id' => 877,
                'name' => 'Eleazar López Contreras',
                'municipality_id' => 366,
            ),
            377 => 
            array (
                'id' => 878,
                'name' => 'San Pablo',
                'municipality_id' => 366,
            ),
            378 => 
            array (
                'id' => 879,
                'name' => 'Torbes',
                'municipality_id' => 367,
            ),
            379 => 
            array (
                'id' => 880,
                'name' => 'Uribante',
                'municipality_id' => 368,
            ),
            380 => 
            array (
                'id' => 881,
                'name' => 'Cárdenas',
                'municipality_id' => 368,
            ),
            381 => 
            array (
                'id' => 882,
                'name' => 'Juan Pablo Peñalosa',
                'municipality_id' => 368,
            ),
            382 => 
            array (
                'id' => 883,
                'name' => 'Potosí',
                'municipality_id' => 368,
            ),
            383 => 
            array (
                'id' => 884,
                'name' => 'San Judas Tadeo',
                'municipality_id' => 369,
            ),
            384 => 
            array (
                'id' => 885,
                'name' => 'Araguaney',
                'municipality_id' => 370,
            ),
            385 => 
            array (
                'id' => 886,
                'name' => 'El Jaguito',
                'municipality_id' => 370,
            ),
            386 => 
            array (
                'id' => 887,
                'name' => 'La Esperanza',
                'municipality_id' => 370,
            ),
            387 => 
            array (
                'id' => 888,
                'name' => 'Santa Isabel',
                'municipality_id' => 370,
            ),
            388 => 
            array (
                'id' => 889,
                'name' => 'Boconó',
                'municipality_id' => 371,
            ),
            389 => 
            array (
                'id' => 890,
                'name' => 'El Carmen',
                'municipality_id' => 371,
            ),
            390 => 
            array (
                'id' => 891,
                'name' => 'Mosquey',
                'municipality_id' => 371,
            ),
            391 => 
            array (
                'id' => 892,
                'name' => 'Ayacucho',
                'municipality_id' => 371,
            ),
            392 => 
            array (
                'id' => 893,
                'name' => 'Burbusay',
                'municipality_id' => 371,
            ),
            393 => 
            array (
                'id' => 894,
                'name' => 'General Ribas',
                'municipality_id' => 371,
            ),
            394 => 
            array (
                'id' => 895,
                'name' => 'Guaramacal',
                'municipality_id' => 371,
            ),
            395 => 
            array (
                'id' => 896,
                'name' => 'Vega de Guaramacal',
                'municipality_id' => 371,
            ),
            396 => 
            array (
                'id' => 897,
                'name' => 'Monseñor Jáuregui',
                'municipality_id' => 371,
            ),
            397 => 
            array (
                'id' => 898,
                'name' => 'Rafael Rangel',
                'municipality_id' => 371,
            ),
            398 => 
            array (
                'id' => 899,
                'name' => 'San Miguel',
                'municipality_id' => 371,
            ),
            399 => 
            array (
                'id' => 900,
                'name' => 'San José',
                'municipality_id' => 371,
            ),
            400 => 
            array (
                'id' => 901,
                'name' => 'Sabana Grande',
                'municipality_id' => 372,
            ),
            401 => 
            array (
                'id' => 902,
                'name' => 'Cheregüé',
                'municipality_id' => 372,
            ),
            402 => 
            array (
                'id' => 903,
                'name' => 'Granados',
                'municipality_id' => 372,
            ),
            403 => 
            array (
                'id' => 904,
                'name' => 'Arnoldo Gabaldón',
                'municipality_id' => 373,
            ),
            404 => 
            array (
                'id' => 905,
                'name' => 'Bolivia',
                'municipality_id' => 373,
            ),
            405 => 
            array (
                'id' => 906,
                'name' => 'Carrillo',
                'municipality_id' => 373,
            ),
            406 => 
            array (
                'id' => 907,
                'name' => 'Cegarra',
                'municipality_id' => 373,
            ),
            407 => 
            array (
                'id' => 908,
                'name' => 'Chejendé',
                'municipality_id' => 373,
            ),
            408 => 
            array (
                'id' => 909,
                'name' => 'Manuel Salvador Ulloa',
                'municipality_id' => 373,
            ),
            409 => 
            array (
                'id' => 910,
                'name' => 'San José',
                'municipality_id' => 373,
            ),
            410 => 
            array (
                'id' => 911,
                'name' => 'Carache',
                'municipality_id' => 374,
            ),
            411 => 
            array (
                'id' => 912,
                'name' => 'La Concepción',
                'municipality_id' => 374,
            ),
            412 => 
            array (
                'id' => 913,
                'name' => 'Cuicas',
                'municipality_id' => 374,
            ),
            413 => 
            array (
                'id' => 914,
                'name' => 'Panamericana',
                'municipality_id' => 374,
            ),
            414 => 
            array (
                'id' => 915,
                'name' => 'Santa Cruz',
                'municipality_id' => 374,
            ),
            415 => 
            array (
                'id' => 916,
                'name' => 'Escuque',
                'municipality_id' => 375,
            ),
            416 => 
            array (
                'id' => 917,
                'name' => 'La Unión',
                'municipality_id' => 375,
            ),
            417 => 
            array (
                'id' => 918,
                'name' => 'Santa Rita',
                'municipality_id' => 375,
            ),
            418 => 
            array (
                'id' => 919,
                'name' => 'Sabana Libre',
                'municipality_id' => 375,
            ),
            419 => 
            array (
                'id' => 920,
                'name' => 'El Socorro',
                'municipality_id' => 376,
            ),
            420 => 
            array (
                'id' => 921,
                'name' => 'Los Caprichos',
                'municipality_id' => 376,
            ),
            421 => 
            array (
                'id' => 922,
                'name' => 'Antonio José de Sucre',
                'municipality_id' => 376,
            ),
            422 => 
            array (
                'id' => 923,
                'name' => 'Campo Elías',
                'municipality_id' => 377,
            ),
            423 => 
            array (
                'id' => 924,
                'name' => 'Arnoldo Gabaldón',
                'municipality_id' => 377,
            ),
            424 => 
            array (
                'id' => 925,
                'name' => 'Santa Apolonia',
                'municipality_id' => 378,
            ),
            425 => 
            array (
                'id' => 926,
                'name' => 'El Progreso',
                'municipality_id' => 378,
            ),
            426 => 
            array (
                'id' => 927,
                'name' => 'La Ceiba',
                'municipality_id' => 378,
            ),
            427 => 
            array (
                'id' => 928,
                'name' => 'Tres de Febrero',
                'municipality_id' => 378,
            ),
            428 => 
            array (
                'id' => 929,
                'name' => 'El Dividive',
                'municipality_id' => 379,
            ),
            429 => 
            array (
                'id' => 930,
                'name' => 'Agua Santa',
                'municipality_id' => 379,
            ),
            430 => 
            array (
                'id' => 931,
                'name' => 'Agua Caliente',
                'municipality_id' => 379,
            ),
            431 => 
            array (
                'id' => 932,
                'name' => 'El Cenizo',
                'municipality_id' => 379,
            ),
            432 => 
            array (
                'id' => 933,
                'name' => 'Valerita',
                'municipality_id' => 379,
            ),
            433 => 
            array (
                'id' => 934,
                'name' => 'Monte Carmelo',
                'municipality_id' => 380,
            ),
            434 => 
            array (
                'id' => 935,
                'name' => 'Buena Vista',
                'municipality_id' => 380,
            ),
            435 => 
            array (
                'id' => 936,
                'name' => 'Santa María del Horcón',
                'municipality_id' => 380,
            ),
            436 => 
            array (
                'id' => 937,
                'name' => 'Motatán',
                'municipality_id' => 381,
            ),
            437 => 
            array (
                'id' => 938,
                'name' => 'El Baño',
                'municipality_id' => 381,
            ),
            438 => 
            array (
                'id' => 939,
                'name' => 'Jalisco',
                'municipality_id' => 381,
            ),
            439 => 
            array (
                'id' => 940,
                'name' => 'Pampán',
                'municipality_id' => 382,
            ),
            440 => 
            array (
                'id' => 941,
                'name' => 'Flor de Patria',
                'municipality_id' => 382,
            ),
            441 => 
            array (
                'id' => 942,
                'name' => 'La Paz',
                'municipality_id' => 382,
            ),
            442 => 
            array (
                'id' => 943,
                'name' => 'Santa Ana',
                'municipality_id' => 382,
            ),
            443 => 
            array (
                'id' => 944,
                'name' => 'Pampanito',
                'municipality_id' => 383,
            ),
            444 => 
            array (
                'id' => 945,
                'name' => 'La Concepción',
                'municipality_id' => 383,
            ),
            445 => 
            array (
                'id' => 946,
                'name' => 'Pampanito II',
                'municipality_id' => 383,
            ),
            446 => 
            array (
                'id' => 947,
                'name' => 'Betijoque',
                'municipality_id' => 384,
            ),
            447 => 
            array (
                'id' => 948,
                'name' => 'José Gregorio Hernández',
                'municipality_id' => 384,
            ),
            448 => 
            array (
                'id' => 949,
                'name' => 'La Pueblita',
                'municipality_id' => 384,
            ),
            449 => 
            array (
                'id' => 950,
                'name' => 'Los Cedros',
                'municipality_id' => 384,
            ),
            450 => 
            array (
                'id' => 951,
                'name' => 'Carvajal',
                'municipality_id' => 385,
            ),
            451 => 
            array (
                'id' => 952,
                'name' => 'Campo Alegre',
                'municipality_id' => 385,
            ),
            452 => 
            array (
                'id' => 953,
                'name' => 'Antonio Nicolás Briceño',
                'municipality_id' => 385,
            ),
            453 => 
            array (
                'id' => 954,
                'name' => 'José Leonardo Suárez',
                'municipality_id' => 385,
            ),
            454 => 
            array (
                'id' => 955,
                'name' => 'Sabana de Mendoza',
                'municipality_id' => 386,
            ),
            455 => 
            array (
                'id' => 956,
                'name' => 'Junín',
                'municipality_id' => 386,
            ),
            456 => 
            array (
                'id' => 957,
                'name' => 'Valmore Rodríguez',
                'municipality_id' => 386,
            ),
            457 => 
            array (
                'id' => 958,
                'name' => 'El Paraíso',
                'municipality_id' => 386,
            ),
            458 => 
            array (
                'id' => 959,
                'name' => 'Andrés Linares',
                'municipality_id' => 387,
            ),
            459 => 
            array (
                'id' => 960,
                'name' => 'Chiquinquirá',
                'municipality_id' => 387,
            ),
            460 => 
            array (
                'id' => 961,
                'name' => 'Cristóbal Mendoza',
                'municipality_id' => 387,
            ),
            461 => 
            array (
                'id' => 962,
                'name' => 'Cruz Carrillo',
                'municipality_id' => 387,
            ),
            462 => 
            array (
                'id' => 963,
                'name' => 'Matriz',
                'municipality_id' => 387,
            ),
            463 => 
            array (
                'id' => 964,
                'name' => 'Monseñor Carrillo',
                'municipality_id' => 387,
            ),
            464 => 
            array (
                'id' => 965,
                'name' => 'Tres Esquinas',
                'municipality_id' => 387,
            ),
            465 => 
            array (
                'id' => 966,
                'name' => 'Cabimbú',
                'municipality_id' => 388,
            ),
            466 => 
            array (
                'id' => 967,
                'name' => 'Jajó',
                'municipality_id' => 388,
            ),
            467 => 
            array (
                'id' => 968,
                'name' => 'La Mesa de Esnujaque',
                'municipality_id' => 388,
            ),
            468 => 
            array (
                'id' => 969,
                'name' => 'Santiago',
                'municipality_id' => 388,
            ),
            469 => 
            array (
                'id' => 970,
                'name' => 'Tuñame',
                'municipality_id' => 388,
            ),
            470 => 
            array (
                'id' => 971,
                'name' => 'La Quebrada',
                'municipality_id' => 388,
            ),
            471 => 
            array (
                'id' => 972,
                'name' => 'Juan Ignacio Montilla',
                'municipality_id' => 389,
            ),
            472 => 
            array (
                'id' => 973,
                'name' => 'La Beatriz',
                'municipality_id' => 389,
            ),
            473 => 
            array (
                'id' => 974,
                'name' => 'La Puerta',
                'municipality_id' => 389,
            ),
            474 => 
            array (
                'id' => 975,
                'name' => 'Mendoza del Valle de Momboy',
                'municipality_id' => 389,
            ),
            475 => 
            array (
                'id' => 976,
                'name' => 'Mercedes Díaz',
                'municipality_id' => 389,
            ),
            476 => 
            array (
                'id' => 977,
                'name' => 'San Luis',
                'municipality_id' => 389,
            ),
            477 => 
            array (
                'id' => 978,
                'name' => 'Caraballeda',
                'municipality_id' => 390,
            ),
            478 => 
            array (
                'id' => 979,
                'name' => 'Carayaca',
                'municipality_id' => 390,
            ),
            479 => 
            array (
                'id' => 980,
                'name' => 'Carlos Soublette',
                'municipality_id' => 390,
            ),
            480 => 
            array (
                'id' => 981,
                'name' => 'Caruao Chuspa',
                'municipality_id' => 390,
            ),
            481 => 
            array (
                'id' => 982,
                'name' => 'Catia La Mar',
                'municipality_id' => 390,
            ),
            482 => 
            array (
                'id' => 983,
                'name' => 'El Junko',
                'municipality_id' => 390,
            ),
            483 => 
            array (
                'id' => 984,
                'name' => 'La Guaira',
                'municipality_id' => 390,
            ),
            484 => 
            array (
                'id' => 985,
                'name' => 'Macuto',
                'municipality_id' => 390,
            ),
            485 => 
            array (
                'id' => 986,
                'name' => 'Maiquetía',
                'municipality_id' => 390,
            ),
            486 => 
            array (
                'id' => 987,
                'name' => 'Naiguatá',
                'municipality_id' => 390,
            ),
            487 => 
            array (
                'id' => 988,
                'name' => 'Urimare',
                'municipality_id' => 390,
            ),
            488 => 
            array (
                'id' => 989,
                'name' => 'Arístides Bastidas',
                'municipality_id' => 391,
            ),
            489 => 
            array (
                'id' => 990,
                'name' => 'Bolívar',
                'municipality_id' => 392,
            ),
            490 => 
            array (
                'id' => 991,
                'name' => 'Chivacoa',
                'municipality_id' => 407,
            ),
            491 => 
            array (
                'id' => 992,
                'name' => 'Campo Elías',
                'municipality_id' => 407,
            ),
            492 => 
            array (
                'id' => 993,
                'name' => 'Cocorote',
                'municipality_id' => 408,
            ),
            493 => 
            array (
                'id' => 994,
                'name' => 'Independencia',
                'municipality_id' => 409,
            ),
            494 => 
            array (
                'id' => 995,
                'name' => 'José Antonio Páez',
                'municipality_id' => 410,
            ),
            495 => 
            array (
                'id' => 996,
                'name' => 'La Trinidad',
                'municipality_id' => 411,
            ),
            496 => 
            array (
                'id' => 997,
                'name' => 'Manuel Monge',
                'municipality_id' => 412,
            ),
            497 => 
            array (
                'id' => 998,
                'name' => 'Salóm',
                'municipality_id' => 413,
            ),
            498 => 
            array (
                'id' => 999,
                'name' => 'Temerla',
                'municipality_id' => 413,
            ),
            499 => 
            array (
                'id' => 1000,
                'name' => 'Nirgua',
                'municipality_id' => 413,
            ),
        ));
        \DB::table('parishes')->insert(array (
            0 => 
            array (
                'id' => 1001,
                'name' => 'San Andrés',
                'municipality_id' => 414,
            ),
            1 => 
            array (
                'id' => 1002,
                'name' => 'Yaritagua',
                'municipality_id' => 414,
            ),
            2 => 
            array (
                'id' => 1003,
                'name' => 'San Javier',
                'municipality_id' => 415,
            ),
            3 => 
            array (
                'id' => 1004,
                'name' => 'Albarico',
                'municipality_id' => 415,
            ),
            4 => 
            array (
                'id' => 1005,
                'name' => 'San Felipe',
                'municipality_id' => 415,
            ),
            5 => 
            array (
                'id' => 1006,
                'name' => 'Sucre',
                'municipality_id' => 416,
            ),
            6 => 
            array (
                'id' => 1007,
                'name' => 'Urachiche',
                'municipality_id' => 417,
            ),
            7 => 
            array (
                'id' => 1008,
                'name' => 'El Guayabo',
                'municipality_id' => 418,
            ),
            8 => 
            array (
                'id' => 1009,
                'name' => 'Farriar',
                'municipality_id' => 418,
            ),
            9 => 
            array (
                'id' => 1010,
                'name' => 'Isla de Toas',
                'municipality_id' => 441,
            ),
            10 => 
            array (
                'id' => 1011,
                'name' => 'Monagas',
                'municipality_id' => 441,
            ),
            11 => 
            array (
                'id' => 1012,
                'name' => 'San Timoteo',
                'municipality_id' => 442,
            ),
            12 => 
            array (
                'id' => 1013,
                'name' => 'General Urdaneta',
                'municipality_id' => 442,
            ),
            13 => 
            array (
                'id' => 1014,
                'name' => 'Libertador',
                'municipality_id' => 442,
            ),
            14 => 
            array (
                'id' => 1015,
                'name' => 'Marcelino Briceño',
                'municipality_id' => 442,
            ),
            15 => 
            array (
                'id' => 1016,
                'name' => 'Pueblo Nuevo',
                'municipality_id' => 442,
            ),
            16 => 
            array (
                'id' => 1017,
                'name' => 'Manuel Guanipa Matos',
                'municipality_id' => 442,
            ),
            17 => 
            array (
                'id' => 1018,
                'name' => 'Ambrosio',
                'municipality_id' => 443,
            ),
            18 => 
            array (
                'id' => 1019,
                'name' => 'Carmen Herrera',
                'municipality_id' => 443,
            ),
            19 => 
            array (
                'id' => 1020,
                'name' => 'La Rosa',
                'municipality_id' => 443,
            ),
            20 => 
            array (
                'id' => 1021,
                'name' => 'Germán Ríos Linares',
                'municipality_id' => 443,
            ),
            21 => 
            array (
                'id' => 1022,
                'name' => 'San Benito',
                'municipality_id' => 443,
            ),
            22 => 
            array (
                'id' => 1023,
                'name' => 'Rómulo Betancourt',
                'municipality_id' => 443,
            ),
            23 => 
            array (
                'id' => 1024,
                'name' => 'Jorge Hernández',
                'municipality_id' => 443,
            ),
            24 => 
            array (
                'id' => 1025,
                'name' => 'Punta Gorda',
                'municipality_id' => 443,
            ),
            25 => 
            array (
                'id' => 1026,
                'name' => 'Arístides Calvani',
                'municipality_id' => 443,
            ),
            26 => 
            array (
                'id' => 1027,
                'name' => 'Encontrados',
                'municipality_id' => 444,
            ),
            27 => 
            array (
                'id' => 1028,
                'name' => 'Udón Pérez',
                'municipality_id' => 444,
            ),
            28 => 
            array (
                'id' => 1029,
                'name' => 'Moralito',
                'municipality_id' => 445,
            ),
            29 => 
            array (
                'id' => 1030,
                'name' => 'San Carlos del Zulia',
                'municipality_id' => 445,
            ),
            30 => 
            array (
                'id' => 1031,
                'name' => 'Santa Cruz del Zulia',
                'municipality_id' => 445,
            ),
            31 => 
            array (
                'id' => 1032,
                'name' => 'Santa Bárbara',
                'municipality_id' => 445,
            ),
            32 => 
            array (
                'id' => 1033,
                'name' => 'Urribarrí',
                'municipality_id' => 445,
            ),
            33 => 
            array (
                'id' => 1034,
                'name' => 'Carlos Quevedo',
                'municipality_id' => 446,
            ),
            34 => 
            array (
                'id' => 1035,
                'name' => 'Francisco Javier Pulgar',
                'municipality_id' => 446,
            ),
            35 => 
            array (
                'id' => 1036,
                'name' => 'Simón Rodríguez',
                'municipality_id' => 446,
            ),
            36 => 
            array (
                'id' => 1037,
                'name' => 'Guamo-Gavilanes',
                'municipality_id' => 446,
            ),
            37 => 
            array (
                'id' => 1038,
                'name' => 'La Concepción',
                'municipality_id' => 448,
            ),
            38 => 
            array (
                'id' => 1039,
                'name' => 'San José',
                'municipality_id' => 448,
            ),
            39 => 
            array (
                'id' => 1040,
                'name' => 'Mariano Parra León',
                'municipality_id' => 448,
            ),
            40 => 
            array (
                'id' => 1041,
                'name' => 'José Ramón Yépez',
                'municipality_id' => 448,
            ),
            41 => 
            array (
                'id' => 1042,
                'name' => 'Jesús María Semprún',
                'municipality_id' => 449,
            ),
            42 => 
            array (
                'id' => 1043,
                'name' => 'Barí',
                'municipality_id' => 449,
            ),
            43 => 
            array (
                'id' => 1044,
                'name' => 'Concepción',
                'municipality_id' => 450,
            ),
            44 => 
            array (
                'id' => 1045,
                'name' => 'Andrés Bello',
                'municipality_id' => 450,
            ),
            45 => 
            array (
                'id' => 1046,
                'name' => 'Chiquinquirá',
                'municipality_id' => 450,
            ),
            46 => 
            array (
                'id' => 1047,
                'name' => 'El Carmelo',
                'municipality_id' => 450,
            ),
            47 => 
            array (
                'id' => 1048,
                'name' => 'Potreritos',
                'municipality_id' => 450,
            ),
            48 => 
            array (
                'id' => 1049,
                'name' => 'Libertad',
                'municipality_id' => 451,
            ),
            49 => 
            array (
                'id' => 1050,
                'name' => 'Alonso de Ojeda',
                'municipality_id' => 451,
            ),
            50 => 
            array (
                'id' => 1051,
                'name' => 'Venezuela',
                'municipality_id' => 451,
            ),
            51 => 
            array (
                'id' => 1052,
                'name' => 'Eleazar López Contreras',
                'municipality_id' => 451,
            ),
            52 => 
            array (
                'id' => 1053,
                'name' => 'Campo Lara',
                'municipality_id' => 451,
            ),
            53 => 
            array (
                'id' => 1054,
                'name' => 'Bartolomé de las Casas',
                'municipality_id' => 452,
            ),
            54 => 
            array (
                'id' => 1055,
                'name' => 'Libertad',
                'municipality_id' => 452,
            ),
            55 => 
            array (
                'id' => 1056,
                'name' => 'Río Negro',
                'municipality_id' => 452,
            ),
            56 => 
            array (
                'id' => 1057,
                'name' => 'San José de Perijá',
                'municipality_id' => 452,
            ),
            57 => 
            array (
                'id' => 1058,
                'name' => 'San Rafael',
                'municipality_id' => 453,
            ),
            58 => 
            array (
                'id' => 1059,
                'name' => 'La Sierrita',
                'municipality_id' => 453,
            ),
            59 => 
            array (
                'id' => 1060,
                'name' => 'Las Parcelas',
                'municipality_id' => 453,
            ),
            60 => 
            array (
                'id' => 1061,
                'name' => 'Luis de Vicente',
                'municipality_id' => 453,
            ),
            61 => 
            array (
                'id' => 1062,
                'name' => 'Monseñor Marcos Sergio Godoy',
                'municipality_id' => 453,
            ),
            62 => 
            array (
                'id' => 1063,
                'name' => 'Ricaurte',
                'municipality_id' => 453,
            ),
            63 => 
            array (
                'id' => 1064,
                'name' => 'Tamare',
                'municipality_id' => 453,
            ),
            64 => 
            array (
                'id' => 1065,
                'name' => 'Antonio Borjas Romero',
                'municipality_id' => 454,
            ),
            65 => 
            array (
                'id' => 1066,
                'name' => 'Bolívar',
                'municipality_id' => 454,
            ),
            66 => 
            array (
                'id' => 1067,
                'name' => 'Cacique Mara',
                'municipality_id' => 454,
            ),
            67 => 
            array (
                'id' => 1068,
                'name' => 'Carracciolo Parra Pérez',
                'municipality_id' => 454,
            ),
            68 => 
            array (
                'id' => 1069,
                'name' => 'Cecilio Acosta',
                'municipality_id' => 454,
            ),
            69 => 
            array (
                'id' => 1070,
                'name' => 'Cristo de Aranza',
                'municipality_id' => 454,
            ),
            70 => 
            array (
                'id' => 1071,
                'name' => 'Coquivacoa',
                'municipality_id' => 454,
            ),
            71 => 
            array (
                'id' => 1072,
                'name' => 'Chiquinquirá',
                'municipality_id' => 454,
            ),
            72 => 
            array (
                'id' => 1073,
                'name' => 'Francisco Eugenio Bustamante',
                'municipality_id' => 454,
            ),
            73 => 
            array (
                'id' => 1074,
                'name' => 'Idelfonzo Vásquez',
                'municipality_id' => 454,
            ),
            74 => 
            array (
                'id' => 1075,
                'name' => 'Juana de Ávila',
                'municipality_id' => 454,
            ),
            75 => 
            array (
                'id' => 1076,
                'name' => 'Luis Hurtado Higuera',
                'municipality_id' => 454,
            ),
            76 => 
            array (
                'id' => 1077,
                'name' => 'Manuel Dagnino',
                'municipality_id' => 454,
            ),
            77 => 
            array (
                'id' => 1078,
                'name' => 'Olegario Villalobos',
                'municipality_id' => 454,
            ),
            78 => 
            array (
                'id' => 1079,
                'name' => 'Raúl Leoni',
                'municipality_id' => 454,
            ),
            79 => 
            array (
                'id' => 1080,
                'name' => 'Santa Lucía',
                'municipality_id' => 454,
            ),
            80 => 
            array (
                'id' => 1081,
                'name' => 'Venancio Pulgar',
                'municipality_id' => 454,
            ),
            81 => 
            array (
                'id' => 1082,
                'name' => 'San Isidro',
                'municipality_id' => 454,
            ),
            82 => 
            array (
                'id' => 1083,
                'name' => 'Altagracia',
                'municipality_id' => 455,
            ),
            83 => 
            array (
                'id' => 1084,
                'name' => 'Faría',
                'municipality_id' => 455,
            ),
            84 => 
            array (
                'id' => 1085,
                'name' => 'Ana María Campos',
                'municipality_id' => 455,
            ),
            85 => 
            array (
                'id' => 1086,
                'name' => 'San Antonio',
                'municipality_id' => 455,
            ),
            86 => 
            array (
                'id' => 1087,
                'name' => 'San José',
                'municipality_id' => 455,
            ),
            87 => 
            array (
                'id' => 1088,
                'name' => 'Donaldo García',
                'municipality_id' => 456,
            ),
            88 => 
            array (
                'id' => 1089,
                'name' => 'El Rosario',
                'municipality_id' => 456,
            ),
            89 => 
            array (
                'id' => 1090,
                'name' => 'Sixto Zambrano',
                'municipality_id' => 456,
            ),
            90 => 
            array (
                'id' => 1091,
                'name' => 'San Francisco',
                'municipality_id' => 457,
            ),
            91 => 
            array (
                'id' => 1092,
                'name' => 'El Bajo',
                'municipality_id' => 457,
            ),
            92 => 
            array (
                'id' => 1093,
                'name' => 'Domitila Flores',
                'municipality_id' => 457,
            ),
            93 => 
            array (
                'id' => 1094,
                'name' => 'Francisco Ochoa',
                'municipality_id' => 457,
            ),
            94 => 
            array (
                'id' => 1095,
                'name' => 'Los Cortijos',
                'municipality_id' => 457,
            ),
            95 => 
            array (
                'id' => 1096,
                'name' => 'Marcial Hernández',
                'municipality_id' => 457,
            ),
            96 => 
            array (
                'id' => 1097,
                'name' => 'Santa Rita',
                'municipality_id' => 458,
            ),
            97 => 
            array (
                'id' => 1098,
                'name' => 'El Mene',
                'municipality_id' => 458,
            ),
            98 => 
            array (
                'id' => 1099,
                'name' => 'Pedro Lucas Urribarrí',
                'municipality_id' => 458,
            ),
            99 => 
            array (
                'id' => 1100,
                'name' => 'José Cenobio Urribarrí',
                'municipality_id' => 458,
            ),
            100 => 
            array (
                'id' => 1101,
                'name' => 'Rafael Maria Baralt',
                'municipality_id' => 459,
            ),
            101 => 
            array (
                'id' => 1102,
                'name' => 'Manuel Manrique',
                'municipality_id' => 459,
            ),
            102 => 
            array (
                'id' => 1103,
                'name' => 'Rafael Urdaneta',
                'municipality_id' => 459,
            ),
            103 => 
            array (
                'id' => 1104,
                'name' => 'Bobures',
                'municipality_id' => 460,
            ),
            104 => 
            array (
                'id' => 1105,
                'name' => 'Gibraltar',
                'municipality_id' => 460,
            ),
            105 => 
            array (
                'id' => 1106,
                'name' => 'Heras',
                'municipality_id' => 460,
            ),
            106 => 
            array (
                'id' => 1107,
                'name' => 'Monseñor Arturo Álvarez',
                'municipality_id' => 460,
            ),
            107 => 
            array (
                'id' => 1108,
                'name' => 'Rómulo Gallegos',
                'municipality_id' => 460,
            ),
            108 => 
            array (
                'id' => 1109,
                'name' => 'El Batey',
                'municipality_id' => 460,
            ),
            109 => 
            array (
                'id' => 1110,
                'name' => 'Rafael Urdaneta',
                'municipality_id' => 461,
            ),
            110 => 
            array (
                'id' => 1111,
                'name' => 'La Victoria',
                'municipality_id' => 461,
            ),
            111 => 
            array (
                'id' => 1112,
                'name' => 'Raúl Cuenca',
                'municipality_id' => 461,
            ),
            112 => 
            array (
                'id' => 1113,
                'name' => 'Sinamaica',
                'municipality_id' => 447,
            ),
            113 => 
            array (
                'id' => 1114,
                'name' => 'Alta Guajira',
                'municipality_id' => 447,
            ),
            114 => 
            array (
                'id' => 1115,
                'name' => 'Elías Sánchez Rubio',
                'municipality_id' => 447,
            ),
            115 => 
            array (
                'id' => 1116,
                'name' => 'Guajira',
                'municipality_id' => 447,
            ),
            116 => 
            array (
                'id' => 1117,
                'name' => 'Altagracia',
                'municipality_id' => 462,
            ),
            117 => 
            array (
                'id' => 1118,
                'name' => 'Antímano',
                'municipality_id' => 462,
            ),
            118 => 
            array (
                'id' => 1119,
                'name' => 'Caricuao',
                'municipality_id' => 462,
            ),
            119 => 
            array (
                'id' => 1120,
                'name' => 'Catedral',
                'municipality_id' => 462,
            ),
            120 => 
            array (
                'id' => 1121,
                'name' => 'Coche',
                'municipality_id' => 462,
            ),
            121 => 
            array (
                'id' => 1122,
                'name' => 'El Junquito',
                'municipality_id' => 462,
            ),
            122 => 
            array (
                'id' => 1123,
                'name' => 'El Paraíso',
                'municipality_id' => 462,
            ),
            123 => 
            array (
                'id' => 1124,
                'name' => 'El Recreo',
                'municipality_id' => 462,
            ),
            124 => 
            array (
                'id' => 1125,
                'name' => 'El Valle',
                'municipality_id' => 462,
            ),
            125 => 
            array (
                'id' => 1126,
                'name' => 'La Candelaria',
                'municipality_id' => 462,
            ),
            126 => 
            array (
                'id' => 1127,
                'name' => 'La Pastora',
                'municipality_id' => 462,
            ),
            127 => 
            array (
                'id' => 1128,
                'name' => 'La Vega',
                'municipality_id' => 462,
            ),
            128 => 
            array (
                'id' => 1129,
                'name' => 'Macarao',
                'municipality_id' => 462,
            ),
            129 => 
            array (
                'id' => 1130,
                'name' => 'San Agustín',
                'municipality_id' => 462,
            ),
            130 => 
            array (
                'id' => 1131,
                'name' => 'San Bernardino',
                'municipality_id' => 462,
            ),
            131 => 
            array (
                'id' => 1132,
                'name' => 'San José',
                'municipality_id' => 462,
            ),
            132 => 
            array (
                'id' => 1133,
                'name' => 'San Juan',
                'municipality_id' => 462,
            ),
            133 => 
            array (
                'id' => 1134,
                'name' => 'San Pedro',
                'municipality_id' => 462,
            ),
            134 => 
            array (
                'id' => 1135,
                'name' => 'Santa Rosalía',
                'municipality_id' => 462,
            ),
            135 => 
            array (
                'id' => 1136,
                'name' => 'Santa Teresa',
                'municipality_id' => 462,
            ),
            136 => 
            array (
                'id' => 1137,
            'name' => 'Sucre (Catia)',
                'municipality_id' => 462,
            ),
            137 => 
            array (
                'id' => 1138,
                'name' => '23 de enero',
                'municipality_id' => 462,
            ),
            138 => 
            array (
                'id' => 0,
                'name' => 'Oculto',
                'municipality_id' => 0,
            ),
        ));
        
        
    }
}