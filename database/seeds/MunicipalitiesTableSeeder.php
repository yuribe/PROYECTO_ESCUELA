<?php

use Illuminate\Database\Seeder;

class MunicipalitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('municipalities')->delete();
        
        \DB::table('municipalities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Alto Orinoco',
                'state_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Atabapo',
                'state_id' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Atures',
                'state_id' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Autana',
                'state_id' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Manapiare',
                'state_id' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Maroa',
                'state_id' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Río Negro',
                'state_id' => 1,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Anaco',
                'state_id' => 2,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Aragua',
                'state_id' => 2,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Manuel Ezequiel Bruzual',
                'state_id' => 2,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Diego Bautista Urbaneja',
                'state_id' => 2,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Fernando Peñalver',
                'state_id' => 2,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Francisco Del Carmen Carvajal',
                'state_id' => 2,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'General Sir Arthur McGregor',
                'state_id' => 2,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Guanta',
                'state_id' => 2,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Independencia',
                'state_id' => 2,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'José Gregorio Monagas',
                'state_id' => 2,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Juan Antonio Sotillo',
                'state_id' => 2,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Juan Manuel Cajigal',
                'state_id' => 2,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Libertad',
                'state_id' => 2,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Francisco de Miranda',
                'state_id' => 2,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Pedro María Freites',
                'state_id' => 2,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Píritu',
                'state_id' => 2,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'San José de Guanipa',
                'state_id' => 2,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'San Juan de Capistrano',
                'state_id' => 2,
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Santa Ana',
                'state_id' => 2,
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Simón Bolívar',
                'state_id' => 2,
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Simón Rodríguez',
                'state_id' => 2,
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Achaguas',
                'state_id' => 3,
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Biruaca',
                'state_id' => 3,
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Muñóz',
                'state_id' => 3,
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Páez',
                'state_id' => 3,
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Pedro Camejo',
                'state_id' => 3,
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Rómulo Gallegos',
                'state_id' => 3,
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'San Fernando',
                'state_id' => 3,
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Atanasio Girardot',
                'state_id' => 4,
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Bolívar',
                'state_id' => 4,
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Camatagua',
                'state_id' => 4,
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Francisco Linares Alcántara',
                'state_id' => 4,
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'José Ángel Lamas',
                'state_id' => 4,
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'José Félix Ribas',
                'state_id' => 4,
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'José Rafael Revenga',
                'state_id' => 4,
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Libertador',
                'state_id' => 4,
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Mario Briceño Iragorry',
                'state_id' => 4,
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'Ocumare de la Costa de Oro',
                'state_id' => 4,
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'San Casimiro',
                'state_id' => 4,
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'San Sebastián',
                'state_id' => 4,
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Santiago Mariño',
                'state_id' => 4,
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Santos Michelena',
                'state_id' => 4,
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Sucre',
                'state_id' => 4,
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Tovar',
                'state_id' => 4,
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Urdaneta',
                'state_id' => 4,
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'Zamora',
                'state_id' => 4,
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'Alberto Arvelo Torrealba',
                'state_id' => 5,
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Andrés Eloy Blanco',
                'state_id' => 5,
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Antonio José de Sucre',
                'state_id' => 5,
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Arismendi',
                'state_id' => 5,
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Barinas',
                'state_id' => 5,
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Bolívar',
                'state_id' => 5,
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Cruz Paredes',
                'state_id' => 5,
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Ezequiel Zamora',
                'state_id' => 5,
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'Obispos',
                'state_id' => 5,
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Pedraza',
                'state_id' => 5,
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Rojas',
                'state_id' => 5,
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Sosa',
                'state_id' => 5,
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'Caroní',
                'state_id' => 6,
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'Cedeño',
                'state_id' => 6,
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'El Callao',
                'state_id' => 6,
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Gran Sabana',
                'state_id' => 6,
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Heres',
                'state_id' => 6,
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Piar',
                'state_id' => 6,
            ),
            71 => 
            array (
                'id' => 72,
            'name' => 'Angostura (Raúl Leoni)',
                'state_id' => 6,
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Roscio',
                'state_id' => 6,
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Sifontes',
                'state_id' => 6,
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'Sucre',
                'state_id' => 6,
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'Padre Pedro Chien',
                'state_id' => 6,
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'Bejuma',
                'state_id' => 7,
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'Carlos Arvelo',
                'state_id' => 7,
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'Diego Ibarra',
                'state_id' => 7,
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Guacara',
                'state_id' => 7,
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Juan José Mora',
                'state_id' => 7,
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Libertador',
                'state_id' => 7,
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Los Guayos',
                'state_id' => 7,
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Miranda',
                'state_id' => 7,
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Montalbán',
                'state_id' => 7,
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Naguanagua',
                'state_id' => 7,
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Puerto Cabello',
                'state_id' => 7,
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'San Diego',
                'state_id' => 7,
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'San Joaquín',
                'state_id' => 7,
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'Valencia',
                'state_id' => 7,
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Anzoátegui',
                'state_id' => 8,
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'Tinaquillo',
                'state_id' => 8,
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'Girardot',
                'state_id' => 8,
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Lima Blanco',
                'state_id' => 8,
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'Pao de San Juan Bautista',
                'state_id' => 8,
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'Ricaurte',
                'state_id' => 8,
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'Rómulo Gallegos',
                'state_id' => 8,
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'San Carlos',
                'state_id' => 8,
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Tinaco',
                'state_id' => 8,
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'Antonio Díaz',
                'state_id' => 9,
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'Casacoima',
                'state_id' => 9,
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'Pedernales',
                'state_id' => 9,
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'Tucupita',
                'state_id' => 9,
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'Acosta',
                'state_id' => 10,
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'Bolívar',
                'state_id' => 10,
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'Buchivacoa',
                'state_id' => 10,
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Cacique Manaure',
                'state_id' => 10,
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'Carirubana',
                'state_id' => 10,
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'Colina',
                'state_id' => 10,
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'Dabajuro',
                'state_id' => 10,
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Democracia',
                'state_id' => 10,
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Falcón',
                'state_id' => 10,
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'Federación',
                'state_id' => 10,
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'Jacura',
                'state_id' => 10,
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'José Laurencio Silva',
                'state_id' => 10,
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'Los Taques',
                'state_id' => 10,
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'Mauroa',
                'state_id' => 10,
            ),
            117 => 
            array (
                'id' => 118,
                'name' => 'Miranda',
                'state_id' => 10,
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'Monseñor Iturriza',
                'state_id' => 10,
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'Palmasola',
                'state_id' => 10,
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'Petit',
                'state_id' => 10,
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'Píritu',
                'state_id' => 10,
            ),
            122 => 
            array (
                'id' => 123,
                'name' => 'San Francisco',
                'state_id' => 10,
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'Sucre',
                'state_id' => 10,
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'Tocópero',
                'state_id' => 10,
            ),
            125 => 
            array (
                'id' => 126,
                'name' => 'Unión',
                'state_id' => 10,
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'Urumaco',
                'state_id' => 10,
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'Zamora',
                'state_id' => 10,
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'Camaguán',
                'state_id' => 11,
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'Chaguaramas',
                'state_id' => 11,
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'El Socorro',
                'state_id' => 11,
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'José Félix Ribas',
                'state_id' => 11,
            ),
            132 => 
            array (
                'id' => 133,
                'name' => 'José Tadeo Monagas',
                'state_id' => 11,
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'Juan Germán Roscio',
                'state_id' => 11,
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'Julián Mellado',
                'state_id' => 11,
            ),
            135 => 
            array (
                'id' => 136,
                'name' => 'Las Mercedes',
                'state_id' => 11,
            ),
            136 => 
            array (
                'id' => 137,
                'name' => 'Leonardo Infante',
                'state_id' => 11,
            ),
            137 => 
            array (
                'id' => 138,
                'name' => 'Pedro Zaraza',
                'state_id' => 11,
            ),
            138 => 
            array (
                'id' => 139,
                'name' => 'Ortíz',
                'state_id' => 11,
            ),
            139 => 
            array (
                'id' => 140,
                'name' => 'San Gerónimo de Guayabal',
                'state_id' => 11,
            ),
            140 => 
            array (
                'id' => 141,
                'name' => 'San José de Guaribe',
                'state_id' => 11,
            ),
            141 => 
            array (
                'id' => 142,
                'name' => 'Santa María de Ipire',
                'state_id' => 11,
            ),
            142 => 
            array (
                'id' => 143,
                'name' => 'Sebastián Francisco de Miranda',
                'state_id' => 11,
            ),
            143 => 
            array (
                'id' => 144,
                'name' => 'Andrés Eloy Blanco',
                'state_id' => 12,
            ),
            144 => 
            array (
                'id' => 145,
                'name' => 'Crespo',
                'state_id' => 12,
            ),
            145 => 
            array (
                'id' => 146,
                'name' => 'Iribarren',
                'state_id' => 12,
            ),
            146 => 
            array (
                'id' => 147,
                'name' => 'Jiménez',
                'state_id' => 12,
            ),
            147 => 
            array (
                'id' => 148,
                'name' => 'Morán',
                'state_id' => 12,
            ),
            148 => 
            array (
                'id' => 149,
                'name' => 'Palavecino',
                'state_id' => 12,
            ),
            149 => 
            array (
                'id' => 150,
                'name' => 'Simón Planas',
                'state_id' => 12,
            ),
            150 => 
            array (
                'id' => 151,
                'name' => 'Torres',
                'state_id' => 12,
            ),
            151 => 
            array (
                'id' => 152,
                'name' => 'Urdaneta',
                'state_id' => 12,
            ),
            152 => 
            array (
                'id' => 179,
                'name' => 'Alberto Adriani',
                'state_id' => 13,
            ),
            153 => 
            array (
                'id' => 180,
                'name' => 'Andrés Bello',
                'state_id' => 13,
            ),
            154 => 
            array (
                'id' => 181,
                'name' => 'Antonio Pinto Salinas',
                'state_id' => 13,
            ),
            155 => 
            array (
                'id' => 182,
                'name' => 'Aricagua',
                'state_id' => 13,
            ),
            156 => 
            array (
                'id' => 183,
                'name' => 'Arzobispo Chacón',
                'state_id' => 13,
            ),
            157 => 
            array (
                'id' => 184,
                'name' => 'Campo Elías',
                'state_id' => 13,
            ),
            158 => 
            array (
                'id' => 185,
                'name' => 'Caracciolo Parra Olmedo',
                'state_id' => 13,
            ),
            159 => 
            array (
                'id' => 186,
                'name' => 'Cardenal Quintero',
                'state_id' => 13,
            ),
            160 => 
            array (
                'id' => 187,
                'name' => 'Guaraque',
                'state_id' => 13,
            ),
            161 => 
            array (
                'id' => 188,
                'name' => 'Julio César Salas',
                'state_id' => 13,
            ),
            162 => 
            array (
                'id' => 189,
                'name' => 'Justo Briceño',
                'state_id' => 13,
            ),
            163 => 
            array (
                'id' => 190,
                'name' => 'Libertador',
                'state_id' => 13,
            ),
            164 => 
            array (
                'id' => 191,
                'name' => 'Miranda',
                'state_id' => 13,
            ),
            165 => 
            array (
                'id' => 192,
                'name' => 'Obispo Ramos de Lora',
                'state_id' => 13,
            ),
            166 => 
            array (
                'id' => 193,
                'name' => 'Padre Noguera',
                'state_id' => 13,
            ),
            167 => 
            array (
                'id' => 194,
                'name' => 'Pueblo Llano',
                'state_id' => 13,
            ),
            168 => 
            array (
                'id' => 195,
                'name' => 'Rangel',
                'state_id' => 13,
            ),
            169 => 
            array (
                'id' => 196,
                'name' => 'Rivas Dávila',
                'state_id' => 13,
            ),
            170 => 
            array (
                'id' => 197,
                'name' => 'Santos Marquina',
                'state_id' => 13,
            ),
            171 => 
            array (
                'id' => 198,
                'name' => 'Sucre',
                'state_id' => 13,
            ),
            172 => 
            array (
                'id' => 199,
                'name' => 'Tovar',
                'state_id' => 13,
            ),
            173 => 
            array (
                'id' => 200,
                'name' => 'Tulio Febres Cordero',
                'state_id' => 13,
            ),
            174 => 
            array (
                'id' => 201,
                'name' => 'Zea',
                'state_id' => 13,
            ),
            175 => 
            array (
                'id' => 223,
                'name' => 'Acevedo',
                'state_id' => 14,
            ),
            176 => 
            array (
                'id' => 224,
                'name' => 'Andrés Bello',
                'state_id' => 14,
            ),
            177 => 
            array (
                'id' => 225,
                'name' => 'Baruta',
                'state_id' => 14,
            ),
            178 => 
            array (
                'id' => 226,
                'name' => 'Brión',
                'state_id' => 14,
            ),
            179 => 
            array (
                'id' => 227,
                'name' => 'Buroz',
                'state_id' => 14,
            ),
            180 => 
            array (
                'id' => 228,
                'name' => 'Carrizal',
                'state_id' => 14,
            ),
            181 => 
            array (
                'id' => 229,
                'name' => 'Chacao',
                'state_id' => 14,
            ),
            182 => 
            array (
                'id' => 230,
                'name' => 'Cristóbal Rojas',
                'state_id' => 14,
            ),
            183 => 
            array (
                'id' => 231,
                'name' => 'El Hatillo',
                'state_id' => 14,
            ),
            184 => 
            array (
                'id' => 232,
                'name' => 'Guaicaipuro',
                'state_id' => 14,
            ),
            185 => 
            array (
                'id' => 233,
                'name' => 'Independencia',
                'state_id' => 14,
            ),
            186 => 
            array (
                'id' => 234,
                'name' => 'Lander',
                'state_id' => 14,
            ),
            187 => 
            array (
                'id' => 235,
                'name' => 'Los Salias',
                'state_id' => 14,
            ),
            188 => 
            array (
                'id' => 236,
                'name' => 'Páez',
                'state_id' => 14,
            ),
            189 => 
            array (
                'id' => 237,
                'name' => 'Paz Castillo',
                'state_id' => 14,
            ),
            190 => 
            array (
                'id' => 238,
                'name' => 'Pedro Gual',
                'state_id' => 14,
            ),
            191 => 
            array (
                'id' => 239,
                'name' => 'Plaza',
                'state_id' => 14,
            ),
            192 => 
            array (
                'id' => 240,
                'name' => 'Simón Bolívar',
                'state_id' => 14,
            ),
            193 => 
            array (
                'id' => 241,
                'name' => 'Sucre',
                'state_id' => 14,
            ),
            194 => 
            array (
                'id' => 242,
                'name' => 'Urdaneta',
                'state_id' => 14,
            ),
            195 => 
            array (
                'id' => 243,
                'name' => 'Zamora',
                'state_id' => 14,
            ),
            196 => 
            array (
                'id' => 258,
                'name' => 'Acosta',
                'state_id' => 15,
            ),
            197 => 
            array (
                'id' => 259,
                'name' => 'Aguasay',
                'state_id' => 15,
            ),
            198 => 
            array (
                'id' => 260,
                'name' => 'Bolívar',
                'state_id' => 15,
            ),
            199 => 
            array (
                'id' => 261,
                'name' => 'Caripe',
                'state_id' => 15,
            ),
            200 => 
            array (
                'id' => 262,
                'name' => 'Cedeño',
                'state_id' => 15,
            ),
            201 => 
            array (
                'id' => 263,
                'name' => 'Ezequiel Zamora',
                'state_id' => 15,
            ),
            202 => 
            array (
                'id' => 264,
                'name' => 'Libertador',
                'state_id' => 15,
            ),
            203 => 
            array (
                'id' => 265,
                'name' => 'Maturín',
                'state_id' => 15,
            ),
            204 => 
            array (
                'id' => 266,
                'name' => 'Piar',
                'state_id' => 15,
            ),
            205 => 
            array (
                'id' => 267,
                'name' => 'Punceres',
                'state_id' => 15,
            ),
            206 => 
            array (
                'id' => 268,
                'name' => 'Santa Bárbara',
                'state_id' => 15,
            ),
            207 => 
            array (
                'id' => 269,
                'name' => 'Sotillo',
                'state_id' => 15,
            ),
            208 => 
            array (
                'id' => 270,
                'name' => 'Uracoa',
                'state_id' => 15,
            ),
            209 => 
            array (
                'id' => 271,
                'name' => 'Antolín del Campo',
                'state_id' => 16,
            ),
            210 => 
            array (
                'id' => 272,
                'name' => 'Arismendi',
                'state_id' => 16,
            ),
            211 => 
            array (
                'id' => 273,
                'name' => 'García',
                'state_id' => 16,
            ),
            212 => 
            array (
                'id' => 274,
                'name' => 'Gómez',
                'state_id' => 16,
            ),
            213 => 
            array (
                'id' => 275,
                'name' => 'Maneiro',
                'state_id' => 16,
            ),
            214 => 
            array (
                'id' => 276,
                'name' => 'Marcano',
                'state_id' => 16,
            ),
            215 => 
            array (
                'id' => 277,
                'name' => 'Mariño',
                'state_id' => 16,
            ),
            216 => 
            array (
                'id' => 278,
                'name' => 'Península de Macanao',
                'state_id' => 16,
            ),
            217 => 
            array (
                'id' => 279,
                'name' => 'Tubores',
                'state_id' => 16,
            ),
            218 => 
            array (
                'id' => 280,
                'name' => 'Villalba',
                'state_id' => 16,
            ),
            219 => 
            array (
                'id' => 281,
                'name' => 'Díaz',
                'state_id' => 16,
            ),
            220 => 
            array (
                'id' => 282,
                'name' => 'Agua Blanca',
                'state_id' => 17,
            ),
            221 => 
            array (
                'id' => 283,
                'name' => 'Araure',
                'state_id' => 17,
            ),
            222 => 
            array (
                'id' => 284,
                'name' => 'Esteller',
                'state_id' => 17,
            ),
            223 => 
            array (
                'id' => 285,
                'name' => 'Guanare',
                'state_id' => 17,
            ),
            224 => 
            array (
                'id' => 286,
                'name' => 'Guanarito',
                'state_id' => 17,
            ),
            225 => 
            array (
                'id' => 287,
                'name' => 'Monseñor José Vicente de Unda',
                'state_id' => 17,
            ),
            226 => 
            array (
                'id' => 288,
                'name' => 'Ospino',
                'state_id' => 17,
            ),
            227 => 
            array (
                'id' => 289,
                'name' => 'Páez',
                'state_id' => 17,
            ),
            228 => 
            array (
                'id' => 290,
                'name' => 'Papelón',
                'state_id' => 17,
            ),
            229 => 
            array (
                'id' => 291,
                'name' => 'San Genaro de Boconoíto',
                'state_id' => 17,
            ),
            230 => 
            array (
                'id' => 292,
                'name' => 'San Rafael de Onoto',
                'state_id' => 17,
            ),
            231 => 
            array (
                'id' => 293,
                'name' => 'Santa Rosalía',
                'state_id' => 17,
            ),
            232 => 
            array (
                'id' => 294,
                'name' => 'Sucre',
                'state_id' => 17,
            ),
            233 => 
            array (
                'id' => 295,
                'name' => 'Turén',
                'state_id' => 17,
            ),
            234 => 
            array (
                'id' => 296,
                'name' => 'Andrés Eloy Blanco',
                'state_id' => 18,
            ),
            235 => 
            array (
                'id' => 297,
                'name' => 'Andrés Mata',
                'state_id' => 18,
            ),
            236 => 
            array (
                'id' => 298,
                'name' => 'Arismendi',
                'state_id' => 18,
            ),
            237 => 
            array (
                'id' => 299,
                'name' => 'Benítez',
                'state_id' => 18,
            ),
            238 => 
            array (
                'id' => 300,
                'name' => 'Bermúdez',
                'state_id' => 18,
            ),
            239 => 
            array (
                'id' => 301,
                'name' => 'Bolívar',
                'state_id' => 18,
            ),
            240 => 
            array (
                'id' => 302,
                'name' => 'Cajigal',
                'state_id' => 18,
            ),
            241 => 
            array (
                'id' => 303,
                'name' => 'Cruz Salmerón Acosta',
                'state_id' => 18,
            ),
            242 => 
            array (
                'id' => 304,
                'name' => 'Libertador',
                'state_id' => 18,
            ),
            243 => 
            array (
                'id' => 305,
                'name' => 'Mariño',
                'state_id' => 18,
            ),
            244 => 
            array (
                'id' => 306,
                'name' => 'Mejía',
                'state_id' => 18,
            ),
            245 => 
            array (
                'id' => 307,
                'name' => 'Montes',
                'state_id' => 18,
            ),
            246 => 
            array (
                'id' => 308,
                'name' => 'Ribero',
                'state_id' => 18,
            ),
            247 => 
            array (
                'id' => 309,
                'name' => 'Sucre',
                'state_id' => 18,
            ),
            248 => 
            array (
                'id' => 310,
                'name' => 'Valdéz',
                'state_id' => 18,
            ),
            249 => 
            array (
                'id' => 341,
                'name' => 'Andrés Bello',
                'state_id' => 19,
            ),
            250 => 
            array (
                'id' => 342,
                'name' => 'Antonio Rómulo Costa',
                'state_id' => 19,
            ),
            251 => 
            array (
                'id' => 343,
                'name' => 'Ayacucho',
                'state_id' => 19,
            ),
            252 => 
            array (
                'id' => 344,
                'name' => 'Bolívar',
                'state_id' => 19,
            ),
            253 => 
            array (
                'id' => 345,
                'name' => 'Cárdenas',
                'state_id' => 19,
            ),
            254 => 
            array (
                'id' => 346,
                'name' => 'Córdoba',
                'state_id' => 19,
            ),
            255 => 
            array (
                'id' => 347,
                'name' => 'Fernández Feo',
                'state_id' => 19,
            ),
            256 => 
            array (
                'id' => 348,
                'name' => 'Francisco de Miranda',
                'state_id' => 19,
            ),
            257 => 
            array (
                'id' => 349,
                'name' => 'García de Hevia',
                'state_id' => 19,
            ),
            258 => 
            array (
                'id' => 350,
                'name' => 'Guásimos',
                'state_id' => 19,
            ),
            259 => 
            array (
                'id' => 351,
                'name' => 'Independencia',
                'state_id' => 19,
            ),
            260 => 
            array (
                'id' => 352,
                'name' => 'Jáuregui',
                'state_id' => 19,
            ),
            261 => 
            array (
                'id' => 353,
                'name' => 'José María Vargas',
                'state_id' => 19,
            ),
            262 => 
            array (
                'id' => 354,
                'name' => 'Junín',
                'state_id' => 19,
            ),
            263 => 
            array (
                'id' => 355,
                'name' => 'Libertad',
                'state_id' => 19,
            ),
            264 => 
            array (
                'id' => 356,
                'name' => 'Libertador',
                'state_id' => 19,
            ),
            265 => 
            array (
                'id' => 357,
                'name' => 'Lobatera',
                'state_id' => 19,
            ),
            266 => 
            array (
                'id' => 358,
                'name' => 'Michelena',
                'state_id' => 19,
            ),
            267 => 
            array (
                'id' => 359,
                'name' => 'Panamericano',
                'state_id' => 19,
            ),
            268 => 
            array (
                'id' => 360,
                'name' => 'Pedro María Ureña',
                'state_id' => 19,
            ),
            269 => 
            array (
                'id' => 361,
                'name' => 'Rafael Urdaneta',
                'state_id' => 19,
            ),
            270 => 
            array (
                'id' => 362,
                'name' => 'Samuel Darío Maldonado',
                'state_id' => 19,
            ),
            271 => 
            array (
                'id' => 363,
                'name' => 'San Cristóbal',
                'state_id' => 19,
            ),
            272 => 
            array (
                'id' => 364,
                'name' => 'Seboruco',
                'state_id' => 19,
            ),
            273 => 
            array (
                'id' => 365,
                'name' => 'Simón Rodríguez',
                'state_id' => 19,
            ),
            274 => 
            array (
                'id' => 366,
                'name' => 'Sucre',
                'state_id' => 19,
            ),
            275 => 
            array (
                'id' => 367,
                'name' => 'Torbes',
                'state_id' => 19,
            ),
            276 => 
            array (
                'id' => 368,
                'name' => 'Uribante',
                'state_id' => 19,
            ),
            277 => 
            array (
                'id' => 369,
                'name' => 'San Judas Tadeo',
                'state_id' => 19,
            ),
            278 => 
            array (
                'id' => 370,
                'name' => 'Andrés Bello',
                'state_id' => 20,
            ),
            279 => 
            array (
                'id' => 371,
                'name' => 'Boconó',
                'state_id' => 20,
            ),
            280 => 
            array (
                'id' => 372,
                'name' => 'Bolívar',
                'state_id' => 20,
            ),
            281 => 
            array (
                'id' => 373,
                'name' => 'Candelaria',
                'state_id' => 20,
            ),
            282 => 
            array (
                'id' => 374,
                'name' => 'Carache',
                'state_id' => 20,
            ),
            283 => 
            array (
                'id' => 375,
                'name' => 'Escuque',
                'state_id' => 20,
            ),
            284 => 
            array (
                'id' => 376,
                'name' => 'José Felipe Márquez Cañizalez',
                'state_id' => 20,
            ),
            285 => 
            array (
                'id' => 377,
                'name' => 'Juan Vicente Campos Elías',
                'state_id' => 20,
            ),
            286 => 
            array (
                'id' => 378,
                'name' => 'La Ceiba',
                'state_id' => 20,
            ),
            287 => 
            array (
                'id' => 379,
                'name' => 'Miranda',
                'state_id' => 20,
            ),
            288 => 
            array (
                'id' => 380,
                'name' => 'Monte Carmelo',
                'state_id' => 20,
            ),
            289 => 
            array (
                'id' => 381,
                'name' => 'Motatán',
                'state_id' => 20,
            ),
            290 => 
            array (
                'id' => 382,
                'name' => 'Pampán',
                'state_id' => 20,
            ),
            291 => 
            array (
                'id' => 383,
                'name' => 'Pampanito',
                'state_id' => 20,
            ),
            292 => 
            array (
                'id' => 384,
                'name' => 'Rafael Rangel',
                'state_id' => 20,
            ),
            293 => 
            array (
                'id' => 385,
                'name' => 'San Rafael de Carvajal',
                'state_id' => 20,
            ),
            294 => 
            array (
                'id' => 386,
                'name' => 'Sucre',
                'state_id' => 20,
            ),
            295 => 
            array (
                'id' => 387,
                'name' => 'Trujillo',
                'state_id' => 20,
            ),
            296 => 
            array (
                'id' => 388,
                'name' => 'Urdaneta',
                'state_id' => 20,
            ),
            297 => 
            array (
                'id' => 389,
                'name' => 'Valera',
                'state_id' => 20,
            ),
            298 => 
            array (
                'id' => 390,
                'name' => 'Vargas',
                'state_id' => 21,
            ),
            299 => 
            array (
                'id' => 391,
                'name' => 'Arístides Bastidas',
                'state_id' => 22,
            ),
            300 => 
            array (
                'id' => 392,
                'name' => 'Bolívar',
                'state_id' => 22,
            ),
            301 => 
            array (
                'id' => 407,
                'name' => 'Bruzual',
                'state_id' => 22,
            ),
            302 => 
            array (
                'id' => 408,
                'name' => 'Cocorote',
                'state_id' => 22,
            ),
            303 => 
            array (
                'id' => 409,
                'name' => 'Independencia',
                'state_id' => 22,
            ),
            304 => 
            array (
                'id' => 410,
                'name' => 'José Antonio Páez',
                'state_id' => 22,
            ),
            305 => 
            array (
                'id' => 411,
                'name' => 'La Trinidad',
                'state_id' => 22,
            ),
            306 => 
            array (
                'id' => 412,
                'name' => 'Manuel Monge',
                'state_id' => 22,
            ),
            307 => 
            array (
                'id' => 413,
                'name' => 'Nirgua',
                'state_id' => 22,
            ),
            308 => 
            array (
                'id' => 414,
                'name' => 'Peña',
                'state_id' => 22,
            ),
            309 => 
            array (
                'id' => 415,
                'name' => 'San Felipe',
                'state_id' => 22,
            ),
            310 => 
            array (
                'id' => 416,
                'name' => 'Sucre',
                'state_id' => 22,
            ),
            311 => 
            array (
                'id' => 417,
                'name' => 'Urachiche',
                'state_id' => 22,
            ),
            312 => 
            array (
                'id' => 418,
                'name' => 'José Joaquín Veroes',
                'state_id' => 22,
            ),
            313 => 
            array (
                'id' => 441,
                'name' => 'Almirante Padilla',
                'state_id' => 23,
            ),
            314 => 
            array (
                'id' => 442,
                'name' => 'Baralt',
                'state_id' => 23,
            ),
            315 => 
            array (
                'id' => 443,
                'name' => 'Cabimas',
                'state_id' => 23,
            ),
            316 => 
            array (
                'id' => 444,
                'name' => 'Catatumbo',
                'state_id' => 23,
            ),
            317 => 
            array (
                'id' => 445,
                'name' => 'Colón',
                'state_id' => 23,
            ),
            318 => 
            array (
                'id' => 446,
                'name' => 'Francisco Javier Pulgar',
                'state_id' => 23,
            ),
            319 => 
            array (
                'id' => 447,
                'name' => 'Páez',
                'state_id' => 23,
            ),
            320 => 
            array (
                'id' => 448,
                'name' => 'Jesús Enrique Losada',
                'state_id' => 23,
            ),
            321 => 
            array (
                'id' => 449,
                'name' => 'Jesús María Semprún',
                'state_id' => 23,
            ),
            322 => 
            array (
                'id' => 450,
                'name' => 'La Cañada de Urdaneta',
                'state_id' => 23,
            ),
            323 => 
            array (
                'id' => 451,
                'name' => 'Lagunillas',
                'state_id' => 23,
            ),
            324 => 
            array (
                'id' => 452,
                'name' => 'Machiques de Perijá',
                'state_id' => 23,
            ),
            325 => 
            array (
                'id' => 453,
                'name' => 'Mara',
                'state_id' => 23,
            ),
            326 => 
            array (
                'id' => 454,
                'name' => 'Maracaibo',
                'state_id' => 23,
            ),
            327 => 
            array (
                'id' => 455,
                'name' => 'Miranda',
                'state_id' => 23,
            ),
            328 => 
            array (
                'id' => 456,
                'name' => 'Rosario de Perijá',
                'state_id' => 23,
            ),
            329 => 
            array (
                'id' => 457,
                'name' => 'San Francisco',
                'state_id' => 23,
            ),
            330 => 
            array (
                'id' => 458,
                'name' => 'Santa Rita',
                'state_id' => 23,
            ),
            331 => 
            array (
                'id' => 459,
                'name' => 'Simón Bolívar',
                'state_id' => 23,
            ),
            332 => 
            array (
                'id' => 460,
                'name' => 'Sucre',
                'state_id' => 23,
            ),
            333 => 
            array (
                'id' => 461,
                'name' => 'Valmore Rodríguez',
                'state_id' => 23,
            ),
            334 => 
            array (
                'id' => 462,
                'name' => 'Libertador',
                'state_id' => 24,
            ),
            335 => 
            array (
                'id' => 0,
                'name' => 'Oculto',
                'state_id' => 0,
            ),
        ));
        
        
    }
}