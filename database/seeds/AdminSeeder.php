<?php

use Illuminate\Database\Seeder;
use App\UserStatus;
use App\GenericStatus;
use App\User;
use App\Role;
use App\Permission;
use Carbon\Carbon;
use App\Module;
use Illuminate\Database\Eloquent\Model;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        /*Creacion de estatus de usuarios*/

        $usact = UserStatus::create([
            'name' => 'ACTIVO',
            'description' => 'ACTIVO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $usina = UserStatus::create([
            'name' => 'INACTIVO',
            'description' => 'INACTIVO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $usinh = UserStatus::create([
            'name' => 'INHABILITADO',
            'description' => 'INHABILITADO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        /*Creacion de usuarios*/

        $useradm = User::create([
            'name' =>'SUPERUSUARIO ADMIN',
            'email' => 'admin@admin.com',
            'email_verified_at' => Carbon::now(),
            'password' => bcrypt('123456'),
            
            'activated' => true,
            'activated_at' => Carbon::now(),
            'identification_number' => 1,
            'first_name' => 'SUPERUSUARIO',
            'last_name' => 'ADMIN',
            'user_status_id' => $usact['id'],
            'photo_directory' => '/img/icono_usuario.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        //dd($useradm);

        /*$userjc = User::create([
            'name' =>'Jose Chong',
            'email' => 'jchong@arthatecnologias.com',
            'email_verified_at' => Carbon::now(),
            'password' => bcrypt('123456'),
            
            'activated' => true,
            'activated_at' => Carbon::now(),
            'identification_number' => 20978761,
            'first_name' => 'Jose',
            'last_name' => 'Chong',
            'user_status_id' => $usact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);*/

        /*Creacion de estatus genericos*/

        $gsnul = GenericStatus::create([
            'id' => 0,
            'name' => 'NULO',
            'description' => 'NULO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $gsact = GenericStatus::create([
            'name' => 'ACTIVO',
            'description' => 'ACTIVO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $gsina = GenericStatus::create([
            'name' => 'INACTIVO',
            'description' => 'INACTIVO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $gsinh = GenericStatus::create([
            'name' => 'INHABILITADO',
            'description' => 'INHABILITADO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        /*Creacion de roles*/

        $hidden = Role::create([
            'id' => 0,
            'name' => 'OCULTO',
            'display_name' => 'OCULTO',
            'description' => 'OCULTO',
            'status_role_id' => $gsact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $super = Role::create([
            'name' => 'SUPERUSUARIO',
            'display_name' => 'SUPERUSUARIO',
            'description' => 'SUPERUSUARIO',
            'status_role_id' => $gsact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $admin = Role::create([
            'name' => 'ADMINISTRADOR DEL SISTEMA',
            'display_name' => 'ADMINISTRADOR DEL SISTEMA',
            'description' => 'ADMINISTRADOR DEL SISTEMA',
            'status_role_id' => $gsact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $test = Role::create([
            'name' => 'PRUEBA',
            'display_name' => 'PRUEBA',
            'description' => 'PRUEBA',
            'status_role_id' => $gsact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);


        /*Creacion de modulos basicos*/

        $modinicio = Module::create([
            'id' => 0,
            'name' => 'INICIO',
            'display_name' => 'Oculto',
            'description' => 'INICIO',
            'status_module_id' => $gsnul['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $modmodules = Module::create([
            'name' => 'modules',
            'display_name' => 'Módulos (Rutas) del sistema ',
            'description' => 'MODULOS (RUTAS DEL SISTEMA)',
            'status_module_id' => $gsact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $modpermissions = Module::create([
            'name' => 'permissions',
            'display_name' => 'Permisos (Abilities) del sistema',
            'description' => 'PERMISOS (ABILITIES) DEL SISTEMA',
            'status_module_id' => $gsact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $modroles = Module::create([
            'name' => 'roles',
            'display_name' => 'Roles',
            'description' => 'ROLES',
            'status_module_id' => $gsact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);


        $modrolepermissions = Module::create([
            'name' => 'rolepermissions',
            'display_name' => 'Permisos por Roles',
            'description' => 'PERMISOS POR ROLES',
            'status_module_id' => $gsact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);



        $modmenus = Module::create([
            'name' => 'menus',
            'display_name' => 'Menús',
            'description' => 'MODULO MENUS',
            'status_module_id' => $gsact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $modgenericstatus = Module::create([
            'name' => 'genericstatus',
            'display_name' => 'Estatus generales del sistema',
            'description' => 'Estatus generales del sistema',
            'status_module_id' => $gsact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $moduserstatus = Module::create([
            'name' => 'userstatus',
            'display_name' => 'Estatus de usuarios del sistema',
            'description' => 'Estatus de usuarios del sistema',
            'status_module_id' => $gsact['id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        /*Creacion de permisos*/
        $config_list = Permission::create([
            'name' => 'config-list',
            'display_name' => 'LISTAR CONFIGURACIONES',
            'description' => 'LISTAR CONFIGURACIONES',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modinicio['id'],
            'generic_status_id' => $gsact['id'],
        ]);


        $config_edit = Permission::create([
            'name' => 'config-edit',
            'display_name' => 'EDITAR CONFIGURACIONES',
            'description' => 'EDITAR CONFIGURACIONES',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modinicio['id'],
            'generic_status_id' => $gsact['id'],
        ]);
        $user_list = Permission::create([
            'name' => 'user-list',
            'display_name' => 'LISTAR USUARIOS',
            'description' => 'LISTAR USUARIOS',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modinicio['id'],
            'generic_status_id' => $gsact['id'],
        ]);


        $user_create = Permission::create([
            'name' => 'user-create',
            'display_name' => 'CREAR USUARIO',
            'description' => 'CREAR USUARIOS',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modinicio['id'],
            'generic_status_id' => $gsact['id'],
        ]);


        $user_edit = Permission::create([
            'name' =>'user-edit',
            'display_name' =>'EDITAR USUARIO',
            'description' =>'EDITAR USUARIO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modinicio['id'],
            'generic_status_id' => $gsact['id'],
        ]);


        $user_delete = Permission::create([
            'name' =>'user-delete',
            'display_name' =>'ELIMINAR USUARIO',
            'description' =>'ELIMINAR USUARIO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modinicio['id'],
            'generic_status_id' => $gsact['id'],
        ]);


        $role_list = Permission::create([
            'name' =>'role-list',
            'display_name' =>'LISTAR ROLES',
            'description' =>'LISTAR ROLES',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modroles['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $role_create = Permission::create([
            'name' => 'role-create',
            'display_name' => 'CREAR ROL',
            'description' => 'CREAR ROL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modroles['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $role_edit = Permission::create([
            'name' => 'role-edit',
            'display_name' => 'EDITAR ROL',
            'description' => 'EDITAR ROL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modroles['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $role_delete = Permission::create([
            'name' => 'role-delete',
            'display_name' => 'ELIMINAR ROL',
            'description' => 'ELIMINAR ROL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modroles['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $role_show = Permission::create([
            'name' => 'role-show',
            'display_name' => 'MOSTRAR ROL',
            'description' => 'MOSTRAR ROL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modroles['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $role_pdf = Permission::create([
            'name' => 'role-pdf',
            'display_name' => 'IMPRIMIR PDF',
            'description' => 'IMPRIMIR PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modroles['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $role_excel = Permission::create([
            'name' => 'role-excel',
            'display_name' => 'IMPRIMIR EXCEL',
            'description' => 'IMPRIMIR EXCEL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modroles['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $role_imprimirpdf = Permission::create([
            'name' => 'role-imprimirPdf',
            'display_name' => 'VISTA IMPRIME PDF',
            'description' => 'VISTA IMPRIME PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modroles['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $module_list = Permission::create([
            'name' => 'module-list',
            'display_name' => 'LISTAR MODULOS',
            'description' => 'LISTAR MODULOS',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmodules['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $module_create = Permission::create([
            'name' => 'module-create',
            'display_name' => 'CREAR MODULO',
            'description' => 'CREAR MODULO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmodules['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $module_edit = Permission::create([
            'name' => 'module-edit',
            'display_name' => 'EDITAR MODULO',
            'description' => 'EDITAR MODULO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmodules['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $module_delete = Permission::create([
            'name' => 'module-delete',
            'display_name' => 'ELIMINAR MODULO',
            'description' => 'ELIMINAR MODULO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmodules['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $module_show = Permission::create([
            'name' => 'module-show',
            'display_name' => 'MOSTRAR MODULO',
            'description' => 'MOSTRAR MODULO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmodules['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $module_pdf = Permission::create([
            'name' => 'module-pdf',
            'display_name' => 'IMPRIMIR PDF',
            'description' => 'IMPRIMIR PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmodules['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $module_excel = Permission::create([
            'name' => 'module-excel',
            'display_name' => 'IMPRIMIR EXCEL',
            'description' => 'IMPRIMIR EXCEL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmodules['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $module_imprimirpdf = Permission::create([
            'name' => 'module-imprimirPdf',
            'display_name' => 'VISTA IMPRIME PDF',
            'description' => 'VISTA IMPRIME PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmodules['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $permission_list = Permission::create([
            'name' => 'permission-list',
            'display_name' => 'LISTAR PERMISOS',
            'description' => 'LISTAR PERMISOS',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modpermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $permission_create = Permission::create([
            'name' => 'permission-create',
            'display_name' => 'CREAR PERMISO',
            'description' => 'CREAR PERMISO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modpermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $permission_edit = Permission::create([
            'name' => 'permission-edit',
            'display_name' => 'EDITAR PERMISO',
            'description' => 'EDITAR PERMISO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modpermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $permission_delete = Permission::create([
            'name' => 'permission-delete',
            'display_name' => 'ELIMINAR PERMISO',
            'description' => 'ELIMINAR PERMISO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modpermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $permission_show = Permission::create([
            'name' => 'permission-show',
            'display_name' => 'MOSTRAR PERMISO',
            'description' => 'MOSTRAR PERMISO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modpermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $permission_pdf = Permission::create([
            'name' => 'permission-pdf',
            'display_name' => 'IMPRIMIR PDF',
            'description' => 'IMPRIMIR PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modpermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $permission_excel = Permission::create([
            'name' => 'permission-excel',
            'display_name' => 'IMPRIMIR EXCEL',
            'description' => 'IMPRIMIR EXCEL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modpermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $permission_imprimirpdf = Permission::create([
            'name' => 'permission-imprimirPdf',
            'display_name' => 'VISTA IMPRIME PDF',
            'description' => 'VISTA IMPRIME PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modpermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $rolepermission_assign = Permission::create([
            'name' => 'rolepermission-asign',
            'display_name' => 'ASIGNAR PERMISOS ROL',
            'description' => 'ASIGNAR PERMISOS ROL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modrolepermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $rolepermission_create = Permission::create([
            'name' => 'rolepermission-create',
            'display_name' => 'CREAR PERMISO ROL',
            'description' => 'CREAR PERMISO POR ROL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modrolepermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $rolepermission_pdf = Permission::create([
            'name' => 'rolepermission-pdf',
            'display_name' => 'IMPRIMIR PDF',
            'description' => 'IMPRIMIR PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modrolepermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $rolepermission_excel = Permission::create([
            'name' => 'rolepermission-excel',
            'display_name' => 'IMPRIMIR EXCEL',
            'description' => 'IMPRIMIR EXCEL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modrolepermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $rolepermission_imprimirpdf = Permission::create([
            'name' => 'rolepermission-imprimirPdf',
            'display_name' => 'VISTA IMPRIME PDF',
            'description' => 'VISTA IMPRIME PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modrolepermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $rolepermission_edit = Permission::create([
            'name' => 'rolepermission-edit',
            'display_name' => 'EDITAR PERMISO POR ROL',
            'description' => 'EDITAR PERMISO POR ROL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modrolepermissions['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $menu_list = Permission::create([
            'name' => 'menu-list',
            'display_name' => 'LISTAR MENUS',
            'description' => 'LISTAR MENUS',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmenus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $menu_create = Permission::create([
            'name' => 'menu-create',
            'display_name' => 'CREAR MENU',
            'description' => 'CREAR MENU',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmenus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $menu_edit = Permission::create([
            'name' => 'menu-edit',
            'display_name' => 'EDITAR MENU',
            'description' => 'EDITAR MENU',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmenus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $menu_delete = Permission::create([
            'name' => 'menu-delete',
            'display_name' => 'ELIMINAR MENU',
            'description' => 'ELIMINAR MENU',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmenus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $menu_show = Permission::create([
            'name' => 'menu-show',
            'display_name' => 'MOSTRAR MENU',
            'description' => 'MOSTRAR MENU',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmenus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $menu_pdf = Permission::create([
            'name' => 'menu-pdf',
            'display_name' => 'IMPRIMIR PDF',
            'description' => 'IMPRIMIR PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmenus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $menu_excel = Permission::create([
            'name' => 'menu-excel',
            'display_name' => 'IMPRIMIR EXCEL',
            'description' => 'IMPRIMIR EXCEL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmenus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $menu_imprimirpdf = Permission::create([
            'name' => 'menu-imprimirPdf',
            'display_name' => 'VISTA IMPRIMIR PDF',
            'description' => 'VISTA IMPRIMIR PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modmenus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $genericstatus_list = Permission::create([
            'name' => 'genericstatus-list',
            'display_name' => 'LISTAR ESTATUS GENERALES',
            'description' => 'LISTAR ESTATUS GENERALES',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modgenericstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $genericstatus_create = Permission::create([
            'name' => 'genericstatus-create',
            'display_name' => 'CREAR ESTATUS GENERAL',
            'description' => 'CREAR ESTATUS GENERAL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modgenericstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $genericstatus_edit = Permission::create([
            'name' => 'genericstatus-edit',
            'display_name' => 'EDITAR ESTATUS GENERAL',
            'description' => 'EDITAR ESTATUS GENERAL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modgenericstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $genericstatus_delete = Permission::create([
            'name' => 'genericstatus-delete',
            'display_name' => 'ELIMINAR ESTATUS GENERAL',
            'description' => 'ELIMINAR ESTATUS GENERAL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modgenericstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $genericstatus_show = Permission::create([
            'name' => 'genericstatus-show',
            'display_name' => 'MOSTRAR ESTATUS GENERAL',
            'description' => 'MOSTRAR ESTATUS GENERAL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modgenericstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $genericstatus_pdf = Permission::create([
            'name' => 'genericstatus-pdf',
            'display_name' => 'IMPRIMIR PDF',
            'description' => 'IMPRIMIR PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modgenericstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $genericstatus_excel = Permission::create([
            'name' => 'genericstatus-excel',
            'display_name' => 'IMPRIMIR EXCEL',
            'description' => 'IMPRIMIR EXCEL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modgenericstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $genericstatus_imprimirpdf = Permission::create([
            'name' => 'genericstatus-imprimirPdf',
            'display_name' => 'VISTA IMPRIMIR PDF',
            'description' => 'VISTA IMPRIMIR PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $modgenericstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $userstatus_list = Permission::create([
            'name' => 'userstatus-list',
            'display_name' => 'LISTAR ESTATUS USUARIOS',
            'description' => 'LISTAR ESTATUS USUARIOS',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $moduserstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $userstatus_create = Permission::create([
            'name' => 'userstatus-create',
            'display_name' => 'CREAR ESTATUS USUARIO',
            'description' => 'CREAR ESTATUS USUARIO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $moduserstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $userstatus_edit = Permission::create([
            'name' => 'userstatus-edit',
            'display_name' => 'EDITAR ESTATUS USUARIO',
            'description' => 'EDITAR ESTATUS USUARIO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $moduserstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $userstatus_delete = Permission::create([
            'name' => 'userstatus-delete',
            'display_name' => 'ELIMINAR ESTATUS USUARIO',
            'description' => 'ELIMINAR ESTATUS USUARIO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $moduserstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $userstatus_show = Permission::create([
            'name' => 'userstatus-show',
            'display_name' => 'MOSTRAR ESTATUS USUARIO',
            'description' => 'MOSTRAR ESTATUS USUARIO',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $moduserstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $userstatus_pdf = Permission::create([
            'name' => 'userstatus-pdf',
            'display_name' => 'IMPRIMIR PDF',
            'description' => 'IMPRIMIR PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $moduserstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $userstatus_excel = Permission::create([
            'name' => 'userstatus-excel',
            'display_name' => 'IMPRIMIR EXCEL',
            'description' => 'IMPRIMIR EXCEL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $moduserstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $userstatus_imprimirpdf = Permission::create([
            'name' => 'userstatus-imprimirPdf',
            'display_name' => 'VISTA IMPRIMIR PDF',
            'description' => 'VISTA IMPRIMIR PDF',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'module_id' => $moduserstatus['id'],
            'generic_status_id' => $gsact['id'],
        ]);

        $useradm->attachRole($super);

        /*1-2*/
        $super->attachPermission($config_list);
        $super->attachPermission($config_edit);
        /*1-4*/
        $super->attachPermission($user_list);
        $super->attachPermission($user_create);
        $super->attachPermission($user_edit);
        $super->attachPermission($user_delete);

        /*5-12*/
        $super->attachPermission($role_list);
        $super->attachPermission($role_create);
        $super->attachPermission($role_edit);
        $super->attachPermission($role_delete);
        $super->attachPermission($role_show);
        $super->attachPermission($role_pdf);
        $super->attachPermission($role_excel);
        $super->attachPermission($role_imprimirpdf);

        /*13-20*/
        $super->attachPermission($module_list);
        $super->attachPermission($module_create);
        $super->attachPermission($module_edit);
        $super->attachPermission($module_delete);
        $super->attachPermission($module_show);
        $super->attachPermission($module_pdf);
        $super->attachPermission($module_excel);
        $super->attachPermission($module_imprimirpdf);

        /*21-28*/
        $super->attachPermission($permission_list);
        $super->attachPermission($permission_create);
        $super->attachPermission($permission_edit);
        $super->attachPermission($permission_delete);
        $super->attachPermission($permission_show);
        $super->attachPermission($permission_pdf);
        $super->attachPermission($permission_excel);
        $super->attachPermission($permission_imprimirpdf);

        /*29-34*/
        $super->attachPermission($rolepermission_assign);
        $super->attachPermission($rolepermission_create);
        $super->attachPermission($rolepermission_pdf);
        $super->attachPermission($rolepermission_excel);
        $super->attachPermission($rolepermission_imprimirpdf);
        $super->attachPermission($rolepermission_edit);

        /*35-42*/
        $super->attachPermission($menu_list);
        $super->attachPermission($menu_create);
        $super->attachPermission($menu_edit);
        $super->attachPermission($menu_delete);
        $super->attachPermission($menu_show);
        $super->attachPermission($menu_pdf);
        $super->attachPermission($menu_excel);
        $super->attachPermission($menu_imprimirpdf);

        /*43-50*/
        $super->attachPermission($genericstatus_list);
        $super->attachPermission($genericstatus_create);
        $super->attachPermission($genericstatus_edit);
        $super->attachPermission($genericstatus_delete);
        $super->attachPermission($genericstatus_show);
        $super->attachPermission($genericstatus_pdf);
        $super->attachPermission($genericstatus_excel);
        $super->attachPermission($genericstatus_imprimirpdf);

        /*51-58*/
        $super->attachPermission($userstatus_list);
        $super->attachPermission($userstatus_create);
        $super->attachPermission($userstatus_edit);
        $super->attachPermission($userstatus_delete);
        $super->attachPermission($userstatus_show);
        $super->attachPermission($userstatus_pdf);
        $super->attachPermission($userstatus_excel);
        $super->attachPermission($userstatus_imprimirpdf);

        /*Permisos para rol oculto*/
        $hidden->attachPermission($menu_list);

        /*Permisos para rol administrador del sistema*/
        /*5-12*/
        $admin->attachPermission($role_list);
        $admin->attachPermission($role_create);
        $admin->attachPermission($role_edit);
        $admin->attachPermission($role_delete);
        $admin->attachPermission($role_show);
        $admin->attachPermission($role_pdf);
        $admin->attachPermission($role_excel);
        $admin->attachPermission($role_imprimirpdf);

        /*13-20*/
        $admin->attachPermission($module_list);
        $admin->attachPermission($module_create);
        $admin->attachPermission($module_edit);
        $admin->attachPermission($module_delete);
        $admin->attachPermission($module_show);
        $admin->attachPermission($module_pdf);
        $admin->attachPermission($module_excel);
        $admin->attachPermission($module_imprimirpdf);

        /*21-28*/
        $admin->attachPermission($permission_list);
        $admin->attachPermission($permission_create);
        $admin->attachPermission($permission_edit);
        $admin->attachPermission($permission_delete);
        $admin->attachPermission($permission_show);
        $admin->attachPermission($permission_pdf);
        $admin->attachPermission($permission_excel);
        $admin->attachPermission($permission_imprimirpdf);



        $admin->attachPermission($rolepermission_pdf);
        $admin->attachPermission($rolepermission_excel);

        $admin->attachPermission($rolepermission_edit);

         /*35-42*/
        $admin->attachPermission($menu_list);
        $admin->attachPermission($menu_create);
        $admin->attachPermission($menu_edit);
        $admin->attachPermission($menu_delete);
        $admin->attachPermission($menu_show);
        $admin->attachPermission($menu_pdf);
        $admin->attachPermission($menu_excel);
        $admin->attachPermission($menu_imprimirpdf);

        /*43-50*/
        $admin->attachPermission($genericstatus_list);
        $admin->attachPermission($genericstatus_create);
        $admin->attachPermission($genericstatus_edit);

        /*Pemisos para rol de Prueba*/
        /*5-12*/
        $test->attachPermission($role_list);
        $test->attachPermission($role_create);
        $test->attachPermission($role_edit);
        $test->attachPermission($role_delete);
        $test->attachPermission($role_show);
        $test->attachPermission($role_pdf);
        $test->attachPermission($role_excel);
        $test->attachPermission($role_imprimirpdf);

        /*13-20*/
        $test->attachPermission($module_list);
        $test->attachPermission($module_create);
        $test->attachPermission($module_edit);
        $test->attachPermission($module_delete);
        $test->attachPermission($module_show);
        $test->attachPermission($module_pdf);
        $test->attachPermission($module_excel);
        $test->attachPermission($module_imprimirpdf);

        /*21-28*/
        $test->attachPermission($permission_list);
        $test->attachPermission($permission_create);
        $test->attachPermission($permission_edit);
        $test->attachPermission($permission_delete);
        $test->attachPermission($permission_show);
        $test->attachPermission($permission_pdf);
        $test->attachPermission($permission_excel);
        $test->attachPermission($permission_imprimirpdf);

        $test->attachPermission($rolepermission_pdf);
        $test->attachPermission($rolepermission_excel);

        $test->attachPermission($rolepermission_edit);

        /*35-42*/
        $test->attachPermission($menu_list);
        $test->attachPermission($menu_create);
        $test->attachPermission($menu_edit);
        $test->attachPermission($menu_delete);
        $test->attachPermission($menu_show);
        $test->attachPermission($menu_pdf);
        $test->attachPermission($menu_excel);
        $test->attachPermission($menu_imprimirpdf);

        
        $test->attachPermission($genericstatus_list);
        $test->attachPermission($genericstatus_create);
        $test->attachPermission($genericstatus_edit);


        //$userjc->attachRole($admin);

        \DB::table('translate')->delete();
        
        \DB::table('translate')->insert(array (
            0 => 
            array (
                'id' => 1,
                'english' => 'created',
                'spanish' => 'Creación',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
            1 => 
            array (
                'id' => 2,
                'english' => 'updated',
                'spanish' => 'Actualización',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
            2 => 
            array (
                'id' => 3,
                'english' => 'deleted',
                'spanish' => 'Eliminación',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));

    }
}