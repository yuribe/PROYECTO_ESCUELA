<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Afganistán',
                'iso_2' => 'AF',
                'iso_3' => 'AFG',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Albania',
                'iso_2' => 'AL',
                'iso_3' => 'ALB',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Alemania',
                'iso_2' => 'DE',
                'iso_3' => 'DEU',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Algeria',
                'iso_2' => 'DZ',
                'iso_3' => 'DZA',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Andorra',
                'iso_2' => 'AD',
                'iso_3' => 'AND',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Angola',
                'iso_2' => 'AO',
                'iso_3' => 'AGO',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Anguila',
                'iso_2' => 'AI',
                'iso_3' => 'AIA',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Antártida',
                'iso_2' => 'AQ',
                'iso_3' => 'ATA',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Antigua y Barbuda',
                'iso_2' => 'AG',
                'iso_3' => 'ATG',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Antillas Neerlandesas',
                'iso_2' => 'AN',
                'iso_3' => 'ANT',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Arabia Saudita',
                'iso_2' => 'SA',
                'iso_3' => 'SAU',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Argentina',
                'iso_2' => 'AR',
                'iso_3' => 'ARG',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Armenia',
                'iso_2' => 'AM',
                'iso_3' => 'ARM',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Aruba',
                'iso_2' => 'AW',
                'iso_3' => 'ABW',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Australia',
                'iso_2' => 'AU',
                'iso_3' => 'AUS',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Austria',
                'iso_2' => 'AT',
                'iso_3' => 'AUT',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Azerbayán',
                'iso_2' => 'AZ',
                'iso_3' => 'AZE',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Bélgica',
                'iso_2' => 'BE',
                'iso_3' => 'BEL',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Bahamas',
                'iso_2' => 'BS',
                'iso_3' => 'BHS',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Bahrein',
                'iso_2' => 'BH',
                'iso_3' => 'BHR',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Bangladesh',
                'iso_2' => 'BD',
                'iso_3' => 'BGD',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Barbados',
                'iso_2' => 'BB',
                'iso_3' => 'BRB',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Belice',
                'iso_2' => 'BZ',
                'iso_3' => 'BLZ',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Benín',
                'iso_2' => 'BJ',
                'iso_3' => 'BEN',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Bhután',
                'iso_2' => 'BT',
                'iso_3' => 'BTN',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Bielorrusia',
                'iso_2' => 'BY',
                'iso_3' => 'BLR',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Birmania',
                'iso_2' => 'MM',
                'iso_3' => 'MMR',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Bolivia',
                'iso_2' => 'BO',
                'iso_3' => 'BOL',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Bosnia y Herzegovina',
                'iso_2' => 'BA',
                'iso_3' => 'BIH',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Botsuana',
                'iso_2' => 'BW',
                'iso_3' => 'BWA',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Brasil',
                'iso_2' => 'BR',
                'iso_3' => 'BRA',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Brunéi',
                'iso_2' => 'BN',
                'iso_3' => 'BRN',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Bulgaria',
                'iso_2' => 'BG',
                'iso_3' => 'BGR',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Burkina Faso',
                'iso_2' => 'BF',
                'iso_3' => 'BFA',
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Burundi',
                'iso_2' => 'BI',
                'iso_3' => 'BDI',
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Cabo Verde',
                'iso_2' => 'CV',
                'iso_3' => 'CPV',
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Camboya',
                'iso_2' => 'KH',
                'iso_3' => 'KHM',
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Camerún',
                'iso_2' => 'CM',
                'iso_3' => 'CMR',
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Canadá',
                'iso_2' => 'CA',
                'iso_3' => 'CAN',
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'Chad',
                'iso_2' => 'TD',
                'iso_3' => 'TCD',
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Chile',
                'iso_2' => 'CL',
                'iso_3' => 'CHL',
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'China',
                'iso_2' => 'CN',
                'iso_3' => 'CHN',
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Chipre',
                'iso_2' => 'CY',
                'iso_3' => 'CYP',
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Ciudad del Vaticano',
                'iso_2' => 'VA',
                'iso_3' => 'VAT',
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'Colombia',
                'iso_2' => 'CO',
                'iso_3' => 'COL',
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'Comoras',
                'iso_2' => 'KM',
                'iso_3' => 'COM',
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'Congo',
                'iso_2' => 'CG',
                'iso_3' => 'COG',
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Congo',
                'iso_2' => 'CD',
                'iso_3' => 'COD',
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Corea del Norte',
                'iso_2' => 'KP',
                'iso_3' => 'PRK',
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Corea del Sur',
                'iso_2' => 'KR',
                'iso_3' => 'KOR',
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Costa de Marfil',
                'iso_2' => 'CI',
                'iso_3' => 'CIV',
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Costa Rica',
                'iso_2' => 'CR',
                'iso_3' => 'CRI',
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'Croacia',
                'iso_2' => 'HR',
                'iso_3' => 'HRV',
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'Cuba',
                'iso_2' => 'CU',
                'iso_3' => 'CUB',
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Dinamarca',
                'iso_2' => 'DK',
                'iso_3' => 'DNK',
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Dominica',
                'iso_2' => 'DM',
                'iso_3' => 'DMA',
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Ecuador',
                'iso_2' => 'EC',
                'iso_3' => 'ECU',
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Egipto',
                'iso_2' => 'EG',
                'iso_3' => 'EGY',
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'El Salvador',
                'iso_2' => 'SV',
                'iso_3' => 'SLV',
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Emiratos Árabes Unidos',
                'iso_2' => 'AE',
                'iso_3' => 'ARE',
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Eritrea',
                'iso_2' => 'ER',
                'iso_3' => 'ERI',
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'Eslovaquia',
                'iso_2' => 'SK',
                'iso_3' => 'SVK',
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Eslovenia',
                'iso_2' => 'SI',
                'iso_3' => 'SVN',
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'España',
                'iso_2' => 'ES',
                'iso_3' => 'ESP',
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Estados Unidos de América',
                'iso_2' => 'US',
                'iso_3' => 'USA',
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'Estonia',
                'iso_2' => 'EE',
                'iso_3' => 'EST',
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'Etiopía',
                'iso_2' => 'ET',
                'iso_3' => 'ETH',
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'Filipinas',
                'iso_2' => 'PH',
                'iso_3' => 'PHL',
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Finlandia',
                'iso_2' => 'FI',
                'iso_3' => 'FIN',
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Fiyi',
                'iso_2' => 'FJ',
                'iso_3' => 'FJI',
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Francia',
                'iso_2' => 'FR',
                'iso_3' => 'FRA',
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'Gabón',
                'iso_2' => 'GA',
                'iso_3' => 'GAB',
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Gambia',
                'iso_2' => 'GM',
                'iso_3' => 'GMB',
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Georgia',
                'iso_2' => 'GE',
                'iso_3' => 'GEO',
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'Ghana',
                'iso_2' => 'GH',
                'iso_3' => 'GHA',
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'Gibraltar',
                'iso_2' => 'GI',
                'iso_3' => 'GIB',
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'Granada',
                'iso_2' => 'GD',
                'iso_3' => 'GRD',
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'Grecia',
                'iso_2' => 'GR',
                'iso_3' => 'GRC',
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'Groenlandia',
                'iso_2' => 'GL',
                'iso_3' => 'GRL',
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Guadalupe',
                'iso_2' => 'GP',
                'iso_3' => 'GLP',
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Guam',
                'iso_2' => 'GU',
                'iso_3' => 'GUM',
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Guatemala',
                'iso_2' => 'GT',
                'iso_3' => 'GTM',
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Guayana Francesa',
                'iso_2' => 'GF',
                'iso_3' => 'GUF',
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Guernsey',
                'iso_2' => 'GG',
                'iso_3' => 'GGY',
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Guinea',
                'iso_2' => 'GN',
                'iso_3' => 'GIN',
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Guinea Ecuatorial',
                'iso_2' => 'GQ',
                'iso_3' => 'GNQ',
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Guinea-Bissau',
                'iso_2' => 'GW',
                'iso_3' => 'GNB',
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'Guyana',
                'iso_2' => 'GY',
                'iso_3' => 'GUY',
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'Haití',
                'iso_2' => 'HT',
                'iso_3' => 'HTI',
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'Honduras',
                'iso_2' => 'HN',
                'iso_3' => 'HND',
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Hong kong',
                'iso_2' => 'HK',
                'iso_3' => 'HKG',
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'Hungría',
                'iso_2' => 'HU',
                'iso_3' => 'HUN',
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'India',
                'iso_2' => 'IN',
                'iso_3' => 'IND',
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Indonesia',
                'iso_2' => 'ID',
                'iso_3' => 'IDN',
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'Irán',
                'iso_2' => 'IR',
                'iso_3' => 'IRN',
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'Irak',
                'iso_2' => 'IQ',
                'iso_3' => 'IRQ',
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'Irlanda',
                'iso_2' => 'IE',
                'iso_3' => 'IRL',
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'Isla Bouvet',
                'iso_2' => 'BV',
                'iso_3' => 'BVT',
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Isla de Man',
                'iso_2' => 'IM',
                'iso_3' => 'IMN',
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'Isla de Navidad',
                'iso_2' => 'CX',
                'iso_3' => 'CXR',
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'Isla Norfolk',
                'iso_2' => 'NF',
                'iso_3' => 'NFK',
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'Islandia',
                'iso_2' => 'IS',
                'iso_3' => 'ISL',
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'Islas Bermudas',
                'iso_2' => 'BM',
                'iso_3' => 'BMU',
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'Islas Caimán',
                'iso_2' => 'KY',
                'iso_3' => 'CYM',
            ),
            104 => 
            array (
                'id' => 105,
            'name' => 'Islas Cocos (Keeling)',
                'iso_2' => 'CC',
                'iso_3' => 'CCK',
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'Islas Cook',
                'iso_2' => 'CK',
                'iso_3' => 'COK',
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Islas de Åland',
                'iso_2' => 'AX',
                'iso_3' => 'ALA',
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'Islas Feroe',
                'iso_2' => 'FO',
                'iso_3' => 'FRO',
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'Islas Georgias del Sur y Sandwich del Sur',
                'iso_2' => 'GS',
                'iso_3' => 'SGS',
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'Islas Heard y McDonald',
                'iso_2' => 'HM',
                'iso_3' => 'HMD',
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Islas Maldivas',
                'iso_2' => 'MV',
                'iso_3' => 'MDV',
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Islas Malvinas',
                'iso_2' => 'FK',
                'iso_3' => 'FLK',
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'Islas Marianas del Norte',
                'iso_2' => 'MP',
                'iso_3' => 'MNP',
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'Islas Marshall',
                'iso_2' => 'MH',
                'iso_3' => 'MHL',
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'Islas Pitcairn',
                'iso_2' => 'PN',
                'iso_3' => 'PCN',
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'Islas Salomón',
                'iso_2' => 'SB',
                'iso_3' => 'SLB',
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'Islas Turcas y Caicos',
                'iso_2' => 'TC',
                'iso_3' => 'TCA',
            ),
            117 => 
            array (
                'id' => 118,
                'name' => 'Islas Ultramarinas Menores de Estados Unidos',
                'iso_2' => 'UM',
                'iso_3' => 'UMI',
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'Islas Vírgenes Británicas',
                'iso_2' => 'VG',
                'iso_3' => 'VG',
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'Islas Vírgenes de los Estados Unidos',
                'iso_2' => 'VI',
                'iso_3' => 'VIR',
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'Israel',
                'iso_2' => 'IL',
                'iso_3' => 'ISR',
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'Italia',
                'iso_2' => 'IT',
                'iso_3' => 'ITA',
            ),
            122 => 
            array (
                'id' => 123,
                'name' => 'Jamaica',
                'iso_2' => 'JM',
                'iso_3' => 'JAM',
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'Japón',
                'iso_2' => 'JP',
                'iso_3' => 'JPN',
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'Jersey',
                'iso_2' => 'JE',
                'iso_3' => 'JEY',
            ),
            125 => 
            array (
                'id' => 126,
                'name' => 'Jordania',
                'iso_2' => 'JO',
                'iso_3' => 'JOR',
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'Kazajistán',
                'iso_2' => 'KZ',
                'iso_3' => 'KAZ',
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'Kenia',
                'iso_2' => 'KE',
                'iso_3' => 'KEN',
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'Kirgizstán',
                'iso_2' => 'KG',
                'iso_3' => 'KGZ',
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'Kiribati',
                'iso_2' => 'KI',
                'iso_3' => 'KIR',
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'Kuwait',
                'iso_2' => 'KW',
                'iso_3' => 'KWT',
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'Líbano',
                'iso_2' => 'LB',
                'iso_3' => 'LBN',
            ),
            132 => 
            array (
                'id' => 133,
                'name' => 'Laos',
                'iso_2' => 'LA',
                'iso_3' => 'LAO',
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'Lesoto',
                'iso_2' => 'LS',
                'iso_3' => 'LSO',
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'Letonia',
                'iso_2' => 'LV',
                'iso_3' => 'LVA',
            ),
            135 => 
            array (
                'id' => 136,
                'name' => 'Liberia',
                'iso_2' => 'LR',
                'iso_3' => 'LBR',
            ),
            136 => 
            array (
                'id' => 137,
                'name' => 'Libia',
                'iso_2' => 'LY',
                'iso_3' => 'LBY',
            ),
            137 => 
            array (
                'id' => 138,
                'name' => 'Liechtenstein',
                'iso_2' => 'LI',
                'iso_3' => 'LIE',
            ),
            138 => 
            array (
                'id' => 139,
                'name' => 'Lituania',
                'iso_2' => 'LT',
                'iso_3' => 'LTU',
            ),
            139 => 
            array (
                'id' => 140,
                'name' => 'Luxemburgo',
                'iso_2' => 'LU',
                'iso_3' => 'LUX',
            ),
            140 => 
            array (
                'id' => 141,
                'name' => 'México',
                'iso_2' => 'MX',
                'iso_3' => 'MEX',
            ),
            141 => 
            array (
                'id' => 142,
                'name' => 'Mónaco',
                'iso_2' => 'MC',
                'iso_3' => 'MCO',
            ),
            142 => 
            array (
                'id' => 143,
                'name' => 'Macao',
                'iso_2' => 'MO',
                'iso_3' => 'MAC',
            ),
            143 => 
            array (
                'id' => 144,
                'name' => 'Macedônia',
                'iso_2' => 'MK',
                'iso_3' => 'MKD',
            ),
            144 => 
            array (
                'id' => 145,
                'name' => 'Madagascar',
                'iso_2' => 'MG',
                'iso_3' => 'MDG',
            ),
            145 => 
            array (
                'id' => 146,
                'name' => 'Malasia',
                'iso_2' => 'MY',
                'iso_3' => 'MYS',
            ),
            146 => 
            array (
                'id' => 147,
                'name' => 'Malawi',
                'iso_2' => 'MW',
                'iso_3' => 'MWI',
            ),
            147 => 
            array (
                'id' => 148,
                'name' => 'Mali',
                'iso_2' => 'ML',
                'iso_3' => 'MLI',
            ),
            148 => 
            array (
                'id' => 149,
                'name' => 'Malta',
                'iso_2' => 'MT',
                'iso_3' => 'MLT',
            ),
            149 => 
            array (
                'id' => 150,
                'name' => 'Marruecos',
                'iso_2' => 'MA',
                'iso_3' => 'MAR',
            ),
            150 => 
            array (
                'id' => 151,
                'name' => 'Martinica',
                'iso_2' => 'MQ',
                'iso_3' => 'MTQ',
            ),
            151 => 
            array (
                'id' => 152,
                'name' => 'Mauricio',
                'iso_2' => 'MU',
                'iso_3' => 'MUS',
            ),
            152 => 
            array (
                'id' => 153,
                'name' => 'Mauritania',
                'iso_2' => 'MR',
                'iso_3' => 'MRT',
            ),
            153 => 
            array (
                'id' => 154,
                'name' => 'Mayotte',
                'iso_2' => 'YT',
                'iso_3' => 'MYT',
            ),
            154 => 
            array (
                'id' => 155,
                'name' => 'Micronesia',
                'iso_2' => 'FM',
                'iso_3' => 'FSM',
            ),
            155 => 
            array (
                'id' => 156,
                'name' => 'Moldavia',
                'iso_2' => 'MD',
                'iso_3' => 'MDA',
            ),
            156 => 
            array (
                'id' => 157,
                'name' => 'Mongolia',
                'iso_2' => 'MN',
                'iso_3' => 'MNG',
            ),
            157 => 
            array (
                'id' => 158,
                'name' => 'Montenegro',
                'iso_2' => 'ME',
                'iso_3' => 'MNE',
            ),
            158 => 
            array (
                'id' => 159,
                'name' => 'Montserrat',
                'iso_2' => 'MS',
                'iso_3' => 'MSR',
            ),
            159 => 
            array (
                'id' => 160,
                'name' => 'Mozambique',
                'iso_2' => 'MZ',
                'iso_3' => 'MOZ',
            ),
            160 => 
            array (
                'id' => 161,
                'name' => 'Namibia',
                'iso_2' => 'NA',
                'iso_3' => 'NAM',
            ),
            161 => 
            array (
                'id' => 162,
                'name' => 'Nauru',
                'iso_2' => 'NR',
                'iso_3' => 'NRU',
            ),
            162 => 
            array (
                'id' => 163,
                'name' => 'Nepal',
                'iso_2' => 'NP',
                'iso_3' => 'NPL',
            ),
            163 => 
            array (
                'id' => 164,
                'name' => 'Nicaragua',
                'iso_2' => 'NI',
                'iso_3' => 'NIC',
            ),
            164 => 
            array (
                'id' => 165,
                'name' => 'Niger',
                'iso_2' => 'NE',
                'iso_3' => 'NER',
            ),
            165 => 
            array (
                'id' => 166,
                'name' => 'Nigeria',
                'iso_2' => 'NG',
                'iso_3' => 'NGA',
            ),
            166 => 
            array (
                'id' => 167,
                'name' => 'Niue',
                'iso_2' => 'NU',
                'iso_3' => 'NIU',
            ),
            167 => 
            array (
                'id' => 168,
                'name' => 'Noruega',
                'iso_2' => 'NO',
                'iso_3' => 'NOR',
            ),
            168 => 
            array (
                'id' => 169,
                'name' => 'Nueva Caledonia',
                'iso_2' => 'NC',
                'iso_3' => 'NCL',
            ),
            169 => 
            array (
                'id' => 170,
                'name' => 'Nueva Zelanda',
                'iso_2' => 'NZ',
                'iso_3' => 'NZL',
            ),
            170 => 
            array (
                'id' => 171,
                'name' => 'Omán',
                'iso_2' => 'OM',
                'iso_3' => 'OMN',
            ),
            171 => 
            array (
                'id' => 172,
                'name' => 'Países Bajos',
                'iso_2' => 'NL',
                'iso_3' => 'NLD',
            ),
            172 => 
            array (
                'id' => 173,
                'name' => 'Pakistán',
                'iso_2' => 'PK',
                'iso_3' => 'PAK',
            ),
            173 => 
            array (
                'id' => 174,
                'name' => 'Palau',
                'iso_2' => 'PW',
                'iso_3' => 'PLW',
            ),
            174 => 
            array (
                'id' => 175,
                'name' => 'Palestina',
                'iso_2' => 'PS',
                'iso_3' => 'PSE',
            ),
            175 => 
            array (
                'id' => 176,
                'name' => 'Panamá',
                'iso_2' => 'PA',
                'iso_3' => 'PAN',
            ),
            176 => 
            array (
                'id' => 177,
                'name' => 'Papúa Nueva Guinea',
                'iso_2' => 'PG',
                'iso_3' => 'PNG',
            ),
            177 => 
            array (
                'id' => 178,
                'name' => 'Paraguay',
                'iso_2' => 'PY',
                'iso_3' => 'PRY',
            ),
            178 => 
            array (
                'id' => 179,
                'name' => 'Perú',
                'iso_2' => 'PE',
                'iso_3' => 'PER',
            ),
            179 => 
            array (
                'id' => 180,
                'name' => 'Polinesia Francesa',
                'iso_2' => 'PF',
                'iso_3' => 'PYF',
            ),
            180 => 
            array (
                'id' => 181,
                'name' => 'Polonia',
                'iso_2' => 'PL',
                'iso_3' => 'POL',
            ),
            181 => 
            array (
                'id' => 182,
                'name' => 'Portugal',
                'iso_2' => 'PT',
                'iso_3' => 'PRT',
            ),
            182 => 
            array (
                'id' => 183,
                'name' => 'Puerto Rico',
                'iso_2' => 'PR',
                'iso_3' => 'PRI',
            ),
            183 => 
            array (
                'id' => 184,
                'name' => 'Qatar',
                'iso_2' => 'QA',
                'iso_3' => 'QAT',
            ),
            184 => 
            array (
                'id' => 185,
                'name' => 'Reino Unido',
                'iso_2' => 'GB',
                'iso_3' => 'GBR',
            ),
            185 => 
            array (
                'id' => 186,
                'name' => 'República Centroafricana',
                'iso_2' => 'CF',
                'iso_3' => 'CAF',
            ),
            186 => 
            array (
                'id' => 187,
                'name' => 'República Checa',
                'iso_2' => 'CZ',
                'iso_3' => 'CZE',
            ),
            187 => 
            array (
                'id' => 188,
                'name' => 'República Dominicana',
                'iso_2' => 'DO',
                'iso_3' => 'DOM',
            ),
            188 => 
            array (
                'id' => 189,
                'name' => 'Reunión',
                'iso_2' => 'RE',
                'iso_3' => 'REU',
            ),
            189 => 
            array (
                'id' => 190,
                'name' => 'Ruanda',
                'iso_2' => 'RW',
                'iso_3' => 'RWA',
            ),
            190 => 
            array (
                'id' => 191,
                'name' => 'Rumanía',
                'iso_2' => 'RO',
                'iso_3' => 'ROU',
            ),
            191 => 
            array (
                'id' => 192,
                'name' => 'Rusia',
                'iso_2' => 'RU',
                'iso_3' => 'RUS',
            ),
            192 => 
            array (
                'id' => 193,
                'name' => 'Sahara Occidental',
                'iso_2' => 'EH',
                'iso_3' => 'ESH',
            ),
            193 => 
            array (
                'id' => 194,
                'name' => 'Samoa',
                'iso_2' => 'WS',
                'iso_3' => 'WSM',
            ),
            194 => 
            array (
                'id' => 195,
                'name' => 'Samoa Americana',
                'iso_2' => 'AS',
                'iso_3' => 'ASM',
            ),
            195 => 
            array (
                'id' => 196,
                'name' => 'San Bartolomé',
                'iso_2' => 'BL',
                'iso_3' => 'BLM',
            ),
            196 => 
            array (
                'id' => 197,
                'name' => 'San Cristóbal y Nieves',
                'iso_2' => 'KN',
                'iso_3' => 'KNA',
            ),
            197 => 
            array (
                'id' => 198,
                'name' => 'San Marino',
                'iso_2' => 'SM',
                'iso_3' => 'SMR',
            ),
            198 => 
            array (
                'id' => 199,
            'name' => 'San Martín (Francia)',
                'iso_2' => 'MF',
                'iso_3' => 'MAF',
            ),
            199 => 
            array (
                'id' => 200,
                'name' => 'San Pedro y Miquelón',
                'iso_2' => 'PM',
                'iso_3' => 'SPM',
            ),
            200 => 
            array (
                'id' => 201,
                'name' => 'San Vicente y las Granadinas',
                'iso_2' => 'VC',
                'iso_3' => 'VCT',
            ),
            201 => 
            array (
                'id' => 202,
                'name' => 'Santa Elena',
                'iso_2' => 'SH',
                'iso_3' => 'SHN',
            ),
            202 => 
            array (
                'id' => 203,
                'name' => 'Santa Lucía',
                'iso_2' => 'LC',
                'iso_3' => 'LCA',
            ),
            203 => 
            array (
                'id' => 204,
                'name' => 'Santo Tomé y Príncipe',
                'iso_2' => 'ST',
                'iso_3' => 'STP',
            ),
            204 => 
            array (
                'id' => 205,
                'name' => 'Senegal',
                'iso_2' => 'SN',
                'iso_3' => 'SEN',
            ),
            205 => 
            array (
                'id' => 206,
                'name' => 'Serbia',
                'iso_2' => 'RS',
                'iso_3' => 'SRB',
            ),
            206 => 
            array (
                'id' => 207,
                'name' => 'Seychelles',
                'iso_2' => 'SC',
                'iso_3' => 'SYC',
            ),
            207 => 
            array (
                'id' => 208,
                'name' => 'Sierra Leona',
                'iso_2' => 'SL',
                'iso_3' => 'SLE',
            ),
            208 => 
            array (
                'id' => 209,
                'name' => 'Singapur',
                'iso_2' => 'SG',
                'iso_3' => 'SGP',
            ),
            209 => 
            array (
                'id' => 210,
                'name' => 'Siria',
                'iso_2' => 'SY',
                'iso_3' => 'SYR',
            ),
            210 => 
            array (
                'id' => 211,
                'name' => 'Somalia',
                'iso_2' => 'SO',
                'iso_3' => 'SOM',
            ),
            211 => 
            array (
                'id' => 212,
                'name' => 'Sri lanka',
                'iso_2' => 'LK',
                'iso_3' => 'LKA',
            ),
            212 => 
            array (
                'id' => 213,
                'name' => 'Sudáfrica',
                'iso_2' => 'ZA',
                'iso_3' => 'ZAF',
            ),
            213 => 
            array (
                'id' => 214,
                'name' => 'Sudán',
                'iso_2' => 'SD',
                'iso_3' => 'SDN',
            ),
            214 => 
            array (
                'id' => 215,
                'name' => 'Suecia',
                'iso_2' => 'SE',
                'iso_3' => 'SWE',
            ),
            215 => 
            array (
                'id' => 216,
                'name' => 'Suiza',
                'iso_2' => 'CH',
                'iso_3' => 'CHE',
            ),
            216 => 
            array (
                'id' => 217,
                'name' => 'Surinám',
                'iso_2' => 'SR',
                'iso_3' => 'SUR',
            ),
            217 => 
            array (
                'id' => 218,
                'name' => 'Svalbard y Jan Mayen',
                'iso_2' => 'SJ',
                'iso_3' => 'SJM',
            ),
            218 => 
            array (
                'id' => 219,
                'name' => 'Swazilandia',
                'iso_2' => 'SZ',
                'iso_3' => 'SWZ',
            ),
            219 => 
            array (
                'id' => 220,
                'name' => 'Tadjikistán',
                'iso_2' => 'TJ',
                'iso_3' => 'TJK',
            ),
            220 => 
            array (
                'id' => 221,
                'name' => 'Tailandia',
                'iso_2' => 'TH',
                'iso_3' => 'THA',
            ),
            221 => 
            array (
                'id' => 222,
                'name' => 'Taiwán',
                'iso_2' => 'TW',
                'iso_3' => 'TWN',
            ),
            222 => 
            array (
                'id' => 223,
                'name' => 'Tanzania',
                'iso_2' => 'TZ',
                'iso_3' => 'TZA',
            ),
            223 => 
            array (
                'id' => 224,
                'name' => 'Territorio Británico del Océano Índico',
                'iso_2' => 'IO',
                'iso_3' => 'IOT',
            ),
            224 => 
            array (
                'id' => 225,
                'name' => 'Territorios Australes y Antárticas Franceses',
                'iso_2' => 'TF',
                'iso_3' => 'ATF',
            ),
            225 => 
            array (
                'id' => 226,
                'name' => 'Timor Oriental',
                'iso_2' => 'TL',
                'iso_3' => 'TLS',
            ),
            226 => 
            array (
                'id' => 227,
                'name' => 'Togo',
                'iso_2' => 'TG',
                'iso_3' => 'TGO',
            ),
            227 => 
            array (
                'id' => 228,
                'name' => 'Tokelau',
                'iso_2' => 'TK',
                'iso_3' => 'TKL',
            ),
            228 => 
            array (
                'id' => 229,
                'name' => 'Tonga',
                'iso_2' => 'TO',
                'iso_3' => 'TON',
            ),
            229 => 
            array (
                'id' => 230,
                'name' => 'Trinidad y Tobago',
                'iso_2' => 'TT',
                'iso_3' => 'TTO',
            ),
            230 => 
            array (
                'id' => 231,
                'name' => 'Tunez',
                'iso_2' => 'TN',
                'iso_3' => 'TUN',
            ),
            231 => 
            array (
                'id' => 232,
                'name' => 'Turkmenistán',
                'iso_2' => 'TM',
                'iso_3' => 'TKM',
            ),
            232 => 
            array (
                'id' => 233,
                'name' => 'Turquía',
                'iso_2' => 'TR',
                'iso_3' => 'TUR',
            ),
            233 => 
            array (
                'id' => 234,
                'name' => 'Tuvalu',
                'iso_2' => 'TV',
                'iso_3' => 'TUV',
            ),
            234 => 
            array (
                'id' => 235,
                'name' => 'Ucrania',
                'iso_2' => 'UA',
                'iso_3' => 'UKR',
            ),
            235 => 
            array (
                'id' => 236,
                'name' => 'Uganda',
                'iso_2' => 'UG',
                'iso_3' => 'UGA',
            ),
            236 => 
            array (
                'id' => 237,
                'name' => 'Uruguay',
                'iso_2' => 'UY',
                'iso_3' => 'URY',
            ),
            237 => 
            array (
                'id' => 238,
                'name' => 'Uzbekistán',
                'iso_2' => 'UZ',
                'iso_3' => 'UZB',
            ),
            238 => 
            array (
                'id' => 239,
                'name' => 'Vanuatu',
                'iso_2' => 'VU',
                'iso_3' => 'VUT',
            ),
            239 => 
            array (
                'id' => 240,
                'name' => 'Venezuela',
                'iso_2' => 'VE',
                'iso_3' => 'VEN',
            ),
            240 => 
            array (
                'id' => 241,
                'name' => 'Vietnam',
                'iso_2' => 'VN',
                'iso_3' => 'VNM',
            ),
            241 => 
            array (
                'id' => 242,
                'name' => 'Wallis y Futuna',
                'iso_2' => 'WF',
                'iso_3' => 'WLF',
            ),
            242 => 
            array (
                'id' => 243,
                'name' => 'Yemen',
                'iso_2' => 'YE',
                'iso_3' => 'YEM',
            ),
            243 => 
            array (
                'id' => 244,
                'name' => 'Yibuti',
                'iso_2' => 'DJ',
                'iso_3' => 'DJI',
            ),
            244 => 
            array (
                'id' => 245,
                'name' => 'Zambia',
                'iso_2' => 'ZM',
                'iso_3' => 'ZMB',
            ),
            245 => 
            array (
                'id' => 246,
                'name' => 'Zimbabue',
                'iso_2' => 'ZW',
                'iso_3' => 'ZWE',
            ),
            246 => 
            array (
                'id' => 0,
                'name' => 'Oculto',
                'iso_2' => 'Oc',
                'iso_3' => 'Ocl',
            ),
        ));
        
        
    }
}