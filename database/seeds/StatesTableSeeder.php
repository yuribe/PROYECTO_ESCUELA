<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('states')->delete();
        
        \DB::table('states')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Amazonas',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Anzoátegui',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Apure',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Aragua',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Barinas',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Bolívar',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Carabobo',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Cojedes',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Delta Amacuro',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Falcón',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Guárico',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Lara',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Mérida',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Miranda',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Monagas',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Nueva Esparta',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Portuguesa',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Sucre',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Táchira',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Trujillo',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Vargas',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Yaracuy',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Zulia',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Distrito Capital',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Dependencias Federales',
            ),
            25 => 
            array (
                'id' => 0,
                'name' => 'Oculto',
            ),
        ));
        
        
    }
}