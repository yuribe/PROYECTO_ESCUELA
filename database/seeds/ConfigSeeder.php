<?php

use Illuminate\Database\Seeder;
use App\Configuration as Config;
use Carbon\Carbon;
class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        $email = json_encode([
            'driver'    => 'smtp',
            'host'      => 'smtp.mailgun.org',
            'username'  => 'jsiewdass@arthatecnologias.com',
            'password'  => '5522zurdo!',
            'port'      => 587,
            'from_address'   => 'no-reply@arthatecnologias.com',
            'from_name'      => 'Jonas Siewdass',
            'encryption'=> 'tls',
        ]);

        Config::create([
            'type' =>'EMAIL',
            'value' => $email,
            'active' => true,
        ]);

        $auth = json_encode([
            'login'    => true,
            'google'      => false,
            'facebook'  => false,
            'twitter'  => false,
            'instagram'      => false,

        ]);
        Config::create([
            'type' =>'AUTENTICACION',
            'value' => $auth,
            'active' => true,
        ]);

        $registro = json_encode([
            'interno'    => true,
            'externo'      => true,
        ]);
        Config::create([
            'type' =>'REGISTRO',
            'value' => $registro,
            'active' => true,
        ]);

        $lifetime = json_encode([
            'lifetime'    => 120
        ]);
        Config::create([
            'type' =>'LIFETIME',
            'value' => $lifetime,
            'active' => true,
        ]);

        $captcha = json_encode([
            'captcha' => true,
        ]);
        Config::create([
            'type' =>'CAPTCHA',
            'value' => $captcha,
            'active' => true,
        ]);

        
    }
}
