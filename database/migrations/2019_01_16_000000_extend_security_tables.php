<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ExtendSecurityTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing statuses for modules and menus
        Schema::create('generic_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 80)->unique();
            $table->string('description', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();

            //$table->primary('id');
        });

        // Create table for storing modules
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id',100);
            $table->string('name')->unique();
            $table->string('display_name')->nullable()->unique();
            $table->string('description')->nullable();
            $table->integer('status_module_id')->default(1)->unsigned();
            $table->timestamps();
            $table->softDeletes();

            //$table->primary('id');
            $table->foreign('status_module_id')->references('id')->on('generic_status')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // Create table for storing menus
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('display_name', 50)->nullable();
            $table->string('description', 255)->nullable();
            $table->integer('menu_parent');
            $table->integer('menu_level');
            $table->string('menu_icon', 50)->nullable();
            //Association between menus and modules
            $table->integer('module_menu_id')->default(1)->unsigned();
            //Status of the menu
            $table->integer('status_menu_id')->unsigned();
            //$table->integer('permission_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            //$table->primary('id');
            $table->foreign('module_menu_id')->references('id')->on('modules')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('status_menu_id')->references('id')->on('generic_status')
                ->onUpdate('cascade')->onDelete('cascade');
            //$table->foreign('permission_id')->references('id')->on('permissions')
            //    ->onUpdate('cascade')->onDelete('cascade');
            $table->unique(array('id','name'));
        });

        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('user_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->unique();
            $table->string('description', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();


            //$table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('menus');
        Schema::drop('modules');
        Schema::drop('generic_status');
        Schema::drop('user_status');
    }
}
