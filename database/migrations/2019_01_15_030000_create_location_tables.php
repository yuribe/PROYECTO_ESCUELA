<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

Class CreateLocationTables extends Migration
{
	/**
     * Run the migrations.
     *
     * @return void
     */

	public function up()
    {

    	Schema::create('countries', function (Blueprint $table) {
    		$table->increments('id');
    		$table->string('name',100);
    		$table->string('iso_2',3);
    		$table->string('iso_3',3);
    	});

    	Schema::create('states', function (Blueprint $table) {
    		$table->increments('id');
    		$table->string('name',100);
    		//$table->string('ine_id',2);
    	});

    	Schema::create('municipalities', function (Blueprint $table) {
    		$table->increments('id');
    		$table->string('name',100);
    		$table->integer('state_id')->unsigned();
    		//$table->string('ine_id',4);

    		$table->foreign('state_id')->references('id')->on('states')
                ->onUpdate('cascade')->onDelete('cascade');
    	});

    	Schema::create('parishes', function (Blueprint $table) {
    		$table->increments('id');
    		$table->string('name',100);
    		$table->integer('municipality_id')->unsigned();
    		//$table->string('ine_id',6);

    		$table->foreign('municipality_id')->references('id')->on('municipalities')
                ->onUpdate('cascade')->onDelete('cascade');
    	});
    }

public function down()
    {
    	Schema::dropIfExists('countries');
    	Schema::dropIfExists('states');
    	Schema::dropIfExists('municipalities');
    	Schema::dropIfExists('parishes');
    }
}