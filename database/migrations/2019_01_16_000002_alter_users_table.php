<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('email')->nullable()->change();
            $table->string('password')->nullable()->change();

            $table->integer('user_status_id')->unsigned()->nullable();
            $table->boolean('activated')->default(false)->nullable();  
            $table->string('activation_code',255)->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->string('reset_password_code',255)->nullable();
            $table->string('identification_number',100);
            $table->string('phone',20)->nullable();
            $table->string('work_phone',20)->nullable();
            $table->string('cellphone',20)->default(' ');
            $table->string('home_address',500)->default(' ');
            $table->string('work_address',500)->nullable();
            $table->string('alternative_email',255)->nullable();
            $table->string('first_name',30);
            $table->string('last_name',30);
            $table->string('middle_name',30)->nullable();
            $table->string('second_surname',30)->nullable();
            $table->string('gender',1)->default('N');
            $table->string('photo_directory',255);
            $table->string('provider',255)->nullable();
            $table->string('provider_id',255)->nullable();

            $table->integer('country_id')->unsigned()->default(0);
            $table->string('location',255)->nullable();

            $table->integer('state_id')->unsigned()->default(0);
            $table->integer('municipality_id')->unsigned()->default(0);
            $table->integer('parish_id')->unsigned()->default(0);

            $table->softDeletes();

            $table->foreign('user_status_id')->references('id')->on('user_status')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('country_id')->references('id')->on('countries')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('state_id')->references('id')->on('states')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('municipality_id')->references('id')->on('municipalities')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('parish_id')->references('id')->on('parishes')
                ->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('email')->unique()->nullable(false)->change();
            $table->string('password')->nullable(false)->change();

            $table->dropForeign('user_status_id');

            $table->dropForeign('country_id');
            //$table->dropForeign('state_id');
            //$table->dropForeign('municipality_id');
            $table->dropForeign('parish_id');


            $table->dropColumn('user_status_id');

            $table->dropColumn('activated');
            $table->dropColumn('activation_code');
            $table->dropColumn('activated_at');
            $table->dropColumn('reset_password_code');
            $table->dropColumn('identification_number');
            $table->dropColumn('phone');
            $table->dropColumn('work_phone');
            $table->dropColumn('cellphone');
            $table->dropColumn('home_address');
            $table->dropColumn('work_address');
            $table->dropColumn('alternative_email');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('middle_name');
            $table->dropColumn('second_surname');
            $table->dropColumn('photo_directory');
            $table->dropColumn('provider');
            $table->dropColumn('provider_id');

            $table->dropColumn('country_id');
            //$table->dropColumn('state_id');
            //$table->dropColumn('municipality_id');
            $table->dropColumn('parish_id');

            $table->dropSoftDeletes();
        });
    }
}
