<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Carbon\Carbon;

class AlterEntrustSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Alter table for storing roles
        Schema::table('roles', function (Blueprint $table) {
            $table->integer('status_role_id')->unsigned();
            $table->softDeletes();

            $table->foreign('status_role_id')->references('id')->on('generic_status')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // Alter table for associating roles to users (Many-to-Many)
        Schema::table('role_user', function (Blueprint $table) {
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });

        // Alter table for storing permissions
        Schema::table('permissions', function (Blueprint $table) {
            $table->softDeletes();
            $table->integer('module_id')->unsigned();
            $table->integer('generic_status_id')->unsigned();


            $table->foreign('module_id')->references('id')->on('modules')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('generic_status_id')->references('id')->on('generic_status')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        //Delete Primary from permission_role

        Schema::table('permission_role', function (Blueprint $table) {
            $table->drop('permission_role');
        });

        // Alter table for associating permissions to roles (Many-to-Many)

        Schema::create('permission_role', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('permission_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->dropPrimary('permission_role_pkey');
            $table->foreign('permission_id')->references('id')->on('permissions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['permission_id', 'role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->dropForeign('status_role_id');
            $table->dropColumn('status_role_id');
            $table->dropSoftDeletes();
        });

        Schema::table('role_user', function (Blueprint $table) {
            $table->dropTimestamps();
        });

        Schema::table('permissions', function (Blueprint $table) {
            $table->dropForeign('module_id');
            $table->dropForeign('generic_status_id');
            $table->dropSoftDeletes();
            $table->dropColumn(array('module_id','generic_status_id'));
        });

        Schema::table('permission_role', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->dropTimestamps();
            $table->dropSoftDeletes();
        });
    }
}
