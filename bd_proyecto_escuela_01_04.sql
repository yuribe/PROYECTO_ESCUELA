--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.16
-- Dumped by pg_dump version 10.7 (Debian 10.7-1.pgdg90+1)

-- Started on 2019-04-01 08:45:44 -04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12361)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2251 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 195 (class 1259 OID 68361)
-- Name: generic_status; Type: TABLE; Schema: public; Owner: colabora
--

CREATE TABLE public.generic_status (
    id integer NOT NULL,
    name character varying(80) NOT NULL,
    description character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone
);


ALTER TABLE public.generic_status OWNER TO colabora;

--
-- TOC entry 2252 (class 0 OID 0)
-- Dependencies: 195
-- Name: TABLE generic_status; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON TABLE public.generic_status IS 'Nomenclador estatus genérico';


--
-- TOC entry 2253 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN generic_status.id; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.generic_status.id IS 'Identificador único de registro';


--
-- TOC entry 2254 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN generic_status.name; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.generic_status.name IS 'Nombre de estatus';


--
-- TOC entry 2255 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN generic_status.description; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.generic_status.description IS 'Descripción de estatus';


--
-- TOC entry 2256 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN generic_status.created_at; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.generic_status.created_at IS 'Fecha y hora creación de registro';


--
-- TOC entry 2257 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN generic_status.updated_at; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.generic_status.updated_at IS 'Fecha y hora actualización de registro';


--
-- TOC entry 2258 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN generic_status.deleted_at; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.generic_status.deleted_at IS 'Fecha y hora eliminación de registro';


--
-- TOC entry 194 (class 1259 OID 68359)
-- Name: generic_status_id_seq; Type: SEQUENCE; Schema: public; Owner: colabora
--

CREATE SEQUENCE public.generic_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.generic_status_id_seq OWNER TO colabora;

--
-- TOC entry 2259 (class 0 OID 0)
-- Dependencies: 194
-- Name: generic_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: colabora
--

ALTER SEQUENCE public.generic_status_id_seq OWNED BY public.generic_status.id;


--
-- TOC entry 199 (class 1259 OID 68436)
-- Name: menus; Type: TABLE; Schema: public; Owner: colabora
--

CREATE TABLE public.menus (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone,
    name character varying(50) NOT NULL,
    display_name character varying(50) NOT NULL,
    description character varying(255),
    menu_padre integer NOT NULL,
    menu_nivel integer NOT NULL,
    menu_icon character varying(50),
    id_module_menu integer DEFAULT 1 NOT NULL,
    id_status_menu integer NOT NULL
);


ALTER TABLE public.menus OWNER TO colabora;

--
-- TOC entry 2260 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN menus.id; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.menus.id IS 'Identificador único de registro';


--
-- TOC entry 2261 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN menus.created_at; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.menus.created_at IS 'Fecha de creación registro';


--
-- TOC entry 198 (class 1259 OID 68434)
-- Name: menus_id_seq; Type: SEQUENCE; Schema: public; Owner: colabora
--

CREATE SEQUENCE public.menus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menus_id_seq OWNER TO colabora;

--
-- TOC entry 2262 (class 0 OID 0)
-- Dependencies: 198
-- Name: menus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: colabora
--

ALTER SEQUENCE public.menus_id_seq OWNED BY public.menus.id;


--
-- TOC entry 182 (class 1259 OID 68230)
-- Name: migrations; Type: TABLE; Schema: public; Owner: colabora
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO colabora;

--
-- TOC entry 181 (class 1259 OID 68228)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: colabora
--

CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO colabora;

--
-- TOC entry 2263 (class 0 OID 0)
-- Dependencies: 181
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: colabora
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 192 (class 1259 OID 68312)
-- Name: modules; Type: TABLE; Schema: public; Owner: colabora
--

CREATE TABLE public.modules (
    id bigint NOT NULL,
    name character varying(50),
    description character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    display_name character varying(50),
    id_status_module integer DEFAULT 1
);


ALTER TABLE public.modules OWNER TO colabora;

--
-- TOC entry 193 (class 1259 OID 68315)
-- Name: modules_id_seq; Type: SEQUENCE; Schema: public; Owner: colabora
--

CREATE SEQUENCE public.modules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modules_id_seq OWNER TO colabora;

--
-- TOC entry 2264 (class 0 OID 0)
-- Dependencies: 193
-- Name: modules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: colabora
--

ALTER SEQUENCE public.modules_id_seq OWNED BY public.modules.id;


--
-- TOC entry 185 (class 1259 OID 68249)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: colabora
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO colabora;

--
-- TOC entry 191 (class 1259 OID 68297)
-- Name: permission_role; Type: TABLE; Schema: public; Owner: colabora
--

CREATE TABLE public.permission_role (
    permission_id integer NOT NULL,
    role_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    deleted_at timestamp without time zone,
    id integer NOT NULL
);


ALTER TABLE public.permission_role OWNER TO colabora;

--
-- TOC entry 200 (class 1259 OID 76674)
-- Name: permission_role_id_seq; Type: SEQUENCE; Schema: public; Owner: colabora
--

CREATE SEQUENCE public.permission_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permission_role_id_seq OWNER TO colabora;

--
-- TOC entry 2265 (class 0 OID 0)
-- Dependencies: 200
-- Name: permission_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: colabora
--

ALTER SEQUENCE public.permission_role_id_seq OWNED BY public.permission_role.id;


--
-- TOC entry 190 (class 1259 OID 68286)
-- Name: permissions; Type: TABLE; Schema: public; Owner: colabora
--

CREATE TABLE public.permissions (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    display_name character varying(255),
    description character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp without time zone,
    id_module integer,
    id_generic_status integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.permissions OWNER TO colabora;

--
-- TOC entry 189 (class 1259 OID 68284)
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: colabora
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO colabora;

--
-- TOC entry 2266 (class 0 OID 0)
-- Dependencies: 189
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: colabora
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- TOC entry 188 (class 1259 OID 68269)
-- Name: role_user; Type: TABLE; Schema: public; Owner: colabora
--

CREATE TABLE public.role_user (
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone DEFAULT now()
);


ALTER TABLE public.role_user OWNER TO colabora;

--
-- TOC entry 187 (class 1259 OID 68258)
-- Name: roles; Type: TABLE; Schema: public; Owner: colabora
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    display_name character varying(255),
    description character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    id_status_role integer DEFAULT 1 NOT NULL,
    deleted_at timestamp without time zone
);


ALTER TABLE public.roles OWNER TO colabora;

--
-- TOC entry 186 (class 1259 OID 68256)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: colabora
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO colabora;

--
-- TOC entry 2267 (class 0 OID 0)
-- Dependencies: 186
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: colabora
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- TOC entry 197 (class 1259 OID 68381)
-- Name: user_status; Type: TABLE; Schema: public; Owner: colabora
--

CREATE TABLE public.user_status (
    id integer NOT NULL,
    name character varying(80) NOT NULL,
    description character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone
);


ALTER TABLE public.user_status OWNER TO colabora;

--
-- TOC entry 2268 (class 0 OID 0)
-- Dependencies: 197
-- Name: TABLE user_status; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON TABLE public.user_status IS 'Nomenclador estatus usuarios';


--
-- TOC entry 2269 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN user_status.id; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.user_status.id IS 'Identificador único de registro';


--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN user_status.name; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.user_status.name IS 'Nombre estatus usuario';


--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN user_status.description; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.user_status.description IS 'Descripción estatus usuario';


--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN user_status.created_at; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.user_status.created_at IS 'Fecha y hora creación de registro';


--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN user_status.updated_at; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.user_status.updated_at IS 'Fecha y hora actualización de registro';


--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN user_status.deleted_at; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.user_status.deleted_at IS 'Fecha y hora eliminación de registro';


--
-- TOC entry 196 (class 1259 OID 68379)
-- Name: user_status_id_seq; Type: SEQUENCE; Schema: public; Owner: colabora
--

CREATE SEQUENCE public.user_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_status_id_seq OWNER TO colabora;

--
-- TOC entry 2275 (class 0 OID 0)
-- Dependencies: 196
-- Name: user_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: colabora
--

ALTER SEQUENCE public.user_status_id_seq OWNED BY public.user_status.id;


--
-- TOC entry 184 (class 1259 OID 68238)
-- Name: users; Type: TABLE; Schema: public; Owner: colabora
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp without time zone,
    id_user_status integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.users OWNER TO colabora;

--
-- TOC entry 2276 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN users.id_user_status; Type: COMMENT; Schema: public; Owner: colabora
--

COMMENT ON COLUMN public.users.id_user_status IS 'Id nomenclador de estatus de usuario relacionado';


--
-- TOC entry 183 (class 1259 OID 68236)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: colabora
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO colabora;

--
-- TOC entry 2277 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: colabora
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2059 (class 2604 OID 68364)
-- Name: generic_status id; Type: DEFAULT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.generic_status ALTER COLUMN id SET DEFAULT nextval('public.generic_status_id_seq'::regclass);


--
-- TOC entry 2061 (class 2604 OID 68439)
-- Name: menus id; Type: DEFAULT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.menus ALTER COLUMN id SET DEFAULT nextval('public.menus_id_seq'::regclass);


--
-- TOC entry 2045 (class 2604 OID 68233)
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 2057 (class 2604 OID 68317)
-- Name: modules id; Type: DEFAULT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.modules ALTER COLUMN id SET DEFAULT nextval('public.modules_id_seq'::regclass);


--
-- TOC entry 2056 (class 2604 OID 76676)
-- Name: permission_role id; Type: DEFAULT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.permission_role ALTER COLUMN id SET DEFAULT nextval('public.permission_role_id_seq'::regclass);


--
-- TOC entry 2052 (class 2604 OID 68289)
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- TOC entry 2048 (class 2604 OID 68261)
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- TOC entry 2060 (class 2604 OID 68384)
-- Name: user_status id; Type: DEFAULT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.user_status ALTER COLUMN id SET DEFAULT nextval('public.user_status_id_seq'::regclass);


--
-- TOC entry 2046 (class 2604 OID 68241)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2237 (class 0 OID 68361)
-- Dependencies: 195
-- Data for Name: generic_status; Type: TABLE DATA; Schema: public; Owner: colabora
--

COPY public.generic_status (id, name, description, created_at, updated_at, deleted_at) FROM stdin;
1	ACTIVO	ACTIVO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N
2	INACTIVO	INACTIVO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N
0	NULO	NULO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N
3	INHABILITADO	INHABILITADO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N
\.


--
-- TOC entry 2241 (class 0 OID 68436)
-- Dependencies: 199
-- Data for Name: menus; Type: TABLE DATA; Schema: public; Owner: colabora
--

COPY public.menus (id, created_at, updated_at, deleted_at, name, display_name, description, menu_padre, menu_nivel, menu_icon, id_module_menu, id_status_menu) FROM stdin;
0	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	Inicio	OCULTO	OCULTO	0	0	0	0	1
1	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	Inicio	Inicio	Inicio	0	0	fa-fa-home	99	1
2	2019-03-29 19:11:55	2019-03-29 19:11:55	\N	maqwerty	maqwerty	maqwertyyu	1	0	fa fa-fw fa-square	99	1
3	2019-03-29 19:12:45	2019-03-29 19:12:45	\N	modulo max	modulo max	modulo max	2	1	fa fa-fw fa-hand-o-left	107	1
\.


--
-- TOC entry 2224 (class 0 OID 68230)
-- Dependencies: 182
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: colabora
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_01_15_025341_entrust_setup_tables	1
\.


--
-- TOC entry 2234 (class 0 OID 68312)
-- Dependencies: 192
-- Data for Name: modules; Type: TABLE DATA; Schema: public; Owner: colabora
--

COPY public.modules (id, name, description, created_at, updated_at, deleted_at, display_name, id_status_module) FROM stdin;
5	menus	MODULO MENUS	2019-03-20 15:07:30	2019-03-20 15:07:30	\N	MENUS	1
107	modulo_max	MODULO_MAX	2019-03-21 19:52:21	2019-03-21 19:52:21	\N	modulo_max	1
108	modulo_max_2	MODULO_MAX_2	2019-03-21 20:17:43	2019-03-21 20:17:43	\N	modulo_max_2	1
109	qwerty	QWERTY QWERTY	2019-03-22 15:15:29	2019-03-22 15:15:29	\N	qwerty	1
110	modulo_jonas	MODULO_JONAS	2019-03-22 15:30:02	2019-03-22 15:30:02	\N	modulo_jonas	1
111	modulo_wilpia	MODULO_WILPIA	2019-03-22 15:33:45	2019-03-22 15:33:45	\N	modulo_wilpia	1
0	INICIO	INICIO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	Oculto	0
99	Elemento de Título	Elemento de Título	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	Elemento de Título	1
3	roles	ROLES	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	Roles	1
4	rolepermissions	PERMISOS POR ROLES	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	Permisos por Roles	1
2	permissions	PERMISOS (ABILITIES) DEL SISTEMA	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	Permisos(abilities) del sistema	1
1	modules	MODULOS (RUTAS DEL SISTEMA	2019-02-14 00:00:00	2019-02-14 20:43:55	\N	Módulos(Rutas) del sistema	1
\.


--
-- TOC entry 2227 (class 0 OID 68249)
-- Dependencies: 185
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: colabora
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- TOC entry 2233 (class 0 OID 68297)
-- Dependencies: 191
-- Data for Name: permission_role; Type: TABLE DATA; Schema: public; Owner: colabora
--

COPY public.permission_role (permission_id, role_id, created_at, updated_at, deleted_at, id) FROM stdin;
1	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	1
2	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	2
3	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	3
4	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	4
5	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	5
6	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	6
7	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	7
8	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	8
38	0	2019-03-20 15:07:30	2019-03-20 15:07:30	\N	204
28	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	13
27	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	14
26	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	15
25	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	16
24	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	17
23	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	18
21	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	19
20	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	20
19	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	21
18	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	22
17	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	23
16	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	24
15	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	25
14	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	26
13	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	27
31	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	28
9	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	29
10	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	30
11	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	31
12	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	32
35	1	2019-02-27 10:42:25.200827	2019-02-27 10:42:25.200827	\N	33
36	1	2019-02-27 10:42:31.252137	2019-02-27 10:42:31.252137	\N	34
34	1	2019-02-26 09:11:00.905954	2019-02-26 09:11:00.905954	\N	35
37	1	2019-03-07 13:27:02.84096	2019-03-07 13:27:02.84096	\N	36
38	1	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	205
39	1	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	206
40	1	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	207
41	1	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	208
22	1	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	41
32	1	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	42
42	1	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	209
43	1	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	210
44	1	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	211
45	1	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	212
5	2	2019-03-19 13:37:40	2019-03-19 13:37:40	\N	63
6	2	2019-03-19 13:37:40	2019-03-19 13:37:40	\N	64
7	2	2019-03-19 13:37:40	2019-03-19 13:37:40	\N	65
8	2	2019-03-19 13:37:40	2019-03-19 13:37:40	\N	66
9	2	2019-03-19 13:37:40	2019-03-19 13:37:40	\N	67
10	2	2019-03-19 13:37:40	2019-03-19 13:37:40	\N	68
11	2	2019-03-19 13:37:40	2019-03-19 13:37:40	\N	69
12	2	2019-03-19 13:37:40	2019-03-19 13:37:40	\N	70
1017	0	2019-03-21 19:52:21	2019-03-21 19:52:21	\N	450
1018	1	2019-03-21 19:52:47	2019-03-21 19:52:47	\N	451
1019	1	2019-03-21 19:53:06	2019-03-21 19:53:06	\N	452
1020	1	2019-03-21 19:53:25	2019-03-21 19:53:25	\N	453
38	22	2019-03-20 18:39:28	2019-03-20 18:39:28	\N	320
39	22	2019-03-20 18:39:28	2019-03-20 18:39:28	\N	321
40	22	2019-03-20 18:39:28	2019-03-20 18:39:28	\N	322
41	22	2019-03-20 18:39:28	2019-03-20 18:39:28	\N	323
42	22	2019-03-20 18:39:28	2019-03-20 18:39:28	\N	324
43	22	2019-03-20 18:39:28	2019-03-20 18:39:28	\N	325
44	22	2019-03-20 18:39:28	2019-03-20 18:39:28	\N	326
5	22	2019-03-20 18:02:02	2019-03-20 18:02:02	\N	245
6	22	2019-03-20 18:02:02	2019-03-20 18:02:02	\N	246
7	22	2019-03-20 18:02:02	2019-03-20 18:02:02	\N	247
8	22	2019-03-20 18:02:02	2019-03-20 18:02:02	\N	248
9	22	2019-03-20 18:02:02	2019-03-20 18:02:02	\N	249
10	22	2019-03-20 18:02:02	2019-03-20 18:02:02	\N	250
11	22	2019-03-20 18:02:02	2019-03-20 18:02:02	\N	251
12	22	2019-03-20 18:02:02	2019-03-20 18:02:02	\N	252
45	22	2019-03-20 18:39:28	2019-03-20 18:39:28	\N	327
1021	1	2019-03-21 19:53:51	2019-03-21 19:53:51	\N	454
13	22	2019-03-21 19:54:23	2019-03-21 19:54:23	\N	455
14	22	2019-03-21 19:54:23	2019-03-21 19:54:23	\N	456
15	22	2019-03-21 19:54:23	2019-03-21 19:54:23	\N	457
16	22	2019-03-21 19:54:23	2019-03-21 19:54:23	\N	458
17	22	2019-03-21 19:54:23	2019-03-21 19:54:23	\N	459
18	22	2019-03-21 19:54:23	2019-03-21 19:54:23	\N	460
19	22	2019-03-21 19:54:23	2019-03-21 19:54:23	\N	461
20	22	2019-03-21 19:54:23	2019-03-21 19:54:23	\N	462
1022	0	2019-03-21 20:17:43	2019-03-21 20:17:43	\N	473
1023	1	2019-03-21 20:18:09	2019-03-21 20:18:09	\N	474
21	22	2019-03-20 18:58:06	2019-03-20 18:58:06	\N	355
22	22	2019-03-20 18:58:06	2019-03-20 18:58:06	\N	356
23	22	2019-03-20 18:58:06	2019-03-20 18:58:06	\N	357
24	22	2019-03-20 18:58:06	2019-03-20 18:58:06	\N	358
25	22	2019-03-20 18:58:06	2019-03-20 18:58:06	\N	359
26	22	2019-03-20 18:58:06	2019-03-20 18:58:06	\N	360
27	22	2019-03-20 18:58:06	2019-03-20 18:58:06	\N	361
28	22	2019-03-20 18:58:06	2019-03-20 18:58:06	\N	362
1024	1	2019-03-21 20:18:30	2019-03-21 20:18:30	\N	475
1025	1	2019-03-21 20:19:14	2019-03-21 20:19:14	\N	476
1026	0	2019-03-22 15:15:29	2019-03-22 15:15:29	\N	557
1027	1	2019-03-22 15:16:28	2019-03-22 15:16:28	\N	558
1028	1	2019-03-22 15:17:05	2019-03-22 15:17:05	\N	559
1023	22	2019-03-22 15:24:06	2019-03-22 15:24:06	\N	562
1024	22	2019-03-22 15:24:06	2019-03-22 15:24:06	\N	563
1025	22	2019-03-22 15:24:06	2019-03-22 15:24:06	\N	564
1018	22	2019-03-22 15:24:11	2019-03-22 15:24:11	\N	565
1019	22	2019-03-22 15:24:11	2019-03-22 15:24:11	\N	566
1020	22	2019-03-22 15:24:11	2019-03-22 15:24:11	\N	567
1021	22	2019-03-22 15:24:11	2019-03-22 15:24:11	\N	568
1027	22	2019-03-22 15:27:25	2019-03-22 15:27:25	\N	571
1028	22	2019-03-22 15:27:25	2019-03-22 15:27:25	\N	572
1029	0	2019-03-22 15:30:02	2019-03-22 15:30:02	\N	573
1030	1	2019-03-22 15:30:26	2019-03-22 15:30:26	\N	574
1031	1	2019-03-22 15:30:45	2019-03-22 15:30:45	\N	575
1032	0	2019-03-22 15:33:45	2019-03-22 15:33:45	\N	579
1033	1	2019-03-22 15:34:15	2019-03-22 15:34:15	\N	580
1034	1	2019-03-22 15:34:32	2019-03-22 15:34:32	\N	581
\.


--
-- TOC entry 2232 (class 0 OID 68286)
-- Dependencies: 190
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: colabora
--

COPY public.permissions (id, name, display_name, description, created_at, updated_at, deleted_at, id_module, id_generic_status) FROM stdin;
15	module-edit	EDITAR MODULO	EDITAR MODULO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	1	1
1	user-list	LISTAR USUARIOS	LISTAR USUARIOS	2019-02-13 18:35:24	2019-02-13 18:35:24	\N	0	1
16	module-delete	ELIMINAR MODULO	ELIMINAR MODULO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	1	1
17	module-show	MOSTRAR MODULO	MOSTRAR MODULO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	1	1
2	user-create	CREAR USUARIO	CREAR USUARIOS	2019-02-13 18:35:24	2019-02-13 18:35:24	\N	0	1
3	user-edit	EDITAR USUARIO	EDITAR USUARIO	2019-02-13 18:35:24	2019-02-13 18:35:24	\N	0	1
4	user-delete	ELIMINAR USUARIO	ELIMINAR USUARIO	2019-02-13 18:35:24	2019-02-13 18:35:24	\N	0	1
5	role-list	LISTAR ROLES	LISTAR ROLES	2019-02-13 18:35:24	2019-02-13 18:35:24	\N	3	1
6	role-create	CREAR ROL	CREAR ROL	2019-02-13 18:35:24	2019-02-13 18:35:24	\N	3	1
7	role-edit	EDITAR ROL	EDITAR ROL	2019-02-13 18:35:24	2019-02-13 18:35:24	\N	3	1
8	role-delete	ELIMINAR ROL	ELIMINAR ROL	2019-02-13 18:35:24	2019-02-13 18:35:24	\N	3	1
9	role-show	MOSTRAR ROL	MOSTRAR ROL	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	3	1
10	role-pdf	IMPRIMIR PDF	IMPRIMIR PDF	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	3	1
11	role-excel	IMPRIMIR EXCEL	IIMPRIMIR EXCEL	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	3	1
1017	107_OCULTO	107_OCULTO	OCULTO	2019-03-21 19:52:21	2019-03-22 15:24:10	\N	107	1
12	role-imprimirPdf	VISTA IMPRIME PDF	VISTA IMPRIME PDF	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	3	1
13	module-list	LISTAR MODULOS	LISTAR MODULOS	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	1	1
14	module-create	CREAR MODULO	CREAR MODULO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	1	1
18	module-pdf	IMPRIMIR PDF	IMPRIMIR PDF	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	1	1
19	module-excel	IMPRIMIR EXCEL	IMPRIMIR EXCEL	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	1	1
20	module-imprimirPdf	VISTA IMPRIME PDF	VISTA IMPRIME PDF	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	1	1
21	permission-list	LISTAR PERMISOS	LISTAR PERMISOS	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	2	1
22	permission-create	CREAR PERMISO	CREAR PERMISO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	2	1
23	permission-edit	EDITAR PERMISO	EDITAR PERMISO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	2	1
24	permission-delete	ELIMINAR PERMISO	ELIMINAR PERMISO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	2	1
25	permission-show	MOSTRAR PERMISO	MOSTRAR PERMISO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	2	1
34	rolepermission-pdf	IMPRIMIR PDF	IMPRIMIR PDF	2019-02-25 13:45:58	2019-02-25 14:01:59	\N	4	1
35	rolepermission-excel	IMPRIMIR EXCEL	IMPRIMIR EXCEL	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	4	1
36	rolepermission-imprimirPdf	VISTA IMPRIME PDF	VISTA IMPRIME PDF	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	4	1
37	rolepermission-edit	EDITAR PERMISO POR ROL	EDITAR PERMISO POR ROL	2019-03-07 17:20:12	2019-03-07 17:20:12	\N	4	1
26	permission-pdf	IMPRIMIR PDF	IMPRIMIR PDF	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	2	1
27	permission-excel	IMPRIMIR EXCEL	IMPRIMIR EXCEL	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	2	1
28	permission-imprimirPdf	VISTA IMPRIME PDF	VISTA IMPRIME PDF	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	2	1
31	rolepermission-asign	ASIGNAR PERMISOS ROL	ASIGNAR PERMISOS ROL	2019-02-22 19:27:34	2019-02-22 20:07:56	\N	4	1
32	rolepermission-create	CREAR PERMISO ROL	CREAR PERMISO POR ROL	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	4	1
1029	110_OCULTO	110_OCULTO	OCULTO	2019-03-22 15:30:02	2019-03-22 15:32:31	2019-03-22 15:32:31	110	1
38	menu-list	LISTAR MENUS	LISTAR MENUS	2019-03-20 15:07:30	2019-03-20 15:07:30	\N	5	1
39	menu-create	CREAR MENU	CREAR MENU	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	5	1
40	menu-edit	EDITAR MENU	EDITAR MENU	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	5	1
41	menu-delete	ELIMINAR MENU	ELIMINAR MENU	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	5	1
42	menu-show	MOSTRAR MENU	MOSTRAR MENU	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	5	1
43	menu-pdf	IMPRIMIR PDF	IMPRIMIR PDF	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	5	1
44	menu-excel	IMPRIMIR EXCEL	IMPRIMIR EXCEL	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	5	1
45	menu-imprimirPdf	VISTA IMPRIMIR PDF	VISTA IMPRIMIR PDF	2019-02-14 00:00:00	2019-02-14 00:00:00	\N	5	1
1033	permisdo_wilpia_1	permisdo_wilpia_1	PERMISDO_WILPIA_1	2019-03-22 15:34:15	2019-03-22 15:34:15	\N	111	1
1023	modulo_max_2_1	modulo_max_2_1	MODULO_MAX_2_1	2019-03-21 20:18:09	2019-03-21 20:18:09	\N	108	1
1024	modulo_max_2_2	modulo_max_2_2	MODULO_MAX_2_2	2019-03-21 20:18:30	2019-03-21 20:18:30	\N	108	1
1025	modulo_max_2_3	modulo_max_2_3	MODULO_MAX_2_3	2019-03-21 20:19:14	2019-03-21 20:19:14	\N	108	1
1026	109_OCULTO	109_OCULTO	OCULTO	2019-03-22 15:15:29	2019-03-22 15:27:25	\N	109	1
1018	modulo_max_1	modulo_max_1	MODULO_MAX_1	2019-03-21 19:52:47	2019-03-21 19:52:47	\N	107	1
1019	modulo_max_2	modulo_max_2	MODULO_MAX_2	2019-03-21 19:53:06	2019-03-21 19:53:06	\N	107	1
1020	modulo_max_3	modulo_max_3	MODULO_MAX_3	2019-03-21 19:53:25	2019-03-21 19:53:25	\N	107	1
1021	modulo_max_4	modulo_max_4	MODULO_MAX_4	2019-03-21 19:53:51	2019-03-21 19:53:51	\N	107	1
1027	qwertyu	qwertyu	QWERTYQWERTYUUU	2019-03-22 15:16:28	2019-03-22 15:16:28	\N	109	1
1028	qwertyop	qwertyop	QWERTYQWERTYOP	2019-03-22 15:17:05	2019-03-22 15:17:05	\N	109	1
1030	permiso_jonas_1	permiso_jonas_1	PERMISO_JONAS_1	2019-03-22 15:30:26	2019-03-22 15:30:26	\N	110	1
1034	permisdo_wilpia_2	permisdo_wilpia_2	PERMISDO_WILPIA_2	2019-03-22 15:34:32	2019-03-22 15:34:32	\N	111	1
1031	permiso_jonas_2	permiso_jonas_2	PERMISO_JONAS_2	2019-03-22 15:30:45	2019-03-22 15:30:45	\N	110	1
1032	111_OCULTO	111_OCULTO	OCULTO	2019-03-22 15:33:45	2019-03-22 15:36:34	2019-03-22 15:36:34	111	1
1022	108_OCULTO	108_OCULTO	OCULTO	2019-03-21 20:17:43	2019-03-22 15:24:06	\N	108	1
\.


--
-- TOC entry 2230 (class 0 OID 68269)
-- Dependencies: 188
-- Data for Name: role_user; Type: TABLE DATA; Schema: public; Owner: colabora
--

COPY public.role_user (user_id, role_id, created_at, updated_at) FROM stdin;
1	1	20189-02-14 00:00:00	2019-02-14 00:00:00
2	1	2019-02-14 00:00:00	2019-02-14 00:00:00
3	1	2019-03-07 13:51:32.991592	2019-03-07 13:51:32.991592
5	22	2019-03-18 10:45:04.872775	2019-03-18 10:45:04.872775
\.


--
-- TOC entry 2229 (class 0 OID 68258)
-- Dependencies: 187
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: colabora
--

COPY public.roles (id, name, display_name, description, created_at, updated_at, id_status_role, deleted_at) FROM stdin;
1	SUPERUSUARIO	SUPERUSUARIO	SUPERUSUARIO	2019-02-13 18:35:24	2019-02-13 18:35:24	1	\N
2	ADMINISTRADOR	ADMINISTRADOR	ADMINISTRADOR	2019-02-13 19:02:57	2019-03-07 15:47:00	1	\N
21	SUPERVISOR ADMINISTRACION	SUPERVISOR ADMINISTRACION	SUPERVISOR ADMINISTRACION	2019-03-07 15:47:54	2019-03-07 15:47:54	1	\N
22	ASISTENTE	ASISTENTE	ASISTENTE PRUEBA	2019-03-07 17:03:15	2019-03-07 17:03:15	1	\N
0	OCULTO	OCULTO	OCULTO	2019-02-14 00:00:00	2019-02-14 00:00:00	1	\N
\.


--
-- TOC entry 2239 (class 0 OID 68381)
-- Dependencies: 197
-- Data for Name: user_status; Type: TABLE DATA; Schema: public; Owner: colabora
--

COPY public.user_status (id, name, description, created_at, updated_at, deleted_at) FROM stdin;
1	ACTIVO	ACTIVO	2019-02-14 00:00:00	2019-02-14 00:00:00	\N
\.


--
-- TOC entry 2226 (class 0 OID 68238)
-- Dependencies: 184
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: colabora
--

COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, deleted_at, id_user_status) FROM stdin;
1	Jeison Villalobos	jvillalobos@arthatecnologias.com	2019-02-13 18:35:23	$2y$10$fLSLSB3Sm5U2h0eGLeM9ue7r9K9kEyFK5jHuyVmpHZKQLzxIdaOGm	\N	2019-02-13 18:35:24	2019-02-13 18:35:24	\N	1
2	Yeferson Uribe	yuribe@arthatecnologias.com	2019-02-13 18:35:24	$2y$10$y0NaRHqktEx5yIuxus9nZew7XR.SChiAWq.blyKWOCVOLfFv45r0i	l6QW6xmIpUrTuNQAwnwESh8uhFjfJBUvDy8HzMv9Ls9mjXCJVl2q0EMa2xjG	2019-02-13 18:35:24	2019-02-13 18:35:24	\N	1
3	Maximo	mgonzalez@arthatecnologias.com	\N	$2y$10$n8f4POuU9PpBf47jtUA0X.UtwR7ZGBlBuB575fHKLD4tU7OovF5TO	nP6wYVT3EFw7U0xwsVjsBuMQWhQ3wPKRTJoJuzS07cfzjNNECJZBYgx8S71E	2019-02-13 18:44:47	2019-03-07 17:51:32	\N	1
5	prueba	prueba@prueba.pru	\N	$2y$10$6.Aypl4urBEvePl/A5Td0Ob8k0SzUagetdYTElP5NNwFZdepEvrVu	FgX8jlVeAei1zNaJgYkOI7KXKSLZJRNOxpdkvgVEEPEIKUkf7Orf0rw5I8AV	2019-03-07 19:08:17	2019-03-18 14:45:04	\N	1
\.


--
-- TOC entry 2278 (class 0 OID 0)
-- Dependencies: 194
-- Name: generic_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: colabora
--

SELECT pg_catalog.setval('public.generic_status_id_seq', 5, true);


--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 198
-- Name: menus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: colabora
--

SELECT pg_catalog.setval('public.menus_id_seq', 3, true);


--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 181
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: colabora
--

SELECT pg_catalog.setval('public.migrations_id_seq', 3, true);


--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 193
-- Name: modules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: colabora
--

SELECT pg_catalog.setval('public.modules_id_seq', 111, true);


--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 200
-- Name: permission_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: colabora
--

SELECT pg_catalog.setval('public.permission_role_id_seq', 585, true);


--
-- TOC entry 2283 (class 0 OID 0)
-- Dependencies: 189
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: colabora
--

SELECT pg_catalog.setval('public.permissions_id_seq', 1034, true);


--
-- TOC entry 2284 (class 0 OID 0)
-- Dependencies: 186
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: colabora
--

SELECT pg_catalog.setval('public.roles_id_seq', 22, true);


--
-- TOC entry 2285 (class 0 OID 0)
-- Dependencies: 196
-- Name: user_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: colabora
--

SELECT pg_catalog.setval('public.user_status_id_seq', 1, true);


--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: colabora
--

SELECT pg_catalog.setval('public.users_id_seq', 5, true);


--
-- TOC entry 2089 (class 2606 OID 68373)
-- Name: generic_status generic_status_name_key; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.generic_status
    ADD CONSTRAINT generic_status_name_key UNIQUE (name);


--
-- TOC entry 2091 (class 2606 OID 68366)
-- Name: generic_status generic_status_pkey; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.generic_status
    ADD CONSTRAINT generic_status_pkey PRIMARY KEY (id);


--
-- TOC entry 2097 (class 2606 OID 76687)
-- Name: menus menus_name_id_key; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.menus
    ADD CONSTRAINT menus_name_id_key UNIQUE (name, id);


--
-- TOC entry 2099 (class 2606 OID 68441)
-- Name: menus menus_pkey; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.menus
    ADD CONSTRAINT menus_pkey PRIMARY KEY (id);


--
-- TOC entry 2064 (class 2606 OID 68235)
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2083 (class 2606 OID 68418)
-- Name: modules modules_display_name_key; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_display_name_key UNIQUE (display_name);


--
-- TOC entry 2085 (class 2606 OID 68416)
-- Name: modules modules_name_key; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_name_key UNIQUE (name);


--
-- TOC entry 2087 (class 2606 OID 68322)
-- Name: modules modules_pkey; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_pkey PRIMARY KEY (id);


--
-- TOC entry 2081 (class 2606 OID 68311)
-- Name: permission_role permission_role_pkey; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_pkey PRIMARY KEY (permission_id, role_id);


--
-- TOC entry 2077 (class 2606 OID 68296)
-- Name: permissions permissions_name_unique; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_name_unique UNIQUE (name);


--
-- TOC entry 2079 (class 2606 OID 68294)
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2075 (class 2606 OID 68283)
-- Name: role_user role_user_pkey; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_pkey PRIMARY KEY (user_id, role_id);


--
-- TOC entry 2071 (class 2606 OID 68268)
-- Name: roles roles_name_unique; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_name_unique UNIQUE (name);


--
-- TOC entry 2073 (class 2606 OID 68266)
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2093 (class 2606 OID 68388)
-- Name: user_status user_status_name_key; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.user_status
    ADD CONSTRAINT user_status_name_key UNIQUE (name);


--
-- TOC entry 2095 (class 2606 OID 68386)
-- Name: user_status user_status_pkey; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.user_status
    ADD CONSTRAINT user_status_pkey PRIMARY KEY (id);


--
-- TOC entry 2066 (class 2606 OID 68248)
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 2068 (class 2606 OID 68246)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2069 (class 1259 OID 68255)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: colabora
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- TOC entry 2108 (class 2606 OID 68395)
-- Name: modules modules_id_status_module_fkey; Type: FK CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_id_status_module_fkey FOREIGN KEY (id_status_module) REFERENCES public.generic_status(id);


--
-- TOC entry 2106 (class 2606 OID 68300)
-- Name: permission_role permission_role_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2107 (class 2606 OID 68305)
-- Name: permission_role permission_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2104 (class 2606 OID 68429)
-- Name: permissions permissions_id_generic_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_id_generic_status_fkey FOREIGN KEY (id_generic_status) REFERENCES public.generic_status(id);


--
-- TOC entry 2105 (class 2606 OID 76688)
-- Name: permissions permissions_id_module_fkey; Type: FK CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_id_module_fkey FOREIGN KEY (id_module) REFERENCES public.modules(id);


--
-- TOC entry 2103 (class 2606 OID 68277)
-- Name: role_user role_user_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2102 (class 2606 OID 68272)
-- Name: role_user role_user_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2101 (class 2606 OID 68456)
-- Name: roles roles_id_status_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_id_status_role_fkey FOREIGN KEY (id_status_role) REFERENCES public.generic_status(id);


--
-- TOC entry 2100 (class 2606 OID 68390)
-- Name: users users_id_user_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: colabora
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_user_status_fkey FOREIGN KEY (id_user_status) REFERENCES public.user_status(id);


--
-- TOC entry 2250 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2019-04-01 08:45:44 -04

--
-- PostgreSQL database dump complete
--

