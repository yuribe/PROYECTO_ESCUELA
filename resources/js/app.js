
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//require('./components/Notifications');


import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Notifications from './components/Notifications';
import socketIOClient from "socket.io-client";

class App extends Component{
	constructor(props) {

        super(props)
            this.state= {
                isAuth:false,
                ip:'',
                idusuario:'',
                idsesion:'',
                endpoint: "http://127.0.0.1:6379"
            }


        this.state = {
              modalIsOpen: false
        };

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    componentDidMount() {
        //const { endpoint } = this.state;
        //const socket = socketIOClient(endpoint);
        const userId = document.getElementById('userId').content;
        const ip = document.getElementById('ip').content;
        var user_id_Data = '';
        var ip_Data = '';



        //console.log(endpoint);

        axios.get(window.location.hostname+"/usersessionUsuario/"+2)
        .then(
            res => {
                console.log("res "+res);
                for(var i in res.data['data']){
                    user_id_Data = res.data['data'][i].user_id.toString();
                    ip_Data = res.data['data'][i].ip;

                    console.log(userId+"-------"+user_id_Data);
                    
                    if(userId == user_id_Data && ip==ip_Data){
                        //console.log("a la verga"+res.data['data'][i].id);
                        //this.openModal();
                    }
                    
                }
              //this.setState({ persons: res.data.network.stations });
        });
        

        Echo.private('App.User.'+ userId)
            .notification(
                (notification)=> {
                    //console.log(this.notifications);
                    //io.emit(notification);
                    console.log(notification);
                    //console.log(userId);
                    this.notifications.push(notification);
        });

    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        this.subtitle.style.color = '#000';
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }


    render() {

        return (
            <div>
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel="Example Modal"
                    role="dialog"
                    ariaHideApp={false}
                >
                    <h1 ref={subtitle => this.subtitle = subtitle}><center>Información</center></h1>
                    <hr/>
                    <div><b>Estimado usuario se le informa que su sesión actual será cerrada por existir otra sesión activa</b></div>
                    <hr />
                    <center><button onClick={this.closeModal} className="btn btn-info">Aceptar</button></center>
                </Modal>
            </div>
        );
        if (document.getElementById('notifications')) {
            ReactDOM.render(<Notifications />, document.getElementById('notifications'));
        }
    }

    
}

/*
if (document.getElementById('notifications')) {
    ReactDOM.render(<Notifications />, document.getElementById('notifications'));
}
*/

