<!DOCTYPE html>
<html>
<head>
  @include('layouts.parciales.head')
  @yield('css')
</head>
<body class="hold-transition login-page">
    @yield('content')

  @include('layouts.parciales.script')
  @yield('script')
</body>
</html>
