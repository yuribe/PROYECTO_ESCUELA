<!DOCTYPE html>
<html>
<head>
  @include('layouts.parciales.head')
  @yield('css')

  <meta name="userId" id="userId"
    content="{{ Auth::check() ? Auth::user()->id : '' }}">

  <meta name="ip" id="ip"
    content="{{ $ip = \Request::ip()}}">

  <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  @include('layouts.parciales.top')

  <!-- Main Sidebar Container -->
  @include('layouts.parciales.menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <ol class="breadcrumb float-sm-left">
              @yield('breadcrumb')
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @yield('content')
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @include('layouts.parciales.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('layouts.parciales.script')

@yield('script')
</body>
</html>
