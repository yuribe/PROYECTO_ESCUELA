<aside class="main-sidebar sidebar-dark-primary elevation-4">
<!-- Brand Logo -->
<a href="#" class="brand-link">
  <img src="{{ asset('/img/arthalogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
  <span class="brand-text font-weight-light">Proyecto Base</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
  <!-- Sidebar user panel (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
      <img src="{{ asset('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
      <a href="#" class="d-block">{{ Auth::user()->name }}</a>
    </div>
  </div>

  <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
        @if (!Auth::guest())
          @foreach(buildMenu() as $qas1 =>$generic1) <!--FOR EACH 1 NO IMPRIME-->
            @if ($generic1->id === 1)
              <li class="nav-header"><b>{{ $generic1->display_names }}</b></li> <!-- Título del menu, primer registro -->
            @continue
            @endif

            @if($generic1->module_menu_id === 99)
              <li class="nav-item has-treeview">
                <a href=""  class="nav-link">
                  <i class="{{ $generic1->menu_icon }}"></i>
                  <b>{{ $generic1->display_names }}</b>
                  <i class="fa fa-angle-left right"></i>
                </a>
                <ul class="nav nav-treeview">

               @foreach(buildMenuChild($generic1->id) as $qas2 =>$generic2) <!--FOR EACH 2 -->
                 @if($generic2->module_menu_id === 99)
                 <li class="nav-item has-treeview">
                   <a href=""  class="nav-link">
                     <i class="{{ $generic2->menu_icon }}"></i>
                     <b>{{ $generic2->display_names }}</b>
                     <i class="fa fa-angle-left right"></i>
                   </a>
                   <ul class="nav nav-treeview">
                   @foreach(buildMenuChild($generic2->id) as $qas3 =>$generic3) <!--FOR EACH 2 -->
                     @if($generic3->module_menu_id === 99)
                      <li class="nav-item has-treeview">
                         <a href=""  class="nav-link">
                           <i class="{{ $generic3->menu_icon }}" style="margin-left:15px;"></i>
                           <b>{{ $generic3->display_names }}</b>
                           <i class="fa fa-angle-left right"></i>
                         </a>
                         <ul class="nav nav-treeview">
                      @else
                        <li class="nav-item">
                           <a href="{{ url($generic3->module_name) }}" class="nav-link">
                             <i class=" {{ $generic3->menu_icon }}" style="margin-left:15px;"></i>
                             {{ $generic3->display_names }}
                           </a>
                        </li>
                      @endif
                   @endforeach
                   </ul>
                 </li>
                 @else
                   <li class="nav-item">
                     <a href="{{ url($generic2->module_name) }}" class="nav-link">
                       <i class=" {{ $generic2->menu_icon }}" style="margin-left:15px;"></i>
                       {{ $generic2->display_names }}
                     </a>
                   </li>
                 @endif
               @endforeach <!--FOR EACH 2-->
                </ul>
              </li>
            @else
              @if($generic1->menu_parent === 1)
                <a href="{{ url($generic1->module_name) }}"  class="nav-link">
                  <i class="{{ $generic1->menu_icon }}"></i>
                  <b>{{ $generic1->display_names }}</b>
                </a>
              @endif
            @endif
          @endforeach <!--FOR EACH 1 NO IMPRIME-->
        @endif
      </ul>
    </nav>

  <!-- Sidebar Menu
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
       Add icons to the links using the .nav-icon class with font-awesome or any other icon font library
      <li class="nav-item has-treeview">
        <a href=""  class="nav-link">
          <i class="nav-icon fa fa-lock"></i>
          <p>
            Seguridad
            <i class="fa fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('modules.index') }}" class="nav-link">
              <i class="fa fa-cir</li>
          </ul>cle-o nav-icon"></i>
              <p>Módulos(Rutas)</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('menus.index') }}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Menús</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('permissions.index') }}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Permisos(Abilities)</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('roles.index') }}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Roles</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('rolepermissions.index') }}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Permisos por Roles</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('users.index') }}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Usuarios</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('genericstatus.index')}}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Estatus generales</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('userstatus.index')}}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Estatus usuarios</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('usersession.index') }}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Sesiones</p>
            </a>
          </li>
        </ul>
      </li>

      <li class="nav-item has-treeview">
        <a href="" class="nav-link">
          <i class="nav-icon fa fa-eye"></i>
          <p>
            Auditoría
            <i class="fa fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('traces.index') }}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Consulta Trazas</p>

            </a>
          </li>
        </ul>
      </li>

      <li class="nav-item has-treeview">
        <a href="" class="nav-link">
          <i class="nav-icon fa fa-user"></i>
          <p>
            Ejemplos
            <i class="fa fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('examples.index-criteros-tabla') }}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Index/Criterios/Tabla</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('examples.index-tabla') }}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Index/Tabla</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('examples.create') }}" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Create/Edit</p>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item">

        <a href="{{ route('config.index') }}" class="nav-link">
          <i class="fa fa-cogs nav-icon"></i>
          <p>Configuraciones</p>
        </a>
      </li>
    </ul>

  </nav>
-->

  <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>
