@if (session('message_success'))
<div class="alert alert-success" role="alert">
  {{ session('message_success') }}
</div>
@elseif (session('message_error'))
<div class="alert alert-danger" role="alert">
  {{ session('message_error') }}
</div>
@endif

@if ($errors->any())
<div class="alert alert-danger" role="alert">
  @foreach ($errors->all() as $error)
  {{ $error }}<br/>
  @endforeach
</div>
@endif
<div  id="notifications"></div>

<div class="alert alert-error" id="msj" style="display:none">
  <p id="message"></p>
</div>

<div class="alert alert-success" id="msjsuccess" style="display:none">
  <p id="message-success"></p>
</div>

<script src="/js/app.js"></script>

