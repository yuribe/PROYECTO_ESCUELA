<!-- /.content-wrapper -->
<footer class="main-footer">
<strong>Copyright &copy; 2019 <a href="http://www.arthatecnologias.com/">ARTHA TECNOLOGÍAS</a>.</strong>
	Todos los derechos reservados.
<div class="float-right d-none d-sm-inline-block">
  <b>Versión</b> 1.0.0-alpha
</div>
</footer>
