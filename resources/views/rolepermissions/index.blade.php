@extends('layouts.app')

<?php

    if (isset($role_id))
      $role_id = $role_id;
    else
      $role_id = "";
    if (isset($module_id))
      $module_id = $module_id;
    else
      $module_id = "";
    if (isset($name))
      $name = $name;
    else
      $name = "";
    if (isset($display_name))
      $display_name = $display_name;
    else
      $display_name = "";

?>

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item active"><a href="{{ route('rolepermissions.index') }}">Permisos por roles</a></li>

@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  @include('layouts.parciales.messages')

	{{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
		</div>
	{{-- FORMULARIO - INICIO --}}
    <form class="form" id="indexRolepermissionsForm" name = "indexRolepermissionsForm" role="form" data-toggle="validator" method="GET" action="{{ route('rolepermissions.index') }}">
			<div class="card-body">
				<div class="row">
          <div class="col-md-3">
						<div class="form-group">
							<label for="role_id">Rol</label>
              <select class="form-control" id="role_id" name="role_id">
                <option value="" selected>Seleccione una opción</option>
                @foreach($roles as $role)
                  <option value="{{ $role->id }}">{{ $role->name }}</option>
                @endforeach
              </select>
						</div>
					</div>
          <div class="col-md-3">
						<div class="form-group">
							<label for="name">Nombre del permiso(Ability)</label>
							<input class="form-control" id="name" name="name">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="display_name">Nombre a mostrar</label>
							<input class="form-control" id="display_name" name="display_name">
						</div>
					</div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="module_id">Módulo(Ruta)</label>
              <select class="form-control" id="module_id" name="module_id">
                <option value="" selected>Seleccione una opción</option>
                @foreach($modules as $module)
                  <option value="{{ $module->id }}">{{ $module->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
				</div>


			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
			<div class="card-footer">
				<div class="pull-right">
					<button type="button" onClick="buscar();" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
				 	  <a href="{{ route('rolepermissions.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
					<button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
				</div>
			</div>
			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

		</form>
		{{-- FORMULARIO - FIN --}}
	</div>

	{{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

	{{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Permisos por roles</b></h3>
    </div>
    <div class="card-footer">
			@permission('rolepermission-create')
        <a href="{{ route('rolepermissions.asign') }}" class="btn btn-info"> <i class="fa fa-plus"></i> Asignar permisos a rol</a>
			@endpermission
      @permission('rolepermission-pdf')
        <button type="button" onClick="imprimePdf();" class="btn btn-info"><i class="fa fa-file"></i> Pdf</button>
      @endpermission
      @permission('rolepermission-excel')
        <button type="button" onClick="imprimeExcel();" class="btn btn-info"><i class="fa fa-file"></i> Excel</button>
      @endpermission
    </div>
    <div class="card-body">
        <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th style="width: 5%">N°</th>
            <th style="width: 20%">Rol</th>
            <th style="width: 25%">Nombre del permiso(Ability)</th>
						<th style="width: 25%">Nombre a mostrar</th>
            <th style="width: 25%">Módulo(Ruta)</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($rolepermissions as $key => $rolepermission)
          <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $rolepermission->role_name }}</td>
            <td>{{ $rolepermission->permission_name }}</td>
            <td>{{ $rolepermission->permission_display }}</td>
            <td>{{ $rolepermission->module_name }}</td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          responsive: true,
          searching: false,
          paginate: true,
        });
        // DATATABLE - FIN //

      });

      function buscar()
      {
        var formEdit = document.forms['indexRolepermissionsForm'];
        var vrole_id = document.getElementById("role_id").value;
        var vmodule_id = document.getElementById("module_id").value;
        var vname = document.getElementById("name").value;
        var vdisplay_name = document.getElementById("display_name").value;

        if (vrole_id === '' && vmodule_id === '' && vname === '' && vdisplay_name === '')
        {
          $('#message').html("@lang('message.seleccionarcriterio')");
          $('#msj').show();
          desvanecer();
        } else
        {
          formEdit.submit();
        }

      }

      function imprimePdf()
      {
        var role_id = '<?php echo $role_id; ?>';
        var module_id = '<?php echo $module_id; ?>';
        var name = '<?php echo $name; ?>';
        var display_name = '<?php echo $display_name; ?>';
        window.open('/rolepermissions/pdf' + '?' + '&name=' + name + '&display_name=' + display_name + '&role_id=' + role_id + '&module_id=' + module_id, '_blank');

      }

      function imprimeExcel()
      {
        var name = '<?php echo $name; ?>';
        var display_name = '<?php echo $display_name; ?>';
        var module_id = '<?php echo $module_id; ?>';
        var role_id = '<?php echo $role_id; ?>';
        window.open('/rolepermissions/excel' + '?' + '&name=' + name + '&display_name=' + display_name + '&role_id=' + role_id + '&module_id=' + module_id, '_blank');

      }

      </script>
    @endsection

@endsection
