
@extends('layouts.app')

<?php

    if ($rol !="NULO")
    {
      $name = $rol->name;
      $id_rol = $rol->id;
    }
    else
    {
      $name = "";
      $id_rol = 0;
    }
?>

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('rolepermissions.index') }}">Permisos por Rol</a></li>
<li class="breadcrumb-item active"><a>Nuevo(s) Permiso(s) por Rol</a></li>

@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  @include('layouts.parciales.messages')

	{{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

	<div class="card card-info">
    @if($name == "")
    <div class="card-header">
			<h3 class="card-title"><b>Selección de Rol a Asignar Permisos</b></h3>
		</div>
	{{-- FORMULARIO - INICIO --}}

      <form class="form" id="asignRolepermissionsForm" name = "asignRolepermissionsForm" role="form" data-toggle="validator" method="GET" action="{{ route('rolepermissions.asign') }}">
  			<div class="card-body">
  				<div class="row">
            <div class="col-md-4">
  						<div class="form-group">
  							<label for="role_id">Rol</label>
                <select class="form-control" id="role_id" name="role_id">
                  <option value="" selected>Seleccione una opción</option>
                  @foreach($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                  @endforeach
                </select>
  						</div>
  					</div>
  				</div>
    			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
    			<div class="card-footer">
    				<div class="pull-right">
    					<button type="button" onClick="buscar();" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
    				 	<a href="{{ route('rolepermissions.index') }}" class="btn btn-danger">Cancelar</a>
    				</div>
    			</div>
    			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}
        </div>
  		</form>
    @endif
		{{-- FORMULARIO - FIN --}}
	</div>

	{{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

	{{-- TARJETA TABLA - INICIO --}}

  @if($name != "")
    <div class="card card-info">
      <div class="card-header">
        <h3 class="card-title"><b>Asignar Permiso(s) al Rol {{ $name }}</b></h3>
      </div>
      <div class="card-footer">
        <h3>Módulos del Sistema</h3>
      </div>
      <div class="card-body">
        @foreach($modulepermissions as $modulepermission)
          <div class="btn-group col-2">
              <button type="button" class="btn btn-secondary col-12" style="margin-right:1px; margin-left:1px; margin-bottom:5px; margin-top:5px" title="Editar permisos" data-toggle="modal" data-target="#myModal_{{ $modulepermission->module_id }}">
                {{ $modulepermission->module_name }}
              </button>
         </div>
              <div class="modal fade" tabindex="-1" role="dialog" id="myModal_{{ $modulepermission->module_id }}">
                <div class="modal-dialog">
                  <div class="modal-content">
                    {!! Form::model($modulepermissions, ['method' => 'PATCH','route' => ['rolepermissions.update', $modulepermission->module_id]]) !!}
                  	{{ csrf_field() }}
                      <div class="modal-header col-md-12">
                        <h4 class="modal-title" id="myModalLabel">Permisos (Abilities) del Módulo: <br><B> {{ $modulepermission->module_name }} </B></h4>
                      </div>
                      @if($modulepermission->module_id > 100)
                        <div class="modal-header col-md-12">
                          <div class="input-group" style="margin-right:10px; margin-bottom:10px; margin-top:10px">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                @if($modulepermission->inhabilitado != null)
                                  <input type="checkbox" onchange="bloquea({{ $modulepermission->module_id }});" id="bloqueaModulo{{ $modulepermission->module_id }}" name="bloqueaModulo" checked>
                                @else
                                  <input type="checkbox" onchange="bloquea({{ $modulepermission->module_id }});" id="bloqueaModulo{{ $modulepermission->module_id }}" name="bloqueaModulo">
                                @endif
                              </div>
                            </div>
                            <input type="text" id='txtinhabilitar' name='txtinhabilitar' class="form-control" value = "Inhabilitar módulo" readonly>
                          </div>
                        </div>
                      @endif
                      <div class="modal-body" id='{{ $modulepermission->module_id }}' name='{{ $modulepermission->module_id }}'>
                      @foreach($rolepermisos as $permiso)
                          @if($permiso->permiso_id_module === $modulepermission->module_id)
                            @if($permiso->permiso_name != $permiso->permiso_id_module.'_OCULTO')
                              @if($modulepermission->inhabilitado === null)
                                <div class="input-group" style="margin-right:10px; margin-bottom:10px; margin-top:10px">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <input type="hidden" class="form-control" name="hiddenPermiso[{{ $permiso->id_permiso }}]" value="default">
                                      @if(($permiso->role_permiso_role_id === $id_rol))
                                        <input type="checkbox" id={{ $permiso->id_permiso }} name="checkPermiso[{{ $permiso->id_permiso }}]" checked >
                                      @else
                                        <input type="checkbox" id={{ $permiso->id_permiso }} name="checkPermiso[{{ $permiso->id_permiso }}]" >
                                      @endif
                                    </div>
                                  </div>
                                  <input type="text" class="form-control" value = "{{ $permiso->permiso_name }}" readonly>
                                  <input type="hidden" class="form-control col-md-12" id="id_rol" name="id_rol" value = {{ $id_rol }}>
                                </div>
                              @endif
                            @endif
                          @endif
                        @endforeach
                      </div>
                      <div class="modal-footer">
                        <button type="submit" id="guardar" class="btn btn-info" >Guardar</button>
                        <button  id='cerrar' type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                      </div>
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
        @endforeach
      </div>
      <div class="card-footer">
        <div class="pull-right">
           <a href="{{ route('rolepermissions.index') }}" class="btn btn-info">Terminar</a>
         </div>
      </div>
    </div>
  @endif
    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {


      });

      function bloquea(valor)
      {
        var checks = document.getElementById(valor);
        var checkBloquea = document.getElementById('bloqueaModulo'+valor);

        if(checkBloquea.checked)
        {
          checks.style.visibility = 'hidden';
        }
        else
        {
          checks.style.visibility = 'visible';
        }
      }


      function buscar()
      {
        var formEdit = document.forms['asignRolepermissionsForm'];
        var vrole_id = document.getElementById("role_id").value;

        if (vrole_id === '')
        {
          $('#message').html("@lang('message.seleccionarcriterio')");
          $('#msj').show();
          desvanecer();
        } else
        {
          formEdit.submit();
        }
      }
      </script>
    @endsection

@endsection
