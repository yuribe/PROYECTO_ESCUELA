@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
<li class="breadcrumb-item"><a>Seguridad</a></li>
<li class="breadcrumb-item"><a href="{{ route('users.index') }}">Usuarios</a></li>
<li class="breadcrumb-item active"><a>Mostrar usuario</a></li>

@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  {{-- TARJETA - INICIO --}}
  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Mostrar Usuario</b></h3>
    </div>

      <div class="card-body">

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}
        <div class="row">
          <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3"> <!-- Columna foto -->
             <br><br><br>
             <div class="row">
               <div class="form-group">
                 <label for="img_imagen">Foto</label>
                 <div class="preview">
                   <img style="margin:10px 10px 10px 30px" src="{{ asset($user->photo_directory) }}" id="img_imagen" class="rounded-circle img-thumbnail img-responsive" alt="Foto usuario">
                 </div>
               </div>
             </div>
          </div> <!-- Columna foto -->

          <div class="col-xs-9 col-sm-9 col-lg-9 col-md-9">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><b>Datos de sistema</b></h3>
              </div>
              <div class="card-body">

                <label for="name">Nombre de usuario</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
              		   <span class="input-group-text"> <i class="fa fa-user"></i> </span>
              		</div>
                  <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" readonly/>
                </div>

                <label for="email">Correo electrónico</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
              		   <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
              		</div>
                  <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}" readonly/>
                </div>

                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                    <div class="form-group">
                      <label for="role">Rol (es) del usuario</label>
                      @if(!empty($user->usuarioroles))
                        @foreach($user->roles as $v)
                        <span class="input-group-text">
                          <i class="fa fa-user-secret"></i>&nbsp;&nbsp;&nbsp;
                          <input type="text" class="form-control" value="{{ $v->display_name }}" readonly/>
                        </span>
                        @endforeach
                      @endif
                    </div>
                  </div>

                  <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                    <label for="Status">Estatus del usuario</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                  		   <span class="input-group-text"> <i class="fa fa-universal-access"></i> </span>
                  		</div>
                      <input type="text" class="form-control" id="user_estatus" name="user_estatus" value="{{ $user->estatususers->name }}" readonly/>
                    </div>
                 </div>

               </div>

            </div><!-- Fin Card Body Sistema -->
          </div><!-- Fin Card Info Sistema -->

          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title"><b>Datos personales</b></h3>
            </div>
          <div class="card-body">

            <div class="row">
              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <label for="identification_number">Cédula</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-id-card-o"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="identification_number" name="identification_number" value="{{ $user->identification_number }}" readonly/>
                </div>
              </div>

              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <label for="gender">Género</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-venus-mars"></i> </span>
                  </div>
                  @if($user->gender === 'M')
                    <input type="text" class="form-control" id="gender" name="gender" value="MASCULINO" readonly/>
                  @else
                    <input type="text" class="form-control" id="gender" name="gender" value="FEMENINO" readonly/>
                  @endif
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <label for="first_name">Primer nombre</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-user-circle"></i> </span>
                  </div>
                  <input type="text" class="form-control mayus" id="first_name" name="first_name" value="{{ $user->first_name }}" readonly/>
                </div>
              </div>

              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <label for="middle_name">Segundo nombre</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-user-circle"></i> </span>
                  </div>
                  <input type="text" class="form-control mayus" id="middle_name" name="middle_name" value="{{ $user->middle_name }}" readonly/>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <label for="last_name">Primer apellido</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-user-circle"></i> </span>
                  </div>
                  <input type="text" class="form-control mayus" id="last_name" name="last_name" value="{{ $user->last_name }}" readonly/>
                </div>
              </div>

              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <label for="second_surname">Segundo apellido</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-user-circle"></i> </span>
                  </div>
                  <input type="text" class="form-control mayus" id="second_surname" name="second_surname" value="{{ $user->second_surname }}" readonly/>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="phone">Teléfono</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}" readonly/>
                </div>
              </div>

              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="work_phone">Teléfono oficina</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-phone-square"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="work_phone" name="work_phone" value="{{ $user->work_phone }}" readonly/>
                </div>
              </div>

              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="cellphone">Teléfono celular</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-mobile"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="cellphone" name="cellphone" value="{{ $user->cellphone }}" readonly/>
                </div>
              </div>
            </div>

            <label for="alternative_email">Correo electrónico alternativo</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-envelope-o"></i> </span>
              </div>
              <input type="text" class="form-control" id="alternative_email" name="alternative_email" value="{{ $user->alternative_email }}" readonly/>
            </div>

            <label for="home_address">Dirección de habitación</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-home"></i> </span>
              </div>
              <textarea class="form-control mayus" name="home_adress"  id="home_adress" style="resize: none" readonly>{{ $user->home_address }}</textarea>
            </div>

            <label for="work_address">Dirección de oficina</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-building-o"></i> </span>
              </div>
              <textarea class="form-control mayus" name="work_adress"  id="work_adress" style="resize: none" readonly>{{ $user->work_address }}</textarea>
            </div>

            <div class="row">
              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="municipio">País</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-map-marker"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="pais" name="pais" value="{{  $user->countries->name }}" readonly/>
                </div>
              </div>
            </div>

            <label for="location">Ubicación</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-map-marker"></i> </span>
              </div>
              <textarea class="form-control mayus" name="location"  id="location" style="resize: none" readonly>{{ $user->location }}</textarea>
            </div>

            <div class="row">
              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="estado">Estado</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-map-marker"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="estado" name="estado" value="{{ $user->states->name }}" readonly/>
                </div>
              </div>

              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="municipio">Municipio</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-map-marker"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="municipio" name="municipio" value="{{  $user->municipalities->name }}" readonly/>
                </div>
              </div>

              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="municipio">Parroquia</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-map-marker"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="parroquia" name="parroquia" value="{{ $user->parishes->name }}" readonly/>
                </div>
              </div>
            </div>


          </div><!-- Fin Card Body Personales -->
        </div><!-- Fin Card Info Personal -->
      </div><!-- Fin col 9 -->
    </div> <!-- Fin row -->
  </div><!-- Fin card body -->


      {{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
      <div class="card-footer">
  			<div class="pull-right">
        	<a href="{{ route('users.index') }}" class="btn btn-danger">Regresar</a>
  			</div>
  		</div>
      {{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}


  </div>


  @section('scripts')
    <script type="text/javascript">

  	$(document).ready( function () {


  	});



  	</script>

  @endsection



  @endsection
