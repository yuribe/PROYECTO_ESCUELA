@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
<li class="breadcrumb-item"><a>Seguridad</a></li>
<li class="breadcrumb-item"><a href="{{ route('users.index') }}">Usuarios</a></li>
<li class="breadcrumb-item active"><a>Edición usuario</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  {{-- TARJETA - INICIO --}}
  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Edición Usuario</b></h3>
    </div>

  {{-- FORMULARIO - INICIO --}}
  {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
  {{ csrf_field() }}

      <div class="card-body">

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}
        <div class="row">
          <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3"> <!-- Columna foto -->
             <br><br><br>
             <div class="row">
                <div class="form-group">
                  <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                    <label for="clvarea">Foto</label>
                    <div class="preview">
                      <img src="{{ asset($user->photo_directory) }}" id="img_imagen" class="img-thumbnail img-responsive" width="100%">
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12" style="margin:9px 0px 10px 0px">
                    <button type="button" class="btn btn-md btn-block btn-info" onclick="botonFoto();" ><i class="fa fa-retweet"></i> Seleccionar imagen</button>
                  </div>
                  <input type="file" class="form-control" style="visibility:hidden"; "padding:0px 0px 0px 0px;" name="imagen_user" id="imagen_user">
                  </div>
              </div>

          </div> <!-- Columna foto -->

          <div class="col-xs-9 col-sm-9 col-lg-9 col-md-9">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><b>Datos de sistema</b></h3>
              </div>
              <div class="card-body">

                <label for="name">Nombre de usuario</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
              		   <span class="input-group-text"> <i class="fa fa-user"></i> </span>
              		</div>
                  <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" />
                  @if ($errors->has('name'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('name') }}</strong>
                      </span>
                  @endif
                </div>

                <label for="email">Correo electrónico</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
              		   <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
              		</div>
                  <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}" />
                  @if ($errors->has('email'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>

                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                    <label for="password">Contraseña</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                  		   <span class="input-group-text"> <i class="fa fa-key"></i> </span>
                  		</div>
                      <input type="text" class="form-control" id="password" name="password" value=" " />
                      @if ($errors->has('password'))
                          <span class="text-danger">
                            <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>

                  <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                    <label for="password">Confirmar contraseña</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                  		   <span class="input-group-text"> <i class="fa fa-check"></i> </span>
                  		</div>
                      <input type="text" class="form-control" id="password" name="password" value=" " />
                      @if ($errors->has('password'))
                          <span class="text-danger">
                            <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                    <div class="form-group">
                      <label for="role">Rol (es) del usuario</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                    		   <span class="input-group-text"> <i class="fa fa-user-secret"></i> </span>
                    		</div>
                        {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple','id' => 'role')) !!}
                        @if ($errors->has('password'))
                            <span class="text-danger">
                              <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                    <div class="form-group">
                      <label for="user_status_id">Estatus del usuario</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                    		   <span class="input-group-text"> <i class="fa fa-universal-access"></i> </span>
                    		</div>
                        <select class="form-control" id="user_status_id" name="user_status_id">
                          <option value = "" disabled>Seleccione una opción</option>
                          @foreach($generics as $generic)
                            @if($generic->id === $user->user_status_id)
                              <option value="{{ $generic->id }}" selected="selected" >{{ $generic->name }}</option>
                            @else
                              <option value="{{ $generic->id }}">{{ $generic->name }}</option>
                            @endif
                          @endforeach
                        </select>
            						@if ($errors->has('user_status_id'))
                            <span class="text-danger">
                              <strong>{{ $errors->first('user_status_id') }}</strong>
                            </span>
                        @endif
                      </div>
          					</div>
                  </div>
               </div>

            </div><!-- Fin Card Body Sistema -->
          </div><!-- Fin Card Info Sistema -->

          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title"><b>Datos personales</b></h3>
            </div>
          <div class="card-body">

            <div class="row">
              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <label for="identification_number">Cédula</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-id-card-o"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="identification_number" name="identification_number" value="{{ $user->identification_number }}" />
                  @if ($errors->has('identification_number'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('identification_number') }}</strong>
                      </span>
                  @endif
                </div>
              </div>

              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <div class="form-group">
                  <label for="gender">Género</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                       <span class="input-group-text"> <i class="fa fa-venus-mars"></i> </span>
                    </div>
                    <select class="form-control" id="gender" name="gender">
                      <option value = "" disabled>Seleccione una opción</option>
                      @if($user->gender === 'M')
                        <option value="M" selected="selected">MASCULINO</option>
                        <option value="F">FEMENINO</option>
                      @else
                      <option value="M">MASCULINO</option>
                      <option value="F" selected="selected">FEMENINO</option>
                      @endif
                    </select>
                    @if ($errors->has('gender'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('gender') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <label for="first_name">Primer nombre</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-user-circle"></i> </span>
                  </div>
                  <input type="text" class="form-control mayus" id="first_name" name="first_name" value="{{ $user->first_name }}" />
                  @if ($errors->has('first_name'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('first_name') }}</strong>
                      </span>
                  @endif
                </div>
              </div>

              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <label for="middle_name">Segundo nombre</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-user-circle"></i> </span>
                  </div>
                  <input type="text" class="form-control mayus" id="middle_name" name="middle_name" value="{{ $user->middle_name }}" />
                  @if ($errors->has('middle_name'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('middle_name') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <label for="last_name">Primer apellido</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-user-circle"></i> </span>
                  </div>
                  <input type="text" class="form-control mayus" id="last_name" name="last_name" value="{{ $user->last_name }}" />
                  @if ($errors->has('last_name'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('last_name') }}</strong>
                      </span>
                  @endif
                </div>
              </div>

              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                <label for="second_surname">Segundo apellido</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-user-circle"></i> </span>
                  </div>
                  <input type="text" class="form-control mayus" id="second_surname" name="second_surname" value="{{ $user->second_surname }}" />
                  @if ($errors->has('second_surname'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('second_surname') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="phone">Teléfono</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}" />
                  @if ($errors->has('phone'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('phone') }}</strong>
                      </span>
                  @endif
                </div>
              </div>

              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="work_phone">Teléfono oficina</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-phone-square"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="work_phone" name="work_phone" value="{{ $user->work_phone }}" />
                  @if ($errors->has('work_phone'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('work_phone') }}</strong>
                      </span>
                  @endif
                </div>
              </div>

              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="cellphone">Teléfono celular</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-mobile"></i> </span>
                  </div>
                  <input type="text" class="form-control" id="cellphone" name="cellphone" value="{{ $user->cellphone }}" />
                  @if ($errors->has('cellphone'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('cellphone') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
            </div>

            <label for="alternative_email">Correo electrónico alternativo</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-envelope-o"></i> </span>
              </div>
              <input type="text" class="form-control" id="alternative_email" name="alternative_email" value="{{ $user->alternative_email }}" />
              @if ($errors->has('alternative_email'))
                  <span class="text-danger">
                    <strong>{{ $errors->first('alternative_email') }}</strong>
                  </span>
              @endif
            </div>

            <label for="home_address">Dirección de habitación</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-home"></i> </span>
              </div>
              <textarea class="form-control mayus" name="home_adress"  id="home_adress" style="resize: none">{{ $user->home_address }}</textarea>
              @if ($errors->has('home_address'))
                  <span class="text-danger">
                    <strong>{{ $errors->first('home_address') }}</strong>
                  </span>
              @endif
            </div>

            <label for="work_address">Dirección de oficina</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-building-o"></i> </span>
              </div>
              <textarea class="form-control mayus" name="work_adress"  id="work_adress" style="resize: none">{{ $user->work_address }}</textarea>
              @if ($errors->has('work_address'))
                  <span class="text-danger">
                    <strong>{{ $errors->first('work_address') }}</strong>
                  </span>
              @endif
            </div>

            <div class="row">
              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <div class="form-group">
                  <label for="country_id">País</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                       <span class="input-group-text"> <i class="fa fa-map-marker"></i> </span>
                    </div>
                    <select class="form-control" id="country_id" name="country_id">
                      <option value = "" selected>Seleccione una opción</option>
                      @foreach($countries as $country)
                       @if($country->id === $user->country_id)
                          <option value="{{ $country->id }}" selected="selected">{{ $country->name }}</option>
                        @else
                          <option value="{{ $country->id }}">{{ $country->name }}</option>
                        @endif
                      @endforeach
                    </select>
                    @if ($errors->has('country_id'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('country_id') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
              </div>
            </div>

            <label for="location">Ubicación</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-map-marker"></i> </span>
              </div>
              <textarea class="form-control mayus" name="location"  id="location" style="resize: none">{{ $user->location }}</textarea>
              @if ($errors->has('location'))
                  <span class="text-danger">
                    <strong>{{ $errors->first('location') }}</strong>
                  </span>
              @endif
            </div>

            <div class="row">
              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <div class="form-group">
                  <label for="state_id">Estado</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                       <span class="input-group-text"> <i class="fa fa-map-marker"></i> </span>
                    </div>
                    <select class="form-control" id="state_id" name="state_id">
                      <option value = "" disabled >Seleccione una opción</option>
                      @foreach($states as $state)
                        @if($state->id === $user->state_id)
                          <option value="{{ $state->id }}" selected="selected">{{ $state->name }}</option>
                        @else
                          <option value="{{ $state->id }}">{{ $state->name }}</option>
                        @endif
                      @endforeach
                    </select>
                    @if ($errors->has('state_id'))
                        <span class="text-danger">
                          <strong>{{ $errors->first('state_id') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="municipality_id">Municipio</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-map-marker"></i> </span>
                  </div>
                  <select class="form-control" id="municipality_id" name="municipality_id">
                    <option value = "" disabled>Seleccione una opción</option>
                      @foreach($municipalities as $municipality)
                        @if($municipality->id === $user->municipality_id)
                          <option value="{{ $municipality->id }}" selected="selected">{{ $municipality->name }}</option>
                        @else
                          <option value="{{ $municipality->id }}">{{ $municipality->name }}</option>
                        @endif
                      @endforeach
                  </select>
                    @if ($errors->has('municipality_id'))
                      <span class="text-danger">
                        <strong>{{ $errors->first('municipality_id') }}</strong>
                      </span>
                    @endif
                </div>
              </div>

              <div class="col-xs-4 col-sm-4 col-lg-4 col-md-4">
                <label for="parishe_id">Parroquia</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text"> <i class="fa fa-map-marker"></i> </span>
                  </div>
                  <select class="form-control" id="parishe_id" name="parishe_id">
                    <option value = "" disabled>Seleccione una opción</option>
                      @foreach($parishes as $parishe)
                        @if($parishe->id === $user->parishe_id)
                          <option value="{{ $parishe->id }}" selected="selected">{{ $parishe->name }}</option>
                        @else
                          <option value="{{ $parishe->id }}">{{ $parishe->name }}</option>
                        @endif
                      @endforeach
                  </select>
                    @if ($errors->has('parishe_id'))
                        <span class="text-danger">
                          <strong>{{ $errors->first('parishe_id') }}</strong>
                        </span>
                    @endif
                </div>
              </div>
            </div>
          </div><!-- Fin Card Body Personales -->
        </div><!-- Fin Card Info Personal -->
      </div><!-- Fin col 9 -->
    </div> <!-- Fin row -->
  </div><!-- Fin card body -->


      {{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
      <div class="card-footer">
  			<div class="pull-right">
          <button type="submit" class="btn btn-info">Aceptar</button>
  				<a href="{{ route('users.index') }}" class="btn btn-danger">Cancelar</a>
  			</div>
  		</div>
      {{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

    {!! Form::close() !!}
    {{-- FORMULARIO - FIN --}}
  </div>

  {!! JsValidator::formRequest('App\Http\Requests\Users\StoreUserRequest') !!}

  @section('script')
    <script type="text/javascript">

    $(document).ready( function () {

      $('#state_id').change(function () {

             $.ajax({
               method: "POST",
               url: "{{ url('/municipioAjax') }}",
               data: {estado: $('#state_id').val(), '_token': $('input[name=_token]').val()},
               success: function (response) {
                 $('#municipality_id').html(response);
                 $("#parishe_id").empty();
                 $('#parishe_id').append('<option value="" selected>Seleccione una opción</option>');
               },
               beforeSend: function () {
                 $('#municipality_id').append('<option value="" selected>Buscando...</option>');
               }
             });
           });

       $('#municipality_id').change(function () {

             $.ajax({
               method: "POST",
               url: "{{ url('/parroquiaAjax') }}",
               data: {municipio: $('#municipality_id').val(), '_token': $('input[name=_token]').val()},
               success: function (response) {
                 $('#parishe_id').html(response);
               },
               beforeSend: function () {
                 $('#parishe_id').append('<option value="" selected>Buscando...</option>');
               }
             });
           });

    });

    </script>
  @endsection

  @endsection
