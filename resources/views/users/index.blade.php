@extends('layouts.app')

<?php
  //dd($roles);

  if (isset($name))
    $name = $name;
  else
    $name = "";
  if (isset($email))
    $email = $email;
  else
    $email = "";
  if (isset($role_id))
    $role_id = $role_id;
  else
    $role_id = "";
  if (isset($user_status_id))
    $user_status_id = $user_status_id;
  else
    $user_status_id = "";
  if (isset($identification_number))
    $identification_number = $identification_number;
  else
    $identification_number = "";
  if (isset($first_name))
    $first_name = $first_name;
  else
    $first_name = "";
  if (isset($last_name))
    $last_name = $last_name;
  else
    $last_name = "";
?>

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
    <li class="breadcrumb-item"><a>Seguridad</a></li>
    <li class="breadcrumb-item active"><a>Usuarios</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

	@include('layouts.parciales.messages')

	{{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
		</div>

	{{-- FORMULARIO - INICIO --}}
		<form class="form" id="indexUsersForm" name = "indexUsersForm" role="form" data-toggle="validator" method="GET" action="{{ route('users.index') }}">
			<div class="card-body">
				{{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="name">Nombre de Usuario</label>
							<input class="form-control mayus" id="name" name="name">
						</div>
					</div>
          <div class="col-md-3">
						<div class="form-group">
							<label for="email">Correo electrónico</label>
							<input class="form-control" id="email" name="email">
						</div>
					</div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="role_id">Rol</label>
              <select class="form-control" id="role_id" name="role_id">
                <option value="" selected>Seleccione una opción</option>
                @foreach($roles as $role)
                  <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="user_status_id">Estatus</label>
              <select class="form-control" id="user_status_id" name="user_status_id">
                <option value="" selected>Seleccione una opción</option>
                @foreach($statusUsers as $statu)
                  <option value="{{ $statu->id }}">{{ $statu->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div> <!--Fin Row-->
        <hr>
        <div class="row">
        	<div class="col-md-2">
  					<div class="form-group">
  						<label for="identification_number">Cédula</label>
  						<input class="form-control" id="identification_number" name="identification_number">
  					</div>
          </div>
          <div class="col-md-5">
  					<div class="form-group">
  						<label for="first_name">Primer nombre</label>
  						<input class="form-control mayus" id="first_name" name="first_name">
  					</div>
          </div>
          <div class="col-md-5">
  					<div class="form-group">
  						<label for="last_name">Primer apellido</label>
  						<input class="form-control mayus" id="last_name" name="last_name">
  					</div>
          </div>
        </div> <!--Fin Row-->
				{{-- EJEMPLO CON DOS COLUMNAS - FIN --}}

			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
			<div class="card-footer">
				<div class="pull-right">
          <button type="button" onClick="buscar();" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
          <a href="{{ route('users.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
					<button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
				</div>
			</div>
			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

		</form>
		{{-- FORMULARIO - FIN --}}
	</div>

	{{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

	{{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Usuarios</b></h3>
    </div>
    <div class="card-footer">
      @permission('user-create')
        <a href="{{ route('users.create') }}" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a>
      @endpermission
      @permission('user-pdf')
        <button type="button" onClick="imprimePdf();" class="btn btn-info"><i class="fa fa-file"></i> Pdf</button>
      @endpermission
      @permission('user-excel')
        <button type="button" onClick="imprimeExcel();" class="btn btn-info"><i class="fa fa-file"></i> Excel</button>
      @endpermission
    </div>
    <div class="card-body">
      <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>N°</th>
            <th>Nombre de usuario</th>
            <th>Correo Electrónico</th>
            <th>Cédula</th>
            <th>Nombre / Apellido</th>
            <th>Rol (es)</th>
            <th>Estatus</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
					@foreach ($data as $key => $user)
          <tr>
            <td style="width: 2%">{{ ++$i }}</td>
            <td style="width: 15%">{{ $user->name }}</td>
            <td style="width: 15%">{{ $user->email }}</td>
            <td style="width: 8%">{{ $user->identification_number }}</td>
            <td style="width: 20%">{{ $user->first_name }} {{ $user->last_name }}</td>
            <td style="width: 15%">
							@if(!empty($user->usuarioroles))
								@foreach($user->roles as $v)
									({{ $v->display_name }})<br>
								@endforeach
							@endif
						</td>
            <td style="width: 10%">{{ $user->estatususers->name }}</td>
            <td class="text-center" style="width: 15%">
							@permission('user-edit')
                 <a href="{{ route('users.edit',$user->id) }}" class="btn btn-info"> <i class="fa fa-edit"></i></a>
							@endpermission
                 <a href="{{ route('users.show',$user->id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
							@permission('user-delete')
                 <button type="button" class="btn btn-danger" title="Eliminar Usuario" data-toggle="modal" data-target="#myModal_{{ $user->id }}">
                   <i class="fa fa-trash"></i>
                 </button>
              @endpermission
                 <div class="modal fade" tabindex="-1" role="dialog" id="myModal_{{ $user->id }}">
                   <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel">Información</h4>
                       </div>
                       <div class="modal-body">
                         <h4>¿Desea eliminar la información?</h4>
                       </div>
                       <div class="modal-footer">
                         {{Form::open(array('route'=> array('users.destroy', $user['id']), 'method'=>'PATCH','id'=>'borraRegistro'))}}
                         {{ method_field('DELETE') }}
                           <button type="submit" id="borra" class="btn btn-danger" >Si</button>
                           <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">No</button>
                         {{Form::close()}}
                       </div>
                     </div>
                   </div>
                 </div>
            </td>
          </tr>
					@endforeach
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          columnDefs: [
              {orderable: false, targets: 7 }
          ],
          responsive: true,
          searching: false,
          paginate: true,
        });
        // DATATABLE - FIN //


      });

      function buscar()
      {

        var formIndex = document.forms['indexUsersForm'];
        var vname = document.getElementById("name").value;
        var vemail = document.getElementById("email").value;
        var vrole_id = document.getElementById("role_id").value;
        var vuser_status_id = document.getElementById("user_status_id").value;
        var videntification_number = document.getElementById("identification_number").value;
        var vfirst_name = document.getElementById("first_name").value;
        var vlast_name = document.getElementById("last_name").value;

        if (
          vname === '' && vemail === '' && vrole_id === '' && vuser_status_id === '' && videntification_number === '' && vfirst_name === '' && vlast_name === '')
        {
          $('#message').html("@lang('message.seleccionarcriterio')");
          $('#msj').show();
          desvanecer();
        } else
        {
          formIndex.submit();
        }

      }

      function imprimePdf()
      {
        var name = '<?php echo $name; ?>';
        var email = '<?php echo $email; ?>';
        var role_id = '<?php echo $role_id; ?>';
        var user_status_id = '<?php echo $user_status_id; ?>';
        var identification_number = '<?php echo $identification_number; ?>';
        var first_name = '<?php echo $first_name; ?>';
        var last_name = '<?php echo $last_name; ?>';

        window.open('/users/pdf' + '?' + '&name=' + name + '&email=' + email + '&role_id=' + role_id + '&user_status_id=' + user_status_id + '&identification_number=' + identification_number + '&first_name=' + first_name + '&last_name=' + last_name, '_blank');
      }

      function imprimeExcel()
      {
        var name = '<?php echo $name; ?>';
        var email = '<?php echo $email; ?>';
        var role_id = '<?php echo $role_id; ?>';
        var user_status_id = '<?php echo $user_status_id; ?>';
        var identification_number = '<?php echo $identification_number; ?>';
        var first_name = '<?php echo $first_name; ?>';
        var last_name = '<?php echo $last_name; ?>';

        window.open('/users/excel' + '?' + '&name=' + name + '&email=' + email + '&role_id=' + role_id + '&user_status_id=' + user_status_id + '&identification_number=' + identification_number + '&first_name=' + first_name + '&last_name=' + last_name, '_blank');
      }

      </script>
    @endsection

@endsection
