@extends('layouts.app')

<?php
	if(!isset($username)){
		$username = '';
	}

	if(!isset($fechaInicio)){
		$fechaInicio = '';
	}

	if(!isset($fechaFinal)){
		$fechaFinal = '';
	}

?>

{{-- BREADCRUMBS - INICIO --}}
@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item active"><a href="{{ route('permissions.index') }}">Sesiones(Abilities)</a></li>

@endsection
{{-- BREADCRUMBS - FIN --}}

@section('content')
	@include('layouts.parciales.messages')


	{{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}
	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
		</div>
		{{-- FORMULARIO - INICIO --}}
		<form class="form" id="indexSessionsForm" name = "indexSessionsForm" role="form" data-toggle="validator" method="GET" action="{{ route('usersession.index') }}">	
			
			<div class="card-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="name">Nombre de Usuario</label>
							@if($username == '')
								<input class="form-control" id="nameUser" name="nameUser">
							@else
								<input class="form-control" id="nameUser" name="nameUser" value="{{ $username }}">
							@endif	
						</div>
					</div>
					<div class="col-md-4">
			            <div class="form-group">
			              <label>Fecha Inicio:</label>
			              <div class="input-group">
			                <div class="input-group-prepend">
			                  <span class="input-group-text">
			                    <i class="fa fa-calendar"></i>
			                  </span>
			                </div>
							@if($fechaInicio == '')
			                	<input type="text" class="form-control float-right" id="datepicker1" name="datepicker1">
							@else
								<input type="text" class="form-control float-right" id="datepicker1" name="datepicker1" value="{{ $fechaInicio }}">
							@endif
			              </div>
			            </div>
          			</div>

          			<div class="col-md-4">
			            <div class="form-group">
			              <label>Fecha Final:</label>
			              <div class="input-group">
			                <div class="input-group-prepend">
			                  <span class="input-group-text">
			                    <i class="fa fa-calendar"></i>
			                  </span>
			                </div>
			                @if($fechaFinal =='' )
			                	<input type="text" class="form-control float-right" id="datepicker2" name="datepicker2">
			                @else
								<input type="text" class="form-control float-right" id="datepicker2" name="datepicker2" value="{{ $fechaFinal }}">
							@endif
			              </div>
			            </div>
          			</div>
				</div>	
				{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
				<div class="card-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-info" onClick="tablaIndex();"><i class="fa fa-search"></i> Buscar</button>
					 	  <a href="{{ route('usersession.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
						<button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
					</div>
				</div>
				{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}
			</div>
		</form>
		{{-- FORMULARIO - FIN --}}
	</div>
	{{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}
	
	{{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}
	<div class="card card-info">
		<div class="card-header">
      		<h3 class="card-title"><b>Sesiones(Abilities)</b></h3>
    	</div>

		<div class="card-footer">

      		<!--@ permission('permission-excel')-->
        		<button type="button"  id="excel" class="btn btn-info"><i class="fa fa-file"></i> Excel</button>
      		<!--@ endpermission-->

    	</div>
    	<div class="card-body" id="tt">
    		<!--<table id="tabla1" class="table table-bordered table-hover">
    			<thead>
    				<tr>
    					<th style="width: 5%">N°</th>
    					<th style="width: 5%">Usuario</th>
    					<th style="width: 5%">Rol - Permisologia</th>
    					<th style="width: 5%">Ip</th>
    					<th style="width: 5%">Fecha - Hora</th>
    					<th style="width: 5%">Acciones</th>
    				</tr>
    			</thead>
    			<tbody>
    				php$i=0 endphp
    				foreach ($sesiones as $key => $v)
						<tr>
							<td>{{--++$i }}</td>
							<td>{{ $v->username }}</td>
							<td>{{ $v->name }}</td>
							<td>{{ $v->ip }}</td>
							<td>{{ $v->access_date }}</td>
							<td>
								<!--@ permission('permission-delete')-->
                 					<button type="button" class="btn-info" title="Eliminar Permiso" data-toggle="modal" data-target="#myModal_{{ $v->id }}">
                   					<i class="fa fa fa-eye"></i>
                 					</button>
              					<!--@ endpermission -->
								<div class="modal fade" tabindex="-1" role="dialog" id="myModal_{{ $v->id }}">
				                   <div class="modal-dialog">
				                     <div class="modal-content">
				                       <div class="modal-header">
				                         <h4 class="modal-title" id="myModalLabel">Información</h4>
				                       </div>
				                       <div class="modal-body">
				                         <h4>¿Desea Cerrar la Sesi&oacute;n?</h4>
				                       </div>
				                       <div class="modal-footer">
				                         {{Form::open(array('route'=> array('usersession.destroy', $v['id']), 'method'=>'PATCH','id'=>'borraRegistro'))}}
				                         {{ method_field('DELETE') }}
				                           <button type="button" id="borra" class="btn btn-danger" onclick="cerrarSesion({{$v->id}});" >Si</button>
				                           <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">No</button>
				                         {{Form::close()--}}
				                       </div>
				                     </div>
				                   </div>
                 				</div>	

							</td>
						</tr>	
    				endforeach
    			</tbody>
    		</table>-->
    	</div>
	</div>	
	{{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

@endsection

@section('script')
	
	<script type="text/javascript">

	function cerrarSesion(id){

		var url = window.location;
		//alert(url+"/usersession/destroy/"+id);
		$.get(url+"/destroy/"+id, function(respuesta){
			tablaIndex();
		});
	}	

	function tablaIndex(){
		var username = $("#nameUser").val();
		var fechaInicio = $("#datepicker1").val();
		var fechaFinal= $("#datepicker2").val();

		if(username==''){ username = 0;}
		if(fechaInicio==''){ fechaInicio = 0;}
		if(fechaFinal==''){ fechaFinal = 0;}

		var url = window.location;
		$.get(url+"/apiSessions/"+username+"/"+fechaInicio+"/"+fechaFinal, function(respuesta){
				console.log(respuesta.sesiones);
	 		var tablaData = respuesta.sesiones;
	 		console.log(tablaData[0]);
	 		var num =1;
	 		var tabla ='<table id="tabla1" class="table table-bordered table-hover">';
	 			tabla+='	<thead>';
	 			tabla+='		<tr>';
	 			tabla+='			<th style="width: 5%">N°</th>';
			    tabla+='			<th style="width: 5%">Usuario</th>';
			    tabla+='			<th style="width: 5%">Rol - Permisologia</th>';
			    tabla+='			<th style="width: 5%">Ip</th>';
			    tabla+='			<th style="width: 5%">Fecha - Hora</th>';
			    tabla+='			<th style="width: 5%">Acciones</th>';
	 			tabla+='		</tr>';
	 			tabla+='	</thead>';
	 			tabla+='	<tbody>';
	 			for(var i in tablaData){
		 			tabla+='		<tr>';
		 			tabla+=' 			<td>'+num+++'</td>';
		 			tabla+=' 			<td>'+tablaData[i]['username']+'</td>';
		 			tabla+=' 			<td>'+tablaData[i]['name']+'</td>';
		 			tabla+=' 			<td>'+tablaData[i]['ip']+'</td>';
		 			tabla+=' 			<td>'+tablaData[i]['access_date']+'</td>';
		 			tabla+='  			<td>';
		 			tabla+='				<button type="button" class="btn-info" title="Eliminar Permiso" data-toggle="modal" data-target="#myModal_"'+tablaData[i]['id']+'"><i class="fa fa fa-eye"></i></button>';

		 			tabla+=' 	<div class="modal fade" tabindex="-1" role="dialog" id="myModal_"'+tablaData[i]['id']+'">';
		 			tabla+=' 		<div class="modal-dialog">';
		 			tabla+='   			<div class="modal-content">';
		 			tabla+='				<div class="modal-header">';
		 			tabla+='					<h4 class="modal-title" id="myModalLabel">Información</h4>';
		 			tabla+=' 				</div>';
		 			tabla+='				<div class="modal-body">';
		 			tabla+='					<h4>¿Desea Cerrar la Sesi&oacute;n?</h4>';
		 			tabla+=' 				</div>';
		 			tabla+='				<div class="modal-footer">';
		 			tabla+='					<button type="button" id="borra" class="btn btn-danger"';
		 			tabla+=' 					onclick="cerrarSesion('+tablaData[i]['id']+');" data-dismiss="modal">Si</button>';
		 			tabla+=' 					<button  id="cerrar" type="button" class="btn btn-info"';
		 			tabla+=' 					data-dismiss="modal">No</button>';
		 			tabla+=' 				</div>';
		 			tabla+=' 			</div>';
		 			tabla+=' 		</div>';
		 			tabla+=' 	</div>';
					tabla+='  			</td>';
		 			tabla+='		</tr>';
		 		}	

	 			tabla+='	</tbody>';
	 			tabla+='</table>';
	 		document.getElementById('tt').innerHTML = tabla;
			});
	 		
	}

	 $(document).ready( function () {

	 	tablaIndex();

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable({
          /*columnDefs: [
              {orderable: false, targets: 6 }
          ],*/
          responsive: true,
          searching: false,
          paginate: true,
        });
        // DATATABLE - FIN //

      });

	 $(document).ready( function () {

	        // SELECT - INICIO //
	        $('.select2').select2();
	        // SELECT - FIN //

	        // DATAPICKER - INICIO //
	        $('#datepicker1').datepicker({
	        	format: 'dd-mm-yyyy',
	        })
	        $('#datepicker2').datepicker({
	        	format: 'dd-mm-yyyy',
	        })
	        // DATAPICKER - FIN //

      });

	 $('#excel').click(function () {
	 	var username = $("#nameUser").val();
	 	var fechaInicio = $("#datepicker1").val();
	 	var fechaFinal = $("#datepicker2").val();

	 	fechaInicio = fechaInicio.split("/");
	 	fechaFinal = fechaFinal.split("/");

	 	fechaInicio = fechaInicio[0]+'-'+fechaInicio[1]+'-'+fechaInicio[2];
	 	fechaFinal = fechaFinal[0]+'-'+fechaFinal[1]+'-'+fechaFinal[2]
	
	 	var request = {
	 		'username':username,
	 		'fechaInicio':fechaInicio,
	 		'fechaFinal':fechaFinal
	 	}
	 	
	 	request = JSON.stringify(request);
	 	window.open('{{ url("usersessionExcel") }}' + '/' + request,'_blank');


	 });
	</script>
@endsection