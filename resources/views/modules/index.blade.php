@extends('layouts.app')

<?php
    if (isset($name))
      $name = $name;
    else
      $name = "";
    if (isset($display_name))
      $display_name = $display_name;
    else
      $display_name = "";
    if (isset($description))
      $description = $description;
    else
      $description = "";
    if (isset($status_module_id))
      $status_module_id = $status_module_id;
    else
      $status_module_id = "";
?>

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item active"><a href="{{ route('modules.index') }}">Módulos(Rutas)</a></li>

@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  @include('layouts.parciales.messages')

	{{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
		</div>
	{{-- FORMULARIO - INICIO --}}
    <form class="form" id="indexModulesForm" name = "indexModulesForm" role="form" data-toggle="validator" method="GET" action="{{ route('modules.index') }}">
			<div class="card-body">
				<div class="row">
					<div class="col-md-5">
						<div class="form-group">
							<label for="name">Nombre del módulo(ruta)</label>
							<input class="form-control" id="name" name="name">
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label for="display_name">Nombre a mostrar</label>
							<input class="form-control" id="display_name" name="display_name">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
              <label for="status_module_id">Estatus</label>
              <select class="form-control" id="status_module_id" name="status_module_id">
                <option value = "" selected>Seleccione una opción</option>
                @foreach($generics as $generic)
                  <option value="{{ $generic->id }}">{{ $generic->name }}</option>
                @endforeach
              </select>
						</div>
					</div>
				</div>

			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
			<div class="card-footer">
				<div class="pull-right">
					<button type="button" onClick="buscar();" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
				 	  <a href="{{ route('modules.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
					<button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
				</div>
			</div>
			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}
     </div>
		</form>
		{{-- FORMULARIO - FIN --}}
	</div>
</div>

	{{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

	{{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Módulos(Rutas)</b></h3>
    </div>
    <div class="card-footer">
			@permission('module-create')
        <a href="{{ route('modules.create') }}" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a>
			@endpermission
      @permission('module-pdf')
        <button type="button" onClick="imprimePdf();" class="btn btn-info"><i class="fa fa-file"></i> Pdf</button>
      @endpermission
      @permission('module-excel')
        <button type="button" onClick="imprimeExcel();" class="btn btn-info"><i class="fa fa-file"></i> Excel</button>
      @endpermission
    </div>
    <div class="card-body">
        <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th style="width: 5%">N°</th>
            <th style="width: 15%">Nombre del módulo(ruta)</th>
						<th style="width: 15%">Nombre a mostrar</th>
            <th style="width: 33%">Descripción</th>
						<th style="width: 12%">Fecha/Hora creación</th>
						<th style="width: 5%">Estatus</th>
            <th style="width: 15%">Acciones</th>
          </tr>
        </thead>
        <tbody>
					@foreach ($modules as $key => $module)
        	<tr>
            <td>{{ ++$i }}</td>
            <td>{{ $module->name }}</td>
						<td>{{ $module->display_name }}</td>
            <td>{{ $module->description }}</td>
						<td>{{ $module->created_at }}</td>
						<td>{{ $module->genericosmod->name }}</td>
            <td class="text-center">
							@permission('module-edit')
                 <a href="{{ route('modules.edit',$module->id) }}" class="btn btn-info"> <i class="fa fa-edit"></i></a>
							@endpermission
                 <a href="{{ route('modules.show',$module->id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
							@permission('module-delete')
                 <button type="button" class="btn btn-danger" title="Eliminar Módulo" data-toggle="modal" data-target="#myModal_{{ $module->id }}">
                   <i class="fa fa-trash"></i>
                 </button>
              @endpermission
                 <div class="modal fade" tabindex="-1" role="dialog" id="myModal_{{ $module->id }}">
                   <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                       <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel">
                           <div class="p-3 mb-2 bg-danger text-white"><b>Atención</b></div>
                         </h4>
                       </div>
                       <div class="modal-body">
                         <h4><b>¿Seguro desea eliminar la información?</b></h4><p><h5><b>Todos los elementos de menú y/o permisos relacionados serán eliminados.<b></h5>
                       </div>
                       <div class="modal-footer">
                         {{Form::open(array('route'=> array('modules.destroy', $module['id']), 'method'=>'PATCH','id'=>'borraRegistro'))}}
                         {{ method_field('DELETE') }}
                           <button type="submit" id="borra" class="btn btn-danger" >Si</button>
                           <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">No</button>
                         {{Form::close()}}
                       </div>
                     </div>
                   </div>
                 </div>
            </td>
          </tr>
					@endforeach
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          columnDefs: [
              {orderable: false, targets: 6 }
          ],
          responsive: true,
          searching: false,
          paginate: true,
        });
        // DATATABLE - FIN //


      });

      function buscar()
      {
        var formEdit = document.forms['indexModulesForm'];
        var vname = document.getElementById("name").value;
        var vdisplay_name = document.getElementById("display_name").value;
        var vstatus_module_id = document.getElementById("status_module_id").value;

        if (vname === '' && vdisplay_name === '' && vstatus_module_id === '')
        {
          $('#message').html("@lang('message.seleccionarcriterio')");
          $('#msj').show();
          desvanecer();
        } else
        {
          formEdit.submit();
        }
      }

      function imprimePdf()
      {
        var name = '<?php echo $name; ?>';
        var display_name = '<?php echo $display_name; ?>';
        var status_module_id = '<?php echo $status_module_id; ?>';
        window.open('/modules/pdf' + '?' + '&name=' + name + '&display_name=' + display_name + '&status_module_id=' + status_module_id, '_blank');
      }

      function imprimeExcel()
      {
        var name = '<?php echo $name; ?>';
        var display_name = '<?php echo $display_name; ?>';
        var status_module_id = '<?php echo $status_module_id; ?>';
        window.open('/modules/excel' + '?' + '&name=' + name + '&display_name=' + display_name + '&status_module_id=' + status_module_id, '_blank');
      }


      </script>
    @endsection

@endsection
