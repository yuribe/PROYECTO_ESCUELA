
@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ route('modules.index') }}">Módulos(Rutas)</a></li>
  <li class="breadcrumb-item active"><a>Nuevo Módulo(Ruta)</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><b>Nuevo Módulo(Ruta)</b></h3>
	</div>

	{{-- FORMULARIO - INICIO --}}
	<form class="form" id="createModule" name="createModule" role="form" data-toggle="validator" method="POST" action="{{ route('modules.store') }}">
  {{ csrf_field() }}

		<div class="card-body">
			<div class="row">
    		<div class="col-md-6">
					<div class="form-group">
						<label for="name">Nombre del módulo(ruta)</label>
						<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" />
						@if ($errors->has('name'))
                <span class="text-danger">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
					</div>
				</div>
    		<div class="col-md-6">
					<div class="form-group">
						<label for="display_name">Nombre a mostrar</label>
						<input type="text" class="form-control mayus"  id="display_name" name="display_name" value="{{ old('display_name') }}" />
						@if ($errors->has('display_name'))
                <span class="text-danger">
                  <strong>{{ $errors->first('display_name') }}</strong>
                </span>
            @endif
					</div>
				</div>
			</div> <!--Fin Row-->
			<div class="row">
    		<div class="col-md-10">
					<div class="form-group">
						<label for="description">Descripción</label>
						<input type="text" class="form-control mayus" id="description" name="description" value="{{ old('description') }}" />
						@if ($errors->has('description'))
                <span class="text-danger">
                  <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
					</div>
				</div>
        <div class="col-md-2">
					<div class="form-group">
						<label for="status_module_id">Estatus</label>
            <select class="form-control" id="status_module_id" name="status_module_id">
              <option value = "" selected>Seleccione una opción</option>
              @foreach($generics as $generic)
                <option value="{{ $generic->id }}">{{ $generic->name }}</option>
              @endforeach
            </select>
						@if ($errors->has('status_module_id'))
                <span class="text-danger">
                  <strong>{{ $errors->first('status_module_id') }}</strong>
                </span>
            @endif
					</div>
				</div>
			</div> <!--Fin Row-->
		</div>

		{{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
		<div class="card-footer">
			<div class="pull-right">
        <button type="submit" class="btn btn-info">Aceptar</button>
				<a href="{{ route('modules.index') }}" class="btn btn-danger">Cancelar</a>
			</div>
		</div>
		{{-- BOTONES (REGRESAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

	</form>
	{{-- FORMULARIO - FIN --}}
</div>

{!! JsValidator::formRequest('App\Http\Requests\Modules\StoreModuleRequest') !!}


@section('scripts')
  <script type="text/javascript">

	$(document).ready( function () {

	});

	</script>
@endsection

@endsection
