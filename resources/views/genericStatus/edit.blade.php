@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ route('genericstatus.index') }}">Estatus generales</a></li>
  <li class="breadcrumb-item active"><a>Edición estatus general</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><b>Edición estatus general</b></h3>
	</div>

	{{-- FORMULARIO - INICIO --}}
	{!! Form::model($generic, ['method' => 'PATCH','route' => ['genericstatus.update', $generic->id]]) !!}
	{{ csrf_field() }}

		<div class="card-body">
			<div class="row">
    		<div class="col-md-6">
					<div class="form-group">
						<label for="name">Nombre</label>
						<input type="text" class="form-control mayus" id="name" name="name" value="{{ $generic->name }}" />
						@if ($errors->has('name'))
                <span class="text-danger">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
					</div>
				</div>
			</div> <!--Fin Row-->
			<div class="row">
    		<div class="col-md-10">
					<div class="form-group">
						<label for="description">Descripción</label>
						<input type="text" class="form-control mayus" id="description" name="description" value="{{ $generic->description }}" />
						@if ($errors->has('description'))
                <span class="text-danger">
                  <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
					</div>
				</div>
			</div> <!--Fin Row-->
		</div>

		{{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
		<div class="card-footer">
			<div class="pull-right">
        <button type="submit" class="btn btn-info">Aceptar</button>
				<a href="{{ route('genericstatus.index') }}" class="btn btn-danger">Cancelar</a>
			</div>
		</div>
		{{-- BOTONES (REGRESAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

	{!! Form::close() !!}
	{{-- FORMULARIO - FIN --}}
</div>

{!! JsValidator::formRequest('App\Http\Requests\GenericStatus\UpdateGenericStatusRequest') !!}


@section('scripts')
  <script type="text/javascript">

	$(document).ready( function () {

	});

	</script>
@endsection

@endsection
