@extends('layouts.app')

<?php
    if (isset($name))
      $name = $name;
    else
      $name = "";
    if (isset($description))
      $description = $description;
    else
      $description = "";
?>

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item active"><a href="{{ route('genericstatus.index') }}">Estatus Generales</a></li>

@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  @include('layouts.parciales.messages')

	{{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
		</div>
	{{-- FORMULARIO - INICIO --}}
    <form class="form" id="indexGenericStatusForm" name = "indexGenericStatus" role="form" data-toggle="validator" method="GET" action="{{ route('genericstatus.index') }}">
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="name">Nombre</label>
							<input class="form-control mayus" id="name" name="name">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="description">Descripción</label>
							<input class="form-control mayus" id="description" name="description">
						</div>
					</div>
				</div>

			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
			<div class="card-footer">
				<div class="pull-right">
					<button type="button" onClick="buscar();" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
				 	  <a href="{{ route('genericstatus.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
					<button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
				</div>
			</div>
			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

		</form>
		{{-- FORMULARIO - FIN --}}
	</div>

	{{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

	{{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Estatus generales</b></h3>
    </div>
    <div class="card-footer">
			@permission('genericstatus-create')
        <a href="{{ route('genericstatus.create') }}" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a>
			@endpermission
      @permission('genericstatus-pdf')
        <button type="button" onClick="imprimePdf();" class="btn btn-info"><i class="fa fa-file"></i> Pdf</button>
      @endpermission
      @permission('genericstatus-excel')
        <button type="button" onClick="imprimeExcel();" class="btn btn-info"><i class="fa fa-file"></i> Excel</button>
      @endpermission
    </div>
    <div class="card-body">
        <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th style="width: 5%">N°</th>
            <th style="width: 25%">Nombre</th>
            <th style="width: 40%">Descripción</th>
						<th style="width: 15%">Fecha/Hora creación</th>
						<th style="width: 15%">Acciones</th>
          </tr>
        </thead>
        <tbody>
					@foreach ($generics as $key => $generic)
        	<tr>
            <td>{{ ++$i }}</td>
            <td>{{ $generic->name }}</td>
            <td>{{ $generic->description }}</td>
						<td>{{ $generic->created_at }}</td>
            <td class="text-center">
							@permission('genericstatus-edit')
                 <a href="{{ route('genericstatus.edit',$generic->id) }}" class="btn btn-info"> <i class="fa fa-edit"></i></a>
							@endpermission
                 <a href="{{ route('genericstatus.show',$generic->id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
							@permission('module-delete')
                 <button type="button" class="btn btn-danger" title="Eliminar Módulo" data-toggle="modal" data-target="#myModal_{{ $generic->id }}">
                   <i class="fa fa-trash"></i>
                 </button>
              @endpermission
                 <div class="modal fade" tabindex="-1" role="dialog" id="myModal_{{ $generic->id }}">
                   <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel">Información</h4>
                       </div>
                       <div class="modal-body">
                         <h4>¿Desea eliminar la información?</h4>
                       </div>
                       <div class="modal-footer">
                         {{Form::open(array('route'=> array('genericstatus.destroy', $generic['id']), 'method'=>'PATCH','id'=>'borraRegistro'))}}
                         {{ method_field('DELETE') }}
                           <button type="submit" id="borra" class="btn btn-danger" >Si</button>
                           <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">No</button>
                         {{Form::close()}}
                       </div>
                     </div>
                   </div>
                 </div>
            </td>
          </tr>
					@endforeach
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          columnDefs: [
              {orderable: false, targets: 6 }
          ],
          responsive: true,
          searching: false,
          paginate: true,
        });
        // DATATABLE - FIN //


      });

      function buscar()
      {
        var formIndex = document.forms['indexGenericStatusForm'];
        var vname = document.getElementById("name").value;
        var vdescription = document.getElementById("description").value;

        if (vname === '' && vdescription === '')
        {
          $('#message').html("@lang('message.seleccionarcriterio')");
          $('#msj').show();
          desvanecer();
        } else
        {
          formIndex.submit();
        }
      }

      function imprimePdf()
      {
        var name = '<?php echo $name; ?>';
        var description = '<?php echo $description; ?>';
        window.open('/genericstatus/pdf' + '?' + '&name=' + name + '&description=' + description, '_blank');
      }

      function imprimeExcel()
      {
        var name = '<?php echo $name; ?>';
        var description = '<?php echo $description; ?>';
        window.open('/genericstatus/excel' + '?' + '&name=' + name + '&description=' + description, '_blank');
      }


      </script>
    @endsection

@endsection
