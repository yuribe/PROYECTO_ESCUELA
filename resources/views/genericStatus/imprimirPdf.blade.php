<?php
use Carbon\Carbon;

$html = '
	<html>
	<head>
		<style>
			body {font-family: sans-serif;
				font-size: 10pt;
			}
			p {	margin: 0pt; }
			table.items {
				border: 0.1mm solid #000000;
			}
			td { vertical-align: top; }
			.items td {
				border-left: 0.1mm solid #000000;
				border-right: 0.1mm solid #000000;
			}
			table thead td { background-color: #EEEEEE;
				text-align: center;
				border: 0.1mm solid #000000;
			}
		</style>
		<title>Reporte Estatus Generales del Sistema</title>
	</head>
	<body>
	<!--mpdf
		<htmlpageheader name="myheader">
			<table width="100%" border = "0">
				<tr>
					<td colspan = "3" rowspan="5" width="20%"></td>
					<td width="60%" style="text-align: right; font-size: 12px"></td>
					<td width="20%" style="text-align: right;font-size: 12px"> Realizado por: '.Auth::user()->name.'</td>
				</tr>
				<tr>
					<td width="80%"> </td>
					<td width="20%" style="text-align: right;font-size: 12px"> Fecha: '.Carbon::createFromDate()->format("d/m/Y").'</td>
				</tr>
				<tr>
					<td width="80%" style="text-align: center;font-size: 10px"><h1 >Sistema Proyecto Base</h1></td>
					<td width="20%" style="text-align: right;font-size: 12px">Hora: '.Carbon::createFromDate()->toTimeString().'</td>
				</tr>
				<tr>
					<td width="80%"></td>
					<td width="20%" style="text-align: right;font-size: 12px"></td>
				</tr>
				<tr>
					<td width="80%" style="text-align: center;font-size: 14px"><h2>Reporte Estatus Generales</h2></td>
					<td width="20%"></td>
				</tr>
			</table>
			</htmlpageheader>
					<htmlpagefooter name="myfooter">
						<table width="100%">
							<tr>
								<td style="text-align: right; font-size: 12px;">Sistema Proyecto Base - Reporte Estatus Generales - {PAGENO} de [pagetotal] páginas</td>
							</tr>
						</table>
					</htmlpagefooter>
					<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
					<sethtmlpagefooter name="myfooter" value="on" />
					mpdf-->

					<table border=1 class="items" width="100%" style="font-size: 12pt; border-collapse: collapse; " cellpadding="8">
						<thead>
							<tr>
					      <th align="center" style="width: 10%">N°</th>
								<th align="center" style="width: 30%">Nombre</th>
					      <th align="center" style="width: 40%">Descripción</th>
								<th align="center" style="width: 20%">Fecha/Hora creación</th>
							</tr>
						</thead>
						<tbody>
						';
						$contador = 1;
						foreach($generics as $value)
						{
							$html.='
								<tr>
									<td align="center" style="font-size: 9pt;">'.strval($contador).'</td>
									<td align="center" style="font-size: 9pt;">'.(string)$value->name.'</td>
									<td align="center" style="font-size: 9pt;">'.(string)$value->description.'</td>
									<td align="center" style="font-size: 9pt;">'.(string)$value->created_at.'</td>
								</tr>';
						$contador += 1;
					}

			$html.='	<tbody>
					</table>
				</body>';

	$mpdf = new \Mpdf\Mpdf([
	'aliasNbPg' => '[pagetotal]',
	'orientation' => 'L',
	'format' => 'Letter',
	'margin_left' => 20,
	'margin_right' => 15,
	'margin_top' => 48,
	'margin_bottom' => 25,
	'margin_header' => 10,
	'margin_footer' => 10,
	]);

	$mpdf->SetDisplayMode('fullpage');
	$mpdf->WriteHTML($html);

	ob_end_clean();
	$mpdf->Output();
	exit;
