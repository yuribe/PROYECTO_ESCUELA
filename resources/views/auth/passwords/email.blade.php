@extends('layouts.app-login')

@section('css')

<link rel="stylesheet" href="{{ asset('css/artha.css') }}">

@endsection

@section('content')

  <div class="md:flex min-h-screen">

    <div class="relative pb-full md:flex md:pb-0 md:min-h-screen w-full md:w-1/2">
      <div style="background-image: url(http://localhost:8000/svg/MarketingVector3.svg);" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
      </div>
    </div>
    
    <div class="w-full md:w-1/2 bg-white flex items-center justify-center">
      <div class="max-w-sm m-8">
        <div class="login-logo">
          <a href="#"><b>Proyecto</b>ESCUELA</a>
        </div>
        <div class="card">
          <div class="card-body login-card-body">
            <p class="login-box-msg"><b>Recuperar Contraseña</b></p>
            <p class="login-box-msg">Ingresa tu correo electrónico para reestablecer la contraseña.</p>

            <form method="POST" action="{{ route('password.email') }}">
              @csrf
              <div class="input-group mb-3">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo Electrónico">

                <div class="input-group-append">
                  <span class="fa fa-envelope input-group-text"></span>
                </div>

                @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
              </div>
              @if($datCaptcha->captcha == true)
                <div class="row">
                  <div class="col-6">
                    <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                              <div class="g-recaptcha" data-sitekey="{{ env('NOCAPTCHA_SECRET') }}"></div>
                              @if ($errors->has('g-recaptcha-response'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                              </span>
                              @endif
                    </div>
                  </div>
                </div>
              @endif

              <div class="row">
                <div class="col-6">
                  <a href="/login" class="btn btn-info btn-block btn-flat">Atras</a>
                </div>
                <div class="col-6">
                  <button type="submit" class="btn btn-info btn-block btn-flat">Enviar</button>
                </div>
              </div>

            </form>

          </div>
        </div>
      </div>
    </div>

  </div>

@section('script')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@endsection
