@extends('layouts.app-login')

@section('css')

<link rel="stylesheet" href="{{ asset('css/artha.css') }}">

@endsection

@section('content')

  <div class="md:flex min-h-screen">

    <div class="relative pb-full md:flex md:pb-0 md:min-h-screen w-full md:w-1/2">
      <div style="background-image: url(http://localhost:8000/svg/MarketingVector3.svg);" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
      </div>
    </div>

    <div class="w-full md:w-1/2 bg-white flex items-center justify-center">
      <div class="max-w-sm m-8">
        <div class="login-box">
          <div class="login-logo">
            <a href="#"><b>Proyecto</b>Base</a>
          </div>
          <div class="card">
            <div class="card-body login-card-body">
              <form method="POST" action="{{ route('login') }}">
                @if(isset($auth->login) && $auth->login == true)
                <p class="login-box-msg"><b>Inicio Sesión</b></p>
                <p class="login-box-msg">Ingrese sus datos para iniciar sesión</p>
                @csrf
                <div class="input-group mb-3">
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo Electrónico">

                  <div class="input-group-append">
                    <span class="fa fa-envelope input-group-text"></span>
                  </div>
                  @if (count($errors) > 0)
                  <span class="invalid-feedback" role="alert">
                      
                      
                          @foreach ($errors->all() as $error)
                           <strong> {{ $error }}</strong>
                          @endforeach
                      
                  </span>
                @endif
                 
                </div>

                <div class="input-group mb-3">
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Contraseña">

                  <div class="input-group-append">
                    <span class="fa fa-lock input-group-text"></span>
                  </div>

                  @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif

                </div>

                <div class="row">
                  <div class="col-12">
                    <div class="checkbox icheck">
                      <label>
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                      <!--<div class="g-recaptcha" data-sitekey="{{--env('CAPTCHA_KEY') }}"></div>
                      if($errors->has('g-recapchat-response'))
                        <span class="invalid-feedback" style="display: block;">
                          <strong>{{--$errors->fist('g-recapchat-response')--}}</strong>
                        </span>
                      endif-->
                      <!--{NoCaptcha::display() !!}
                      <!--if ($errors->has('g-recaptcha-response'))
                          <span class="help-block">
                              <strong>{{--$errors->first('g-recaptcha-response')--}}</strong>
                          </span>
                      endif
                    -->

                  @if($datCaptcha->captcha == true)
                      <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                          <div class="g-recaptcha" data-sitekey="{{ env('NOCAPTCHA_SECRET') }}"></div>
                          @if ($errors->has('g-recaptcha-response'))
                          <span class="help-block">
                              <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                          </span>
                          @endif
                      </div>
                  @endif


                  </div>
                </div>
                <br>

                <div class="row">
                  <div class="col-12">
                    <button type="submit" class="btn btn-info btn-block btn-flat">Iniciar Sesión</button>
                    
                  </div>
                </div>
                @endif
                <div class="row">
                  <div class="col-12">
                    @if((isset($auth->google) && $auth->google == true) or (isset($auth->facebook) && $auth->facebook == true) or (isset($auth->twitter) && $auth->twitter == true) or (isset($auth->instagram) && $auth->instagram == true))
                    Iniciar con <br>
                    @endif
                    @if(isset($auth->google) && $auth->google == true)
                      <a href="{{ url('login/google') }}" class="btn btn-default"> 
                      <i class="fa fa-google"></i>
                      Google
                    </a>
                    @endif
                    @if(isset($auth->facebook) && $auth->facebook == true)
                    <a href="{{-- url('login/facebook') --}}" class="btn btn-default"> 
                      <i class="fa fa-facebook"></i>
                      Facebook
                    </a>
                    @endif
                    @if(isset($auth->twitter) && $auth->twitter == true)
                    <a href="#" class="btn btn-default"> 
                      <i class="fa fa-twitter"></i>
                      Twitter
                    </a>
                    @endif
                    @if(isset($auth->instagram) && $auth->instagram == true)
                    <a href="{{-- url('login/instagram') --}}" class="btn btn-default"> 
                      <i class="fa fa-instagram"></i>
                      Instagram
                    </a>
                    @endif
                    
                  </div>
                </div>
              </form>
              
                
                @if(isset($registro->externo) && $registro->externo ==true)
                  <a class="btn btn-link" style="color:#17A2B8 !important" href="#{{-- route('password.request') --}}">
                    Registrate
                  </a>
                @endif
                @if (Route::has('password.request'))
                  @if (isset($auth->login) && $auth->login == true)
                    <a class="btn btn-link" style="color:#17A2B8 !important" href="{{ route('password.request') }}">
                      ¿Olvidaste tu contraseña?
                    </a>
                  @endif
                @endif

            </div>
          </div>

        </div>
      </div>
    </div>

  </div>

@section('script')
  <script src='https://www.google.com/recaptcha/api.js'></script>

  <script type="text/javascript">

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })

  </script>

@endsection

@endsection
