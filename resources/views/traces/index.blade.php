@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
    <li class="breadcrumb-item">Auditoría</li>
    <li class="breadcrumb-item active"><a>Consulta de Trazas</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

@include('layouts.parciales.messages')

  {{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
    </div>

  {{-- FORMULARIO - INICIO --}}
    <form class="form" id="formTraces" name="formTraces" role="form" method="GET" action="{{ route('traces.index') }}">
      <div class="card-body">

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Fecha Desde:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                  </span>
                </div>
                <input type="text" class="form-control float-right" id="datepicker1" name="desde" autocomplete="off">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Fecha Hasta:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                  </span>
                </div>
                <input type="text" class="form-control float-right" id="datepicker2" name="hasta" autocomplete="off">
              </div>
            </div>
          </div>
        </div>
        {{-- EJEMPLO CON DOS COLUMNAS - FIN --}}

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}

        @php

        $created = 0;
        $updated = 0;
        $deleted = 0;

        foreach ($trace as $key => $traces2) {
          if ($traces2->event == 'created') {
            $created = 1;
          }
          if ($traces2->event == 'updated') {
            $updated = 1;
          }
          if ($traces2->event == 'deleted') {
            $deleted = 1;
          }
        }
            
        @endphp

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="operation">Operación</label>
              <select id="operation" name="operation" class="form-control">
                <option value="">Seleccione una opción</option>
                @if($created == 1)
                <option value="created">CREACIÓN</option>
                @endif
                @if($updated == 1)
                <option value="updated">ACTUALIZACIÓN</option>
                @endif
                @if($deleted == 1)
                <option value="deleted">ELIMINACIÓN</option>
                @endif
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="email">Correo Electrónico</label>
              <input id="email" type="text" name="email" class="form-control">
            </div>
          </div>
        </div>
        {{-- EJEMPLO CON DOS COLUMNAS - FIN --}}
      </div>

      {{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
      <div class="card-footer">
        <div class="pull-right">
          <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
          <a href="{{ route('traces.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
          <button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
        </div>
      </div>
      {{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

    </form>
    {{-- FORMULARIO - FIN --}}
  </div>

  {{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

  {{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Consulta de Trazas</b></h3>
    </div>
    <div class="card-footer">
      <a href="{{ route('traces.excel') }}" class="btn btn-info"><i class="fa fa-file"></i> Excel</a>
    </div>

      <div class="card-body table-bordered table-hover table-responsive">
        <table id="tabla1" class="table">
          <thead>
            <tr>
              <th>N°</th>
              <th>IP</th>
              <th>Usuario</th>
              <th>Operación</th>
              <th>Dato Anterior</th>
              <th>Dato Posterior</th>
              <th>Fecha/Hora Creación</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($trace as $key => $traces)
            <tr>
              <td>{{ ++$i }}</td>
              <td>{{ $traces->ip_address }}</td>
              <td>{{ $traces->email }}</td>
              @if ($traces->event == 'updated')
              <td>Actualización</td>
              @elseif ($traces->event == 'created')
              <td>Creación</td>
              @elseif ($traces->event == 'deleted')
              <td>Eliminación</td>
              @endif
              <td>{{ $traces->old_values }}</td>
              <td>{{ $traces->new_values }}</td>
              <td>{{ $traces->created_at }}</td>
            </tr>
            @endforeach
            </tbody>
          </table>
          {{-- {!! $trace->links() !!} --}}
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {!! JsValidator::formRequest('App\Http\Requests\Audits\TraceRequest') !!}

      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          responsive: true,
          searching: false,
          paginate: true,  
        });
        // DATATABLE - FIN //

        // DATAPICKER - INICIO //
        $('#datepicker1').datepicker({
          format: 'dd/mm/yyyy',
          endDate: '+0d'
        });
        // DATAPICKER - FIN //

        // DATAPICKER - INICIO //
        $('#datepicker2').datepicker({
          format: 'dd/mm/yyyy',
          endDate: '+0d'
        });
        // DATAPICKER - FIN //

      });

      // function buscar()
      // {
      //   var formIndex = document.forms['formTraces'];
      //   var desde = document.getElementById("datepicker1").value;
      //   var hasta = document.getElementById("datepicker2").value;
      //   var operation = document.getElementById("operation").value;
      //   var email = document.getElementById("email").value;

      //   if (desde === '' && hasta === '' && operation === '' && email === '')
      //   {
      //     $('#message').html("@lang('message.seleccionarcriterio')");
      //     $('#msj').show();
      //     desvanecer();
      //   } else
      //   {
      //     formIndex.submit();
      //   }
      // }

      </script>
    @endsection

  @endsection
