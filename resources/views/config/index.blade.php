@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Configuraciones</a></li>

@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  @include('layouts.parciales.messages')

  {{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Criterio de Búsqueda {{-- Config::get('mail.from.address') --}}
      </b></h3>
    </div>
  {{-- FORMULARIO - INICIO --}}
    <form class="form" id="indexConfigForm" name = "indexConfigForm" role="form" data-toggle="validator" method="GET" action="{{ route('config.index') }}">
      <div class="card-body">
        <div class="row">
          <div class="col-md-5">
            <div class="form-group">
              <label for="name">Nombre de Configuración</label>
              <input type="text" class="form-control mayus" id="name" name="name">
            </div>
          </div>
          
        </div>

      {{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
      <div class="card-footer">
        <div class="pull-right">
          <button type="button" onClick="buscar();" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
            <a href="{{ route('config.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
          <button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
        </div>
      </div>
      {{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

    </form>
    {{-- FORMULARIO - FIN --}}
  </div>

  {{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

  {{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Configuraciones</b></h3>
    </div>
    <div class="card-footer">
      
      
    </div>
    <div class="card-body">
        <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th style="width: 5%">N°</th>
            <th style="width: 15%">Nombre</th>
            <th style="width: 15%" colspan="5">Descripción</th>
            <th style="width: 13%">Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($configs as $key => $config)
          <tr>
            <td >{{ strtoupper($config->id) }}</td>
            <td>{{ strtoupper($config->type) }}</td>
            @php $decode = json_decode($config->value); @endphp
            @if($config->id == 1)
              <td style="width: 15%">{{ strtoupper($decode->driver) }}</td>
              <td style="width: 15%">{{ strtoupper($decode->host) }}</td>
              <td style="width: 12%">{{ strtoupper($decode->username) }}</td>
              <td style="width: 10%">{{ $decode->port }}</td>
              <td style="width: 18%">{{ strtoupper($decode->from_name) }}</td>
            @elseif($config->id == 2)
              <td>Login 
                @if($decode->login == true) 
                  <i class="fa fa-check-circle-o" style="color: green"></i> 
                @else
                  <i class="fa fa-times-circle-o" style="color: red"></i> 
                @endif
              </td>
              <td>Google 
                @if($decode->google == true) 
                  <i class="fa fa-check-circle-o" style="color: green"></i> 
                @else
                  <i class="fa fa-times-circle-o" style="color: red"></i> 
                @endif
              </td>
              <td>Facebook 
                @if($decode->facebook == true) 
                  <i class="fa fa-check-circle-o" style="color: green"></i> 
                @else
                  <i class="fa fa-times-circle-o" style="color: red"></i> 
                @endif
              </td>
              <td>Twitter
                @if($decode->twitter == true) 
                  <i class="fa fa-check-circle-o" style="color: green"></i> 
                @else
                  <i class="fa fa-times-circle-o" style="color: red"></i> 
                @endif
              </td>
              <td>Instagram 
                @if($decode->instagram == true) 
                  <i class="fa fa-check-circle-o" style="color: green"></i> 
                @else
                  <i class="fa fa-times-circle-o" style="color: red"></i> 
                @endif
              </td>
            @elseif($config->id == 3)
              <td colspan="2">
                Registro interno 
                @if($decode->interno == true) 
                  <i class="fa fa-check-circle-o" style="color: green"></i> 
                @else
                  <i class="fa fa-times-circle-o" style="color: red"></i> 
                @endif
              </td>
              <td colspan="3">
                Registro Externo 
                @if($decode->interno == true) 
                  <i class="fa fa-check-circle-o" style="color: green"></i> 
                @else
                  <i class="fa fa-times-circle-o" style="color: red"></i> 
                @endif
              </td>
            @elseif($config->id == 4)
              <td colspan="5">
                Tiempo de expiración
                <span class="text-danger">{{ $decode->lifetime }}</span> Segundos
              </td>
              @elseif($config->id == 5)
              <td colspan="5">
                Captcha
                @if($decode->captcha == true) 
                  <i class="fa fa-check-circle-o" style="color: green"></i> 
                @else
                  <i class="fa fa-times-circle-o" style="color: red"></i> 
                @endif
              </td>
            @endif
            <td class="text-center">
              @permission('config-edit')
                 <a href="{{ route('config.edit',$config->id) }}" class="btn btn-info"> <i class="fa fa-edit"></i></a>
              @endpermission
                 
              
            </td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          columnDefs: [
              {orderable: false, targets: 6 }
          ],
          responsive: true,
          searching: false,
          paginate: true,
        });
        // DATATABLE - FIN //


      });

      function buscar()
      {
        var formEdit = document.forms['indexConfigForm'];
        var vname = document.getElementById("name").value;
        /*var vdisplay_name = document.getElementById("display_name").value;
        var vstatus_role_id = document.getElementById("status_role_id").value;*/

        if (vname === '' ) //&& vdisplay_name === '' && vstatus_role_id === '')
        {
          $('#message').html("@lang('message.seleccionarcriterio')");
          $('#msj').show();
          desvanecer();
        } else
        {
          formEdit.submit();
        }

      }

      

      function upperInput(entrada)
      {
        var transform = document.getElementById(entrada.id).value;
        transform = transform.toUpperCase();
        document.getElementById(entrada.id).value=transform;
      }

      </script>
    @endsection

@endsection
