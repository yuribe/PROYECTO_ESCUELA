@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ route('config.index') }}">Configuraciones</a></li>
  <li class="breadcrumb-item active"><a>Edición {{ $config->nombre }}</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><b>Edición {{ $config->type }}</b></h3>
	</div>

	{{-- FORMULARIO - INICIO --}}
	{!! Form::model($config, ['method' => 'PATCH','route' => ['config.update', $config->id], 'id' => 'configForm']) !!}
	

		<div class="card-body">
			<div class="row">
    			<div class="col-md-6">
					<div class="form-group">
						<label for="name">Nombre</label>
						<input type="text" class="form-control" value="{{ $config->type }}" disabled />
					</div>
				</div>
				@if($config->id == 1)
    			<div class="col-md-6">
					<div class="form-group">
						<label for="name">Nombre a mostrar</label>
						<input type="text" class="form-control" id="name" name="name" value="{{ $config->type }}" />
					</div>
				</div>
				
				@endif
			</div> <!--Fin Row-->
			@if($config->id == 1)
			<div class="row">
    			<div class="col-md-4">
					<div class="form-group">
						<label for="name">Controlador (DRIVER)</label>
						<input type="text" class="form-control" value="{{ $decode->driver }}" name="driver"  id="driver" />
					</div>
				</div>
    			<div class="col-md-4">
					<div class="form-group">
						<label for="name">Host</label>
						<input type="text" class="form-control" id="host" name="host" value="{{ $decode->host }}" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="port">Puerto</label>
						<input type="text" class="form-control" id="port" name="port" value="{{ $decode->port }}" />
					</div>
				</div>
			</div> <!--Fin Row-->
			<div class="row">
    			<div class="col-md-6">
					<div class="form-group">
						<label for="name">Usuario</label>
						<input type="text" class="form-control" value="{{ $decode->username }}" name="username" id="username" />
					</div>
				</div>
    			<div class="col-md-6">
					<div class="form-group">
						<label for="name">Contraseña</label>
						<input type="password" class="form-control" id="password" name="password" value="{{ $decode->password }}"/>
					</div>
				</div>
			</div> <!--Fin Row-->
			<div class="row">
    			<div class="col-md-6">
					<div class="form-group">
						<label for="address">Direccion de correo a mostrar</label>
						<input type="text" class="form-control" value="{{ $decode->from_address }}" 
						name="address" id="address" />
					</div>
				</div>
    			<div class="col-md-6">
					<div class="form-group">
						<label for="encryption">encryption</label>
						<input type="text" class="form-control mayus" id="encryption" name="encryption" value="{{ $decode->encryption }}"/>
					</div>
				</div>
			</div> <!--Fin Row-->
			@elseif($config->id ==2)
			  <div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label class="checkbox"><br>
					        <input name="login" @if($decode->login ==true )checked @endif type="checkbox">
					        <span class="label-text">Login<span class="icon-check"></span></span>
					    </label>
					</div>
				</div>
			
				<div class="col-md-2">
					<div class="form-group">
						<label class="checkbox"><br>
					        <input name="google" @if($decode->google ==true )checked @endif type="checkbox">
					        <span class="label-text">Google<span class="icon-check"></span></span>
					    </label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="checkbox"><br>
					        <input name="facebook" @if($decode->facebook ==true )checked @endif type="checkbox">
					        <span class="label-text">Facebook<span class="icon-check"></span></span>
					    </label>
					</div>
				</div>
			
				<div class="col-md-2">
					<div class="form-group">
						<label class="checkbox"><br>
					        <input name="twitter"  @if($decode->twitter ==true )checked @endif type="checkbox">
					        <span class="label-text">Twitter<span class="icon-check"></span></span>
					    </label>
					</div>
				</div>	
				<div class="col-md-2">
					<div class="form-group">
						<label class="checkbox"><br>
					        <input name="instagram"  @if($decode->instagram ==true )checked @endif type="checkbox">
					        <span class="label-text">Instagram<span class="icon-check"></span></span>
					    </label>
					</div>
				</div>	
			  </div> <!--Fin Row-->		
			@elseif($config->id ==3)
			  <div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label class="checkbox"><br>
					        <input name="interno" @if($decode->interno ==true )checked @endif type="checkbox">
					        <span class="label-text">Registro interno<span class="icon-check"></span></span>
					    </label>
					</div>
				</div>
			
				<div class="col-md-2">
					<div class="form-group">
						<label class="checkbox"><br>
					        <input name="externo" @if($decode->externo ==true )checked @endif type="checkbox">
					        <span class="label-text">Registro externo<span class="icon-check"></span></span>
					    </label>
					</div>
				</div>	
			  </div>
			@elseif($config->id ==4)
			  <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="name">Tiempo de Expiración</label>
						<input type="text" class="form-control" value="{{ $decode->lifetime }}" name="lifetime" id="lifetime" />
					</div>
				</div>
			  </div>
			@elseif($config->id ==5)
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="checkbox"><br>
					        <input name="captcha" @if($decode->captcha ==true )checked @endif type="checkbox">
					        <span class="label-text">Captcha<span class="icon-check"></span></span>
					    </label>
					</div>
				</div>
			  </div>
			@endif
        
		{{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
		<div class="card-footer">
			<div class="pull-right">
        		<button type="submit" class="btn btn-info">Aceptar</button>
				<a href="{{ route('config.index') }}" class="btn btn-danger">Cancelar</a>
			</div>
		</div>
		{{-- BOTONES (REGRESAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

	{!! Form::close() !!}
	{{-- FORMULARIO - FIN --}}
</div>



@section('script')
<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

{!! JsValidator::formRequest('App\Http\Requests\UpdateConfigRequest') !!}
  <script type="text/javascript">
  	$(document).ready(function(){
		  $('.checkbox').iCheck({
		    checkboxClass: 'icheckbox_square-blue',
		    radioClass: 'iradio_square-blue',
		    increaseArea: '20%' // optional
		  });
		});
	</script>
@endsection

@endsection
