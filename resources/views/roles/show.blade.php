@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Roles</a></li>
	<li class="breadcrumb-item active"><a>Mostrar Rol</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><b>Mostrar Rol</b></h3>
	</div>
  	<div class="card-body">
			<div class="row">
    		<div class="col-md-6">
					<div class="form-group">
						<label for="name">Nombre del Rol</label>
						<input type="text" class="form-control mayus" id="name" name="name" value="{{ $role->name }}" readonly />
  				</div>
				</div>
    		<div class="col-md-6">
					<div class="form-group">
						<label for="display_name">Nombre a mostrar</label>
						<input type="text" class="form-control mayus" id="display_name" name="display_name" value="{{ $role->display_name }}" readonly />
					</div>
				</div>
			</div> <!--Fin Row-->
			<div class="row">
    		<div class="col-md-10">
					<div class="form-group">
						<label for="description">Descripción</label>
						<input type="text" class="form-control mayus" id="description" name="description" value="{{ $role->description }}" readonly />
					</div>
				</div>
        <div class="col-md-2">
					<div class="form-group">
						<label for="status_role_id">Estatus</label>
					  <input type="text" class="form-control mayus" id="status_role_id" name="status_role_id" value="{{ $role->generics->name }}" readonly />
					</div>
				</div>
			</div> <!--Fin Row-->
		</div>

		{{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
		<div class="card-footer">
			<div class="pull-right">
      	<a href="{{ route('roles.index') }}" class="btn btn-danger">Regresar</a>
			</div>
		</div>
		{{-- BOTONES (REGRESAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

</div>

@endsection
