@extends('layouts.app')

<?php
    //dd($roles);

    if (isset($name))
      $name = $name;
    else
      $name = "";
    if (isset($display_name))
      $display_name = $display_name;
    else
      $display_name = "";
    if (isset($description))
      $description = $description;
    else
      $description = "";
    if (isset($status_role_id))
      $status_role_id = $status_role_id;
    else
      $status_role_id = "";
?>

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item active"><a href="{{ route('roles.index') }}">Roles</a></li>

@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  @include('layouts.parciales.messages')

	{{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
		</div>
	{{-- FORMULARIO - INICIO --}}
    <form class="form" id="indexRolesForm" name = "indexRolesForm" role="form" data-toggle="validator" method="GET" action="{{ route('roles.index') }}">
			<div class="card-body">
				<div class="row">
					<div class="col-md-5">
						<div class="form-group">
							<label for="name">Nombre del rol</label>
							<input type="text" onchange="upperInput(this);" onblur="upperInput(this);" class="form-control mayus" id="name" name="name">
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label for="display_name">Nombre a mostrar</label>
							<input onchange="upperInput(this);" onblur="upperInput(this);" class="form-control mayus" id="display_name" name="display_name">
						</div>
					</div>
          <div class="col-md-2">
						<div class="form-group">
							<label for="status_role_id">Estatus</label>
              <select class="form-control" id="status_role_id" name="status_role_id">
                <option value = "" selected>Seleccione una opción</option>
                @foreach($generics as $generic)
                  <option value="{{ $generic->id }}">{{ $generic->name }}</option>
                @endforeach
              </select>
						</div>
					</div>
				</div>

			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
			<div class="card-footer">
				<div class="pull-right">
					<button type="button" onClick="buscar();" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
				 	  <a href="{{ route('roles.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
					<button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
				</div>
			</div>
			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

		</form>
		{{-- FORMULARIO - FIN --}}
	</div>

	{{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

	{{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Roles</b></h3>
    </div>
    <div class="card-footer">
			@permission('role-create')
        <a href="{{ route('roles.create') }}" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a>
			@endpermission
      @permission('role-pdf')
        <button type="button" onClick="imprimePdf();" class="btn btn-info"><i class="fa fa-file"></i> Pdf</button>
      @endpermission
      @permission('role-excel')
        <button type="button" onClick="imprimeExcel();" class="btn btn-info"><i class="fa fa-file"></i> Excel</button>
      @endpermission
    </div>
    <div class="card-body">
      <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th style="width: 5%">N°</th>
            <th style="width: 15%">Nombre del rol</th>
						<th style="width: 15%">Nombre a mostrar</th>
            <th style="width: 33%">Descripción</th>
						<th style="width: 12%">Fecha/Hora creación</th>
						<th style="width: 5%">Estatus</th>
            <th style="width: 15%">Acciones</th>
          </tr>
        </thead>
        <tbody>
					@foreach ($roles as $key => $role)
        	<tr>
            <td>{{ ++$i }}</td>
            <td>{{ $role->name }}</td>
						<td>{{ $role->display_name }}</td>
            <td>{{ $role->description }}</td>
						<td>{{ $role->created_at }}</td>
						<td>{{ $role->generics->name }}</td>
            <td class="text-center">
							@permission('role-edit')
                 <a href="{{ route('roles.edit',$role->id) }}" class="btn btn-info"> <i class="fa fa-edit"></i></a>
							@endpermission
                 <a href="{{ route('roles.show',$role->id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
							@permission('role-delete')
                 <button type="button" class="btn btn-danger" title="Eliminar Rol" data-toggle="modal" data-target="#myModal_{{ $role->id }}">
                   <i class="fa fa-trash"></i>
                 </button>
              @endpermission
                 <div class="modal fade" tabindex="-1" role="dialog" id="myModal_{{ $role->id }}">
                   <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel">Información</h4>
                       </div>
                       <div class="modal-body">
                         <h4>¿Desea eliminar la información?</h4>
                       </div>
                       <div class="modal-footer">
                         {{Form::open(array('route'=> array('roles.destroy', $role['id']), 'method'=>'PATCH','id'=>'borraRegistro'))}}
                         {{ method_field('DELETE') }}
                           <button type="submit" id="borra" class="btn btn-danger" >Si</button>
                           <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">No</button>
                         {{Form::close()}}
                       </div>
                     </div>
                   </div>
                 </div>
            </td>
          </tr>
					@endforeach
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          columnDefs: [
              {orderable: false, targets: 6 }
          ],
          responsive: true,
          searching: false,
          paginate: true,
        });
        // DATATABLE - FIN //


      });

      function buscar()
      {
        var formEdit = document.forms['indexRolesForm'];
        var vname = document.getElementById("name").value;
        var vdisplay_name = document.getElementById("display_name").value;
        var vstatus_role_id = document.getElementById("status_role_id").value;

        if (vname === '' && vdisplay_name === '' && vstatus_role_id === '')
        {
          $('#message').html("@lang('message.seleccionarcriterio')");
          $('#msj').show();
          desvanecer();
        } else
        {
          formEdit.submit();
        }

      }

      function imprimePdf()
      {
        var name = '<?php echo $name; ?>';
        var display_name = '<?php echo $display_name; ?>';
        var status_role_id = '<?php echo $status_role_id; ?>';
        window.open('/roles/pdf' + '?' + '&name=' + name + '&display_name=' + display_name + '&status_role_id=' + status_role_id, '_blank');
      }

      function imprimeExcel()
      {
        var name = '<?php echo $name; ?>';
        var display_name = '<?php echo $display_name; ?>';
        var status_role_id = '<?php echo $status_role_id; ?>';
        window.open('/roles/excel' + '?' + '&name=' + name + '&display_name=' + display_name + '&status_role_id=' + status_role_id, '_blank');
      }

      function upperInput(entrada)
      {
        var transform = document.getElementById(entrada.id).value;
        transform = transform.toUpperCase();
        document.getElementById(entrada.id).value=transform;
      }

      </script>
    @endsection

@endsection
