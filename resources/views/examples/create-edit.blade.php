@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Ejemplos</a></li>
    <li class="breadcrumb-item active"><a>Ejemplo Criterio de Busqueda</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  {{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Nuevo_Título_Modulo</b></h3>
    </div>

  {{-- FORMULARIO - INICIO --}}
    <form method="" id="" name="">

      <div class="card-body">

        <div class="row">

          {{-- INPUT TEXT NORMAL - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label for="exampleInput1">Campo_1</label>
              <input type="" class="form-control" id="exampleInput1" >
            </div>
          </div>
          {{-- INPUT TEXT NORMAL - FIN --}}

          {{-- SELECT NORMAL - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label>Campo_2</label>
              <select class="form-control">
                <option>option_1</option>
                <option>option_2</option>
                <option>option_3</option>
                <option>option_4</option>
                <option>option_5</option>
              </select>
            </div>
          </div>
          {{-- SELECT NORMAL - FIN --}}

          {{-- SELECT 2 - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label for="">Campo_3</label>
              <select class="form-control select2" id="exampleInput2" style="width:100%">
                <option value="" selected>Seleccione una opción</option>
                <option value=""></option>
              </select>
            </div>
          </div>
          {{-- SELECT 2 - FIN --}}

          {{-- DATAPICKER - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label>Campo_Fecha 1:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                  </span>
                </div>
                <input type="text" class="form-control float-right" id="datepicker1">
              </div>
            </div>
          </div>
          {{-- DATAPICKER - FIN --}}

          {{-- DATAPICKERRANGE - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label>Campo_Fecha Rango 1:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                  </span>
                </div>
                <input type="text" class="form-control float-right" id="datepickerange1">
              </div>
            </div>
          </div>
        {{-- DATAPICKERRANGE - FIN --}}

        {{-- TEXTAREA - INICIO --}}
        <div class="col-md-12">
          <div class="form-group">
            <label>Campo_4</label>
            <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
          </div>
        </div>
        {{-- TEXTAREA - FIN --}}

        {{-- INPUT CON BOTON - INICIO --}}
        <div class="col-md-12">
          <label>Campo_5</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <button type="button" class="btn btn-info">Acción</button>
            </div>
            <!-- /btn-group -->
            <input type="text" class="form-control">
          </div>
        </div>
        {{-- INPUT CON BOTON - FIN --}}

      </div>

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInput3">Campo_6 </label>
              <input class="form-control" id="exampleInput3">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInput4">Campo_7 </label>
              <input type="" class="form-control" id="exampleInput4" >
            </div>
          </div>
        </div>
        {{-- EJEMPLO CON DOS COLUMNAS - FIN --}}

        {{-- EJEMPLO CON TRES COLUMNAS - INICIO --}}
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="exampleInput5">Campo_8 </label>
              <input class="form-control" id="exampleInput5">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="exampleInput6">Campo_9 </label>
              <input type="" class="form-control" id="exampleInput6" >
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="exampleInput7">Campo_9 </label>
              <input type="" class="form-control" id="exampleInput7" >
            </div>
          </div>
        </div>
        {{-- EJEMPLO CON TRES COLUMNAS - FIN --}}
      </div>

      {{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
      <div class="card-footer">
        <div class="pull-right">
          <a href="#" class="btn btn-danger">Regresar</a>
          <button type="submit" class="btn btn-info">Guardar</button>
        </div>
      </div>
      {{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

    </form>
    {{-- FORMULARIO - FIN --}}
  </div>

  {{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          responsive: true,
        });
        // DATATABLE - FIN //

        // SELECT - INICIO //
        $('.select2').select2();
        // SELECT - FIN //

        // DATAPICKER - INICIO //
        $('#datepicker1').datepicker()
        // DATAPICKER - FIN //

        // DATAPICKER RANGE - INICIO //
        $('#datepickerange1').daterangepicker()
        // DATAPICKER RANGE - FIN //

      });

      </script>
    @endsection

  @endsection
