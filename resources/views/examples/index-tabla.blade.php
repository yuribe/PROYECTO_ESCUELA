@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Ejemplos</a></li>
    <li class="breadcrumb-item active"><a>Ejemplo Criterio de Busqueda</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  {{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Título_Modulo</b></h3>
    </div>
    <div class="card-footer">
      <a href="#" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a>
      <a href="#" class="btn btn-info"><i class="fa fa-file"></i> Pdf</a>
      <a href="#" class="btn btn-info"><i class="fa fa-file"></i> Excell</a>
    </div>
    <div class="card-body">
      <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>N°</th>
            <th>Campo_1</th>
            <th>Campo_2</th>
            <th>Campo_3</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Información_1</td>
            <td>Información_2</td>
            <td>Información_3</td>
            <td class="text-center">
              <a href="#" class="btn btn-info"> <i class="fa fa-edit"></i></a>
              <a href="#" class="btn btn-info"><i class="fa fa-eye"></i></a>
              <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
            </td>
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          responsive: true,
        });
        // DATATABLE - FIN //

        // SELECT - INICIO //
        $('.select2').select2();
        // SELECT - FIN //

        // DATAPICKER - INICIO //
        $('#datepicker1').datepicker()
        // DATAPICKER - FIN //

        // DATAPICKER RANGE - INICIO //
        $('#datepickerange1').daterangepicker()
        // DATAPICKER RANGE - FIN //

      });

      </script>
    @endsection

  @endsection
