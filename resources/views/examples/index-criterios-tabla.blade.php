@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Ejemplos</a></li>
    <li class="breadcrumb-item active"><a>Ejemplo Criterio de Busqueda</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  {{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
    </div>

  {{-- FORMULARIO - INICIO --}}
    <form method="" id="" name="">

      <div class="card-body">

        <div class="row">

          {{-- INPUT TEXT NORMAL - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label for="exampleInput1">Criterio_1</label>
              <input type="" class="form-control" id="exampleInput1" >
            </div>
          </div>
          {{-- INPUT TEXT NORMAL - FIN --}}

          {{-- SELECT NORMAL - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label>Criterio_2</label>
              <select class="form-control">
                <option>option_1</option>
                <option>option_2</option>
                <option>option_3</option>
                <option>option_4</option>
                <option>option_5</option>
              </select>
            </div>
          </div>
          {{-- SELECT NORMAL - FIN --}}

          {{-- SELECT 2 - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label for="">Criterio_3</label>
              <select class="form-control select2" id="exampleInput2" style="width:100%">
                <option value="" selected>Seleccione una opción</option>
                <option value=""></option>
              </select>
            </div>
          </div>
          {{-- SELECT 2 - FIN --}}

          {{-- DATAPICKER - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label>Criterio_Fecha 1:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                  </span>
                </div>
                <input type="text" class="form-control float-right" id="datepicker1">
              </div>
            </div>
          </div>
          {{-- DATAPICKER - FIN --}}

          {{-- DATAPICKERRANGE - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label>Criterio_Fecha Rango 1:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                  </span>
                </div>
                <input type="text" class="form-control float-right" id="datepickerange1">
              </div>
            </div>
          </div>
        {{-- DATAPICKERRANGE - FIN --}}
      </div>

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInput3">Criterio_4 </label>
              <input class="form-control" id="exampleInput3">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInput4">Criterio_5 </label>
              <input type="" class="form-control" id="exampleInput4" >
            </div>
          </div>
        </div>
        {{-- EJEMPLO CON DOS COLUMNAS - FIN --}}

        {{-- EJEMPLO CON TRES COLUMNAS - INICIO --}}
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="exampleInput5">Criterio_6 </label>
              <input class="form-control" id="exampleInput5">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="exampleInput6">Criterio_7 </label>
              <input type="" class="form-control" id="exampleInput6" >
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="exampleInput7">Criterio_8 </label>
              <input type="" class="form-control" id="exampleInput7" >
            </div>
          </div>
        </div>
        {{-- EJEMPLO CON TRES COLUMNAS - FIN --}}
      </div>

      {{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
      <div class="card-footer">
        <div class="pull-right">
          <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
          <a href="#" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
          <button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
        </div>
      </div>
      {{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

    </form>
    {{-- FORMULARIO - FIN --}}
  </div>

  {{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

  {{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Título_Modulo</b></h3>
    </div>
    <div class="card-footer">
      <a href="#" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a>
      <a href="#" class="btn btn-info"><i class="fa fa-file"></i> Pdf</a>
      <a href="#" class="btn btn-info"><i class="fa fa-file"></i> Excell</a>
    </div>
    <div class="card-body">
      <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>N°</th>
            <th>Campo_1</th>
            <th>Campo_2</th>
            <th>Campo_3</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Información_1</td>
            <td>Información_2</td>
            <td>Información_3</td>
            <td class="text-center">
              <a href="#" class="btn btn-info"> <i class="fa fa-edit"></i></a>
              <a href="#" class="btn btn-info"><i class="fa fa-eye"></i></a>
              <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          responsive: true,
        });
        // DATATABLE - FIN //

        // SELECT - INICIO //
        $('.select2').select2();
        // SELECT - FIN //

        // DATAPICKER - INICIO //
        $('#datepicker1').datepicker()
        // DATAPICKER - FIN //

        // DATAPICKER RANGE - INICIO //
        $('#datepickerange1').daterangepicker()
        // DATAPICKER RANGE - FIN //

      });

      </script>
    @endsection

  @endsection
