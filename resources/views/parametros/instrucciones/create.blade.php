@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="#">Parametros</a></li>
  <li class="breadcrumb-item"><a href="{{ route('parametros.instrucciones.index') }}">Instrucciones</a></li>
  <li class="breadcrumb-item active"><a>Nueva Instrucción</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  {{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Nueva Instrucción</b></h3>
    </div>

  {{-- FORMULARIO - INICIO --}}
    <form method="" id="" name="">

      <div class="card-body">

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}
        <div class="row">
          <div class="col-md-6">
            {{-- SELECT 2 - INICIO --}}
            <div class="form-group">
              <label for="">Tipo Colaborador</label>
              <select class="form-control form-control-lg select2" id="exampleInput2" style="width:100%">
                <option value="" selected>Seleccione una opción</option>
                <option value=""></option>
              </select>
            </div>
            {{-- SELECT 2 - FIN --}}
          </div>
          <div class="col-md-6">
            {{-- INPUT TEXT NORMAL - INICIO --}}
            <div class="form-group">
              <label for="exampleInput1">Instrucción</label>
              <input type="" class="form-control" id="exampleInput1" >
            </div>
            {{-- INPUT TEXT NORMAL - FIN --}}
          </div>
        </div>
        {{-- EJEMPLO CON DOS COLUMNAS - FIN --}}

        {{-- EDITOR TEXTO - INICIO --}}
        <div class="row">
          <div class="col-md-12">
            <div class="card card-outline card-info">
              <div class="card-header">
                <h3 class="card-title">
                  Descripción
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool btn-sm" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
                  <i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body pad">
                <div class="mb-3">
                  <textarea class="textarea" placeholder="Ingrese una descripción aquí..."
                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        {{-- EDITOR TEXTO - FIN --}}

      </div>

      {{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
      <div class="card-footer">
        <div class="pull-right">
          <a href="{{ route('parametros.instrucciones.index') }}" class="btn btn-danger">Regresar</a>
          <button type="submit" class="btn btn-info">Guardar</button>
        </div>
      </div>
      {{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

    </form>
    {{-- FORMULARIO - FIN --}}
  </div>

  {{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // SELECT - INICIO //
        $('.select2').select2();
        // SELECT - FIN //

        // EDITOR - INICIO /
        $('.textarea').wysihtml5({
          toolbar: { fa: true },
          locale: 'es-ES'
        });
        // EDITOR - FIN /

      });

      </script>
    @endsection

  @endsection
