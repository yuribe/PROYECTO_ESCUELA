@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="#">Parametros</a></li>
  <li class="breadcrumb-item"><a href="{{ route('parametros.categorias.index') }}">Categoría</a></li>
  <li class="breadcrumb-item active"><a>Nueva Categoría</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  {{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Nueva Categoría</b></h3>
    </div>

    {{-- FORMULARIO - INICIO --}}
    <form method="" id="" name="">

      <div class="card-body">

        <div class="row">

          {{-- INPUT TEXT NORMAL - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label for="exampleInput1">Categoría</label>
              <input type="" class="form-control" id="exampleInput1" >
            </div>
          </div>
          {{-- INPUT TEXT NORMAL - FIN --}}

        </div>
      </div>

      {{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
      <div class="card-footer">
        <div class="pull-right">
          <a href="{{ route('parametros.categorias.index') }}" class="btn btn-danger">Regresar</a>
          <button type="submit" class="btn btn-info">Guardar</button>
        </div>
      </div>
      {{-- BOTONES (REGRESAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

    </form>
    {{-- FORMULARIO - FIN --}}
  </div>

  {{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

      });

      </script>
    @endsection

  @endsection
