@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Parametros</a></li>
    <li class="breadcrumb-item active"><a>Tipo Colaborador</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  {{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Tipo Colaborador</b></h3>
    </div>
    <div class="card-footer">
      <a href="{{ route('parametros.tipo-colaborador.create') }}" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a>
      <a href="#" class="btn btn-info"><i class="fa fa-file"></i> Pdf</a>
    </div>
    <div class="card-body">
      <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>N°</th>
            <th>Tipo Colaborador</th>
            <th>Estatus</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Información_1</td>
            <td>Información_2</td>
            <td class="text-center">
              <a href="{{ route('parametros.tipo-colaborador.edit') }}" class="btn btn-info"> <i class="fa fa-edit"></i></a>
              <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
            </td>
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          responsive: true,
        });
        // DATATABLE - FIN //

      });

      </script>
    @endsection

  @endsection
