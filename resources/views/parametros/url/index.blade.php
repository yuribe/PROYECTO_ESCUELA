@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Parametros</a></li>
    <li class="breadcrumb-item active"><a>Url</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  {{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
    </div>

  {{-- FORMULARIO - INICIO --}}
    <form method="" id="" name="">

      <div class="card-body">

        <div class="row">

          {{-- SELECT 2 - INICIO --}}
          <div class="col-md-12">
            <div class="form-group">
              <label for="">Web</label>
              <select class="form-control select2" id="exampleInput2" style="width:100%">
                <option value="" selected>Seleccione una opción</option>
                <option value=""></option>
              </select>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label for="">Categoría</label>
              <select class="form-control select2" id="exampleInput2" style="width:100%">
                <option value="" selected>Seleccione una opción</option>
                <option value=""></option>
              </select>
            </div>
          </div>
          {{-- SELECT 2 - FIN --}}

      </div>

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}
        <div class="row">
          {{-- SELECT NORMAL - INICIO --}}
          <div class="col-md-6">
            <div class="form-group">
              <label>Estatus</label>
              <select class="form-control">
                <option>option 1</option>
                <option>option 2</option>
                <option>option 3</option>
                <option>option 4</option>
                <option>option 5</option>
              </select>
            </div>
          </div>
          {{-- SELECT NORMAL - FIN --}}
        </div>
        {{-- EJEMPLO CON DOS COLUMNAS - FIN --}}

      </div>

      {{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
      <div class="card-footer">
        <div class="pull-right">
          <a href="#" class="btn btn-info"><i class="fa fa-search"></i> Buscar</a>
          <a href="#" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
          <button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
        </div>
      </div>
      {{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

    </form>
    {{-- FORMULARIO - FIN --}}
  </div>

  {{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

  {{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Url</b></h3>
    </div>
    <div class="card-footer">
      <a href="{{ route('parametros.url.create') }}" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a>
      <a href="#" class="btn btn-info"><i class="fa fa-file"></i> Excell</a>
    </div>
    <div class="card-body">
      <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>N°</th>
            <th>Web</th>
            <th>Clase</th>
            <th>Categoría</th>
            <th>Url</th>
            <th>Estatus</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Información_1</td>
            <td>Información_2</td>
            <td>Información_3</td>
            <td>Información_4</td>
            <td>Información_5</td>
            <td class="text-center">
              <a href="{{ route('parametros.url.edit') }}" class="btn btn-info"> <i class="fa fa-edit"></i></a>
              <a href="#" class="btn btn-info"><i class="fa fa-eye"></i></a>
              <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
            </td>
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          responsive: true,
        });
        // DATATABLE - FIN //

        // SELECT - INICIO //
        $('.select2').select2();
        // SELECT - FIN //

      });

      </script>
    @endsection

  @endsection
