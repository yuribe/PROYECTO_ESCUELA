@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="#">Parametros</a></li>
  <li class="breadcrumb-item active"><a href="{{ route('parametros.url.index') }}">Url</a></li>
  <li class="breadcrumb-item active"><a>Editar Url</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  {{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Editar Url</b></h3>
    </div>

  {{-- FORMULARIO - INICIO --}}
    <form method="" id="" name="">

      <div class="card-body">

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}
        <div class="row">
          {{-- SELECT 2 - INICIO --}}
          <div class="col-md-6">
            <div class="form-group">
              <label for="">Web</label>
              <select class="form-control select2" id="exampleInput2" style="width:100%">
                <option value="" selected>Seleccione una opción</option>
                <option value=""></option>
              </select>
            </div>
          </div>
          {{-- SELECT 2 - FIN --}}

          {{-- SELECT 2 - INICIO --}}
          <div class="col-md-6">
            <div class="form-group">
              <label for="">Categoría</label>
              <select class="form-control select2" id="exampleInput2" style="width:100%">
                <option value="" selected>Seleccione una opción</option>
                <option value=""></option>
              </select>
            </div>
          </div>
          {{-- SELECT 2 - FIN --}}
        </div>
        {{-- EJEMPLO CON DOS COLUMNAS - FIN --}}

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}
        <div class="row">
          {{-- SELECT 2 - INICIO --}}
          <div class="col-md-6">
            <div class="form-group">
              <label for="">Clase</label>
              <select class="form-control select2" id="exampleInput2" style="width:100%">
                <option value="" selected>Seleccione una opción</option>
                <option value=""></option>
              </select>
            </div>
          </div>
          {{-- SELECT 2 - FIN --}}

          {{-- INPUT TEXT NORMAL - INICIO --}}
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInput1">Url</label>
              <input type="" class="form-control" id="exampleInput1" >
            </div>
          </div>
          {{-- INPUT TEXT NORMAL - FIN --}}
        </div>
        {{-- EJEMPLO CON DOS COLUMNAS - FIN --}}

      </div>

      {{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
      <div class="card-footer">
        <div class="pull-right">
          <a href="{{ route('parametros.url.index') }}" class="btn btn-danger">Regresar</a>
          <button type="submit" class="btn btn-info">Guardar</button>
        </div>
      </div>
      {{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

    </form>
    {{-- FORMULARIO - FIN --}}
  </div>

  {{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // SELECT - INICIO //
        $('.select2').select2();
        // SELECT - FIN //
      });

      </script>
    @endsection

  @endsection
