
@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ route('userstatus.index') }}">Estatus usuarios</a></li>
  <li class="breadcrumb-item active"><a>Nuevo estatus usuario</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><b>Nuevo estatus usuario</b></h3>
	</div>

	{{-- FORMULARIO - INICIO --}}
	<form class="form" id="createUserStatus" name="createUserStatus" role="form" data-toggle="validator" method="POST" action="{{ route('userstatus.store') }}">
  {{ csrf_field() }}

		<div class="card-body">
			<div class="row">
    		<div class="col-md-6">
					<div class="form-group">
						<label for="name">Nombre</label>
						<input type="text" class="form-control mayus" id="name" name="name" value="{{ old('name') }}" />
						@if ($errors->has('name'))
                <span class="text-danger">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
					</div>
				</div>
    		<div class="col-md-10">
					<div class="form-group">
						<label for="description">Descripción</label>
						<input type="text" class="form-control mayus" id="description" name="description" value="{{ old('description') }}" />
						@if ($errors->has('description'))
                <span class="text-danger">
                  <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
					</div>
				</div>
			</div> <!--Fin Row-->
		</div>

		{{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
		<div class="card-footer">
			<div class="pull-right">
        <button type="submit" class="btn btn-info">Aceptar</button>
				<a href="{{ route('userstatus.index') }}" class="btn btn-danger">Cancelar</a>
			</div>
		</div>
		{{-- BOTONES (REGRESAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

	</form>
	{{-- FORMULARIO - FIN --}}
</div>

{!! JsValidator::formRequest('App\Http\Requests\UserStatus\StoreUserStatusRequest') !!}


@section('scripts')
  <script type="text/javascript">

	$(document).ready( function () {

	});

	</script>
@endsection

@endsection
