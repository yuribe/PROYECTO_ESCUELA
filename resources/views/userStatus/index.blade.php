@extends('layouts.app')

<?php
    if (isset($name))
      $name = $name;
    else
      $name = "";
    if (isset($description))
      $description = $description;
    else
      $description = "";
?>

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item active"><a href="{{ route('userstatus.index') }}">Estatus Usuarios</a></li>

@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  @include('layouts.parciales.messages')

	{{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
		</div>
	{{-- FORMULARIO - INICIO --}}
    <form class="form" id="indexUserStatusForm" name = "indexUserStatus" role="form" data-toggle="validator" method="GET" action="{{ route('userstatus.index') }}">
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="name">Nombre</label>
							<input class="form-control mayus" id="name" name="name">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="description">Descripción</label>
							<input class="form-control mayus" id="description" name="description">
						</div>
					</div>
				</div>

			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
			<div class="card-footer">
				<div class="pull-right">
					<button type="button" onClick="buscar();" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
				 	  <a href="{{ route('userstatus.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
					<button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
				</div>
			</div>
			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

		</form>
		{{-- FORMULARIO - FIN --}}
	</div>

	{{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

	{{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Estatus usuarios</b></h3>
    </div>
    <div class="card-footer">
			@permission('userstatus-create')
        <a href="{{ route('userstatus.create') }}" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a>
			@endpermission
      @permission('userstatus-pdf')
        <button type="button" onClick="imprimePdf();" class="btn btn-info"><i class="fa fa-file"></i> Pdf</button>
      @endpermission
      @permission('userstatus-excel')
        <button type="button" onClick="imprimeExcel();" class="btn btn-info"><i class="fa fa-file"></i> Excel</button>
      @endpermission
    </div>
    <div class="card-body">
        <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th style="width: 5%">N°</th>
            <th style="width: 25%">Nombre</th>
            <th style="width: 40%">Descripción</th>
						<th style="width: 15%">Fecha/Hora creación</th>
						<th style="width: 15%">Acciones</th>
          </tr>
        </thead>
        <tbody>
					@foreach ($userstatus as $key => $userstatu)
        	<tr>
            <td>{{ ++$i }}</td>
            <td>{{ $userstatu->name }}</td>
            <td>{{ $userstatu->description }}</td>
						<td>{{ $userstatu->created_at }}</td>
            <td class="text-center">
							@permission('userstatus-edit')
                 <a href="{{ route('userstatus.edit',$userstatu->id) }}" class="btn btn-info"> <i class="fa fa-edit"></i></a>
							@endpermission
                 <a href="{{ route('userstatus.show',$userstatu->id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
							@permission('userstatus-delete')
                 <button type="button" class="btn btn-danger" title="Eliminar Módulo" data-toggle="modal" data-target="#myModal_{{ $userstatu->id }}">
                   <i class="fa fa-trash"></i>
                 </button>
              @endpermission
                 <div class="modal fade" tabindex="-1" role="dialog" id="myModal_{{ $userstatu->id }}">
                   <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel">Información</h4>
                       </div>
                       <div class="modal-body">
                         <h4>¿Desea eliminar la información?</h4>
                       </div>
                       <div class="modal-footer">
                         {{Form::open(array('route'=> array('userstatus.destroy', $userstatu['id']), 'method'=>'PATCH','id'=>'borraRegistro'))}}
                         {{ method_field('DELETE') }}
                           <button type="submit" id="borra" class="btn btn-danger" >Si</button>
                           <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">No</button>
                         {{Form::close()}}
                       </div>
                     </div>
                   </div>
                 </div>
            </td>
          </tr>
					@endforeach
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          columnDefs: [
              {orderable: false, targets: 6 }
          ],
          responsive: true,
          searching: false,
          paginate: true,
        });
        // DATATABLE - FIN //


      });

      function buscar()
      {
        var formIndex = document.forms['indexUserStatusForm'];
        var vname = document.getElementById("name").value;
        var vdescription = document.getElementById("description").value;

        if (vname === '' && vdescription === '')
        {
          $('#message').html("@lang('message.seleccionarcriterio')");
          $('#msj').show();
          desvanecer();
        } else
        {
          formIndex.submit();
        }
      }

      function imprimePdf()
      {
        var name = '<?php echo $name; ?>';
        var description = '<?php echo $description; ?>';
        window.open('/userstatus/pdf' + '?' + '&name=' + name + '&description=' + description, '_blank');
      }

      function imprimeExcel()
      {
        var name = '<?php echo $name; ?>';
        var description = '<?php echo $description; ?>';
        window.open('/userstatus/excel' + '?' + '&name=' + name + '&description=' + description, '_blank');
      }

      </script>
    @endsection

@endsection
