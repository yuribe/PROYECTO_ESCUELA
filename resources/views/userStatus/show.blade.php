@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item"><a href="{{ route('userstatus.index') }}">Estatus usuarios</a></li>
	<li class="breadcrumb-item active"><a>Mostrar estatus usuario</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><b>Mostrar estatus usuario</b></h3>
	</div>
  	<div class="card-body">
			<div class="row">
    		<div class="col-md-6">
					<div class="form-group">
						<label for="name">Nombre</label>
						<input type="text" class="form-control mayus" id="name" name="name" value="{{ $userstatus->name }}" readonly />
  				</div>
				</div>
    		<div class="col-md-10">
					<div class="form-group">
						<label for="description">Descripción</label>
						<input type="text" class="form-control mayus" id="description" name="description" value="{{ $userstatus->description }}" readonly />
					</div>
				</div>
  		</div> <!--Fin Row-->
		</div>

		{{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
		<div class="card-footer">
			<div class="pull-right">
      	<a href="{{ route('userstatus.index') }}" class="btn btn-danger">Regresar</a>
			</div>
		</div>
		{{-- BOTONES (REGRESAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

</div>

@endsection
