@extends('layouts.app')

<?php

    if (isset($name))
      $name = $name;
    else
      $name = "";
    if (isset($display_name))
      $display_name = $display_name;
    else
      $display_name = "";
    if (isset($module_id))
      $module_id = $module_id;
    else
      $module_id = "";
    if (isset($generic_status_id))
      $generic_status_id = $generic_status_id;
    else
      $generic_status_id = "";

?>

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item active"><a href="{{ route('permissions.index') }}">Permisos(Abilities)</a></li>

@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  @include('layouts.parciales.messages')

	{{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
		</div>
	{{-- FORMULARIO - INICIO --}}
    <form class="form" id="indexPermissionsForm" name = "indexPermissionsForm" role="form" data-toggle="validator" method="GET" action="{{ route('permissions.index') }}">
			<div class="card-body">
				<div class="row">
          <div class="col-md-2">
						<div class="form-group">
							<label for="module_id">Módulo(Ruta)</label>
              <select class="form-control" id="module_id" name="module_id">
                <option value="" selected>Seleccione una opción</option>
                @foreach($modules as $module)
                  <option value="{{ $module->id }}">{{ $module->name }}</option>
                @endforeach
              </select>
						</div>
					</div>
          <div class="col-md-4">
						<div class="form-group">
							<label for="name">Nombre del permiso(Ability)</label>
							<input class="form-control" id="name" name="name">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="display_name">Nombre a mostrar</label>
							<input class="form-control" id="display_name" name="display_name">
						</div>
					</div>
          <div class="col-md-2">
						<div class="form-group">
							<label for="generic_status_id">Estatus</label>
              <select class="form-control" id="generic_status_id" name="generic_status_id">
                <option value = "" selected>Seleccione una opción</option>
                @foreach($generics as $generic)
                  <option value="{{ $generic->id }}">{{ $generic->name }}</option>
                @endforeach
              </select>
						</div>
					</div>
				</div>

			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
			<div class="card-footer">
				<div class="pull-right">
					<button type="button" onClick="buscar();" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
				 	  <a href="{{ route('permissions.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
					<button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
				</div>
			</div>
			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

		</form>
		{{-- FORMULARIO - FIN --}}
	</div>

	{{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

	{{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Permisos(Abilities)</b></h3>
    </div>
    <div class="card-footer">
			@permission('permission-create')
        <a href="{{ route('permissions.create') }}" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a>
			@endpermission
      @permission('permission-pdf')
        <button type="button" onClick="imprimePdf();" class="btn btn-info"><i class="fa fa-file"></i> Pdf</button>
      @endpermission
      @permission('permission-excel')
        <button type="button" onClick="imprimeExcel();" class="btn btn-info"><i class="fa fa-file"></i> Excel</button>
      @endpermission
    </div>
    <div class="card-body">
        <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th style="width: 5%">N°</th>
            <th style="width: 10%">Módulo(Ruta)</th>
            <th style="width: 15%">Nombre del permiso(Ability)</th>
						<th style="width: 15%">Nombre a mostrar</th>
            <th style="width: 20%">Descripción</th>
						<th style="width: 12%">Fecha/Hora creación</th>
						<th style="width: 10%">Estatus</th>
            <th style="width: 13%">Acciones</th>
          </tr>
        </thead>
        <tbody>
					@foreach ($permissions as $key => $permission)
        	<tr>
            <td>{{ ++$i }}</td>
            <td>{{ $permission->modules->name }}</td>
            <td>{{ $permission->name }}</td>
						<td>{{ $permission->display_name }}</td>
            <td>{{ $permission->description }}</td>
						<td>{{ $permission->created_at }}</td>
						<td>{{ $permission->generics->name }}</td>
            <td class="text-center">
							@permission('permission-edit')
                 <a href="{{ route('permissions.edit',$permission->id) }}" class="btn btn-info"> <i class="fa fa-edit"></i></a>
							@endpermission
                 <a href="{{ route('permissions.show',$permission->id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
							@permission('permission-delete')
                 <button type="button" class="btn btn-danger" title="Eliminar Permiso" data-toggle="modal" data-target="#myModal_{{ $permission->id }}">
                   <i class="fa fa-trash"></i>
                 </button>
              @endpermission
                 <div class="modal fade" tabindex="-1" role="dialog" id="myModal_{{ $permission->id }}">
                   <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel">Información</h4>
                       </div>
                       <div class="modal-body">
                         <h4>¿Desea eliminar la información?</h4>
                       </div>
                       <div class="modal-footer">
                         {{Form::open(array('route'=> array('permissions.destroy', $permission['id']), 'method'=>'PATCH','id'=>'borraRegistro'))}}
                         {{ method_field('DELETE') }}
                           <button type="submit" id="borra" class="btn btn-danger" >Si</button>
                           <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">No</button>
                         {{Form::close()}}
                       </div>
                     </div>
                   </div>
                 </div>
            </td>
          </tr>
					@endforeach
          </tbody>
        </table>
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          columnDefs: [
              {orderable: false, targets: 6 }
          ],
          responsive: true,
          searching: false,
          paginate: true,
        });
        // DATATABLE - FIN //


      });

      function buscar()
      {
        var formEdit = document.forms['indexPermissionsForm'];
        var vname = document.getElementById("name").value;
        var vdisplay_name = document.getElementById("display_name").value;
        var vmodule_id = document.getElementById("module_id").value;
        var vgeneric_status_id = document.getElementById("generic_status_id").value;

        if (vname === '' && vdisplay_name === '' && vmodule_id === '' && vgeneric_status_id === '')
        {
          $('#message').html("@lang('message.seleccionarcriterio')");
          $('#msj').show();
          desvanecer();
        } else
        {
          formEdit.submit();
        }

      }

      function imprimePdf()
      {
        var name = '<?php echo $name; ?>';
        var display_name = '<?php echo $display_name; ?>';
        var module_id = '<?php echo $module_id; ?>';
        var generic_status_id = '<?php echo $generic_status_id; ?>';
        window.open('/permissions/pdf' + '?' + '&name=' + name + '&display_name=' + display_name + '&module_id=' + module_id + '&generic_status_id=' + generic_status_id, '_blank');
      }

      function imprimeExcel()
      {
        var name = '<?php echo $name; ?>';
        var display_name = '<?php echo $display_name; ?>';
        var module_id = '<?php echo $module_id; ?>';
        var generic_status_id = '<?php echo $generic_status_id; ?>';
        window.open('/permissions/excel' + '?' + '&name=' + name + '&display_name=' + display_name + '&module_id=' + module_id + '&generic_status_id=' + generic_status_id, '_blank');
      }


      </script>
    @endsection

@endsection
