@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item"><a href="{{ route('permissions.index') }}">Permisos(Abilities)</a></li>
	<li class="breadcrumb-item active"><a>Mostrar Permiso(Ability)</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><b>Mostrar Permiso(Ability)</b></h3>
	</div>
  	<div class="card-body">
      <div class="row">
        <div class="col-md-6">
					<div class="form-group">
  						<label for="module_id">Nombre del módulo(ruta)</label>
						<input type="text" class="form-control" id="module_id" name="module_id" value="{{ $permission->modules->name }}" readonly />
  				</div>
				</div>
      </div>
      <div class="row">
    		<div class="col-md-6">
					<div class="form-group">
						<label for="name">Nombre del permiso(ability)</label>
						<input type="text" class="form-control" id="name" name="name" value="{{ $permission->name }}" readonly />
  				</div>
				</div>
    		<div class="col-md-6">
					<div class="form-group">
						<label for="display_name">Nombre a mostrar</label>
						<input type="text" class="form-control" id="display_name" name="display_name" value="{{ $permission->display_name }}" readonly />
					</div>
				</div>
			</div> <!--Fin Row-->
			<div class="row">
    		<div class="col-md-10">
					<div class="form-group">
						<label for="description">Descripción</label>
						<input type="text" class="form-control mayus" id="description" name="description" value="{{ $permission->description }}" readonly />
					</div>
				</div>
        <div class="col-md-2">
					<div class="form-group">
						<label for="generic_status_id">Estatus</label>
					  <input type="text" class="form-control mayus" id="generic_status_id" name="generic_status_id" value="{{ $permission->generics->name }}" readonly />
					</div>
				</div>
			</div> <!--Fin Row-->
		</div>

		{{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
		<div class="card-footer">
			<div class="pull-right">
      	<a href="{{ route('permissions.index') }}" class="btn btn-danger">Regresar</a>
			</div>
		</div>
		{{-- BOTONES (REGRESAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

</div>

@endsection
