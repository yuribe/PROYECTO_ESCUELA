
@extends('layouts.app')

<?php

   //dd($menus);

 ?>


{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ route('menus.index') }}">Menús</a></li>
  <li class="breadcrumb-item active"><a>Nuevo Menú</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><b>Nuevo Menú</b></h3>
	</div>

	{{-- FORMULARIO - INICIO --}}
	<form class="form" id="createMenu" name="createMenu" role="form" data-toggle="validator" method="POST" action="{{ route('menus.store') }}">
  {{ csrf_field() }}

		<div class="card-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="module_menu_id">Módulo(Ruta)</label>
            <select class="form-control" id="module_menu_id" name="module_menu_id">
              <option value="" selected>Seleccione una opción</option>
              @foreach($modules as $module)
                <option value="{{ $module->id }}">{{ $module->name }}</option>
              @endforeach
            </select>
            @if ($errors->has('module_menu_id'))
              <span class="text-danger">
                <strong>{{ $errors->first('module_menu_id') }}</strong>
              </span>
            @endif
          </div>
        </div>
      	<div class="col-md-6">
					<div class="form-group">
						<label for="name">Nombre del menú</label>
						<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" />
						@if ($errors->has('name'))
              <span class="text-danger">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
            @endif
					</div>
				</div>
      </div>
      <div class="row">
    		<div class="col-md-6">
					<div class="form-group">
						<label for="display_name">Nombre a mostrar</label>
						<input type="text" class="form-control" id="display_name" name="display_name" value="{{ old('display_name') }}" />
						@if ($errors->has('display_name'))
              <span class="text-danger">
                <strong>{{ $errors->first('display_name') }}</strong>
              </span>
            @endif
					</div>
				</div>
    		<div class="col-md-6">
					<div class="form-group">
						<label for="description">Descripción</label>
						<input type="text" class="form-control mayus" id="description" name="description" value="{{ old('description') }}" />
						@if ($errors->has('description'))
              <span class="text-danger">
                <strong>{{ $errors->first('description') }}</strong>
              </span>
            @endif
					</div>
				</div>
      </div> <!--Fin Row-->
			<div class="row">
        <div class="col-md-5">
					<div class="form-group">
						<label for="menu_parent">Elemento de menú padre</label>
            <select class="form-control" id="menu_parent" name="menu_parent">
              <option value="" selected>Seleccione una opción</option>
              @foreach($menus as $menu)
                <option value="{{ $menu->id }}">{{ $menu->display_name }}</option>
              @endforeach
            </select>
						@if ($errors->has('menu_parent'))
              <span class="text-danger">
                <strong>{{ $errors->first('menu_parent') }}</strong>
              </span>
            @endif
					</div>
				</div>
        <div class="col-md-1">
					<div class="form-group">
						<label for="menu_level">Nivel</label>
						<input type="text" class="form-control" id="menu_level" name="menu_level" value="{{ old('menu_level') }}" />
						@if ($errors->has('menu_level'))
              <span class="text-danger">
                <strong>{{ $errors->first('menu_level') }}</strong>
              </span>
            @endif
					</div>
				</div>
        <div class="col-md-2">
         <div class="form-group">
          <label for="menu_icon">Icono Menú</label>
             <button type="button" class="btn btn-m btn-info dropdown-toggle dropdown-toggle-split" style="width: 105%" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <span class="sr-only"></span> Seleccione una categoría
             </button>
           <div class="dropdown dropup">
             <div class="dropdown-menu">
               <a class="dropdown-item" data-toggle="modal" data-target="#new_icons"> 66 New Icons in 4.4 </a>
               <a class="dropdown-item" data-toggle="modal" data-target="#web_app">Web Application Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Hand_Icons">Hand Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Transportation">Transportation Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Gender">Gender Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#File">File Type Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Spinner">Spinner Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Form">Form Control Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Payment">Payment Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Chart">Chart Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Currency">Currency Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Text">Text Editor Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Directional">Directional Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Video">Video Player  Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#Brand">Brand Icons</a>
               <a class="dropdown-item" data-toggle="modal" data-target="#medical_icons">Medical Icons</a>
             </div>
           </div>
			    </div>
		    </div>
        <div class="col-md-2">
         <div class="form-group">
           <label for="menu_icon">Código ícono menú</label>
           <div class="input-group mb-2">
             <div class="input-group-prepend">
               <div class="input-group-text">
                 <i id="ver_icon" name="ver_icon"></i>
               </div>
            </div>
            <input type="text" onchange="actualizaIcono(this);" onblur="actualizaIcono(this);" class="form-control" id="menu_icon" name="menu_icon" value="{{ old('menu_icon') }}">
          </div>
           @if ($errors->has('menu_icon'))
             <span class="text-danger">
               <strong>{{ $errors->first('menu_icon') }}</strong>
             </span>
           @endif
         </div>
       </div>
       <div class="col-md-2">
         <div class="form-group">
           <label for="status_menu_id">Estatus</label>
           <select class="form-control" id="status_menu_id" name="status_menu_id">
             <option value = "" selected>Seleccione una opción</option>
             @foreach($generics as $generic)
               <option value="{{ $generic->id }}">{{ $generic->name }}</option>
             @endforeach
           </select>
           @if ($errors->has('status_menu_id'))
             <span class="text-danger">
               <strong>{{ $errors->first('status_menu_id') }}</strong>
             </span>
           @endif
         </div>
       </div>
      </div>
      <!-- Inicio modales selección icono -->
      <div class="modal fade right" tabindex="-1" role="dialog" id="new_icons">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>66 New Icons in 4.4</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onClick="llenarInput('fa fa-fw fa-500px'); return false;"><i class="fa fa-fw fa-500px"></i></a>fa-500px</div>
                  <div class="col-md-4"><a class="btn" onClick="llenarInput('fa fa-fw fa-amazon'); return false;"><i class="fa fa-fw fa-amazon"></i></a>fa-amazon</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-balance-scale'); return false;"><i class="fa fa-fw fa-balance-scale"></i></a>fa-balance-scale</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-0'); return false;"><i class="fa fa-fw fa-battery-0"></i></a>fa-battery-0<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-1'); return false;"><i class="fa fa-fw fa-battery-1"></i></a>fa-battery-1<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-2'); return false;"><i class="fa fa-fw fa-battery-2"></i></a>fa-battery-2<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-3'); return false;"><i class="fa fa-fw fa-battery-3"></i></a>fa-battery-3<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-4'); return false;"><i class="fa fa-fw fa-battery-4"></i></a>fa-battery-4<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-empty'); return false;"><i class="fa fa-fw fa-battery-empty"></i></a> fa-battery-empty</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-full'); return false;"><i class="fa fa-fw fa-battery-full"></i></a> fa-battery-full</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-half'); return false;"><i class="fa fa-fw fa-battery-half"></i></a> fa-battery-half</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-quarter'); return false;"><i class="fa fa-fw fa-battery-quarter"></i></a> fa-battery-quarter</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-three-quarters'); return false;"><i class="fa fa-fw fa-battery-three-quarters"></i></a>fa-battery-three-quarters</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-black-tie'); return false;"><i class="fa fa-fw fa-black-tie"></i></a> fa-black-tie</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-calendar-check-o'); return false;"><i class="fa fa-fw fa-calendar-check-o"></i></a> fa-calendar-check-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-calendar-minus-o'); return false;"><i class="fa fa-fw fa-calendar-minus-o"></i></a> fa-calendar-minus-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-calendar-plus-o'); return false;"><i class="fa fa-fw fa-calendar-plus-o"></i></a> fa-calendar-plus-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-calendar-times-o'); return false;"><i class="fa fa-fw fa-calendar-times-o"></i></a> fa-calendar-times-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-diners-club'); return false;"><i class="fa fa-fw fa-cc-diners-club"></i></a> fa-cc-diners-club</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-jcb'); return false;"><i class="fa fa-fw fa-cc-jcb"></i></a> fa-cc-jcb</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chrome'); return false;"><i class="fa fa-fw fa-chrome"></i></a> fa-chrome</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-clone'); return false;"><i class="fa fa-fw fa-clone"></i></a> fa-clone</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-commenting'); return false;"><i class="fa fa-fw fa-commenting"></i></a> fa-commenting</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-commenting-o'); return false;"><i class="fa fa-fw fa-commenting-o"></i></a> fa-commenting-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-contao'); return false;"><i class="fa fa-fw fa-contao"></i></a> fa-contao</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-creative-commons'); return false;"><i class="fa fa-fw fa-creative-commons"></i></a> fa-creative-commons</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-expeditedssl'); return false;"><i class="fa fa-fw fa-expeditedssl"></i></a> fa-expeditedssl</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-firefox'); return false;"><i class="fa fa-fw fa-firefox"></i></a> fa-firefox</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-fonticons'); return false;"><i class="fa fa-fw fa-fonticons"></i></a> fa-fonticons</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-genderless'); return false;"><i class="fa fa-fw fa-genderless"></i></a> fa-genderless</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-get-pocket'); return false;"><i class="fa fa-fw fa-get-pocket"></i></a> fa-get-pocket</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gg'); return false;"><i class="fa fa-fw fa-gg"></i></a> fa-gg</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gg-circle'); return false;"><i class="fa fa-fw fa-gg-circle"></i></a> fa-gg-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-grab-o'); return false;"><i class="fa fa-fw fa-hand-grab-o"></i></a> fa-hand-grab-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-lizard-o'); return false;"><i class="fa fa-fw fa-hand-lizard-o"></i></a> fa-hand-lizard-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-paper-o'); return false;"><i class="fa fa-fw fa-hand-paper-o"></i></a> fa-hand-paper-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-peace-o'); return false;"><i class="fa fa-fw fa-hand-peace-o"></i></a> fa-hand-peace-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-pointer-o'); return false;"><i class="fa fa-fw fa-hand-pointer-o"></i></a> fa-hand-pointer-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-rock-o'); return false;"><i class="fa fa-fw fa-hand-rock-o"></i></a> fa-hand-rock-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-scissors-o'); return false;"><i class="fa fa-fw fa-hand-scissors-o"></i></a> fa-hand-scissors-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-spock-o'); return false;"><i class="fa fa-fw fa-hand-spock-o"></i></a> fa-hand-spock-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-stop-o'); return false;"><i class="fa fa-fw fa-hand-stop-o"></i></a> fa-hand-stop-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass'); return false;"><i class="fa fa-fw fa-hourglass"></i></a> fa-hourglass</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-1'); return false;"><i class="fa fa-fw fa-hourglass-1"></i></a> fa-hourglass-1<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-2'); return false;"><i class="fa fa-fw fa-hourglass-2"></i></a> fa-hourglass-2<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-3'); return false;"><i class="fa fa-fw fa-hourglass-3"></i></a> fa-hourglass-3<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-end'); return false;"><i class="fa fa-fw fa-hourglass-end"></i></a> fa-hourglass-end</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-half'); return false;"><i class="fa fa-fw fa-hourglass-half"></i></a> fa-hourglass-half</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-o'); return false;"><i class="fa fa-fw fa-hourglass-o"></i></a> fa-hourglass-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-start'); return false;"><i class="fa fa-fw fa-hourglass-start"></i></a> fa-hourglass-start</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-houzz'); return false;"><i class="fa fa-fw fa-houzz"></i></a> fa-houzz</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-i-cursor'); return false;"><i class="fa fa-fw fa-i-cursor"></i></a> fa-i-cursor</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-industry'); return false;"><i class="fa fa-fw fa-industry"></i></a> fa-industry</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-internet-explorer'); return false;"><i class="fa fa-fw fa-internet-explorer"></i></a> fa-internet-explorer</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-map'); return false;"><i class="fa fa-fw fa-map"></i></a> fa-map</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-map-o'); return false;"><i class="fa fa-fw fa-map-o"></i></a> fa-map-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-map-pin'); return false;"><i class="fa fa-fw fa-map-pin"></i></a> fa-map-pin</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-map-signs'); return false;"><i class="fa fa-fw fa-map-signs"></i></a> fa-map-signs</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mouse-pointer'); return false;"><i class="fa fa-fw fa-mouse-pointer"></i></a> fa-mouse-pointer</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-object-group'); return false;"><i class="fa fa-fw fa-object-group"></i></a> fa-object-group</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-object-ungroup'); return false;"><i class="fa fa-fw fa-object-ungroup"></i></a> fa-object-ungroup</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-odnoklassniki'); return false;"><i class="fa fa-fw fa-odnoklassniki"></i></a> fa-odnoklassniki</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-odnoklassniki-square'); return false;"><i class="fa fa-fw fa-odnoklassniki-square"></i></a>fa-odnoklassniki-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-opencart'); return false;"><i class="fa fa-fw fa-opencart"></i></a> fa-opencart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-opera'); return false;"><i class="fa fa-fw fa-opera"></i></a> fa-opera</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-optin-monster'); return false;"><i class="fa fa-fw fa-optin-monster"></i></a> fa-optin-monster</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-registered'); return false;"><i class="fa fa-fw fa-registered"></i></a> fa-registered</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-safari'); return false;"><i class="fa fa-fw fa-safari"></i></a> fa-safari</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sticky-note'); return false;"><i class="fa fa-fw fa-sticky-note"></i></a> fa-sticky-note</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sticky-note-o'); return false;"><i class="fa fa-fw fa-sticky-note-o"></i></a> fa-sticky-note-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-television'); return false;"><i class="fa fa-fw fa-television"></i></a> fa-television</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-trademark'); return false;"><i class="fa fa-fw fa-trademark"></i></a> fa-trademark</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tripadvisor'); return false;"><i class="fa fa-fw fa-tripadvisor"></i></a> fa-tripadvisor</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tv'); return false;"><i class="fa fa-fw fa-tv"></i></a> fa-tv<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-vimeo'); return false;"><i class="fa fa-fw fa-vimeo"></i></a> fa-vimeo</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-wikipedia-w'); return false;"><i class="fa fa-fw fa-wikipedia-w"></i></a> fa-wikipedia-w</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-y-combinator'); return false;"><i class="fa fa-fw fa-y-combinator"></i></a> fa-y-combinator</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-yc'); return false;"><i class="fa fa-fw fa-yc"></i></a> fa-yc<span class="text-muted">(alias)</span></div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="web_app">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Web Application Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-adjust'); return false;"><i class="fa fa-fw fa-adjust"></i></a> fa-adjust</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-anchor'); return false;"><i class="fa fa-fw fa-anchor"></i></a> fa-anchor</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-archive'); return false;"><i class="fa fa-fw fa-archive"></i></a> fa-archive</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-area-chart'); return false;"><i class="fa fa-fw fa-area-chart"></i></a> fa-area-chart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrows'); return false;"><i class="fa fa-fw fa-arrows"></i></a> fa-arrows</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrows-h'); return false;"><i class="fa fa-fw fa-arrows-h"></i></a> fa-arrows-h</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrows-v'); return false;"><i class="fa fa-fw fa-arrows-v"></i></a> fa-arrows-v</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-asterisk'); return false;"><i class="fa fa-fw fa-asterisk"></i></a> fa-asterisk</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-at'); return false;"><i class="fa fa-fw fa-at"></i></a> fa-at</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-automobile'); return false;"><i class="fa fa-fw fa-automobile"></i></a> fa-automobile<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-balance-scale'); return false;"><i class="fa fa-fw fa-balance-scale"></i></a> fa-balance-scale</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-ban'); return false;"><i class="fa fa-fw fa-ban"></i></a> fa-ban</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bank'); return false;"><i class="fa fa-fw fa-bank"></i></a> fa-bank <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bar-chart'); return false;"><i class="fa fa-fw fa-bar-chart"></i></a> fa-bar-chart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bar-chart-o'); return false;"><i class="fa fa-fw fa-bar-chart-o"></i></a> fa-bar-chart-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-barcode'); return false;"><i class="fa fa-fw fa-barcode"></i></a> fa-barcode</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bars'); return false;"><i class="fa fa-fw fa-bars"></i></a> fa-bars</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-0'); return false;"><i class="fa fa-fw fa-battery-0"></i></a> fa-battery-0<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-1'); return false;"><i class="fa fa-fw fa-battery-1"></i></a> fa-battery-1<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-2'); return false;"><i class="fa fa-fw fa-battery-2"></i></a> fa-battery-2<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-3'); return false;"><i class="fa fa-fw fa-battery-3"></i></a> fa-battery-3<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-4'); return false;"><i class="fa fa-fw fa-battery-4"></i></a> fa-battery-4<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-empty'); return false;"><i class="fa fa-fw fa-battery-empty"></i></a> fa-battery-empty</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-full'); return false;"><i class="fa fa-fw fa-battery-full"></i></a> fa-battery-full</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-half'); return false;"><i class="fa fa-fw fa-battery-half"></i></a> fa-battery-half</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-quarter'); return false;"><i class="fa fa-fw fa-battery-quarter"></i></a> fa-battery-quarter</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-battery-three-quarters'); return false;"><i class="fa fa-fw fa-battery-three-quarters"></i></a>fa-battery-three-quarters</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bed'); return false;"><i class="fa fa-fw fa-bed"></i></a> fa-bed</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-beer'); return false;"><i class="fa fa-fw fa-beer"></i></a> fa-beer</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bell'); return false;"><i class="fa fa-fw fa-bell"></i></a> fa-bell</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bell-o'); return false;"><i class="fa fa-fw fa-bell-o"></i></a> fa-bell-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bell-slash'); return false;"><i class="fa fa-fw fa-bell-slash"></i></a> fa-bell-slash</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bell-slash-o'); return false;"><i class="fa fa-fw fa-bell-slash-o"></i></a> fa-bell-slash-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bicycle'); return false;"><i class="fa fa-fw fa-bicycle"></i></a> fa-bicycle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-binoculars'); return false;"><i class="fa fa-fw fa-binoculars"></i></a> fa-binoculars</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-birthday-cake'); return false;"><i class="fa fa-fw fa-birthday-cake"></i></a> fa-birthday-cake</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bolt'); return false;"><i class="fa fa-fw fa-bolt"></i></a> fa-bolt</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bomb'); return false;"><i class="fa fa-fw fa-bomb"></i></a> fa-bomb</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-book'); return false;"><i class="fa fa-fw fa-book"></i></a> fa-book</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bookmark'); return false;"><i class="fa fa-fw fa-bookmark"></i></a> fa-bookmark</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bookmark-o'); return false;"><i class="fa fa-fw fa-bookmark-o"></i></a> fa-bookmark-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-briefcase'); return false;"><i class="fa fa-fw fa-briefcase"></i></a> fa-briefcase</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bug'); return false;"><i class="fa fa-fw fa-bug"></i></a> fa-bug</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-building'); return false;"><i class="fa fa-fw fa-building"></i></a> fa-building</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-building-o'); return false;"><i class="fa fa-fw fa-building-o"></i></a> fa-building-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bullhorn'); return false;"><i class="fa fa-fw fa-bullhorn"></i></a> fa-bullhorn</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bullseye'); return false;"><i class="fa fa-fw fa-bullseye"></i></a> fa-bullseye</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bus'); return false;"><i class="fa fa-fw fa-bus"></i></a> fa-bus</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cab'); return false;"><i class="fa fa-fw fa-cab"></i></a> fa-cab <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-calculator'); return false;"><i class="fa fa-fw fa-calculator"></i></a> fa-calculator</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-calendar'); return false;"><i class="fa fa-fw fa-calendar"></i></a> fa-calendar</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-calendar-check-o'); return false;"><i class="fa fa-fw fa-calendar-check-o"></i></a> fa-calendar-check-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-calendar-minus-o'); return false;"><i class="fa fa-fw fa-calendar-minus-o"></i></a> fa-calendar-minus-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-calendar-o'); return false;"><i class="fa fa-fw fa-calendar-o"></i></a> fa-calendar-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-calendar-plus-o'); return false;"><i class="fa fa-fw fa-calendar-plus-o"></i></a> fa-calendar-plus-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-calendar-times-o'); return false;"><i class="fa fa-fw fa-calendar-times-o"></i></a> fa-calendar-times-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-camera'); return false;"><i class="fa fa-fw fa-camera"></i></a> fa-camera</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-camera-retro'); return false;"><i class="fa fa-fw fa-camera-retro"></i></a> fa-camera-retro</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-car'); return false;"><i class="fa fa-fw fa-car"></i></a> fa-car</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-square-o-down'); return false;"><i class="fa fa-fw fa-caret-square-o-down"></i></a>fa-caret-square-o-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-square-o-left'); return false;"><i class="fa fa-fw fa-caret-square-o-left"></i></a>fa-caret-square-o-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-square-o-right'); return false;"><i class="fa fa-fw fa-caret-square-o-right"></i></a>fa-caret-square-o-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-square-o-up'); return false;"><i class="fa fa-fw fa-caret-square-o-up"></i></a> fa-caret-square-o-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cart-arrow-down'); return false;"><i class="fa fa-fw fa-cart-arrow-down"></i></a> fa-cart-arrow-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cart-plus'); return false;"><i class="fa fa-fw fa-cart-plus"></i></a> fa-cart-plus</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc'); return false;"><i class="fa fa-fw fa-cc"></i></a> fa-cc</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-certificate'); return false;"><i class="fa fa-fw fa-certificate"></i></a> fa-certificate</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-check'); return false;"><i class="fa fa-fw fa-check"></i></a> fa-check</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-check-circle'); return false;"><i class="fa fa-fw fa-check-circle"></i></a> fa-check-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-check-circle-o'); return false;"><i class="fa fa-fw fa-check-circle-o"></i></a> fa-check-circle-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-check-square'); return false;"><i class="fa fa-fw fa-check-square"></i></a> fa-check-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-check-square-o'); return false;"><i class="fa fa-fw fa-check-square-o"></i></a> fa-check-square-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-child'); return false;"><i class="fa fa-fw fa-child"></i></a> fa-child</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-circle'); return false;"><i class="fa fa-fw fa-circle"></i></a> fa-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-circle-o'); return false;"><i class="fa fa-fw fa-circle-o"></i></a> fa-circle-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-circle-o-notch'); return false;"><i class="fa fa-fw fa-circle-o-notch"></i></a> fa-circle-o-notch</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-circle-thin'); return false;"><i class="fa fa-fw fa-circle-thin"></i></a> fa-circle-thin</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-clock-o'); return false;"><i class="fa fa-fw fa-clock-o"></i></a> fa-clock-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-clone'); return false;"><i class="fa fa-fw fa-clone"></i></a> fa-clone</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-close'); return false;"><i class="fa fa-fw fa-close"></i></a> fa-close <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cloud'); return false;"><i class="fa fa-fw fa-cloud"></i></a> fa-cloud</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cloud-download'); return false;"><i class="fa fa-fw fa-cloud-download"></i></a> fa-cloud-download</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cloud-upload'); return false;"><i class="fa fa-fw fa-cloud-upload"></i></a> fa-cloud-upload</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-code'); return false;"><i class="fa fa-fw fa-code"></i></a> fa-code</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-code-fork'); return false;"><i class="fa fa-fw fa-code-fork"></i></a> fa-code-fork</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-coffee'); return false;"><i class="fa fa-fw fa-coffee"></i></a> fa-coffee</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cog'); return false;"><i class="fa fa-fw fa-cog"></i></a> fa-cog</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cogs'); return false;"><i class="fa fa-fw fa-cogs"></i></a> fa-cogs</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-comment'); return false;"><i class="fa fa-fw fa-comment"></i></a> fa-comment</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-comment-o'); return false;"><i class="fa fa-fw fa-comment-o"></i></a> fa-comment-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-commenting'); return false;"><i class="fa fa-fw fa-commenting"></i></a> fa-commenting</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-commenting-o'); return false;"><i class="fa fa-fw fa-commenting-o"></i></a> fa-commenting-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-comments'); return false;"><i class="fa fa-fw fa-comments"></i></a> fa-comments</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-comments-o'); return false;"><i class="fa fa-fw fa-comments-o"></i></a> fa-comments-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-compass'); return false;"><i class="fa fa-fw fa-compass"></i></a> fa-compass</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-copyright'); return false;"><i class="fa fa-fw fa-copyright"></i></a> fa-copyright</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-creative-commons'); return false;"><i class="fa fa-fw fa-creative-commons"></i></a> fa-creative-commons</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-credit-card'); return false;"><i class="fa fa-fw fa-credit-card"></i></a> fa-credit-card</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-crop'); return false;"><i class="fa fa-fw fa-crop"></i></a> fa-crop</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-crosshairs'); return false;"><i class="fa fa-fw fa-crosshairs"></i></a> fa-crosshairs</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cube'); return false;"><i class="fa fa-fw fa-cube"></i></a> fa-cube</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cubes'); return false;"><i class="fa fa-fw fa-cubes"></i></a> fa-cubes</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cutlery'); return false;"><i class="fa fa-fw fa-cutlery"></i></a> fa-cutlery</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-dashboard'); return false;"><i class="fa fa-fw fa-dashboard"></i></a> fa-dashboard<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-database'); return false;"><i class="fa fa-fw fa-database"></i></a> fa-database</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-desktop'); return false;"><i class="fa fa-fw fa-desktop"></i></a> fa-desktop</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-diamond'); return false;"><i class="fa fa-fw fa-diamond"></i></a> fa-diamond</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-dot-circle-o'); return false;"><i class="fa fa-fw fa-dot-circle-o"></i></a> fa-dot-circle-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-download'); return false;"><i class="fa fa-fw fa-download"></i></a> fa-download</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-edit'); return false;"><i class="fa fa-fw fa-edit"></i></a> fa-edit <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-ellipsis-h'); return false;"><i class="fa fa-fw fa-ellipsis-h"></i></a> fa-ellipsis-h</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-ellipsis-v'); return false;"><i class="fa fa-fw fa-ellipsis-v"></i></a> fa-ellipsis-v</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-envelope'); return false;"><i class="fa fa-fw fa-envelope"></i></a> fa-envelope</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-envelope-o'); return false;"><i class="fa fa-fw fa-envelope-o"></i></a> fa-envelope-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-envelope-square'); return false;"><i class="fa fa-fw fa-envelope-square"></i></a> fa-envelope-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-eraser'); return false;"><i class="fa fa-fw fa-eraser"></i></a> fa-eraser</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-exchange'); return false;"><i class="fa fa-fw fa-exchange"></i></a> fa-exchange</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-exclamation'); return false;"><i class="fa fa-fw fa-exclamation"></i></a> fa-exclamation</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-exclamation-circle'); return false;"><i class="fa fa-fw fa-exclamation-circle"></i></a> fa-exclamation-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-exclamation-triangle'); return false;"><i class="fa fa-fw fa-exclamation-triangle"></i></a>fa-exclamation-triangle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-external-link'); return false;"><i class="fa fa-fw fa-external-link"></i></a> fa-external-link</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-external-link-square'); return false;"><i class="fa fa-fw fa-external-link-square"></i></a>fa-external-link-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-eye'); return false;"><i class="fa fa-fw fa-eye"></i></a> fa-eye</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-eye-slash'); return false;"><i class="fa fa-fw fa-eye-slash"></i></a> fa-eye-slash</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-eyedropper'); return false;"><i class="fa fa-fw fa-eyedropper"></i></a> fa-eyedropper</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-fax'); return false;"><i class="fa fa-fw fa-fax"></i></a> fa-fax</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-feed'); return false;"><i class="fa fa-fw fa-feed"></i></a> fa-feed <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-female'); return false;"><i class="fa fa-fw fa-female"></i></a> fa-female</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-fighter-jet'); return false;"><i class="fa fa-fw fa-fighter-jet"></i></a> fa-fighter-jet</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-archive-o'); return false;"><i class="fa fa-fw fa-file-archive-o"></i></a> fa-file-archive-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-audio-o'); return false;"><i class="fa fa-fw fa-file-audio-o"></i></a> fa-file-audio-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-code-o'); return false;"><i class="fa fa-fw fa-file-code-o"></i></a> fa-file-code-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-excel-o'); return false;"><i class="fa fa-fw fa-file-excel-o"></i></a> fa-file-excel-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-image-o'); return false;"><i class="fa fa-fw fa-file-image-o"></i></a> fa-file-image-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-movie-o'); return false;"><i class="fa fa-fw fa-file-movie-o"></i></a> fa-file-movie-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-pdf-o'); return false;"><i class="fa fa-fw fa-file-pdf-o"></i></a> fa-file-pdf-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-photo-o'); return false;"><i class="fa fa-fw fa-file-photo-o"></i></a> fa-file-photo-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-picture-o'); return false;"><i class="fa fa-fw fa-file-picture-o"></i></a> fa-file-picture-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-powerpoint-o'); return false;"><i class="fa fa-fw fa-file-powerpoint-o"></i></a> fa-file-powerpoint-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-sound-o'); return false;"><i class="fa fa-fw fa-file-sound-o"></i></a> fa-file-sound-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-video-o'); return false;"><i class="fa fa-fw fa-file-video-o"></i></a> fa-file-video-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-word-o'); return false;"><i class="fa fa-fw fa-file-word-o"></i></a> fa-file-word-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-zip-o'); return false;"><i class="fa fa-fw fa-file-zip-o"></i></a> fa-file-zip-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-film'); return false;"><i class="fa fa-fw fa-film"></i></a> fa-film</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-filter'); return false;"><i class="fa fa-fw fa-filter"></i></a> fa-filter</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-fire'); return false;"><i class="fa fa-fw fa-fire"></i></a> fa-fire</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-fire-extinguisher'); return false;"><i class="fa fa-fw fa-fire-extinguisher"></i></a> fa-fire-extinguisher</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-flag'); return false;"><i class="fa fa-fw fa-flag"></i></a> fa-flag</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-flag-checkered'); return false;"><i class="fa fa-fw fa-flag-checkered"></i></a> fa-flag-checkered</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-flag-o'); return false;"><i class="fa fa-fw fa-flag-o"></i></a> fa-flag-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-flash'); return false;"><i class="fa fa-fw fa-flash"></i></a> fa-flash <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-flask'); return false;"><i class="fa fa-fw fa-flask"></i></a> fa-flask</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-folder'); return false;"><i class="fa fa-fw fa-folder"></i></a> fa-folder</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-folder-o'); return false;"><i class="fa fa-fw fa-folder-o"></i></a> fa-folder-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-folder-open'); return false;"><i class="fa fa-fw fa-folder-open"></i></a> fa-folder-open</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-folder-open-o'); return false;"><i class="fa fa-fw fa-folder-open-o"></i></a> fa-folder-open-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-frown-o'); return false;"><i class="fa fa-fw fa-frown-o"></i></a> fa-frown-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-futbol-o'); return false;"><i class="fa fa-fw fa-futbol-o"></i></a> fa-futbol-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gamepad'); return false;"><i class="fa fa-fw fa-gamepad"></i></a> fa-gamepad</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gavel'); return false;"><i class="fa fa-fw fa-gavel"></i></a> fa-gavel</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gear'); return false;"><i class="fa fa-fw fa-gear"></i></a> fa-gear <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gears'); return false;"><i class="fa fa-fw fa-gears"></i></a> fa-gears <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gift'); return false;"><i class="fa fa-fw fa-gift"></i></a> fa-gift</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-glass'); return false;"><i class="fa fa-fw fa-glass"></i></a> fa-glass</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-globe'); return false;"><i class="fa fa-fw fa-globe"></i></a> fa-globe</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-graduation-cap'); return false;"><i class="fa fa-fw fa-graduation-cap"></i></a> fa-graduation-cap</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-group'); return false;"><i class="fa fa-fw fa-group"></i></a> fa-group <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-grab-o'); return false;"><i class="fa fa-fw fa-hand-grab-o"></i></a> fa-hand-grab-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-lizard-o'); return false;"><i class="fa fa-fw fa-hand-lizard-o"></i></a> fa-hand-lizard-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-paper-o'); return false;"><i class="fa fa-fw fa-hand-paper-o"></i></a> fa-hand-paper-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-peace-o'); return false;"><i class="fa fa-fw fa-hand-peace-o"></i></a> fa-hand-peace-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-pointer-o'); return false;"><i class="fa fa-fw fa-hand-pointer-o"></i></a> fa-hand-pointer-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-rock-o'); return false;"><i class="fa fa-fw fa-hand-rock-o"></i></a> fa-hand-rock-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-scissors-o'); return false;"><i class="fa fa-fw fa-hand-scissors-o"></i></a> fa-hand-scissors-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-spock-o'); return false;"><i class="fa fa-fw fa-hand-spock-o"></i></a> fa-hand-spock-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-stop-o'); return false;"><i class="fa fa-fw fa-hand-stop-o"></i></a> fa-hand-stop-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hdd-o'); return false;"><i class="fa fa-fw fa-hdd-o"></i></a> fa-hdd-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-headphones'); return false;"><i class="fa fa-fw fa-headphones"></i></a> fa-headphones</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-heart'); return false;"><i class="fa fa-fw fa-heart"></i></a> fa-heart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-heart-o'); return false;"><i class="fa fa-fw fa-heart-o"></i></a> fa-heart-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-heartbeat'); return false;"><i class="fa fa-fw fa-heartbeat"></i></a> fa-heartbeat</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-history'); return false;"><i class="fa fa-fw fa-history"></i></a> fa-history</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-home'); return false;"><i class="fa fa-fw fa-home"></i></a> fa-home</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hotel'); return false;"><i class="fa fa-fw fa-hotel"></i></a> fa-hotel <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass'); return false;"><i class="fa fa-fw fa-hourglass"></i></a> fa-hourglass</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-1'); return false;"><i class="fa fa-fw fa-hourglass-1"></i></a> fa-hourglass-1<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-2'); return false;"><i class="fa fa-fw fa-hourglass-2"></i></a> fa-hourglass-2<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-3'); return false;"><i class="fa fa-fw fa-hourglass-3"></i></a> fa-hourglass-3<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-end'); return false;"><i class="fa fa-fw fa-hourglass-end"></i></a> fa-hourglass-end</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-half'); return false;"><i class="fa fa-fw fa-hourglass-half"></i></a> fa-hourglass-half</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-o'); return false;"><i class="fa fa-fw fa-hourglass-o"></i></a> fa-hourglass-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hourglass-start'); return false;"><i class="fa fa-fw fa-hourglass-start"></i></a> fa-hourglass-start</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-i-cursor'); return false;"><i class="fa fa-fw fa-i-cursor"></i></a> fa-i-cursor</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-image'); return false;"><i class="fa fa-fw fa-image"></i></a> fa-image <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-inbox'); return false;"><i class="fa fa-fw fa-inbox"></i></a> fa-inbox</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-industry'); return false;"><i class="fa fa-fw fa-industry"></i></a> fa-industry</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-info'); return false;"><i class="fa fa-fw fa-info"></i></a> fa-info</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-info-circle'); return false;"><i class="fa fa-fw fa-info-circle"></i></a> fa-info-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-institution'); return false;"><i class="fa fa-fw fa-institution"></i></a> fa-institution<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-key'); return false;"><i class="fa fa-fw fa-key"></i></a> fa-key</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-keyboard-o'); return false;"><i class="fa fa-fw fa-keyboard-o"></i></a> fa-keyboard-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-language'); return false;"><i class="fa fa-fw fa-language"></i></a> fa-language</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-laptop'); return false;"><i class="fa fa-fw fa-laptop"></i></a> fa-laptop</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-leaf'); return false;"><i class="fa fa-fw fa-leaf"></i></a> fa-leaf</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-legal'); return false;"><i class="fa fa-fw fa-legal"></i></a> fa-legal <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-lemon-o'); return false;"><i class="fa fa-fw fa-lemon-o"></i></a> fa-lemon-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-level-down'); return false;"><i class="fa fa-fw fa-level-down"></i></a> fa-level-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-level-up'); return false;"><i class="fa fa-fw fa-level-up"></i></a> fa-level-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-life-bouy'); return false;"><i class="fa fa-fw fa-life-bouy"></i></a> fa-life-bouy<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-life-buoy'); return false;"><i class="fa fa-fw fa-life-buoy"></i></a> fa-life-buoy<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-life-ring'); return false;"><i class="fa fa-fw fa-life-ring"></i></a> fa-life-ring</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-life-saver'); return false;"><i class="fa fa-fw fa-life-saver"></i></a> fa-life-saver<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-lightbulb-o'); return false;"><i class="fa fa-fw fa-lightbulb-o"></i></a> fa-lightbulb-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-line-chart'); return false;"><i class="fa fa-fw fa-line-chart"></i></a> fa-line-chart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-location-arrow'); return false;"><i class="fa fa-fw fa-location-arrow"></i></a> fa-location-arrow</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-lock'); return false;"><i class="fa fa-fw fa-lock"></i></a> fa-lock</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-magic'); return false;"><i class="fa fa-fw fa-magic"></i></a> fa-magic</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-magnet'); return false;"><i class="fa fa-fw fa-magnet"></i></a> fa-magnet</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mail-forward'); return false;"><i class="fa fa-fw fa-mail-forward"></i></a> fa-mail-forward<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mail-reply'); return false;"><i class="fa fa-fw fa-mail-reply"></i></a> fa-mail-reply<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mail-reply-all'); return false;"><i class="fa fa-fw fa-mail-reply-all"></i></a> fa-mail-reply-all<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-male'); return false;"><i class="fa fa-fw fa-male"></i></a> fa-male</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-map'); return false;"><i class="fa fa-fw fa-map"></i></a> fa-map</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-map-marker'); return false;"><i class="fa fa-fw fa-map-marker"></i></a> fa-map-marker</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-map-o'); return false;"><i class="fa fa-fw fa-map-o"></i></a> fa-map-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-map-pin'); return false;"><i class="fa fa-fw fa-map-pin"></i></a> fa-map-pin</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-map-signs'); return false;"><i class="fa fa-fw fa-map-signs"></i></a> fa-map-signs</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-meh-o'); return false;"><i class="fa fa-fw fa-meh-o"></i></a> fa-meh-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-microphone'); return false;"><i class="fa fa-fw fa-microphone"></i></a> fa-microphone</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-microphone-slash'); return false;"><i class="fa fa-fw fa-microphone-slash"></i></a> fa-microphone-slash</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-minus'); return false;"><i class="fa fa-fw fa-minus"></i></a> fa-minus</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-minus-circle'); return false;"><i class="fa fa-fw fa-minus-circle"></i></a> fa-minus-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-minus-square'); return false;"><i class="fa fa-fw fa-minus-square"></i></a> fa-minus-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-minus-square-o'); return false;"><i class="fa fa-fw fa-minus-square-o"></i></a> fa-minus-square-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mobile'); return false;"><i class="fa fa-fw fa-mobile"></i></a> fa-mobile</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mobile-phone'); return false;"><i class="fa fa-fw fa-mobile-phone"></i></a> fa-mobile-phone<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-money'); return false;"><i class="fa fa-fw fa-money"></i></a> fa-money</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-moon-o'); return false;"><i class="fa fa-fw fa-moon-o"></i></a> fa-moon-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mortar-board'); return false;"><i class="fa fa-fw fa-mortar-board"></i></a> fa-mortar-board<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-motorcycle'); return false;"><i class="fa fa-fw fa-motorcycle"></i></a> fa-motorcycle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mouse-pointer'); return false;"><i class="fa fa-fw fa-mouse-pointer"></i></a> fa-mouse-pointer</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-music'); return false;"><i class="fa fa-fw fa-music"></i></a> fa-music</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-navicon'); return false;"><i class="fa fa-fw fa-navicon"></i></a> fa-navicon<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-newspaper-o'); return false;"><i class="fa fa-fw fa-newspaper-o"></i></a> fa-newspaper-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-object-group'); return false;"><i class="fa fa-fw fa-object-group"></i></a> fa-object-group</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-object-ungroup'); return false;"><i class="fa fa-fw fa-object-ungroup"></i></a> fa-object-ungroup</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-paint-brush'); return false;"><i class="fa fa-fw fa-paint-brush"></i></a> fa-paint-brush</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-paper-plane'); return false;"><i class="fa fa-fw fa-paper-plane"></i></a> fa-paper-plane</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-paper-plane-o'); return false;"><i class="fa fa-fw fa-paper-plane-o"></i></a> fa-paper-plane-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-paw'); return false;"><i class="fa fa-fw fa-paw"></i></a> fa-paw</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pencil'); return false;"><i class="fa fa-fw fa-pencil"></i></a> fa-pencil</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pencil-square'); return false;"><i class="fa fa-fw fa-pencil-square"></i></a> fa-pencil-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pencil-square-o'); return false;"><i class="fa fa-fw fa-pencil-square-o"></i></a> fa-pencil-square-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-phone'); return false;"><i class="fa fa-fw fa-phone"></i></a> fa-phone</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-phone-square'); return false;"><i class="fa fa-fw fa-phone-square"></i></a> fa-phone-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-photo'); return false;"><i class="fa fa-fw fa-photo"></i></a> fa-photo <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-picture-o'); return false;"><i class="fa fa-fw fa-picture-o"></i></a> fa-picture-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pie-chart'); return false;"><i class="fa fa-fw fa-pie-chart"></i></a> fa-pie-chart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-plane'); return false;"><i class="fa fa-fw fa-plane"></i></a> fa-plane</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-plug'); return false;"><i class="fa fa-fw fa-plug"></i></a> fa-plug</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-plus'); return false;"><i class="fa fa-fw fa-plus"></i></a> fa-plus</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-plus-circle'); return false;"><i class="fa fa-fw fa-plus-circle"></i></a> fa-plus-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-plus-square'); return false;"><i class="fa fa-fw fa-plus-square"></i></a> fa-plus-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-plus-square-o'); return false;"><i class="fa fa-fw fa-plus-square-o"></i></a> fa-plus-square-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-power-off'); return false;"><i class="fa fa-fw fa-power-off"></i></a> fa-power-off</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-print'); return false;"><i class="fa fa-fw fa-print"></i></a> fa-print</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-puzzle-piece'); return false;"><i class="fa fa-fw fa-puzzle-piece"></i></a> fa-puzzle-piece</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-qrcode'); return false;"><i class="fa fa-fw fa-qrcode"></i></a> fa-qrcode</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-question'); return false;"><i class="fa fa-fw fa-question"></i></a> fa-question</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-question-circle'); return false;"><i class="fa fa-fw fa-question-circle"></i></a> fa-question-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-quote-left'); return false;"><i class="fa fa-fw fa-quote-left"></i></a> fa-quote-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-quote-right'); return false;"><i class="fa fa-fw fa-quote-right"></i></a> fa-quote-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-random'); return false;"><i class="fa fa-fw fa-random"></i></a> fa-random</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-recycle'); return false;"><i class="fa fa-fw fa-recycle"></i></a> fa-recycle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-refresh'); return false;"><i class="fa fa-fw fa-refresh"></i></a> fa-refresh</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-registered'); return false;"><i class="fa fa-fw fa-registered"></i></a> fa-registered</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-remove'); return false;"><i class="fa fa-fw fa-remove"></i></a> fa-remove<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-reorder'); return false;"><i class="fa fa-fw fa-reorder"></i></a> fa-reorder<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-reply'); return false;"><i class="fa fa-fw fa-reply"></i></a> fa-reply</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-reply-all'); return false;"><i class="fa fa-fw fa-reply-all"></i></a> fa-reply-all</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-retweet'); return false;"><i class="fa fa-fw fa-retweet"></i></a> fa-retweet</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-road'); return false;"><i class="fa fa-fw fa-road"></i></a> fa-road</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-rocket'); return false;"><i class="fa fa-fw fa-rocket"></i></a> fa-rocket</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-rss'); return false;"><i class="fa fa-fw fa-rss"></i></a> fa-rss</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-rss-square'); return false;"><i class="fa fa-fw fa-rss-square"></i></a> fa-rss-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-search'); return false;"><i class="fa fa-fw fa-search"></i></a> fa-search</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-search-minus'); return false;"><i class="fa fa-fw fa-search-minus"></i></a> fa-search-minus</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-search-plus'); return false;"><i class="fa fa-fw fa-search-plus"></i></a> fa-search-plus</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-send'); return false;"><i class="fa fa-fw fa-send"></i></a> fa-send <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-send-o'); return false;"><i class="fa fa-fw fa-send-o"></i></a> fa-send-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-server'); return false;"><i class="fa fa-fw fa-server"></i></a> fa-server</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-share'); return false;"><i class="fa fa-fw fa-share"></i></a> fa-share</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-share-alt'); return false;"><i class="fa fa-fw fa-share-alt"></i></a> fa-share-alt</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-share-alt-square'); return false;"><i class="fa fa-fw fa-share-alt-square"></i></a> fa-share-alt-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-share-square'); return false;"><i class="fa fa-fw fa-share-square"></i></a> fa-share-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-share-square-o'); return false;"><i class="fa fa-fw fa-share-square-o"></i></a> fa-share-square-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-shield'); return false;"><i class="fa fa-fw fa-shield"></i></a> fa-shield</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-ship'); return false;"><i class="fa fa-fw fa-ship"></i></a> fa-ship</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-shopping-cart'); return false;"><i class="fa fa-fw fa-shopping-cart"></i></a> fa-shopping-cart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sign-in'); return false;"><i class="fa fa-fw fa-sign-in"></i></a> fa-sign-in</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sign-out'); return false;"><i class="fa fa-fw fa-sign-out"></i></a> fa-sign-out</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-signal'); return false;"><i class="fa fa-fw fa-signal"></i></a> fa-signal</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sitemap'); return false;"><i class="fa fa-fw fa-sitemap"></i></a> fa-sitemap</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sliders'); return false;"><i class="fa fa-fw fa-sliders"></i></a> fa-sliders</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-smile-onclick'); return false;"><i class="fa fa-fw fa-smile-o"></i></a> fa-smile-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-soccer-ball-o'); return false;"><i class="fa fa-fw fa-soccer-ball-o"></i></a> fa-soccer-ball-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sort'); return false;"><i class="fa fa-fw fa-sort"></i></a> fa-sort</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sort-alpha-asc'); return false;"><i class="fa fa-fw fa-sort-alpha-asc"></i></a> fa-sort-alpha-asc</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sort-alpha-desc'); return false;"><i class="fa fa-fw fa-sort-alpha-desc"></i></a> fa-sort-alpha-desc</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sort-amount-asc'); return false;"><i class="fa fa-fw fa-sort-amount-asc"></i></a> fa-sort-amount-asc</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sort-amount-desc'); return false;"><i class="fa fa-fw fa-sort-amount-desc"></i></a> fa-sort-amount-desc</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sort-asc'); return false;"><i class="fa fa-fw fa-sort-asc"></i></a> fa-sort-asc</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sort-desc'); return false;"><i class="fa fa-fw fa-sort-desc"></i></a> fa-sort-desc</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sort-down'); return false;"><i class="fa fa-fw fa-sort-down"></i></a> fa-sort-down<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sort-numeric-asc'); return false;"><i class="fa fa-fw fa-sort-numeric-asc"></i></a> fa-sort-numeric-asc</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sort-numeric-desc'); return false;"><i class="fa fa-fw fa-sort-numeric-desc"></i></a> fa-sort-numeric-desc</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sort-up'); return false;"><i class="fa fa-fw fa-sort-up"></i></a> fa-sort-up<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-space-shuttle'); return false;"><i class="fa fa-fw fa-space-shuttle"></i></a> fa-space-shuttle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-spinner'); return false;"><i class="fa fa-fw fa-spinner"></i></a> fa-spinner</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-spoon'); return false;"><i class="fa fa-fw fa-spoon"></i></a> fa-spoon</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-square'); return false;"><i class="fa fa-fw fa-square"></i></a> fa-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-square-o'); return false;"><i class="fa fa-fw fa-square-o"></i></a> fa-square-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-star'); return false;"><i class="fa fa-fw fa-star"></i></a> fa-star</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-star-half'); return false;"><i class="fa fa-fw fa-star-half"></i></a> fa-star-half</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-star-half-empty'); return false;"><i class="fa fa-fw fa-star-half-empty"></i></a> fa-star-half-empty<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-star-half-full'); return false;"><i class="fa fa-fw fa-star-half-full"></i></a> fa-star-half-full<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-star-half-o'); return false;"><i class="fa fa-fw fa-star-half-o"></i></a> fa-star-half-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-star-o'); return false;"><i class="fa fa-fw fa-star-o"></i></a> fa-star-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sticky-note'); return false;"><i class="fa fa-fw fa-sticky-note"></i></a> fa-sticky-note</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sticky-note-o'); return false;"><i class="fa fa-fw fa-sticky-note-o"></i></a> fa-sticky-note-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-street-view'); return false;"><i class="fa fa-fw fa-street-view"></i></a> fa-street-view</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-suitcase'); return false;"><i class="fa fa-fw fa-suitcase"></i></a> fa-suitcase</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sun-o'); return false;"><i class="fa fa-fw fa-sun-o"></i></a> fa-sun-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sun-o'); return false;"><i class="fa fa-fw fa-sun-o"></i></a> fa-sun-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-support'); return false;"><i class="fa fa-fw fa-support"></i></a> fa-support<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tablet'); return false;"><i class="fa fa-fw fa-tablet"></i></a> fa-tablet</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tachometer'); return false;"><i class="fa fa-fw fa-tachometer"></i></a> fa-tachometer</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tag'); return false;"><i class="fa fa-fw fa-tag"></i></a> fa-tag</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tags'); return false;"><i class="fa fa-fw fa-tags"></i></a> fa-tags</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tasks'); return false;"><i class="fa fa-fw fa-tasks"></i></a> fa-tasks</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-taxi'); return false;"><i class="fa fa-fw fa-taxi"></i></a> fa-taxi</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-television'); return false;"><i class="fa fa-fw fa-television"></i></a> fa-television</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-terminal'); return false;"><i class="fa fa-fw fa-terminal"></i></a> fa-terminal</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-thumb-tack'); return false;"><i class="fa fa-fw fa-thumb-tack"></i></a> fa-thumb-tack</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-thumbs-down'); return false;"><i class="fa fa-fw fa-thumbs-down"></i></a> fa-thumbs-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-thumbs-o-down'); return false;"><i class="fa fa-fw fa-thumbs-o-down"></i></a> fa-thumbs-o-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-thumbs-o-up'); return false;"><i class="fa fa-fw fa-thumbs-o-up"></i></a> fa-thumbs-o-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-thumbs-up'); return false;"><i class="fa fa-fw fa-thumbs-up"></i></a> fa-thumbs-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-ticket'); return false;"><i class="fa fa-fw fa-ticket"></i></a> fa-ticket</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-times'); return false;"><i class="fa fa-fw fa-times"></i></a> fa-times</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-times-circle'); return false;"><i class="fa fa-fw fa-times-circle"></i></a> fa-times-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-times-circle-o'); return false;"><i class="fa fa-fw fa-times-circle-o"></i></a> fa-times-circle-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tint'); return false;"><i class="fa fa-fw fa-tint"></i></a> fa-tint</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-toggle-down'); return false;"><i class="fa fa-fw fa-toggle-down"></i></a> fa-toggle-down<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-toggle-left'); return false;"><i class="fa fa-fw fa-toggle-left"></i></a> fa-toggle-left<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-toggle-off'); return false;"><i class="fa fa-fw fa-toggle-off"></i></a> fa-toggle-off</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-toggle-on'); return false;"><i class="fa fa-fw fa-toggle-on"></i></a> fa-toggle-on</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-toggle-right'); return false;"><i class="fa fa-fw fa-toggle-right"></i></a> fa-toggle-right<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-toggle-up'); return false;"><i class="fa fa-fw fa-toggle-up"></i></a> fa-toggle-up<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-trademark'); return false;"><i class="fa fa-fw fa-trademark"></i></a> fa-trademark</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-trash'); return false;"><i class="fa fa-fw fa-trash"></i></a> fa-trash</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-trash-o'); return false;"><i class="fa fa-fw fa-trash-o"></i></a> fa-trash-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tree'); return false;"><i class="fa fa-fw fa-tree"></i></a> fa-tree</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-trophy'); return false;"><i class="fa fa-fw fa-trophy"></i></a> fa-trophy</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-truck'); return false;"><i class="fa fa-fw fa-truck"></i></a> fa-truck</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tty'); return false;"><i class="fa fa-fw fa-tty"></i></a> fa-tty</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tv'); return false;"><i class="fa fa-fw fa-tv"></i></a> fa-tv<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-umbrella'); return false;"><i class="fa fa-fw fa-umbrella"></i></a> fa-umbrella</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-university'); return false;"><i class="fa fa-fw fa-university"></i></a> fa-university</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-unlock'); return false;"><i class="fa fa-fw fa-unlock"></i></a> fa-unlock</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-unlock-alt'); return false;"><i class="fa fa-fw fa-unlock-alt"></i></a> fa-unlock-alt</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-unsorted'); return false;"><i class="fa fa-fw fa-unsorted"></i></a> fa-unsorted<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-upload'); return false;"><i class="fa fa-fw fa-upload"></i></a> fa-upload</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-user'); return false;"><i class="fa fa-fw fa-user"></i></a> fa-user</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-user-plus'); return false;"><i class="fa fa-fw fa-user-plus"></i></a> fa-user-plus</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-user-secret'); return false;"><i class="fa fa-fw fa-user-secret"></i></a> fa-user-secret</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-user-times'); return false;"><i class="fa fa-fw fa-user-times"></i></a> fa-user-times</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-users'); return false;"><i class="fa fa-fw fa-users"></i></a> fa-users</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-video-camera'); return false;"><i class="fa fa-fw fa-video-camera"></i></a> fa-video-camera</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-volume-down'); return false;"><i class="fa fa-fw fa-volume-down"></i></a> fa-volume-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-volume-off'); return false;"><i class="fa fa-fw fa-volume-off"></i></a> fa-volume-off</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-volume-up'); return false;"><i class="fa fa-fw fa-volume-up"></i></a> fa-volume-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-warning'); return false;"><i class="fa fa-fw fa-warning"></i></a> fa-warning<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-wheelchair'); return false;"><i class="fa fa-fw fa-wheelchair"></i></a> fa-wheelchair</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-wifi'); return false;"><i class="fa fa-fw fa-wifi"></i></a> fa-wifi</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-wrench'); return false;"><i class="fa fa-fw fa-wrench"></i></a> fa-wrench</div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="Hand_Icons">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Hand Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-grab-o'); return false;"><i class="fa fa-fw fa-hand-grab-o"></i></a> fa-hand-grab-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-lizard-o'); return false;"><i class="fa fa-fw fa-hand-lizard-o"></i></a> fa-hand-lizard-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-o-down'); return false;"><i class="fa fa-fw fa-hand-o-down"></i></a> fa-hand-o-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-o-left'); return false;"><i class="fa fa-fw fa-hand-o-left"></i></a> fa-hand-o-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-o-right'); return false;"><i class="fa fa-fw fa-hand-o-right"></i></a> fa-hand-o-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-o-up'); return false;"><i class="fa fa-fw fa-hand-o-up"></i></a> fa-hand-o-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-paper-o'); return false;"><i class="fa fa-fw fa-hand-paper-o"></i></a> fa-hand-paper-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-peace-o'); return false;"><i class="fa fa-fw fa-hand-peace-o"></i></a> fa-hand-peace-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-pointer-o'); return false;"><i class="fa fa-fw fa-hand-pointer-o"></i></a> fa-hand-pointer-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-rock-o'); return false;"><i class="fa fa-fw fa-hand-rock-o"></i></a> fa-hand-rock-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-scissors-o'); return false;"><i class="fa fa-fw fa-hand-scissors-o"></i></a> fa-hand-scissors-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-spock-o'); return false;"><i class="fa fa-fw fa-hand-spock-o"></i></a> fa-hand-spock-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-stop-o'); return false;"><i class="fa fa-fw fa-hand-stop-o"></i></a> fa-hand-stop-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-thumbs-down'); return false;"><i class="fa fa-fw fa-thumbs-down"></i></a> fa-thumbs-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-thumbs-o-down'); return false;"><i class="fa fa-fw fa-thumbs-o-down"></i></a> fa-thumbs-o-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-thumbs-o-up'); return false;"><i class="fa fa-fw fa-thumbs-o-up"></i></a> fa-thumbs-o-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-thumbs-up'); return false;"><i class="fa fa-fw fa-thumbs-up"></i></a> fa-thumbs-up</div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="Gender">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Gender Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-genderless'); return false;"><i class="fa fa-fw fa-genderless"></i></a> fa-genderless</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-intersex'); return false;"><i class="fa fa-fw fa-intersex"></i></a> fa-intersex<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mars'); return false;"><i class="fa fa-fw fa-mars"></i></a> fa-mars</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mars-double'); return false;"><i class="fa fa-fw fa-mars-double"></i></a> fa-mars-double</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mars-stroke'); return false;"><i class="fa fa-fw fa-mars-stroke"></i></a> fa-mars-stroke</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mars-stroke-h'); return false;"><i class="fa fa-fw fa-mars-stroke-h"></i></a> fa-mars-stroke-h</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mars-stroke-v'); return false;"><i class="fa fa-fw fa-mars-stroke-v"></i></a> fa-mars-stroke-v</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-mercury'); return false;"><i class="fa fa-fw fa-mercury"></i></a> fa-mercury</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-neuter'); return false;"><i class="fa fa-fw fa-neuter"></i></a> fa-neuter</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-transgender'); return false;"><i class="fa fa-fw fa-transgender"></i></a> fa-transgender</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-transgender-alt'); return false;"><i class="fa fa-fw fa-transgender-alt"></i></a> fa-transgender-alt</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-venus'); return false;"><i class="fa fa-fw fa-venus"></i></a> fa-venus</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-venus-double'); return false;"><i class="fa fa-fw fa-venus-double"></i></a> fa-venus-double</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-venus-mars'); return false;"><i class="fa fa-fw fa-venus-mars"></i></a> fa-venus-mars</div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="File">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>File Type Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file'); return false;"><i class="fa fa-fw fa-file"></i></a> fa-file</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-archive-o'); return false;"><i class="fa fa-fw fa-file-archive-o"></i></a> fa-file-archive-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-audio-o'); return false;"><i class="fa fa-fw fa-file-audio-o"></i></a> fa-file-audio-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-code-o'); return false;"><i class="fa fa-fw fa-file-code-o"></i></a> fa-file-code-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-excel-o'); return false;"><i class="fa fa-fw fa-file-excel-o"></i></a> fa-file-excel-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-image-o'); return false;"><i class="fa fa-fw fa-file-image-o"></i></a> fa-file-image-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-movie-o'); return false;"><i class="fa fa-fw fa-file-movie-o"></i></a> fa-file-movie-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-o'); return false;"><i class="fa fa-fw fa-file-o"></i></a> fa-file-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-pdf-o'); return false;"><i class="fa fa-fw fa-file-pdf-o"></i></a> fa-file-pdf-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-photo-o'); return false;"><i class="fa fa-fw fa-file-photo-o"></i></a> fa-file-photo-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-picture-o'); return false;"><i class="fa fa-fw fa-file-picture-o"></i></a> fa-file-picture-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-powerpoint-onclick'); return false;"><i class="fa fa-fw fa-file-powerpoint-o"></i></a> fa-file-powerpoint-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-sound-o'); return false;"><i class="fa fa-fw fa-file-sound-o"></i></a> fa-file-sound-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-text'); return false;"><i class="fa fa-fw fa-file-text"></i></a> fa-file-text</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-text-o'); return false;"><i class="fa fa-fw fa-file-text-o"></i></a> fa-file-text-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-video-o'); return false;"><i class="fa fa-fw fa-file-video-o"></i></a> fa-file-video-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-word-o'); return false;"><i class="fa fa-fw fa-file-word-o"></i></a> fa-file-word-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-zip-o'); return false;"><i class="fa fa-fw fa-file-zip-o"></i></a> fa-file-zip-o<span class="text-muted">(alias)</span></div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="Spinner">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Spinner Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-circle-o-notch'); return false;"><i class="fa fa-fw fa-circle-o-notch"></i></a> fa-circle-o-notch</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cog'); return false;"><i class="fa fa-fw fa-cog"></i></a> fa-cog</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gear'); return false;"><i class="fa fa-fw fa-gear"></i></a> fa-gear <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-refresh'); return false;"><i class="fa fa-fw fa-refresh"></i></a> fa-refresh</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-spinner'); return false;"><i class="fa fa-fw fa-spinner"></i></a> fa-spinner</div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="Form">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Form Control Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-check-square'); return false;"><i class="fa fa-fw fa-check-square"></i></a> fa-check-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-check-square-o'); return false;"><i class="fa fa-fw fa-check-square-o"></i></a> fa-check-square-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-circle'); return false;"><i class="fa fa-fw fa-circle"></i></a> fa-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-circle-o'); return false;"><i class="fa fa-fw fa-circle-o"></i></a> fa-circle-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-dot-circle-o'); return false;"><i class="fa fa-fw fa-dot-circle-o"></i></a> fa-dot-circle-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-minus-square'); return false;"><i class="fa fa-fw fa-minus-square"></i></a> fa-minus-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-minus-square-o'); return false;"><i class="fa fa-fw fa-minus-square-o"></i></a> fa-minus-square-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-plus-square'); return false;"><i class="fa fa-fw fa-plus-square"></i></a> fa-plus-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-plus-square-o'); return false;"><i class="fa fa-fw fa-plus-square-o"></i></a> fa-plus-square-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-square'); return false;"><i class="fa fa-fw fa-square"></i></a> fa-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-square-o'); return false;"><i class="fa fa-fw fa-square-o"></i></a> fa-square-o</div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="Payment">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Payment Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-amex'); return false;"><i class="fa fa-fw fa-cc-amex"></i></a> fa-cc-amex</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-diners-club'); return false;"><i class="fa fa-fw fa-cc-diners-club"></i></a> fa-cc-diners-club</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-discover'); return false;"><i class="fa fa-fw fa-cc-discover"></i></a> fa-cc-discover</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-jcb'); return false;"><i class="fa fa-fw fa-cc-jcb"></i></a> fa-cc-jcb</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-mastercard'); return false;"><i class="fa fa-fw fa-cc-mastercard"></i></a> fa-cc-mastercard</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-paypal'); return false;"><i class="fa fa-fw fa-cc-paypal"></i></a> fa-cc-paypal</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-stripe'); return false;"><i class="fa fa-fw fa-cc-stripe"></i></a> fa-cc-stripe</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-visa'); return false;"><i class="fa fa-fw fa-cc-visa"></i></a> fa-cc-visa</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-credit-card'); return false;"><i class="fa fa-fw fa-credit-card"></i></a> fa-credit-card</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-google-wallet'); return false;"><i class="fa fa-fw fa-google-wallet"></i></a> fa-google-wallet</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-paypal'); return false;"><i class="fa fa-fw fa-paypal"></i></a> fa-paypal</div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="Chart">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Chart Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-area-chart'); return false;"><i class="fa fa-fw fa-area-chart"></i></a> fa-area-chart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bar-chart'); return false;"><i class="fa fa-fw fa-bar-chart"></i></a> fa-bar-chart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bar-chart-o'); return false;"><i class="fa fa-fw fa-bar-chart-o"></i></a> fa-bar-chart-o<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-line-chart'); return false;"><i class="fa fa-fw fa-line-chart"></i></a> fa-line-chart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pie-chart'); return false;"><i class="fa fa-fw fa-pie-chart"></i></a> fa-pie-chart</div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="Currency">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Currency Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bitcoin'); return false;"><i class="fa fa-fw fa-bitcoin"></i></a> fa-bitcoin<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-btc'); return false;"><i class="fa fa-fw fa-btc"></i></a> fa-btc</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cny'); return false;"><i class="fa fa-fw fa-cny"></i></a> fa-cny <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-dollar'); return false;"><i class="fa fa-fw fa-dollar"></i></a> fa-dollar<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-eur'); return false;"><i class="fa fa-fw fa-eur"></i></a> fa-eur</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-euro'); return false;"><i class="fa fa-fw fa-euro"></i></a> fa-euro <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gbp'); return false;"><i class="fa fa-fw fa-gbp"></i></a> fa-gbp</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gg'); return false;"><i class="fa fa-fw fa-gg"></i></a> fa-gg</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gg-circle'); return false;"><i class="fa fa-fw fa-gg-circle"></i></a> fa-gg-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-ils'); return false;"><i class="fa fa-fw fa-ils"></i></a> fa-ils</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-inr'); return false;"><i class="fa fa-fw fa-inr"></i></a> fa-inr</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-jpy'); return false;"><i class="fa fa-fw fa-jpy"></i></a> fa-jpy</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-krw'); return false;"><i class="fa fa-fw fa-krw"></i></a> fa-krw</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-money'); return false;"><i class="fa fa-fw fa-money"></i></a> fa-money</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-rmb'); return false;"><i class="fa fa-fw fa-rmb"></i></a> fa-rmb <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-rouble'); return false;"><i class="fa fa-fw fa-rouble"></i></a> fa-rouble<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-rub'); return false;"><i class="fa fa-fw fa-rub"></i></a> fa-rub</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-ruble'); return false;"><i class="fa fa-fw fa-ruble"></i></a> fa-ruble<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-rupee'); return false;"><i class="fa fa-fw fa-rupee"></i></a> fa-rupee <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-shekel'); return false;"><i class="fa fa-fw fa-shekel"></i></a> fa-shekel<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sheqel'); return false;"><i class="fa fa-fw fa-sheqel"></i></a> fa-sheqel<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-try'); return false;"><i class="fa fa-fw fa-try"></i></a> fa-try</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-turkish-lira'); return false;"><i class="fa fa-fw fa-turkish-lira"></i></a> fa-turkish-lira<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-usd'); return false;"><i class="fa fa-fw fa-usd"></i></a> fa-usd</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-won'); return false;"><i class="fa fa-fw fa-won"></i></a> fa-won <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-yen'); return false;"><i class="fa fa-fw fa-yen"></i></a> fa-yen <span class="text-muted">(alias)</span></div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="Text">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Text Editor Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-align-center'); return false;"><i class="fa fa-fw fa-align-center"></i></a> fa-align-center</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-align-justify'); return false;"><i class="fa fa-fw fa-align-justify"></i></a> fa-align-justify</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-align-left'); return false;"><i class="fa fa-fw fa-align-left"></i></a> fa-align-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-align-right'); return false;"><i class="fa fa-fw fa-align-right"></i></a> fa-align-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bold'); return false;"><i class="fa fa-fw fa-bold"></i></a> fa-bold</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chain'); return false;"><i class="fa fa-fw fa-chain"></i></a> fa-chain <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chain-broken'); return false;"><i class="fa fa-fw fa-chain-broken"></i></a> fa-chain-broken</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-clipboard'); return false;"><i class="fa fa-fw fa-clipboard"></i></a> fa-clipboard</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-columns'); return false;"><i class="fa fa-fw fa-columns"></i></a> fa-columns</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-copy'); return false;"><i class="fa fa-fw fa-copy"></i></a> fa-copy <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cut'); return false;"><i class="fa fa-fw fa-cut"></i></a> fa-cut <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-dedent'); return false;"><i class="fa fa-fw fa-dedent"></i></a> fa-dedent<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-eraser'); return false;"><i class="fa fa-fw fa-eraser"></i></a> fa-eraser</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file'); return false;"><i class="fa fa-fw fa-file"></i></a> fa-file</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-o'); return false;"><i class="fa fa-fw fa-file-o"></i></a> fa-file-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-text'); return false;"><i class="fa fa-fw fa-file-text"></i></a> fa-file-text</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-file-text-o'); return false;"><i class="fa fa-fw fa-file-text-o"></i></a> fa-file-text-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-files-o'); return false;"><i class="fa fa-fw fa-files-o"></i></a> fa-files-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-floppy-o'); return false;"><i class="fa fa-fw fa-floppy-o"></i></a> fa-floppy-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-font'); return false;"><i class="fa fa-fw fa-font"></i></a> fa-font</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-header'); return false;"><i class="fa fa-fw fa-header"></i></a> fa-header</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-indent'); return false;"><i class="fa fa-fw fa-indent"></i></a> fa-indent</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-italic'); return false;"><i class="fa fa-fw fa-italic"></i></a> fa-italic</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-link'); return false;"><i class="fa fa-fw fa-link"></i></a> fa-link</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-list'); return false;"><i class="fa fa-fw fa-list"></i></a> fa-list</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-list-alt'); return false;"><i class="fa fa-fw fa-list-alt"></i></a> fa-list-alt</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-list-ol'); return false;"><i class="fa fa-fw fa-list-ol"></i></a> fa-list-ol</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-list-ul'); return false;"><i class="fa fa-fw fa-list-ul"></i></a> fa-list-ul</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-outdent'); return false;"><i class="fa fa-fw fa-outdent"></i></a> fa-outdent</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-paperclip'); return false;"><i class="fa fa-fw fa-paperclip"></i></a> fa-paperclip</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-paragraph'); return false;"><i class="fa fa-fw fa-paragraph"></i></a> fa-paragraph</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-paste'); return false;"><i class="fa fa-fw fa-paste"></i></a> fa-paste <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-repeat'); return false;"><i class="fa fa-fw fa-repeat"></i></a> fa-repeat</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-rotate-left'); return false;"><i class="fa fa-fw fa-rotate-left"></i></a> fa-rotate-left<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-rotate-right'); return false;"><i class="fa fa-fw fa-rotate-right"></i></a> fa-rotate-right<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-save'); return false;"><i class="fa fa-fw fa-save"></i></a> fa-save <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-scissors'); return false;"><i class="fa fa-fw fa-scissors"></i></a> fa-scissors</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-strikethrough'); return false;"><i class="fa fa-fw fa-strikethrough"></i></a> fa-strikethrough</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-subscript'); return false;"><i class="fa fa-fw fa-subscript"></i></a> fa-subscript</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-superscript'); return false;"><i class="fa fa-fw fa-superscript"></i></a> fa-superscript</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-table'); return false;"><i class="fa fa-fw fa-table"></i></a> fa-table</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-text-height'); return false;"><i class="fa fa-fw fa-text-height"></i></a> fa-text-height</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-text-width'); return false;"><i class="fa fa-fw fa-text-width"></i></a> fa-text-width</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-th'); return false;"><i class="fa fa-fw fa-th"></i></a> fa-th</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-th-large'); return false;"><i class="fa fa-fw fa-th-large"></i></a> fa-th-large</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-th-list'); return false;"><i class="fa fa-fw fa-th-list"></i></a> fa-th-list</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-underline'); return false;"><i class="fa fa-fw fa-underline"></i></a> fa-underline</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-undo'); return false;"><i class="fa fa-fw fa-undo"></i></a> fa-undo</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-unlink'); return false;"><i class="fa fa-fw fa-unlink"></i></a> fa-unlink<span class="text-muted">(alias)</span></div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="Directional">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Directional Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-angle-double-down'); return false;"><i class="fa fa-fw fa-angle-double-down"></i></a> fa-angle-double-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-angle-double-left'); return false;"><i class="fa fa-fw fa-angle-double-left"></i></a> fa-angle-double-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-angle-double-right'); return false;"><i class="fa fa-fw fa-angle-double-right"></i></a> fa-angle-double-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-angle-double-up'); return false;"><i class="fa fa-fw fa-angle-double-up"></i></a> fa-angle-double-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-angle-down'); return false;"><i class="fa fa-fw fa-angle-down"></i></a> fa-angle-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-angle-left'); return false;"><i class="fa fa-fw fa-angle-left"></i></a> fa-angle-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-angle-right'); return false;"><i class="fa fa-fw fa-angle-right"></i></a> fa-angle-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-angle-up'); return false;"><i class="fa fa-fw fa-angle-up"></i></a> fa-angle-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-circle-down'); return false;"><i class="fa fa-fw fa-arrow-circle-down"></i></a> fa-arrow-circle-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-circle-left'); return false;"><i class="fa fa-fw fa-arrow-circle-left"></i></a> fa-arrow-circle-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-circle-o-down'); return false;"><i class="fa fa-fw fa-arrow-circle-o-down"></i></a>fa-arrow-circle-o-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-circle-o-left'); return false;"><i class="fa fa-fw fa-arrow-circle-o-left"></i></a>fa-arrow-circle-o-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-circle-o-right'); return false;"><i class="fa fa-fw fa-arrow-circle-o-right"></i></a>fa-arrow-circle-o-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-circle-o-up'); return false;"><i class="fa fa-fw fa-arrow-circle-o-up"></i></a> fa-arrow-circle-o-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-circle-right'); return false;"><i class="fa fa-fw fa-arrow-circle-right"></i></a> fa-arrow-circle-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-circle-up'); return false;"><i class="fa fa-fw fa-arrow-circle-up"></i></a> fa-arrow-circle-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-down'); return false;"><i class="fa fa-fw fa-arrow-down"></i></a> fa-arrow-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-left'); return false;"><i class="fa fa-fw fa-arrow-left"></i></a> fa-arrow-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-right'); return false;"><i class="fa fa-fw fa-arrow-right"></i></a> fa-arrow-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrow-up'); return false;"><i class="fa fa-fw fa-arrow-up"></i></a> fa-arrow-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrows'); return false;"><i class="fa fa-fw fa-arrows"></i></a> fa-arrows</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrows-alt'); return false;"><i class="fa fa-fw fa-arrows-alt"></i></a> fa-arrows-alt</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrows-h'); return false;"><i class="fa fa-fw fa-arrows-h"></i></a> fa-arrows-h</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrows-v'); return false;"><i class="fa fa-fw fa-arrows-v"></i></a> fa-arrows-v</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-down'); return false;"><i class="fa fa-fw fa-caret-down"></i></a> fa-caret-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-left'); return false;"><i class="fa fa-fw fa-caret-left"></i></a> fa-caret-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-right'); return false;"><i class="fa fa-fw fa-caret-right"></i></a> fa-caret-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-square-o-down'); return false;"><i class="fa fa-fw fa-caret-square-o-down"></i></a>fa-caret-square-o-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-square-o-left'); return false;"><i class="fa fa-fw fa-caret-square-o-left"></i></a>fa-caret-square-o-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-square-o-right'); return false;"><i class="fa fa-fw fa-caret-square-o-right"></i></a>fa-caret-square-o-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-square-o-up'); return false;"><i class="fa fa-fw fa-caret-square-o-up"></i></a> fa-caret-square-o-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-caret-up'); return false;"><i class="fa fa-fw fa-caret-up"></i></a> fa-caret-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chevron-circle-down'); return false;"><i class="fa fa-fw fa-chevron-circle-down"></i></a>fa-chevron-circle-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chevron-circle-left'); return false;"><i class="fa fa-fw fa-chevron-circle-left"></i></a>fa-chevron-circle-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chevron-circle-right'); return false;"><i class="fa fa-fw fa-chevron-circle-right"></i></a>fa-chevron-circle-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chevron-circle-up'); return false;"><i class="fa fa-fw fa-chevron-circle-up"></i></a> fa-chevron-circle-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chevron-down'); return false;"><i class="fa fa-fw fa-chevron-down"></i></a> fa-chevron-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chevron-left'); return false;"><i class="fa fa-fw fa-chevron-left"></i></a> fa-chevron-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chevron-right'); return false;"><i class="fa fa-fw fa-chevron-right"></i></a> fa-chevron-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chevron-up'); return false;"><i class="fa fa-fw fa-chevron-up"></i></a> fa-chevron-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-exchange'); return false;"><i class="fa fa-fw fa-exchange"></i></a> fa-exchange</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-o-down'); return false;"><i class="fa fa-fw fa-hand-o-down"></i></a> fa-hand-o-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-o-left'); return false;"><i class="fa fa-fw fa-hand-o-left"></i></a> fa-hand-o-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-o-right'); return false;"><i class="fa fa-fw fa-hand-o-right"></i></a> fa-hand-o-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hand-o-up'); return false;"><i class="fa fa-fw fa-hand-o-up"></i></a> fa-hand-o-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-long-arrow-down'); return false;"><i class="fa fa-fw fa-long-arrow-down"></i></a> fa-long-arrow-down</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-long-arrow-left'); return false;"><i class="fa fa-fw fa-long-arrow-left"></i></a> fa-long-arrow-left</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-long-arrow-right'); return false;"><i class="fa fa-fw fa-long-arrow-right"></i></a> fa-long-arrow-right</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-long-arrow-up'); return false;"><i class="fa fa-fw fa-long-arrow-up"></i></a> fa-long-arrow-up</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-toggle-down'); return false;"><i class="fa fa-fw fa-toggle-down"></i></a> fa-toggle-down<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-toggle-left'); return false;"><i class="fa fa-fw fa-toggle-left"></i></a> fa-toggle-left<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-toggle-right'); return false;"><i class="fa fa-fw fa-toggle-right"></i></a> fa-toggle-right<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-toggle-up'); return false;"><i class="fa fa-fw fa-toggle-up"></i></a> fa-toggle-up<span class="text-muted">(alias)</span></div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="Video">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Video Player Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-arrows-alt'); return false;"><i class="fa fa-fw fa-arrows-alt"></i></a> fa-arrows-alt</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-backward'); return false;"><i class="fa fa-fw fa-backward"></i></a> fa-backward</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-compress'); return false;"><i class="fa fa-fw fa-compress"></i></a> fa-compress</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-eject'); return false;"><i class="fa fa-fw fa-eject"></i></a> fa-eject</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-expand'); return false;"><i class="fa fa-fw fa-expand"></i></a> fa-expand</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-fast-backward'); return false;"><i class="fa fa-fw fa-fast-backward"></i></a> fa-fast-backward</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-fast-forward'); return false;"><i class="fa fa-fw fa-fast-forward"></i></a> fa-fast-forward</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-forward'); return false;"><i class="fa fa-fw fa-forward"></i></a> fa-forward</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pause'); return false;"><i class="fa fa-fw fa-pause"></i></a> fa-pause</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-play'); return false;"><i class="fa fa-fw fa-play"></i></a> fa-play</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-play-circle'); return false;"><i class="fa fa-fw fa-play-circle"></i></a> fa-play-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-play-circle-o'); return false;"><i class="fa fa-fw fa-play-circle-o"></i></a> fa-play-circle-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-random'); return false;"><i class="fa fa-fw fa-random"></i></a> fa-random</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-step-backward'); return false;"><i class="fa fa-fw fa-step-backward"></i></a> fa-step-backward</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-step-forward'); return false;"><i class="fa fa-fw fa-step-forward"></i></a> fa-step-forward</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-stop'); return false;"><i class="fa fa-fw fa-stop"></i></a> fa-stop</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-youtube-play'); return false;"><i class="fa fa-fw fa-youtube-play"></i></a> fa-youtube-play</div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="Brand">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Brand Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-500px'); return false;"><i class="fa fa-fw fa-500px"></i></a> fa-500px</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-adn'); return false;"><i class="fa fa-fw fa-adn"></i></a> fa-adn</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-amazon'); return false;"><i class="fa fa-fw fa-amazon"></i></a> fa-amazon</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-android'); return false;"><i class="fa fa-fw fa-android"></i></a> fa-android</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-angellist'); return false;"><i class="fa fa-fw fa-angellist"></i></a> fa-angellist</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-apple'); return false;"><i class="fa fa-fw fa-apple"></i></a> fa-apple</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-behance'); return false;"><i class="fa fa-fw fa-behance"></i></a> fa-behance</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-behance-square'); return false;"><i class="fa fa-fw fa-behance-square"></i></a> fa-behance-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bitbucket'); return false;"><i class="fa fa-fw fa-bitbucket"></i></a> fa-bitbucket</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bitbucket-square'); return false;"><i class="fa fa-fw fa-bitbucket-square"></i></a> fa-bitbucket-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-bitcoin'); return false;"><i class="fa fa-fw fa-bitcoin"></i></a> fa-bitcoin<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-black-tie'); return false;"><i class="fa fa-fw fa-black-tie"></i></a> fa-black-tie</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-btc'); return false;"><i class="fa fa-fw fa-btc"></i></a> fa-btc</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-buysellads'); return false;"><i class="fa fa-fw fa-buysellads"></i></a> fa-buysellads</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-amex'); return false;"><i class="fa fa-fw fa-cc-amex"></i></a> fa-cc-amex</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-diners-club'); return false;"><i class="fa fa-fw fa-cc-diners-club"></i></a> fa-cc-diners-club</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-discover'); return false;"><i class="fa fa-fw fa-cc-discover"></i></a> fa-cc-discover</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-jcb'); return false;"><i class="fa fa-fw fa-cc-jcb"></i></a> fa-cc-jcb</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-mastercard'); return false;"><i class="fa fa-fw fa-cc-mastercard"></i></a> fa-cc-mastercard</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-paypal'); return false;"><i class="fa fa-fw fa-cc-paypal"></i></a> fa-cc-paypal</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-stripe'); return false;"><i class="fa fa-fw fa-cc-stripe"></i></a> fa-cc-stripe</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-cc-visa'); return false;"><i class="fa fa-fw fa-cc-visa"></i></a> fa-cc-visa</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-chrome'); return false;"><i class="fa fa-fw fa-chrome"></i></a> fa-chrome</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-codepen'); return false;"><i class="fa fa-fw fa-codepen"></i></a> fa-codepen</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-connectdevelop'); return false;"><i class="fa fa-fw fa-connectdevelop"></i></a> fa-connectdevelop</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-contao'); return false;"><i class="fa fa-fw fa-contao"></i></a> fa-contao</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-css3'); return false;"><i class="fa fa-fw fa-css3"></i></a> fa-css3</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-dashcube'); return false;"><i class="fa fa-fw fa-dashcube"></i></a> fa-dashcube</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-delicious'); return false;"><i class="fa fa-fw fa-delicious"></i></a> fa-delicious</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-deviantart'); return false;"><i class="fa fa-fw fa-deviantart"></i></a> fa-deviantart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-digg'); return false;"><i class="fa fa-fw fa-digg"></i></a> fa-digg</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-dribbble'); return false;"><i class="fa fa-fw fa-dribbble"></i></a> fa-dribbble</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-dropbox'); return false;"><i class="fa fa-fw fa-dropbox"></i></a> fa-dropbox</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-drupal'); return false;"><i class="fa fa-fw fa-drupal"></i></a> fa-drupal</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-empire'); return false;"><i class="fa fa-fw fa-empire"></i></a> fa-empire</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-expeditedssl'); return false;"><i class="fa fa-fw fa-expeditedssl"></i></a> fa-expeditedssl</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-facebook'); return false;"><i class="fa fa-fw fa-facebook"></i></a> fa-facebook</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-facebook-f'); return false;"><i class="fa fa-fw fa-facebook-f"></i></a> fa-facebook-f<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-facebook-official'); return false;"><i class="fa fa-fw fa-facebook-official"></i></a> fa-facebook-official</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-facebook-square'); return false;"><i class="fa fa-fw fa-facebook-square"></i></a> fa-facebook-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-firefox'); return false;"><i class="fa fa-fw fa-firefox"></i></a> fa-firefox</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-flickr'); return false;"><i class="fa fa-fw fa-flickr"></i></a> fa-flickr</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-fonticons'); return false;"><i class="fa fa-fw fa-fonticons"></i></a> fa-fonticons</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-forumbee'); return false;"><i class="fa fa-fw fa-forumbee"></i></a> fa-forumbee</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-foursquare'); return false;"><i class="fa fa-fw fa-foursquare"></i></a> fa-foursquare</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-ge'); return false;"><i class="fa fa-fw fa-ge"></i></a> fa-ge<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-get-pocket'); return false;"><i class="fa fa-fw fa-get-pocket"></i></a> fa-get-pocket</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gg'); return false;"><i class="fa fa-fw fa-gg"></i></a> fa-gg</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gg-circle'); return false;"><i class="fa fa-fw fa-gg-circle"></i></a> fa-gg-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-git'); return false;"><i class="fa fa-fw fa-git"></i></a> fa-git</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-git-square'); return false;"><i class="fa fa-fw fa-git-square"></i></a> fa-git-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-github'); return false;"><i class="fa fa-fw fa-github"></i></a> fa-github</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-github-alt'); return false;"><i class="fa fa-fw fa-github-alt"></i></a> fa-github-alt</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-github-square'); return false;"><i class="fa fa-fw fa-github-square"></i></a> fa-github-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gittip'); return false;"><i class="fa fa-fw fa-gittip"></i></a> fa-gittip<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-google'); return false;"><i class="fa fa-fw fa-google"></i></a> fa-google</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-google-plus'); return false;"><i class="fa fa-fw fa-google-plus"></i></a> fa-google-plus</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-google-plus-square'); return false;"><i class="fa fa-fw fa-google-plus-square"></i></a> fa-google-plus-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-google-wallet'); return false;"><i class="fa fa-fw fa-google-wallet"></i></a> fa-google-wallet</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-gratipay'); return false;"><i class="fa fa-fw fa-gratipay"></i></a> fa-gratipay</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hacker-news'); return false;"><i class="fa fa-fw fa-hacker-news"></i></a> fa-hacker-news</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-houzz'); return false;"><i class="fa fa-fw fa-houzz"></i></a> fa-houzz</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-html5'); return false;"><i class="fa fa-fw fa-html5"></i></a> fa-html5</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-instagram'); return false;"><i class="fa fa-fw fa-instagram"></i></a> fa-instagram</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-internet-explorer'); return false;"><i class="fa fa-fw fa-internet-explorer"></i></a> fa-internet-explorer</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-ioxhost'); return false;"><i class="fa fa-fw fa-ioxhost"></i></a> fa-ioxhost</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-joomla'); return false;"><i class="fa fa-fw fa-joomla"></i></a> fa-joomla</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-jsfiddle'); return false;"><i class="fa fa-fw fa-jsfiddle"></i></a> fa-jsfiddle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-lastfm'); return false;"><i class="fa fa-fw fa-lastfm"></i></a> fa-lastfm</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-lastfm-square'); return false;"><i class="fa fa-fw fa-lastfm-square"></i></a> fa-lastfm-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-leanpub'); return false;"><i class="fa fa-fw fa-leanpub"></i></a> fa-leanpub</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-linkedin'); return false;"><i class="fa fa-fw fa-linkedin"></i></a> fa-linkedin</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-linkedin-square'); return false;"><i class="fa fa-fw fa-linkedin-square"></i></a> fa-linkedin-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-linux'); return false;"><i class="fa fa-fw fa-linux"></i></a> fa-linux</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-maxcdn'); return false;"><i class="fa fa-fw fa-maxcdn"></i></a> fa-maxcdn</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-meanpath'); return false;"><i class="fa fa-fw fa-meanpath"></i></a> fa-meanpath</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-medium'); return false;"><i class="fa fa-fw fa-medium"></i></a> fa-medium</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-odnoklassniki'); return false;"><i class="fa fa-fw fa-odnoklassniki"></i></a> fa-odnoklassniki</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-odnoklassniki-square'); return false;"><i class="fa fa-fw fa-odnoklassniki-square"></i></a>fa-odnoklassniki-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-opencart'); return false;"><i class="fa fa-fw fa-opencart"></i></a> fa-opencart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-openid'); return false;"><i class="fa fa-fw fa-openid"></i></a> fa-openid</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-opera'); return false;"><i class="fa fa-fw fa-opera"></i></a> fa-opera</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-optin-monster'); return false;"><i class="fa fa-fw fa-optin-monster"></i></a> fa-optin-monster</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pagelines'); return false;"><i class="fa fa-fw fa-pagelines"></i></a> fa-pagelines</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-paypal'); return false;"><i class="fa fa-fw fa-paypal"></i></a> fa-paypal</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pied-piper'); return false;"><i class="fa fa-fw fa-pied-piper"></i></a> fa-pied-piper</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pied-piper-alt'); return false;"><i class="fa fa-fw fa-pied-piper-alt"></i></a> fa-pied-piper-alt</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pinterest'); return false;"><i class="fa fa-fw fa-pinterest"></i></a> fa-pinterest</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pinterest-p'); return false;"><i class="fa fa-fw fa-pinterest-p"></i></a> fa-pinterest-p</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-pinterest-square'); return false;"><i class="fa fa-fw fa-pinterest-square"></i></a> fa-pinterest-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-qq'); return false;"><i class="fa fa-fw fa-qq"></i></a> fa-qq</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-ra'); return false;"><i class="fa fa-fw fa-ra"></i></a> fa-ra<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-rebel'); return false;"><i class="fa fa-fw fa-rebel"></i></a> fa-rebel</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-reddit'); return false;"><i class="fa fa-fw fa-reddit"></i></a> fa-reddit</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-reddit-square'); return false;"><i class="fa fa-fw fa-reddit-square"></i></a> fa-reddit-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-renren'); return false;"><i class="fa fa-fw fa-renren"></i></a> fa-renren</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-safari'); return false;"><i class="fa fa-fw fa-safari"></i></a> fa-safari</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-sellsy'); return false;"><i class="fa fa-fw fa-sellsy"></i></a> fa-sellsy</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-share-alt'); return false;"><i class="fa fa-fw fa-share-alt"></i></a> fa-share-alt</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-share-alt-square'); return false;"><i class="fa fa-fw fa-share-alt-square"></i></a> fa-share-alt-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-shirtsinbulk'); return false;"><i class="fa fa-fw fa-shirtsinbulk"></i></a> fa-shirtsinbulk</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-simplybuilt'); return false;"><i class="fa fa-fw fa-simplybuilt"></i></a> fa-simplybuilt</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-skyatlas'); return false;"><i class="fa fa-fw fa-skyatlas"></i></a> fa-skyatlas</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-skype'); return false;"><i class="fa fa-fw fa-skype"></i></a> fa-skype</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-slack'); return false;"><i class="fa fa-fw fa-slack"></i></a> fa-slack</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-slideshare'); return false;"><i class="fa fa-fw fa-slideshare"></i></a> fa-slideshare</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-soundcloud'); return false;"><i class="fa fa-fw fa-soundcloud"></i></a> fa-soundcloud</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-spotify'); return false;"><i class="fa fa-fw fa-spotify"></i></a> fa-spotify</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-stack-exchange'); return false;"><i class="fa fa-fw fa-stack-exchange"></i></a> fa-stack-exchange</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-stack-overflow'); return false;"><i class="fa fa-fw fa-stack-overflow"></i></a> fa-stack-overflow</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-steam'); return false;"><i class="fa fa-fw fa-steam"></i></a> fa-steam</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-steam-square'); return false;"><i class="fa fa-fw fa-steam-square"></i></a> fa-steam-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-stumbleupon'); return false;"><i class="fa fa-fw fa-stumbleupon"></i></a> fa-stumbleupon</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-stumbleupon-circle'); return false;"><i class="fa fa-fw fa-stumbleupon-circle"></i></a> fa-stumbleupon-circle</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tencent-weibo'); return false;"><i class="fa fa-fw fa-tencent-weibo"></i></a> fa-tencent-weibo</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-trello'); return false;"><i class="fa fa-fw fa-trello"></i></a> fa-trello</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tripadvisor'); return false;"><i class="fa fa-fw fa-tripadvisor"></i></a> fa-tripadvisor</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tumblr'); return false;"><i class="fa fa-fw fa-tumblr"></i></a> fa-tumblr</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-tumblr-square'); return false;"><i class="fa fa-fw fa-tumblr-square"></i></a> fa-tumblr-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-twitch'); return false;"><i class="fa fa-fw fa-twitch"></i></a> fa-twitch</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-twitter'); return false;"><i class="fa fa-fw fa-twitter"></i></a> fa-twitter</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-twitter-square'); return false;"><i class="fa fa-fw fa-twitter-square"></i></a> fa-twitter-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-viacoin'); return false;"><i class="fa fa-fw fa-viacoin"></i></a> fa-viacoin</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-vimeo'); return false;"><i class="fa fa-fw fa-vimeo"></i></a> fa-vimeo</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-vimeo-square'); return false;"><i class="fa fa-fw fa-vimeo-square"></i></a> fa-vimeo-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-vine'); return false;"><i class="fa fa-fw fa-vine"></i></a> fa-vine</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-vk'); return false;"><i class="fa fa-fw fa-vk"></i></a> fa-vk</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-wechat'); return false;"><i class="fa fa-fw fa-wechat"></i></a> fa-wechat<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-weibo'); return false;"><i class="fa fa-fw fa-weibo"></i></a> fa-weibo</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-weixin'); return false;"><i class="fa fa-fw fa-weixin"></i></a> fa-weixin</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-whatsapp'); return false;"><i class="fa fa-fw fa-whatsapp"></i></a> fa-whatsapp</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-wikipedia-w'); return false;"><i class="fa fa-fw fa-wikipedia-w"></i></a> fa-wikipedia-w</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-windows'); return false;"><i class="fa fa-fw fa-windows"></i></a> fa-windows</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-wordpress'); return false;"><i class="fa fa-fw fa-wordpress"></i></a> fa-wordpress</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-xing'); return false;"><i class="fa fa-fw fa-xing"></i></a> fa-xing</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-xing-square'); return false;"><i class="fa fa-fw fa-xing-square"></i></a> fa-xing-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-y-combinator'); return false;"><i class="fa fa-fw fa-y-combinator"></i></a> fa-y-combinator</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-y-combinator-square'); return false;"><i class="fa fa-fw fa-y-combinator-square"></i></a>fa-y-combinator-square <span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-yahoo'); return false;"><i class="fa fa-fw fa-yahoo"></i></a> fa-yahoo</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-yc'); return false;"><i class="fa fa-fw fa-yc"></i></a> fa-yc<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-yc-square'); return false;"><i class="fa fa-fw fa-yc-square"></i></a> fa-yc-square<span class="text-muted">(alias)</span></div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-yelp'); return false;"><i class="fa fa-fw fa-yelp"></i></a> fa-yelp</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-youtube'); return false;"><i class="fa fa-fw fa-youtube"></i></a> fa-youtube</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-youtube-play'); return false;"><i class="fa fa-fw fa-youtube-play"></i></a> fa-youtube-play</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-youtube-square'); return false;"><i class="fa fa-fw fa-youtube-square"></i></a> fa-youtube-square</div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade right" tabindex="-1" role="dialog" id="medical_icons">
        <div class="modal-dialog modal-lg modal-left modal-notify modal-info">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Categoría: <B>Medical Icons</B></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-ambulance'); return false;"><i class="fa fa-fw fa-ambulance"></i></a> fa-ambulance</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-h-square'); return false;"><i class="fa fa-fw fa-h-square"></i></a> fa-h-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-heart'); return false;"><i class="fa fa-fw fa-heart"></i></a> fa-heart</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-heart-o'); return false;"><i class="fa fa-fw fa-heart-o"></i></a> fa-heart-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-heartbeat'); return false;"><i class="fa fa-fw fa-heartbeat"></i></a> fa-heartbeat</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-hospital-o'); return false;"><i class="fa fa-fw fa-hospital-o"></i></a> fa-hospital-o</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-medkit'); return false;"><i class="fa fa-fw fa-medkit"></i></a> fa-medkit</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-plus-square'); return false;"><i class="fa fa-fw fa-plus-square"></i></a> fa-plus-square</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-stethoscope'); return false;"><i class="fa fa-fw fa-stethoscope"></i></a> fa-stethoscope</div>
                  <div class="col-md-4"><a class="btn" onclick="llenarInput('fa fa-fw fa-user-md'); return false;"><i class="fa fa-fw fa-user-md"></i></a> fa-user-md</div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>

		{{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
		<div class="card-footer">
			<div class="pull-right">
        <button type="submit" class="btn btn-info">Aceptar</button>
				<a href="{{ route('menus.index') }}" class="btn btn-danger">Cancelar</a>
			</div>
		</div>
		{{-- BOTONES (REGRESAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

	</form>
	{{-- FORMULARIO - FIN --}}
</div>

{!! JsValidator::formRequest('App\Http\Requests\Menus\StoreMenuRequest') !!}

@section('script')
  <script type="text/javascript">

	$(document).ready( function () {



	});

  function llenarInput(valor)
  {
    document.getElementById("menu_icon").value=valor;
    document.getElementById("ver_icon").className = valor;
  }

  function actualizaIcono(icono)
  {
    document.getElementById("ver_icon").className = icono.value;
  }

	</script>

@endsection

@endsection
