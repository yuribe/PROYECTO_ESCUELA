@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item"><a href="{{ route('menus.index') }}">Menús</a></li>
	<li class="breadcrumb-item active"><a>Mostrar Menú</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><b>Mostrar Menú</b></h3>
	</div>
  	<div class="card-body">
			<div class="row">
        <div class="col-md-6">
					<div class="form-group">
						<label>Módulo(Ruta)</label>
						<input type="text" class="form-control" value="{{ $menu->modulos->name }}" readonly />
  				</div>
				</div>
        <div class="col-md-6">
					<div class="form-group">
						<label>Nombre del menú</label>
						<input type="text" class="form-control" value="{{ $menu->name }}" readonly />
  				</div>
				</div>
      </div> <!--Fin Row-->
			<div class="row">
    		<div class="col-md-6">
					<div class="form-group">
						<label>Nombre a mostrar</label>
						<input type="text" class="form-control" value="{{ $menu->display_name }}" readonly />
					</div>
				</div>
		 		<div class="col-md-6">
					<div class="form-group">
						<label>Descripción</label>
						<input type="text" class="form-control mayus" value="{{ $menu->description }}" readonly />
					</div>
				</div>
      </div> <!--Fin Row-->
			<div class="row">
        <div class="col-md-5">
					<div class="form-group">
						<label>Elemento de menú padre</label>
						<input type="text" class="form-control" value="{{ $menu->padres->name }}" readonly />
					</div>
				</div>
        <div class="col-md-1">
					<div class="form-group">
						<label>Nivel</label>
						<input type="text" class="form-control" value="{{ $menu->menu_level}}" readonly />
					</div>
				</div>
        <div class="col-md-4">
          <label>Icono Menú</label>
          <div class="input-group mb-3">
           <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"><i class="{{ $menu->menu_icon }}"></i></span>
            </div>
            <input type="text" class="form-control" value="{{ $menu->menu_icon}}" aria-label="{{ $menu->menu_icon}}" aria-describedby="{{ $menu->menu_icon}}" readonly />
          </div>
				</div>
        <div class="col-md-2">
					<div class="form-group">
						<label for="status_menu_id">Estatus</label>
					  <input type="text" class="form-control mayus" value="{{ $menu->genericos->name }}" readonly />
					</div>
				</div>
			</div> <!--Fin Row-->
		</div>

		{{-- BOTONES (REGRESAR/CANCELAR Y CONTINUAR/GUARDAR/ACEPTAR - INICIO --}}
		<div class="card-footer">
			<div class="pull-right">
      	<a href="{{ route('menus.index') }}" class="btn btn-danger">Regresar</a>
			</div>
		</div>
		{{-- BOTONES (REGRESAR Y CONTINUAR/GUARDAR/ACEPTAR - FIN --}}

</div>

@endsection
