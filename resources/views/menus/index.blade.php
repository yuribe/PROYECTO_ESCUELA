@extends('layouts.app')

<?php
    if (isset($name))
      $name = $name;
    else
      $name = "";
    if (isset($display_name))
      $display_name = $display_name;
    else
      $display_name = "";
    if (isset($description))
      $description = $description;
    else
      $description = "";
    if (isset($status_menu_id))
      $status_menu_id = $status_menu_id;
    else
      $status_menu_id = "";
    if (isset($module_id))
        $module_id = $module_id;
    else
        $module_id = "";
?>

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
  <li class="breadcrumb-item"><a>Seguridad</a></li>
	<li class="breadcrumb-item active"><a href="{{ route('menus.index') }}">Menús</a></li>

@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

  @include('layouts.parciales.messages')

	{{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
		</div>
	{{-- FORMULARIO - INICIO --}}
    <form class="form" id="indexMenusForm" name = "indexMenusForm" role="form" data-toggle="validator" method="GET" action="{{ route('menus.index') }}">
			<div class="card-body">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="name">Nombre del menú</label>
							<input class="form-control" id="name" name="name">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="display_name">Nombre a mostrar</label>
							<input class="form-control" id="display_name" name="display_name">
						</div>
					</div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="module_id">Módulo</label>
              <select class="form-control" id="module_id" name="module_id">
                <option value="" selected>Seleccione una opción</option>
                @foreach($modules as $module)
                  <option value="{{ $module->id }}">{{ $module->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="status_menu_id">Estatus</label>
              <select class="form-control" id="status_menu_id" name="status_menu_id">
                <option value = "" selected>Seleccione una opción</option>
                @foreach($generics as $generic)
                  <option value="{{ $generic->id }}">{{ $generic->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
				</div>

			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
			<div class="card-footer">
				<div class="pull-right">
					<button type="button" onClick="buscar();" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
				 	  <a href="{{ route('menus.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
					<button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
				</div>
			</div>
			{{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

		</form>
		{{-- FORMULARIO - FIN --}}
	</div>



	{{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

	{{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Menús</b></h3>
    </div>
    <div class="card-footer">
			@permission('menu-create')
        <a href="{{ route('menus.create') }}" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a>
			@endpermission
      @permission('menu-pdf')
        <button type="button" onClick="imprimePdf();" class="btn btn-info"><i class="fa fa-file"></i> Pdf</button>
      @endpermission
      @permission('menu-excel')
        <button type="button" onClick="imprimeExcel();" class="btn btn-info"><i class="fa fa-file"></i> Excel</button>
      @endpermission
    </div>
    <div class="card-body">
      <table id="tabla1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th style="width: 2%">N°</th>
            <th style="width: 15%">Nombre módulo</th>
            <th style="width: 15%">Nombre menú</th>
            <th style="width: 15%">Descripción</th>
						<th style="width: 12%">Elemento Padre</th>
            <th style="width: 5%">Nivel</th>
            <th style="width: 5%">Icono</th>
						<th style="width: 5%">Estatus</th>
            <th style="width: 18%">Acciones</th>
          </tr>
        </thead>
        <tbody>
					@foreach ($menus as $key => $menu)
        	<tr>
            <td>{{ ++$i }}</td>
            <td>{{ $menu->modulos->name }}</td>
            <td>{{ $menu->display_name }}</td>
            <td>{{ $menu->description }}</td>
            <td>{{ $menu->padres->name }}</td>
            <td align="center">{{ $menu->menu_level }}</td>
            <td align="center"><i class="{{ $menu->menu_icon }}"></i></td>
						<td>{{ $menu->genericos->name }}</td>
            <td class="text-center">
							@permission('menu-edit')
                 <a href="{{ route('menus.edit',$menu->id) }}" class="btn btn-info"> <i class="fa fa-edit"></i></a>
							@endpermission
                 <a href="{{ route('menus.show',$menu->id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
							@permission('menu-delete')
                 <button type="button" class="btn btn-danger" title="Eliminar Menú" data-toggle="modal" data-target="#myModal_{{ $menu->id }}">
                   <i class="fa fa-trash"></i>
                 </button>
              @endpermission
                 <div class="modal fade" tabindex="-1" role="dialog" id="myModal_{{ $menu->id }}">
                   <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel">Información</h4>
                       </div>
                       <div class="modal-body">
                         <h4>¿Desea eliminar la información?</h4>
                       </div>
                       <div class="modal-footer">
                         {{Form::open(array('route'=> array('menus.destroy', $menu['id']), 'method'=>'PATCH','id'=>'borraRegistro'))}}
                         {{ method_field('DELETE') }}
                           <button type="submit" id="borra" class="btn btn-danger" >Si</button>
                           <button  id='cerrar' type="button" class="btn btn-info" data-dismiss="modal">No</button>
                         {{Form::close()}}
                       </div>
                     </div>
                   </div>
                 </div>
            </td>
          </tr>
					@endforeach
          </tbody>
        </table>
      </div>
    </div>


    {{-- TARJETA TABLA - FIN --}}

    @section('script')
      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          columnDefs: [
              {orderable: false, targets: 8 }
          ],
          responsive: true,
          searching: false,
          paginate: true,
        });
        // DATATABLE - FIN //


      });

      function buscar()
      {
        var formEdit = document.forms['indexMenusForm'];
        var vname = document.getElementById("name").value;
        var vdisplay_name = document.getElementById("display_name").value;
        var vstatus_menu_id = document.getElementById("status_menu_id").value;
        var vmodule_id = document.getElementById("module_id").value;

        if (vname === '' && vdisplay_name === ''&& vmodule_id === ''  && vstatus_menu_id === '')
        {
          $('#message').html("@lang('message.seleccionarcriterio')");
          $('#msj').show();
          desvanecer();
        } else
        {
          formEdit.submit();
        }

      }

      function imprimePdf()
      {
        var name = '<?php echo $name; ?>';
        var display_name = '<?php echo $display_name; ?>';
        var status_menu_id = '<?php echo $status_menu_id; ?>';
        var module_id = '<?php echo $module_id; ?>';
        window.open('/menus/pdf' + '?' + '&name=' + name + '&display_name=' + display_name + '&module_id=' + module_id +'&status_menu_id=' + status_menu_id, '_blank');
      }

      function imprimeExcel()
      {
        var name = '<?php echo $name; ?>';
        var display_name = '<?php echo $display_name; ?>';
        var status_menu_id = '<?php echo $status_menu_id; ?>';
        var module_id = '<?php echo $module_id; ?>';
        window.open('/menus/excel' + '?' + '&name=' + name + '&display_name=' + display_name + '&module_id=' + module_id + '&status_menu_id=' + status_menu_id, '_blank');
      }

      </script>
    @endsection

@endsection
