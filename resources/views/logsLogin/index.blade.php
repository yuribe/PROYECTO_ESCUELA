@extends('layouts.app')

{{-- BREADCRUMBS - INICIO --}}

@section('breadcrumb')
    <li class="breadcrumb-item">Auditoría</li>
    <li class="breadcrumb-item active"><a>Inicios de Sesión</a></li>
@endsection

{{-- BREADCRUMBS - FIN --}}

@section('content')

@include('layouts.parciales.messages')

  {{-- TARJETA CRITERIO DE BUSQUEDA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Criterio de Búsqueda</b></h3>
    </div>

  {{-- FORMULARIO - INICIO --}}
    <form class="form" id="formLogsLogin" name="formLogsLogin" role="form" data-toggle="validator" method="GET" action="{{ route('logslogin.index') }}">
      <div class="card-body">

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Fecha Desde:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                  </span>
                </div>
                <input type="text" class="form-control float-right" id="datepicker1" name="desde" autocomplete="off">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Fecha Hasta:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                  </span>
                </div>
                <input type="text" class="form-control float-right" id="datepicker2" name="hasta" autocomplete="off">
              </div>
            </div>
          </div>
        </div>
        {{-- EJEMPLO CON DOS COLUMNAS - FIN --}}

        {{-- EJEMPLO CON DOS COLUMNAS - INICIO --}}

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="email">Correo Electrónico</label>
              <input id="email" type="email" name="email" class="form-control">
              @if ($errors->has('email'))
                  <span class="text-danger">
                    <strong>{{ $errors->first('email') }}</strong>
                  f</span>
              @endif
            </div>
          </div>
        </div>
        {{-- EJEMPLO CON DOS COLUMNAS - FIN --}}
      </div>

      {{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - INICIO --}}
      <div class="card-footer">
        <div class="pull-right">
          <button type="button" onClick="buscar();" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
          <a href="{{ route('logslogin.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Ver todo</a>
          <button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
        </div>
      </div>
      {{-- BOTONES (VER TODO, BUSCAR, LIMPIAR - FIN --}}

    </form>
    {{-- FORMULARIO - FIN --}}
  </div>

  {{-- TARJETA CRITERIO DE BUSQUEDA - FIN --}}

  {{-- TARJETA TABLA - INICIO --}}

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><b>Consulta de Inicio de Sesión</b></h3>
    </div>
    <div class="card-footer">
      <a href="{{ route('traces.excel') }}" class="btn btn-info"><i class="fa fa-file"></i> Excel</a>
      <a href="{{ route('traces.excel') }}" class="btn btn-info"><i class="fa fa-file"></i> Pdf</a>
    </div>

      <div class="card-body table-bordered table-hover table-responsive">
        <table id="tabla1" class="table">
          <thead>
            <tr>
              <th>N°</th>
              <th>IP</th>
              <th>Usuario</th>
              <th>Permisología</th>
              <th>Fecha/Hora</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($log as $key => $logs)
            <tr>
              <td>{{ ++$i }}</td>
              <td>{{ $logs->last_login_ip }}</td>
              <td>{{ $logs->email }}</td>
              <td>{{ $logs->rol }}</td>
              <td>{{ $logs->created_at }}</td>
            </tr>
            @endforeach
            </tbody>
          </table>
          {{-- {!! $trace->links() !!} --}}
      </div>
    </div>

    {{-- TARJETA TABLA - FIN --}}
    {{-- {!! JsValidator::formRequest('App\Http\Requests\Audits\IndexTraceRequest') !!} --}}

    @section('script')


      <script type="text/javascript">

      $(document).ready( function () {

        // DATATABLE - INICIO //
        $('#tabla1')
        .dataTable( {
          responsive: true,
          searching: false,
          paginate: true,  
        });
        // DATATABLE - FIN //

        // DATAPICKER - INICIO //
        $('#datepicker1').datepicker({
          format: 'dd/mm/yyyy',
          endDate: '+0d'
        });
        // DATAPICKER - FIN //

        // DATAPICKER - INICIO //
        $('#datepicker2').datepicker({
          format: 'dd/mm/yyyy',
          endDate: '+0d'
        });
        // DATAPICKER - FIN //

      });

      function buscar()
      {
        var formIndex = document.forms['formLogsLogin'];
        var desde = document.getElementById("datepicker1").value;
        var hasta = document.getElementById("datepicker2").value;
        var email = document.getElementById("email").value;

        if (desde === '' && hasta === '' && email === '')
        {
          $('#message').html("@lang('message.seleccionarcriterio')");
          $('#msj').show();
          desvanecer();
        } else
        {
          formIndex.submit();
        }
      }

      </script>
    @endsection

  @endsection
